#ifndef ADboundaryVoxelizer_cuh
#define ADboundaryVoxelizer_cuh
// ************************************************************************************************
// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

// DeviceArray header
#include "DeviceArray.cuh"

// LBM header
#include "LBMconstant.hpp"

// prefixsum header
#include "GpuPrefixSum.cuh"

// a record that stores pointer, length and id of a curve which is being registered into voxelizer
struct _CurveRecord{
    const REAL*__restrict__ _x;
    const REAL*__restrict__ _y;
    int len;
    bool is_circle;
    int id_boundary;
    int id_curve;
    int id_cudaStream;
    int idx_entry; // an exlusive prefix sum of curve total segment number calculated out from len

    inline _CurveRecord
        (
        const REAL*__restrict__ __x = nullptr,
        const REAL*__restrict__ __y = nullptr,
        const int _len = 0,
        const bool _is_circle = false,
        const int _id_boundary = 0,
        const int _id_curve = 0,
        const int _id_cudaStream = 0,
        const int _idx_entry = 0
        ) :
        _x(__x),
        _y(__y),
        len(_len),
        is_circle(_is_circle),
        id_boundary(_id_boundary),
        id_curve(_id_curve),
        id_cudaStream(_id_cudaStream),
        idx_entry(_idx_entry)
    {}
};



// ************************************************************************************************
// 
class LBMGPUvoxelizer{
public:

    int m;
    int n;
    bool is_x_periodic;
    bool is_y_periodic;
    int MAX_SEGMENT; // maximum number of segments that could be handled
    int MAX_RECORD; // maximum number of records

    GpuPrefixSum<int> prefixSum; // a prefixsum (exclusive) calculator
    static const int MAX_CUDA_STREAM = 16;
    cudaStream_t cudaStream[MAX_CUDA_STREAM];

    DeviceArray<char> tag; // m*n in size, tag==-1: internal, tag==1: external
    DeviceArray<char> tag_previous; // tag map of previous step
    DeviceArray<char> tag_conn8; // a scratch for fill a tag table with connection-8 neighbor definition, will only be used in maketag
    DeviceArray<int> mutex; // a mutex lock for tagging process

    // the distance between tagged mesh point and the intersecting point that labels this tag point.
    // the intersecting point with shortest distance will success in labeling the tag 
    // m*n in size
    DeviceArray<double> min_distance;
    std::vector<_CurveRecord> curve_records; // each curve owns it cudaStream pointer
    int curve_records_size;

    DeviceArray<int> boundary_size_on_segment;

    // an exlusive prefix sum of total segment number in a curve.
    // calculated out from intersecting_number_on_segment
    // will be used as index entry from a segment to its first corresponding boundary
    DeviceArray<int> boundary_entry_from_segment;


    // this is a set of functions that helps to estimate the volume of each lattice near the boundary
    // Volume information will be used during boundary condition calculation and flipped lattice recovery
    // VolumeEstimator holds a fractional volume field and a (voronoi-like) true volume that comprises its own volume
    // and partial volumes from its neighbor with inversed tag.
    // DeviceArray<REAL> fractional_volume;



    inline LBMGPUvoxelizer
        (
        const int _m,
        const int _n,
        const bool _is_x_periodic,
        const bool _is_y_periodic,
        const int _MAX_SEGMENT,
        const int _MAX_RECORD
        ) :
        m(_m),
        n(_n),
        is_x_periodic(_is_x_periodic),
        is_y_periodic(_is_y_periodic),
        MAX_SEGMENT(_MAX_SEGMENT>9999 ? _MAX_SEGMENT : 9999),
        MAX_RECORD(_MAX_RECORD),
		prefixSum(GpuPrefixSum<int>(std::max(_m*_n * 4, _MAX_SEGMENT) > 9999 ? std::max(_m*_n * 4, _MAX_SEGMENT) : 9999)),
        tag(DeviceArray<char>((char)0, _m, _n)),
        tag_previous(DeviceArray<char>((char)0, _m, _n)),
        tag_conn8(DeviceArray<char>((char)0, _m, _n)),
        mutex(DeviceArray<int>(0, _m, _n)),
        min_distance(DeviceArray<double>((double)2.0, _m, _n)),
        curve_records(std::vector<_CurveRecord>(_MAX_RECORD)),
        curve_records_size(0),
        boundary_size_on_segment(DeviceArray<int>(0, _MAX_SEGMENT>9999 ? _MAX_SEGMENT : 9999)),
        boundary_entry_from_segment(DeviceArray<int>(0, _MAX_SEGMENT>9999 ? _MAX_SEGMENT : 9999))
    {
        for (int i = 0; i != MAX_CUDA_STREAM; ++i)
            cudaStreamCreate(&cudaStream[i]);
    }

    inline ~LBMGPUvoxelizer()
    {
        for (int i = 0; i != MAX_CUDA_STREAM; ++i)
            cudaStreamDestroy(cudaStream[i]);
    }

    // actually this method only register a curve record into the voxelizer
    // real voxelize work will be done in method setBoundaryCurveFinished()
    inline void setBoundaryCurve
        (
        const REAL* __restrict__ __x,
        const REAL* __restrict__ __y,
        const int _len,
        const bool _is_circle,
        const int _id_boundary = 0, // id for boundary condition numbering
        const int _id_curve = 0     // id for curve numbering
    )
    {
        // assert(curve_records_size < MAX_RECORD);
        int id_cudaStream = curve_records_size % MAX_CUDA_STREAM;
        int idx_entry = (curve_records_size == 0) ? 0 :
            (
            curve_records[curve_records_size - 1].idx_entry +
            curve_records[curve_records_size - 1].len +
            (curve_records[curve_records_size - 1].is_circle ? 0 : -1) // the length of a curve depends on if it's a circle or not
            );
        curve_records[curve_records_size++] = _CurveRecord(__x, __y, _len, _is_circle, _id_boundary, _id_curve, id_cudaStream, idx_entry);

    }

public: // debug // private: // passed
    void makeTag();
    void fill_boundary_size_on_segment();
    inline int get_total_segment_size(){
        return
            curve_records[curve_records_size - 1].idx_entry +
            curve_records[curve_records_size - 1].len - 1 +
            (int)curve_records[curve_records_size - 1].is_circle;
    }
    void fill_boundary_list
        (
        int* __restrict__ _idx,
        int* __restrict__ _id,
        double* __restrict__ _offset,
        double* __restrict__ _nx,
        double* __restrict__ _ny,
        double* __restrict__ _s,
        int* __restrict__ _boundary_size
        );
    void recoverMovingBoundary(REAL* _f, REAL* _rho);
public:
    void setBoundaryCurveFinished
        (
        int* __restrict__ _idx = nullptr,
        int* __restrict__ _id = nullptr,
        double* __restrict__ _offset = nullptr,
        double* __restrict__ _nx = nullptr,
        double* __restrict__ _ny = nullptr,
        double* __restrict__ _s = nullptr,
        int* __restrict__ _boundary_size = nullptr,
        REAL* __restrict__ _f = nullptr,
        REAL* __restrict__ _rho = nullptr,
        const bool is_write_boundary = true
        );
    void trim_label_by_curve
        (
        int* __restrict__ _label,
        const int val_in,
        const int val_ex,
        const REAL* __restrict__ _x,
        const REAL* __restrict__ _y,
        const int len
        );

    // build a DeviceArray<int> array out from current tag map
    // a simple flood fill algorithm will fill all points in a tag map (defined previously in voxelizer)
    // with internal points valued -1 and external ones 1.
    // This is simply implemented with depth-first search method
    //
    // In visual studio, please set:
    //     PROJECT->Properties->Configuration Properties->Linker->System->Stack Reserve Size a very large numbe such as 4194304 (4mb)
    // under linux, please add this suffix when running the code:
    //     ulimit -s 4194304 (also, a very large numbe)
    DeviceArray<int> build_filled_tag_map();

    
    // a helper function that split boundary curve into segments with a length less than dl
    // this function works on cpu, don't use it in a GPU loop
    // be careful, this function will change the value of x_refined.data_ptr() and y_refined.data_ptr()
    static void refine_curve
        (
        DeviceArray<REAL>& x_refined,
        DeviceArray<REAL>& y_refined,
        const REAL* __restrict__ _x_original,
        const REAL* __restrict__ _y_original,
        const int len,
        const REAL dl,
        const bool is_circular
        );
    
};

#endif