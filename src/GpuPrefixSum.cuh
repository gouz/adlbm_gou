#ifndef GpuPrefixSum_cuh
#define GpuPrefixSum_cuh

// GPU prefix sum (exclusive) written by Hengdi ZHANG, Nov 2017
// This is a simple parallel prefix sum calculator for array with arbitrary length n
// (sum_{i=1}^(inf){n/blocksize^i})*sizeof(T) extral space will be used during the process
// for array with size smaller than a block, a code from nvidia blog is used, the algorithm
// is O(log n) https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch39.html
// when array size n is much larger than blocksize, a recursive stratedy is employed
//
// example:
// // assume i_data and o_data are float type array with length n = 800
// // create a prefixSum calculator that handles array with length less than nextPow2(n) = 1024
// GpuPrefixSum<float> prefixSum(nextPow2(n))
// prefixSum(i_data, o_data, n); // calculate the prefixSum and write into o_data
// //done!

#include "DeviceArray.cuh"
void _gpuPrefixSumPerBlock(const float* X, float* Y, const int n, float* blockSum);
void _gpuPrefixSumPerBlock(const double* X, double* Y, const int n, double* blockSum);
void _gpuPrefixSumPerBlock(const int* X, int* Y, const int n, int* blockSum);

void _gpuPrefixSumBackwardAddition(const float* g_PrefixSumOfBlockSum, float* g_odata, const int n);
void _gpuPrefixSumBackwardAddition(const double* g_PrefixSumOfBlockSum, double* g_odata, const int n);
void _gpuPrefixSumBackwardAddition(const int* g_PrefixSumOfBlockSum, int* g_odata, const int n);

template<typename T>
class GpuPrefixSum{
private:
    static const int BLOCK_DIM_MAX = 1024;
    size_t aSizeMax;
    DeviceArray<T>* sumPerBlock;
    DeviceArray<T>* prefixSumOfSumPerBlock;
    int* sizeOfSumPerBlock;
    inline int nextPow2(int x)
    {
        if (x < 0)
            return 0;
        --x;
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;
        return x + 1;
    }
public:
    inline GpuPrefixSum(const int _aSizeMax = 2048*2048) :
        aSizeMax(nextPow2(_aSizeMax)),
        sumPerBlock(nullptr),
        prefixSumOfSumPerBlock(nullptr),
        sizeOfSumPerBlock(nullptr)
    {
        size_t s = aSizeMax / (BLOCK_DIM_MAX * 2);
        int d = 0;
        while (s > 1)
        {
            d++;
            s /= (BLOCK_DIM_MAX * 2);
        }
        if (d > 0)
        {
            sumPerBlock = new DeviceArray<T>[d];
            prefixSumOfSumPerBlock = new DeviceArray<T>[d];
            sizeOfSumPerBlock = new int[d]();
            size_t st = 1;
            for (int i = 0; i != d; ++i)
            {
                st *= (BLOCK_DIM_MAX * 2);
                sumPerBlock[i] = DeviceArray<T>((T)0, st);
                prefixSumOfSumPerBlock[i] = DeviceArray<T>((T)0, st);
            }

        }
    }
    inline ~GpuPrefixSum()
    {
        if (nullptr != sumPerBlock)
        {
            delete[]sumPerBlock;
            sumPerBlock = nullptr;
        }
        if (nullptr != prefixSumOfSumPerBlock)
        {
            delete[]prefixSumOfSumPerBlock;
            prefixSumOfSumPerBlock = nullptr;
        }
        if (nullptr != sizeOfSumPerBlock){
            delete[]sizeOfSumPerBlock;
            sizeOfSumPerBlock = nullptr;
        }
    }

    inline void operator()(const T* g_idata, T* g_odata, const int n)
    {
        // assert(n > 0);
        // assert(n <= aSizeMax);

        int recur_depth = 0;
        int nt = nextPow2(n) / (BLOCK_DIM_MAX * 2);
        while (nt > 1){
            recur_depth++;
            nt /= (BLOCK_DIM_MAX * 2);
        }

        if (recur_depth == 0)
        {
            _gpuPrefixSumPerBlock(g_idata, g_odata, n, nullptr);
            return;
        }
        else
        {
            // upward block-wise prefix recursive loop
            _gpuPrefixSumPerBlock(g_idata, g_odata, n, sumPerBlock[recur_depth - 1].data_ptr());
            for (int i = recur_depth - 1; i >= 0; --i){
                int st = (i == recur_depth - 1) ? n : sizeOfSumPerBlock[i + 1];
                sizeOfSumPerBlock[i] = (st + BLOCK_DIM_MAX * 2 - 1) / (BLOCK_DIM_MAX * 2);
                _gpuPrefixSumPerBlock(
                    sumPerBlock[i].data_ptr(),
                    prefixSumOfSumPerBlock[i].data_ptr(),
                    sizeOfSumPerBlock[i],
                    i == 0 ? nullptr : sumPerBlock[i - 1].data_ptr());
            }

            // downward block-wise add back loop
            for (int i = 0; i < recur_depth - 1; ++i){
                _gpuPrefixSumBackwardAddition(
                    prefixSumOfSumPerBlock[i].data_ptr(),
                    prefixSumOfSumPerBlock[i + 1].data_ptr(),
                    sizeOfSumPerBlock[i + 1]);
            }
            _gpuPrefixSumBackwardAddition(prefixSumOfSumPerBlock[recur_depth - 1].data_ptr(), g_odata, n);
        }



    }
};

#endif