#include "ADmethod.cuh"

// ************************************************************************************************
// alias for kernel argument
static const int Q = ADmethod::Q; // D2Q5 model
// ************************************************************************************************
// DEFINE cuda launch pattern
#ifdef AD_DEFAULT_STREAM
#define CUDALAUNCH(grid, block, sharedMemSize, streamID) <<<grid, block>>>
#else
#define CUDALAUNCH(grid, block, sharedMemSize, streamID) <<<grid, block, sharedMemSize, streamID>>>
#endif

// block size
#define BSIZEM 32
#define BSIZEN 16

#define DEFINE_LAUNCH_PARAMETER(grid, block, m, n) \
dim3 grid((m + BSIZEM - 1) / BSIZEM, (n + BSIZEN - 1) / BSIZEN); \
const dim3 block(BSIZEM, BSIZEN)

// if IGNORE_BOUNDARY_CONDITION is valid
// config tag from ADmethod will be ignored
// #define IGNORE_BOUNDARY_CONDITION

// ************************************************************************************************
// components in kernel_advance:
//   W0, W1, Feq...      : formulas for equilibirum distribution function Feq(rho, Ux, Uy)[i]
//   device_compute_index: indexing
//   device_load_f0      : read global memory
//   COMPUTE_RHO         : compute macro concentration rho
//   WRITE_RHO           : write rho[idx] to global memory
//   device_advection    : advection after collision step and write results into global memory

// weight and macro rho
#define W0 (0.333333333333333333)
#define W1 (0.166666666666666667)

// pure diffusion Feq
#define Feq0(rho) (W0*(rho))
#define Feq1(rho) (W1*(rho))

// advection diffusion Feq
#define EPS 3.0
#define Feq0ad(rho,Ux,Uy) ( W0*(rho) )
#define Feq1ad(rho,Ux,Uy) ( W1*(rho)* (1.0 + EPS*(Ux)) )
#define Feq2ad(rho,Ux,Uy) ( W1*(rho)* (1.0 - EPS*(Ux)) )
#define Feq3ad(rho,Ux,Uy) ( W1*(rho)* (1.0 + EPS*(Uy)) )
#define Feq4ad(rho,Ux,Uy) ( W1*(rho)* (1.0 - EPS*(Uy)) )

// compute macro concentration rho in register
// for D2Q5 only
#define COMPUTE_RHO(f) (f[0]+f[1]+f[2]+f[3]+f[4])

#define WRITE_RHO(rho1, idx, rho) rho1[idx] = rho // if (config.ALWAYS_WRITE_RHO) rho1[idx] = rho

// compute global index
__device__ __forceinline__ void device_compute_index(int& MN, int& i, int& j, int& idx, const int& M, const int& N)
{
    MN = M*N;
    i = blockIdx.x * blockDim.x + threadIdx.x;
    j = blockIdx.y * blockDim.y + threadIdx.y;
    idx = j*M + i;
}

// load f0 into fin[Q] in register
__device__ __forceinline__ void device_load_f0(REAL* __restrict__ fin, const REAL* __restrict__ f0, const int& idx, const int& MN){
#pragma unroll
    for (int i = 0; i != Q; ++i)
        fin[i] = f0[idx + i*MN];
}

// advection step, for D2Q5 only
//
// As device equivalent to or newer than sm_35 have L1 cache on global memory accessing,
// it is not necessary to do coalesced writing, in which case code complexity would
// increase a lot due to halo threads and shared memory usage. Infact a practice in
// that manner was trying to use on-chip resources to do the work of L1 cache.
// Sounds not a good idea?
//
__device__ __forceinline__ void device_advection(
REAL* f1, const REAL* fout,
const int& i, const int& j, const int& idx,
const int& M, const int& N, const int& MN)
{
    f1[idx] = fout[0];                               // Cx[0] =  0, Cy[0] =  0
    if (i < M - 1) f1[idx + 1 + MN] = fout[1];       // Cx[1] =  1, Cy[1] =  0
    if (i > 0) f1[idx - 1 + MN * 2] = fout[2];       // Cx[2] = -1, Cy[2] =  0
    if (j < N - 1) f1[idx + M + MN * 3] = fout[3];   // Cx[3] =  0, Cy[3] =  1
    if (j > 0) f1[idx - M + MN * 4] = fout[4];       // Cx[4] =  0, Cy[4] = -1
}

// domain boundary condition for D2Q5 only
// this boundary condition works only on the edge of calculation domain
// it is NOT PHYSICAL BOUNDARY CONDITION such as Dirichlet or Neumann BC
//
// if you are using joint calculation domain
// set ADmethod.config.X_BOUNDARY_TYPE as AD::FREE
// then handle the copying of overlaped domains outside the
// collision-advection kernel body
__device__ __forceinline__ void device_boundary_condition(
const AD::CONFIG& config,
REAL* f1, const REAL* fout,
const int& i, const int& j, const int& idx,
const int& M, const int& N, const int& MN)
{
#ifndef IGNORE_BOUNDARY_CONDITION
    if (config.X_BOUNDARY_TYPE == AD::BOUNCEBACK)
    {
        if (i == 0) f1[idx + MN] = fout[2];
        if (i == M - 1) f1[idx + MN * 2] = fout[1];
    }
    if (config.X_BOUNDARY_TYPE == AD::PERIODIC)
    {
        if (i == 0) f1[idx + M - 1 + MN * 2] = fout[2];
        if (i == M - 1) f1[idx + 1 - M + MN] = fout[1];
    }

    if (config.Y_BOUNDARY_TYPE == AD::BOUNCEBACK)
    {
        if (j == 0) f1[idx + MN * 3] = fout[4];
        if (j == N - 1) f1[idx + MN * 4] = fout[3];
    }
    if (config.Y_BOUNDARY_TYPE == AD::PERIODIC)
    {
        if (j == 0) f1[idx - M + MN * 5] = fout[4];
        if (j == N - 1) f1[idx + M + MN * 2] = fout[3];
    }
#endif
}

// ************************************************************************************************
// kernels for advance, export macro concentration rho
// calculate distribution function feq, 
__global__
void kernel_exportRho(REAL* __restrict__ rho, const REAL* __restrict__ f, const int M, const int N){
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N){
        REAL fin[Q];
        device_load_f0(fin, f, idx, MN);
        rho[idx] = COMPUTE_RHO(fin);
    }
}

__global__
void kernel_feq(REAL* __restrict__ f, const REAL* __restrict__ rho, const int M, const int N){
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N){
        REAL rhoij = rho[idx];
        REAL feq0ij = Feq0(rhoij);
        REAL feq1ij = Feq1(rhoij);
        f[idx] = feq0ij;
#pragma unroll
        for (int it = 1; it != Q; ++it){
            f[idx + MN * it] = feq1ij;
        }
    }
}
__global__
void kernel_feq
(
REAL* __restrict__ f,
const REAL* __restrict__ _rho,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const int M,
const int N
)
{
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        REAL rho = _rho[idx];
        REAL ux = _ux[idx];
        REAL uy = _uy[idx];
        f[idx] = Feq0ad(rho, ux, uy);
        f[idx + MN] = Feq1ad(rho, ux, uy);
        f[idx + MN * 2] = Feq2ad(rho, ux, uy);
        f[idx + MN * 3] = Feq3ad(rho, ux, uy);
        f[idx + MN * 4] = Feq4ad(rho, ux, uy);
    }
}

// ************************************************************************************************
// Lattice-Boltzmann advection-diffusion-reaction kernels

// kernel for advance in pure diffusion, single diffusion coefficient, single-relaxation-time model
__global__
void kernel_advance_diffusion
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0,
const int M, const int N,
const REAL tao_inv
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL rho = COMPUTE_RHO(fin);

        // collision
        fin[0] += tao_inv * (Feq0(rho) - fin[0]);
        REAL feq1 = Feq1(rho);
        fin[1] += tao_inv * (feq1 - fin[1]);
        fin[2] += tao_inv * (feq1 - fin[2]);
        fin[3] += tao_inv * (feq1 - fin[3]);
        fin[4] += tao_inv * (feq1 - fin[4]);

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// load diffusion coefficient indexing from Didx map
// if Didx(i,j) and all its neighbours are negative, then this lattice is masked, then return.
// with the help of L1 cache I didn't consider the global loading redundancy
///*
#define DEFINE_Didx \
int Didx = _Didx[idx]; \
if (Didx < 0) return
//*/
/*
#define DEFINE_Didx \
int Didx = _Didx[idx]; \
bool is_masked = Didx < 0; \
if (i < M - 1) is_masked &= _Didx[idx + 1] < 0; \
if (i > 0) is_masked &= _Didx[idx - 1] < 0; \
if (j < N - 1) is_masked &= _Didx[idx + M] < 0; \
if (j>0) is_masked &= _Didx[idx - M] < 0; \
if (is_masked) return
*/

// kernel for advance in pure diffusion, multiple diffusion coefficient, single-relaxation-time model
__global__
void kernel_advance_diffusion
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const int* __restrict__ _Didx,
const int M, const int N,
const REAL* __restrict__ tao_inv_list
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        DEFINE_Didx;

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);

        REAL rho = COMPUTE_RHO(fin);
        REAL tao_inv = tao_inv_list[Didx];
        // collision
        fin[0] += tao_inv * (Feq0(rho) - fin[0]);
        REAL feq1 = Feq1(rho);
        fin[1] += tao_inv * (feq1 - fin[1]);
        fin[2] += tao_inv * (feq1 - fin[2]);
        fin[3] += tao_inv * (feq1 - fin[3]);
        fin[4] += tao_inv * (feq1 - fin[4]);

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in advection-diffusion, single diffusion coefficient, single-relaxation-time model
__global__
void kernel_advance_advection_diffusion
(
const AD::CONFIG config,
REAL* __restrict__ rho1,
REAL* __restrict__ f1,
const REAL* __restrict__ f0,
const REAL* __restrict__ _Ux,
const REAL* __restrict__ _Uy,
const int M, const int N,
const REAL tao_inv
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL Ux = _Ux[idx];
        REAL Uy = _Uy[idx];

        REAL rho = COMPUTE_RHO(fin);
        // collision
        fin[0] += tao_inv * (Feq0ad(rho, Ux, Uy) - fin[0]);
        fin[1] += tao_inv * (Feq1ad(rho, Ux, Uy) - fin[1]);
        fin[2] += tao_inv * (Feq2ad(rho, Ux, Uy) - fin[2]);
        fin[3] += tao_inv * (Feq3ad(rho, Ux, Uy) - fin[3]);
        fin[4] += tao_inv * (Feq4ad(rho, Ux, Uy) - fin[4]);

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in advection-diffusion, multiple diffusion coefficient, single-relaxation-time model
__global__
void kernel_advance_advection_diffusion
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _Ux, const REAL* __restrict__ _Uy, const int* __restrict__ _Didx,
const int M, const int N,
const REAL* __restrict__ tao_inv_list
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        DEFINE_Didx;

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL Ux = _Ux[idx];
        REAL Uy = _Uy[idx];
        REAL tao_inv = tao_inv_list[Didx];
        REAL rho = COMPUTE_RHO(fin);
        // collision
        fin[0] += tao_inv * (Feq0ad(rho, Ux, Uy) - fin[0]);
        fin[1] += tao_inv * (Feq1ad(rho, Ux, Uy) - fin[1]);
        fin[2] += tao_inv * (Feq2ad(rho, Ux, Uy) - fin[2]);
        fin[3] += tao_inv * (Feq3ad(rho, Ux, Uy) - fin[3]);
        fin[4] += tao_inv * (Feq4ad(rho, Ux, Uy) - fin[4]);

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in diffusion-reaction, single diffusion coefficient, single-relaxation-time model
__global__
void kernel_advance_diffusion_reaction
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _s,
const int M, const int N,
const REAL tao_inv
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL s = _s[idx];
        REAL rho = COMPUTE_RHO(fin);
        // collision
        fin[0] += tao_inv * (Feq0(rho) - fin[0]) + Feq0(s);
        REAL feq1 = Feq1(rho);
        REAL feq1s = Feq1(s);
        fin[1] += tao_inv * (feq1 - fin[1]) + feq1s;
        fin[2] += tao_inv * (feq1 - fin[2]) + feq1s;
        fin[3] += tao_inv * (feq1 - fin[3]) + feq1s;
        fin[4] += tao_inv * (feq1 - fin[4]) + feq1s;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in diffusion-reaction, multiple diffusion coefficient, single-relaxation-time model
__global__
void kernel_advance_diffusion_reaction
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _s, const int* __restrict__ _Didx,
const int M, const int N,
const REAL* __restrict__ tao_inv_list
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        DEFINE_Didx;

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL s = _s[idx];
        REAL rho = COMPUTE_RHO(fin);
        REAL tao_inv = tao_inv_list[Didx];

        // collision
        fin[0] += tao_inv * (Feq0(rho) - fin[0]) + Feq0(s);
        REAL feq1 = Feq1(rho);
        REAL feq1s = Feq1(s);
        fin[1] += tao_inv * (feq1 - fin[1]) + feq1s;
        fin[2] += tao_inv * (feq1 - fin[2]) + feq1s;
        fin[3] += tao_inv * (feq1 - fin[3]) + feq1s;
        fin[4] += tao_inv * (feq1 - fin[4]) + feq1s;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in advection-diffusion-reaction, single diffusion coefficient, single-relaxation-time model
__global__
void kernel_advance_advection_diffusion_reaction
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _Ux, const REAL* __restrict__ _Uy, const REAL* __restrict__ _s,
const int M, const int N,
const REAL tao_inv
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL Ux = _Ux[idx];
        REAL Uy = _Uy[idx];
        REAL s = _s[idx];
        REAL rho = COMPUTE_RHO(fin);

        // collision
        fin[0] += tao_inv * (Feq0ad(rho, Ux, Uy) - fin[0]) + Feq0(s);
        REAL feq1s = Feq1(s);
        fin[1] += tao_inv * (Feq1ad(rho, Ux, Uy) - fin[1]) + feq1s;
        fin[2] += tao_inv * (Feq2ad(rho, Ux, Uy) - fin[2]) + feq1s;
        fin[3] += tao_inv * (Feq3ad(rho, Ux, Uy) - fin[3]) + feq1s;
        fin[4] += tao_inv * (Feq4ad(rho, Ux, Uy) - fin[4]) + feq1s;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in advection-diffusion-reaction, multiple diffusion coefficient, single-relaxation-time model
__global__
void kernel_advance_advection_diffusion_reaction
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _Ux, const REAL* __restrict__ _Uy, const REAL* __restrict__ _s, const int* __restrict__ _Didx,
const int M, const int N,
const REAL* __restrict__ tao_inv_list
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        DEFINE_Didx;

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL Ux = _Ux[idx];
        REAL Uy = _Uy[idx];
        REAL s = _s[idx];
        REAL tao_inv = tao_inv_list[Didx];
        REAL rho = COMPUTE_RHO(fin);

        // collision
        fin[0] += tao_inv * (Feq0ad(rho, Ux, Uy) - fin[0]) + Feq0(s);
        REAL feq1s = Feq1(s);
        fin[1] += tao_inv * (Feq1ad(rho, Ux, Uy) - fin[1]) + feq1s;
        fin[2] += tao_inv * (Feq2ad(rho, Ux, Uy) - fin[2]) + feq1s;
        fin[3] += tao_inv * (Feq3ad(rho, Ux, Uy) - fin[3]) + feq1s;
        fin[4] += tao_inv * (Feq4ad(rho, Ux, Uy) - fin[4]) + feq1s;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in pure diffusion, single diffusion coefficient, multi-relaxation-time model
__global__
void kernel_advance_diffusion_MRT
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0,
const int M, const int N,
const REAL tao_inv
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL rho = COMPUTE_RHO(fin);

        // collision
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0(rho);
        REAL feq1 = Feq1(rho);
        REAL df1 = feq1 - p1*fin[1] - p2*fin[2];
        REAL df2 = feq1 - p2*fin[1] - p1*fin[2];
        fin[1] += df1;
        fin[2] += df2;
        df1 = feq1 - p1*fin[3] - p2*fin[4];
        df2 = feq1 - p2*fin[3] - p1*fin[4];
        fin[3] += df1;
        fin[4] += df2;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in pure diffusion, multiple diffusion coefficient, multi-relaxation-time model
__global__
void kernel_advance_diffusion_MRT
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const int* __restrict__ _Didx,
const int M, const int N,
const REAL* __restrict__ tao_inv_list
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {

        DEFINE_Didx;

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);

        REAL rho = COMPUTE_RHO(fin);
        REAL tao_inv = tao_inv_list[Didx];

        // collision
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0(rho);
        REAL feq1 = Feq1(rho);
        REAL df1 = feq1 - p1*fin[1] - p2*fin[2];
        REAL df2 = feq1 - p2*fin[1] - p1*fin[2];
        fin[1] += df1;
        fin[2] += df2;
        df1 = feq1 - p1*fin[3] - p2*fin[4];
        df2 = feq1 - p2*fin[3] - p1*fin[4];
        fin[3] += df1;
        fin[4] += df2;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in advection-diffusion, single diffusion coefficient, multi-relaxation-time model
__global__
void kernel_advance_advection_diffusion_MRT
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _Ux, const REAL* __restrict__ _Uy,
const int M, const int N,
const REAL tao_inv
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL Ux = _Ux[idx];
        REAL Uy = _Uy[idx];

        REAL rho = COMPUTE_RHO(fin);

        // collision
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0ad(rho, Ux, Uy);
        REAL df1_bc = Feq1ad(rho, Ux, Uy) - fin[1]; // bc means before-collide
        REAL df2_bc = Feq2ad(rho, Ux, Uy) - fin[2];
        fin[1] += p1*df1_bc + p2*df2_bc;
        fin[2] += p2*df1_bc + p1*df2_bc;
        df1_bc = Feq3ad(rho, Ux, Uy) - fin[3];
        df2_bc = Feq4ad(rho, Ux, Uy) - fin[4];
        fin[3] += p1*df1_bc + p2*df2_bc;
        fin[4] += p2*df1_bc + p1*df2_bc;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in advection-diffusion, multiple diffusion coefficient, multi-relaxation-time model
__global__
void kernel_advance_advection_diffusion_MRT
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _Ux, const REAL* __restrict__ _Uy, const int* __restrict__ _Didx,
const int M, const int N,
const REAL* __restrict__ tao_inv_list
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        DEFINE_Didx;

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL Ux = _Ux[idx];
        REAL Uy = _Uy[idx];
        REAL tao_inv = tao_inv_list[Didx];

        REAL rho = COMPUTE_RHO(fin);

        // collision
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0ad(rho, Ux, Uy);
        REAL df1_bc = Feq1ad(rho, Ux, Uy) - fin[1]; // bc means before-collide
        REAL df2_bc = Feq2ad(rho, Ux, Uy) - fin[2];
        fin[1] += p1*df1_bc + p2*df2_bc;
        fin[2] += p2*df1_bc + p1*df2_bc;
        df1_bc = Feq3ad(rho, Ux, Uy) - fin[3];
        df2_bc = Feq4ad(rho, Ux, Uy) - fin[4];
        fin[3] += p1*df1_bc + p2*df2_bc;
        fin[4] += p2*df1_bc + p1*df2_bc;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in diffusion-reaction, single diffusion coefficient, multi-relaxation-time model
__global__
void kernel_advance_diffusion_reaction_MRT
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _s,
const int M, const int N,
const REAL tao_inv
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL s = _s[idx];

        REAL rho = COMPUTE_RHO(fin);

        // collision
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0(rho) + Feq0(s);
        REAL feq1 = Feq1(rho);
        REAL feq1s = Feq1(s);
        REAL df1 = feq1 - p1*fin[1] - p2*fin[2] + feq1s;
        REAL df2 = feq1 - p2*fin[1] - p1*fin[2] + feq1s;
        fin[1] += df1;
        fin[2] += df2;
        df1 = feq1 - p1*fin[3] - p2*fin[4] + feq1s;
        df2 = feq1 - p2*fin[3] - p1*fin[4] + feq1s;
        fin[3] += df1;
        fin[4] += df2;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in diffusion-reaction, multiple diffusion coefficient, multi-relaxation-time model
__global__
void kernel_advance_diffusion_reaction_MRT
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _s, const int* __restrict__ _Didx,
const int M, const int N,
const REAL* __restrict__ tao_inv_list
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        DEFINE_Didx;

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL s = _s[idx];
        REAL tao_inv = tao_inv_list[Didx];

        REAL rho = COMPUTE_RHO(fin);

        // collision
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0(rho) + Feq0(s);
        REAL feq1 = Feq1(rho);
        REAL feq1s = Feq1(s);
        REAL df1 = feq1 - p1*fin[1] - p2*fin[2] + feq1s;
        REAL df2 = feq1 - p2*fin[1] - p1*fin[2] + feq1s;
        fin[1] += df1;
        fin[2] += df2;
        df1 = feq1 - p1*fin[3] - p2*fin[4] + feq1s;
        df2 = feq1 - p2*fin[3] - p1*fin[4] + feq1s;
        fin[3] += df1;
        fin[4] += df2;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in advection-diffusion-reaction, single diffusion coefficient, multi-relaxation-time model
__global__
void kernel_advance_advection_diffusion_reaction_MRT
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _Ux, const REAL* __restrict__ _Uy, const REAL* __restrict__ _s,
const int M, const int N,
const REAL tao_inv
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL Ux = _Ux[idx];
        REAL Uy = _Uy[idx];
        REAL s = _s[idx];

        REAL rho = COMPUTE_RHO(fin);

        // collision
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0ad(rho, Ux, Uy) + Feq0(s);
        REAL feq1s = Feq1(s);
        REAL df1_bc = Feq1ad(rho, Ux, Uy) - fin[1]; // bc means before-collide
        REAL df2_bc = Feq2ad(rho, Ux, Uy) - fin[2];
        fin[1] += p1*df1_bc + p2*df2_bc + feq1s;
        fin[2] += p2*df1_bc + p1*df2_bc + feq1s;
        df1_bc = Feq3ad(rho, Ux, Uy) - fin[3];
        df2_bc = Feq4ad(rho, Ux, Uy) - fin[4];
        fin[3] += p1*df1_bc + p2*df2_bc + feq1s;
        fin[4] += p2*df1_bc + p1*df2_bc + feq1s;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for advance in advection-diffusion-reaction, multiple diffusion coefficient, multi-relaxation-time model
__global__
void kernel_advance_advection_diffusion_reaction_MRT
(
const AD::CONFIG config,
REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const REAL* __restrict__ _Ux, const REAL* __restrict__ _Uy, const REAL* __restrict__ _s, const int* __restrict__ _Didx,
const int M, const int N,
const REAL* __restrict__ tao_inv_list
)
{
    // step1 fc = f - tao_inv * (f-feq)
    // step2 f1 = adcecting(fc)
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        DEFINE_Didx;

        REAL fin[Q];
        device_load_f0(fin, f0, idx, MN);
        REAL Ux = _Ux[idx];
        REAL Uy = _Uy[idx];
        REAL s = _s[idx];
        REAL tao_inv = tao_inv_list[Didx];
        REAL rho = COMPUTE_RHO(fin);

        // collision
        /*
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0ad(rho, Ux, Uy) + Feq0(s);
        REAL feq1s = Feq1(s);
        REAL df1 = Feq1ad(rho, Ux, Uy) - p1*fin[1] - p2*fin[2] +feq1s;
        REAL df2 = Feq2ad(rho, Ux, Uy) - p2*fin[1] - p1*fin[2] +feq1s;
        fin[1] += df1;
        fin[2] += df2;
        df1 = Feq3ad(rho, Ux, Uy) - p1*fin[3] - p2*fin[4] +feq1s;
        df2 = Feq4ad(rho, Ux, Uy) - p2*fin[3] - p1*fin[4] +feq1s;
        fin[3] += df1;
        fin[4] += df2;
        */

        // collision
        REAL p1 = 0.5 + 0.5 * tao_inv;
        REAL p2 = 0.5 - 0.5 * tao_inv;
        fin[0] = Feq0ad(rho, Ux, Uy) + Feq0(s);
        REAL feq1s = Feq1(s);
        REAL df1_bc = Feq1ad(rho, Ux, Uy) - fin[1]; // bc means before-collide
        REAL df2_bc = Feq2ad(rho, Ux, Uy) - fin[2];
        fin[1] += p1*df1_bc + p2*df2_bc + feq1s;
        fin[2] += p2*df1_bc + p1*df2_bc + feq1s;
        df1_bc = Feq3ad(rho, Ux, Uy) - fin[3];
        df2_bc = Feq4ad(rho, Ux, Uy) - fin[4];
        fin[3] += p1*df1_bc + p2*df2_bc + feq1s;
        fin[4] += p2*df1_bc + p1*df2_bc + feq1s;

        // write macro value rho1
        WRITE_RHO(rho1, idx, rho);

        device_advection(f1, fin, i, j, idx, M, N, MN);
        device_boundary_condition(config, f1, fin, i, j, idx, M, N, MN);
    }
}

// kernel for test the pure data copy efficiency
// if a advance kernel comsumes more or less the same time as this kernel
// it means:
// 1. kernel reaches it's highest performance
// 2. kernel is bottlenecked by global memory bandwidth.
__global__
void kernel_copy(AD::CONFIG config, REAL* __restrict__ rho1, REAL* __restrict__ f1, const REAL* __restrict__ f0, const int M, const int N, REAL tao_inv){
    int MN = M*N;
    int i = blockIdx.x * BSIZEM + threadIdx.x;
    int j = blockIdx.y * BSIZEN + threadIdx.y;
    int idx = j*M + i;
    if (i < M && j < N){
        REAL fin[Q];
        fin[0] = f0[idx];
        fin[1] = f0[idx + MN];
        fin[2] = f0[idx + MN * 2];
        fin[3] = f0[idx + MN * 3];
        fin[4] = f0[idx + MN * 4];

        // pure copy
        f1[idx] = fin[0];
        f1[idx + MN] = fin[1];
        f1[idx + MN * 2] = fin[2];
        f1[idx + MN * 3] = fin[3];
        f1[idx + MN * 4] = fin[4];
    }
}
// ************************************************************************************************
// methods of ADmethod class

// calculate equilibrium distribution function from macro concentration rho
void ADmethod::feq(REAL* f, const REAL* rho, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_feq CUDALAUNCH(grid, block, 0, stream) (f, rho, m, n);
}
// calculate equilibrium distribution function from macro concentration rho
void ADmethod::feq(REAL* f, const REAL* rho, const REAL* ux, const REAL* uy, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_feq CUDALAUNCH(grid, block, 0, stream) (f, rho, ux, uy, m, n);
}

// cauculate macro concentration rho alone from distribution function f
void ADmethod::exportRho(REAL* rho, const REAL* f, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_exportRho CUDALAUNCH(grid, block, 0, stream)
        (rho, f, m, n);
}

// ************************************************************************************************
// single-relaxation-time, BGK model

// advance time step on pure diffusion model
// single diffusion coefficient, single-relaxation-time
void ADmethod::advance_diffusion(
    REAL* rho1, REAL* f1, const REAL* f0, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_diffusion CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, m, n, tao_inv);
}

// advance time step on pure diffusion model
// multiple diffusion coefficient, single-relaxation-time
void ADmethod::advance_diffusion(
    REAL* rho1, REAL* f1, const REAL* f0, const int* Didx, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_diffusion CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Didx, m, n, tao_inv_list.data_ptr());
}

// advance time step on advection-diffusion model
// single diffusion coefficient, single-relaxation-time
void ADmethod::advance_advection_diffusion(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_advection_diffusion CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Ux, Uy, m, n, tao_inv);
}

// advance time step on advection-diffusion model
// multiple diffusion coefficient, single-relaxation-time
void ADmethod::advance_advection_diffusion(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const int* Didx, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_advection_diffusion CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Ux, Uy, Didx, m, n, tao_inv_list.data_ptr());
}

// advance time step on diffusion-reaction model
// single diffusion coefficient, single-relaxation-time
void ADmethod::advance_diffusion_reaction(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* s, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_diffusion_reaction CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, s, m, n, tao_inv);
}

// advance time step on diffusion-reaction model
// multiple diffusion coefficient, single-relaxation-time
void ADmethod::advance_diffusion_reaction(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* s, const int* Didx, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_diffusion_reaction CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, s, Didx, m, n, tao_inv_list.data_ptr());
}

// advance time step on advection-diffusion-reaction model
// single diffusion coefficient, single-relaxation-time
void ADmethod::advance_advection_diffusion_reaction(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const REAL* s, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_advection_diffusion_reaction CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Ux, Uy, s, m, n, tao_inv);
}

// advance time step on advection-diffusion-reaction model
// multiple diffusion coefficient, single-relaxation-time
void ADmethod::advance_advection_diffusion_reaction(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const REAL* s, const int* Didx, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_advection_diffusion_reaction CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Ux, Uy, s, Didx, m, n, tao_inv_list.data_ptr());
}

// ************************************************************************************************
// multi-relaxation-time, isotropic MRT model

// advance time step on pure diffusion model
// single diffusion coefficient, single-relaxation-time
void ADmethod::advance_diffusion_MRT(
    REAL* rho1, REAL* f1, const REAL* f0, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_diffusion_MRT CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, m, n, tao_inv);
}

// advance time step on pure diffusion model
// multiple diffusion coefficient, single-relaxation-time
void ADmethod::advance_diffusion_MRT(
    REAL* rho1, REAL* f1, const REAL* f0, const int* Didx, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_diffusion_MRT CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Didx, m, n, tao_inv_list.data_ptr());
}

// advance time step on advection-diffusion model
// single diffusion coefficient, single-relaxation-time
void ADmethod::advance_advection_diffusion_MRT(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_advection_diffusion_MRT CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Ux, Uy, m, n, tao_inv);
}

// advance time step on advection-diffusion model
// multiple diffusion coefficient, single-relaxation-time
void ADmethod::advance_advection_diffusion_MRT(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const int* Didx, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_advection_diffusion_MRT CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Ux, Uy, Didx, m, n, tao_inv_list.data_ptr());
}

// advance time step on diffusion-reaction model
// single diffusion coefficient, single-relaxation-time
void ADmethod::advance_diffusion_reaction_MRT(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* s, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_diffusion_reaction_MRT CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, s, m, n, tao_inv);
}

// advance time step on diffusion-reaction model
// multiple diffusion coefficient, single-relaxation-time
void ADmethod::advance_diffusion_reaction_MRT(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* s, const int* Didx, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_diffusion_reaction_MRT CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, s, Didx, m, n, tao_inv_list.data_ptr());
}

// advance time step on advection-diffusion-reaction model
// single diffusion coefficient, single-relaxation-time
void ADmethod::advance_advection_diffusion_reaction_MRT(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const REAL* s, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_advection_diffusion_reaction_MRT CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Ux, Uy, s, m, n, tao_inv);
}

// advance time step on advection-diffusion-reaction model
// multiple diffusion coefficient, single-relaxation-time
void ADmethod::advance_advection_diffusion_reaction_MRT(
    REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const REAL* s, const int* Didx, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_advance_advection_diffusion_reaction_MRT CUDALAUNCH(grid, block, 0, stream)
        (config, rho1, f1, f0, Ux, Uy, s, Didx, m, n, tao_inv_list.data_ptr());
}
// ************************************************************************************************

// kernel for reset concentration
// added by Zhe Gou on 20211208
__global__
void kernel_reset(
REAL* __restrict__ rho1,
REAL* __restrict__ f1, 
REAL* __restrict__ f0,
const int* __restrict__ label, 
const int M, const int N)
{
    int MN, i, j, idx;
    device_compute_index(MN, i, j, idx, M, N);
    if (i < M && j < N)
    {
        int label_this = label[idx];
        if (label_this > 0)
        {
            rho1[idx] = 0;
#pragma unroll
            for (int it = 0; it != Q; ++it)
            {
                f1[idx + MN * it] = 0;
                f0[idx + MN * it] = 0;
            }
        }
    }
}

// reset concentration inside vesicle to 0
// added by Zhe Gou, 20211208
void ADmethod::reset(
    REAL* rho1, REAL* f1, REAL* f0, const int* label, const int m, const int n){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_reset CUDALAUNCH(grid, block, 0, stream)
        (rho1, f1, f0, label, m, n);
}