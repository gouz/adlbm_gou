#include "NBeadsSwimmer.cuh"

// CUDA LAUNCH PARAMETER
#define BSIZE 128
#define CUDALAUNCH(n) <<<((n) + BSIZE - 1) / BSIZE, BSIZE>>>

#define ACTIVE_FORCE(t, T, f0, alp) (f0) * sin(2*PI * (t)/(T) + (alp))

__global__ void kernel_calculateForce
(
REAL* __restrict__ _fx,
REAL* __restrict__ _fy,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const REAL* __restrict__ _alps,
const REAL t, // current time
const REAL l0,
const REAL k,
const REAL f0,
const REAL T,
const int n
)
{
    // active force in shared mem, be careful, BSIZE must be larger than n
    __shared__ REAL fs[BSIZE];

    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < n){

        // prepare indexes
        int idxR = (idx - 1 + n) % n;
        int idxL = (idx + 1) % n;

        // "u" represents the coordinates
        REAL ux = _x[idx];
        REAL uy = _y[idx];
        REAL uxroll = _x[idxL];
        REAL uyroll = _y[idxL];
        REAL uxrollp = _x[idxR];
        REAL uyrollp = _y[idxR];

        // spring length
        REAL ls = sqrt((ux - uxroll) * (ux - uxroll) + (uy - uyroll) * (uy - uyroll));
        REAL lsrollp = sqrt((ux - uxrollp) * (ux - uxrollp) + (uy - uyrollp) * (uy - uyrollp));
        
        // active force along spring directions
        fs[idx] = ACTIVE_FORCE(t, T, f0, _alps[idx]);
        __syncthreads();
        REAL f = fs[idx];
        REAL frollp = fs[idxR];

        // active force along x, y directions
        REAL faxs = f*(ux - uxroll) / ls + frollp*(ux - uxrollp) / lsrollp;
        REAL fays = f*(uy - uyroll) / ls + frollp*(uy - uyrollp) / lsrollp;

        // spring force along x, y directions
        REAL fsxs = k*((ls - l0) / l0)*(uxroll - ux) / ls + k*((lsrollp - l0) / l0)*(uxrollp - ux) / lsrollp;
        REAL fsys = k*((ls - l0) / l0)*(uyroll - uy) / ls + k*((lsrollp - l0) / l0)*(uyrollp - uy) / lsrollp;

        // write to fx, fy
        _fx[idx] = faxs + fsxs;
        _fy[idx] = fays + fsys;

    }
}

void NBeadsSwimmer::calculateForce(){
    kernel_calculateForce CUDALAUNCH(n)
        (fx.data_ptr(), fy.data_ptr(), x.data_ptr(), y.data_ptr(), alps.data_ptr(), t, l0, k, f0, T, n);
}

void __global__ kernel_move_freespace
(
REAL* __restrict__ _x,
REAL* __restrict__ _y,
REAL* __restrict__ _ux, // velocity x
REAL* __restrict__ _uy, // velocity y
const REAL* __restrict__ _fx,
const REAL* __restrict__ _fy,
const int n,
const REAL dt
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < n){
        REAL ux = _ux[idx];
        REAL uy = _uy[idx];
        REAL x = _x[idx] + ux * dt;
        REAL y = _y[idx] + uy * dt;
        ux += _fx[idx] * dt;
        uy += _fy[idx] * dt;
        _x[idx] = x;
        _y[idx] = y;
        _ux[idx] = ux;
        _uy[idx] = uy;
    }
}

void NBeadsSwimmer::move_freespace(const REAL dt){
    t += dt;
    calculateForce();
    kernel_move_freespace CUDALAUNCH(n)
        (x.data_ptr(), y.data_ptr(), ux.data_ptr(), uy.data_ptr(), fx.data_ptr(), fy.data_ptr(), n, dt);
}

__global__ void kernel_move_in_fluid
(
REAL* __restrict__ _x,
REAL* __restrict__ _y,
const REAL* __restrict__ _ux, // velocity x
const REAL* __restrict__ _uy, // velocity y
const REAL* __restrict__ _fx,
const REAL* __restrict__ _fy,
const int n,
const REAL gamma, // fluid viscosity // all with LB units
const REAL rs, // bead radius
const REAL rho, // fluid density
const REAL dt // time step
)
{
    // drag force on a cylinder (with 2-dim correction) at small Re is estimated via this expression from
    // http://www.lmm.jussieu.fr/~lagree/COURS/M2MHP/petitRe.pdf
    // drag_force = (4*PI*eta) / (0.5 - gamma_E - ln(Re / 4))
    // here gamma_E = \int_1^{\inf} (1/[x]-1/x)dx is the Euler�CMascheroni constant, valued 0.5772156649
    // Re is the local reynolds number
    // a newton method is used by 2 iterations when estimating the drag velocity
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < n){

        REAL fx = _fx[idx];
        REAL fy = _fy[idx];
        REAL ftot = sqrt(fx*fx + fy*fy);
        REAL ux_drag, uy_drag;
        REAL u_drag = 1e-10; // initial value before newton iteration
        REAL A = ftot / (4.0*PI*gamma*rho);
        REAL B = (-0.0772156649 - log(rs / gamma * 0.25)) * A;
        if (ftot > 0.0){
#pragma unroll
            for (int iter = 0; iter != 10; ++iter){
                REAL Fu = -u_drag - A*log(u_drag) + B;
                REAL dFdu = -1.0 - A / u_drag;
                /*
                if (idx == 1){
                    printf("*********************\n");
                    printf("u_drag = %e\n", u_drag);
                    printf("Fu = %e\n", Fu);
                    printf("dFdu = %e\n", dFdu);
                    printf("u_drag_new = %e\n", u_drag - Fu / dFdu);
                    printf("*********************\n");
                }
                */
                u_drag = u_drag - Fu / dFdu;
            }
            ux_drag = fx / ftot * u_drag;
            uy_drag = fy / ftot * u_drag;
        }
        else{
            u_drag = 0.0;
            ux_drag = 0.0;
            uy_drag = 0.0;
        }

        REAL xnew = _x[idx] + (_ux[idx] + ux_drag)*dt;
        REAL ynew = _y[idx] + (_uy[idx] + uy_drag)*dt;

        _x[idx] = xnew;
        _y[idx] = ynew;
    }
}

void NBeadsSwimmer::move_in_fluid(const REAL dt){
    kernel_move_in_fluid CUDALAUNCH(n)
        (x.data_ptr(), y.data_ptr(), ux.data_ptr(), uy.data_ptr(), fx.data_ptr(), fy.data_ptr(), n, gamma, rs, rho, dt);
    cudaDeviceSynchronize();
}