#include "NSsolver.cuh"

#include "_deviceImmersedBoundaryMethod_distributeForce.cuh"

// ************************************************************************************************
// DEFINE cuda launch pattern
#ifdef NS_DEFAULT_STREAM
#define CUDALAUNCH(grid, block, sharedMemSize, streamID) <<<grid, block>>>
#else
#define CUDALAUNCH(grid, block, sharedMemSize, streamID) <<<grid, block, sharedMemSize, streamID>>>
#endif

#define POSITIVE_MOD(i, m) (((i) % (m)+(m)) % (m)) // positive modulo for periodic boundary

// please turn this label off, otherwise streaming function will print all streaming information out
// #define NS_DEBUG

// block size
#define BSIZEM 16
#define BSIZEN 16

#define DEFINE_LAUNCH_PARAMETER(grid, block, m, n) \
dim3 grid((m + BSIZEM - 1) / BSIZEM, (n + BSIZEN - 1) / BSIZEN); \
const dim3 block(BSIZEM, BSIZEN)
// ************************************************************************************************
// D2Q9 model: classical LB method

static const int Q = NSsolver::Q;

// pseudo lattice sound speed
#define CS2 (1.0/3.0) //(0.333333333333333333)
#define CS2_inv (3.0)

// weight
#define W0 (4.0/9.0) //(0.444444444444444444)
#define W1 (1.0/9.0) //(0.111111111111111111)
#define W5 (1.0/36.0) //(0.0277777777777777777)

#define COMPUTE_RHO(f) (f[0] + f[1] + f[2] + f[3] + f[4] + f[5] + f[6] + f[7] + f[8])
#define COMPUTE_UX_WITH_BODYFORCE(f, Fx, rho) (f[1] - f[3] + f[5] - f[6] - f[7] + f[8] + 0.5*Fx) / rho
#define COMPUTE_UY_WITH_BODYFORCE(f, Fy, rho) (f[2] - f[4] + f[5] + f[6] - f[7] - f[8] + 0.5*Fy) / rho
#define COMPUTE_UX(f, rho) (f[1] - f[3] + f[5] - f[6] - f[7] + f[8]) / rho
#define COMPUTE_UY(f, rho) (f[2] - f[4] + f[5] + f[6] - f[7] - f[8]) / rho

__global__ void kernel_initializeByDensity
(
REAL* __restrict__ f,
const REAL _rho,
const int m,
const int n
){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;
    if (i < m && j < n){
        REAL rho = _rho;
        f[idx] = W0 * rho;
        f[idx + mn] = W1 * rho;
        f[idx + mn * 2] = W1 * rho;
        f[idx + mn * 3] = W1 * rho;
        f[idx + mn * 4] = W1 * rho;
        f[idx + mn * 5] = W5 * rho;
        f[idx + mn * 6] = W5 * rho;
        f[idx + mn * 7] = W5 * rho;
        f[idx + mn * 8] = W5 * rho;
    }
}

void NSsolver::initializeByDensity(const REAL _rho){
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    this->rho = _rho;
    this->ux = 0.0;
    this->uy = 0.0;
    kernel_initializeByDensity CUDALAUNCH(grid, block, 0, stream) (f.data_ptr(), _rho, m, n);
    f1 = f;
}

__global__
void kernel_calculate_macro_variables_LBM
(
REAL* __restrict__ _ux,
REAL* __restrict__ _uy,
REAL* __restrict__ _rho,
const REAL* __restrict__ _f,
const int m,
const int n,
const int* _eta_idx // label
);
__global__
void kernel_calculate_macro_variables_LBM
(
REAL* __restrict__ _ux,
REAL* __restrict__ _uy,
REAL* __restrict__ _rho,
const REAL* __restrict__ _f,
const REAL* __restrict__ _Fx,
const REAL* __restrict__ _Fy,
const int m,
const int n,
const int* _eta_idx // label
);

// read flow field from file exported from af_save_flow_field()
// file must be formated in ArrayFire binary files
void NSsolver::initializeByFile(const char* f_filename){

    auto rho_temp = DA::readArray<REAL>(f_filename, 0);
    auto ux_temp = DA::readArray<REAL>(f_filename, 1);
    auto uy_temp = DA::readArray<REAL>(f_filename, 2);
    auto f_temp = DA::readArray<REAL>(f_filename, 3);
    auto f1_temp = DA::readArray<REAL>(f_filename, 4);
    bool size_validity;

    size_validity = rho_temp.dim(0) == m && rho_temp.dim(1) == n && rho_temp.dim(2) == 1 && rho_temp.dim(3) == 1;
    assert(size_validity);
    size_validity = ux_temp.dim(0) == m && ux_temp.dim(1) == n && ux_temp.dim(2) == 1 && ux_temp.dim(3) == 1;
    assert(size_validity);
    size_validity = uy_temp.dim(0) == m && uy_temp.dim(1) == n && uy_temp.dim(2) == 1 && uy_temp.dim(3) == 1;
    assert(size_validity);
    size_validity = f_temp.dim(0) == m && f_temp.dim(1) == n && f_temp.dim(2) == Q && f_temp.dim(3) == 1;
    assert(size_validity);
    size_validity = f1_temp.dim(0) == m && f1_temp.dim(1) == n && f1_temp.dim(2) == Q && f1_temp.dim(3) == 1;
    assert(size_validity);

    ux = ux_temp;
    uy = uy_temp;
    rho = rho_temp;

    f = f_temp;
    f1 = f1_temp;
}


// _NS_device_collision: function for calculating the collision in LBM, Single relaxation time method
//**************************************
// numbering of discretized velocity:
// Cx = [1, 0, -1, 0, 1, -1, -1, 0];
// Cy = [0, 1, 0, -1, 1, 1, -1, -1];
//
//              6  2  5
//                \|/
//              3--0--1
//                /|\
//              7  4  8
//
//**************************************
// rho: macro density
// ux, uy: macro velocity
// f[Q]: distribution function
// Fx, Fy: body force density
// tao_inv: inversed relaxation time
#define EPS (3.0) // EPS = 1.0 / (Cs * Cs), Cs is pseudo lattice sound speed
__device__ __forceinline__ void device_collision
(
const REAL& rho,
const REAL& ux,
const REAL& uy,
REAL* f,
const REAL& Fx,
const REAL& Fy,
const REAL& tao_inv
)
{
    // time factor of body force
    REAL tao_inv_F = 1.0 - 0.5 * tao_inv;

    // the third term in the equilibrium distribution function Si
    REAL u_squ_term = 0.5*EPS*(ux*ux + uy*uy);

    // body force scalar projection on velocity direction
    REAL uFn = ux*Fx + uy*Fy;

    //**************************************
    // i is the numbering of micro speed
    REAL Fi;    // body force distribution on ith position
    REAL feqi;  // equilibrium distribution function on ith position
    REAL ui;    // macro velocity scalar projection on ith micro velocity C[i]
    REAL Fin;   // body force scalar projection on ith micro velocity C[i]

    //**************************************
    // i = 0
    f[0] += tao_inv * (W0 * rho * (1.0 - u_squ_term) - f[0]) - tao_inv_F * W0 * EPS * uFn;
    //**************************************
    // i = 1, i_inverse = 3
    // ui = ux; // no need to store this value
    // Fin = Fx; // no need to store this value
    Fi = W1 * EPS * (Fx - uFn) + W1 * EPS*EPS * Fx * ux;
    feqi = W1 * rho * (1.0 + EPS * ux + 0.5*EPS*EPS * ux*ux - u_squ_term);
    f[1] += tao_inv * (feqi - f[1]) + tao_inv_F * Fi;
    f[3] += tao_inv * (feqi - 2.0*W1*EPS * rho * ux - f[3]) + tao_inv_F * (Fi - 2.0*W1*EPS*Fx);
    //**************************************
    // i = 2, i_inverse = 4
    // ui = uy; // no need to store this value
    // Fin = Fy; // no need to store this value
    Fi = W1 * EPS * (Fy - uFn) + W1 * EPS*EPS * Fy * uy;
    feqi = W1 * rho * (1.0 + EPS * uy + 0.5*EPS*EPS * uy*uy - u_squ_term);
    f[2] += tao_inv * (feqi - f[2]) + tao_inv_F * Fi;
    f[4] += tao_inv * (feqi - 2.0*W1*EPS * rho * uy - f[4]) + tao_inv_F * (Fi - 2.0*W1*EPS*Fy);
    //**************************************
    // i = 5, i_inverse = 7
    ui = ux + uy;
    Fin = Fx + Fy;
    Fi = W5 * EPS * (Fin - uFn) + W5 * EPS*EPS * Fin * ui;
    feqi = W5 * rho * (1.0 + EPS * ui + 0.5*EPS*EPS * ui*ui - u_squ_term);
    f[5] += tao_inv * (feqi - f[5]) + tao_inv_F * Fi;
    f[7] += tao_inv * (feqi - 2.0*W5*EPS * rho * ui - f[7]) + tao_inv_F * (Fi - 2.0*W5*EPS*Fin);
    //**************************************
    // i = 6, i_inverse = 8
    ui = -ux + uy;
    Fin = -Fx + Fy;
    Fi = W5 * EPS * (Fin - uFn) + W5 * EPS*EPS * Fin * ui;
    feqi = W5 * rho * (1.0 + EPS * ui + 0.5*EPS*EPS * ui*ui - u_squ_term);
    f[6] += tao_inv * (feqi - f[6]) + tao_inv_F * Fi;
    f[8] += tao_inv * (feqi - 2.0*W5*EPS * rho * ui - f[8]) + tao_inv_F * (Fi - 2.0*W5*EPS*Fin);
    //**************************************
}
__device__ __forceinline__ void device_collision
(
const REAL& rho,
const REAL& ux,
const REAL& uy,
REAL* f,
const REAL& tao_inv
)
{
    // the third term in the equilibrium distribution function
    REAL u_squ_term = 0.5*EPS*(ux*ux + uy*uy);

    //**************************************
    // i is the numbering of micro speed
    REAL feqi;  // equilibrium distribution function on ith position
    REAL ui;    // velocity scalar projection on ith micro velocity C[i]
    //**************************************
    // i = 0
    f[0] += tao_inv * (W0 * rho * (1.0 - u_squ_term) - f[0]);
    //**************************************
    // i = 1, i_inverse = 3
    // ui = ux; // no need to store this value
    feqi = W1 * rho * (1.0 + EPS * ux + 0.5*EPS*EPS * ux*ux - u_squ_term);
    f[1] += tao_inv * (feqi - f[1]);
    f[3] += tao_inv * (feqi - 2.0*W1*EPS * rho * ux - f[3]);
    //**************************************
    // i = 2, i_inverse = 4
    // ui = uy; // no need to store this value
    feqi = W1 * rho * (1.0 + EPS * uy + 0.5*EPS*EPS * uy*uy - u_squ_term);
    f[2] += tao_inv * (feqi - f[2]);
    f[4] += tao_inv * (feqi - 2.0*W1*EPS * rho * uy - f[4]);
    //**************************************
    // i = 5, i_inverse = 7
    ui = ux + uy;
    feqi = W5 * rho * (1.0 + EPS * ui + 0.5*EPS*EPS * ui*ui - u_squ_term);
    f[5] += tao_inv * (feqi - f[5]);
    f[7] += tao_inv * (feqi - 2.0*W5*EPS * rho * ui - f[7]);
    //**************************************
    // i = 6, i_inverse = 8
    ui = -ux + uy;
    feqi = W5 * rho * (1.0 + EPS * ui + 0.5*EPS*EPS * ui*ui - u_squ_term);
    f[6] += tao_inv * (feqi - f[6]);
    f[8] += tao_inv * (feqi - 2.0*W5*EPS * rho * ui - f[8]);
    //**************************************
}

// defining MRT parameters
// inherited from LBMconstant.hpp
#ifndef _LBM_MRT_SE
#define SE 1.1
#else
#define SE _LBM_MRT_SE
#endif
#ifndef _LBM_MRT_SEPS
#define SEPS 1.1
#else
#define SEPS _LBM_MRT_SEPS
#endif
#ifndef _LBM_MRT_SQ
#define SQ 1.1
#else
#define SQ _LBM_MRT_SQ
#endif

// a multiple relaxation time collision function from book by Zhaoli GUO
// MRT collisionn operator with bodyforce:
/*
@book{
guo2013LBM,
title={Lattice Boltzmann method and its applications in engineering},
author={Guo, Zhaoli and Shu, Chang},
volume={3},
year={2013},
publisher={World Scientific}
}
*/
// MRT projection from moments to distribution function
__device__ __forceinline__ void device_collision_MRT_m2f
(
REAL* f,
const REAL* m
)
{
    REAL scratch01, scratch02, scratch03, scratch04;
    f[0] = (m[0] - m[1] + m[2]) / 9.0;

    scratch01 = (m[0] - 0.25*m[1] - 0.5*m[2]) / 9.0;
    scratch02 = (m[3] - m[4]) / 6.0;
    scratch03 = 0.25*m[7];
    f[1] = scratch01 + scratch02 + scratch03;
    f[3] = scratch01 - scratch02 + scratch03;
    scratch02 = (m[5] - m[6]) / 6.0;
    f[2] = scratch01 + scratch02 - scratch03;
    f[4] = scratch01 - scratch02 - scratch03;

    scratch01 = (m[0] + 0.5*m[1] + 0.25*m[2]) / 9.0;
    scratch02 = m[3] / 6.0 + m[4] / 12.0;
    scratch03 = m[5] / 6.0 + m[6] / 12.0;
    scratch04 = 0.25*m[8];
    f[5] = scratch01 + scratch02 + scratch03 + scratch04;
    f[6] = scratch01 - scratch02 + scratch03 - scratch04;
    f[7] = scratch01 - scratch02 - scratch03 + scratch04;
    f[8] = scratch01 + scratch02 - scratch03 - scratch04;
}

__device__ __forceinline__ void device_collision_MRT_f2m_relax
(
REAL* m,
const REAL* f,
const REAL& rho,
const REAL& ux,
const REAL& uy,
const REAL& Se,
const REAL& Seps,
const REAL& Sq,
const REAL& tao_inv
)
{

    REAL ux2 = ux*ux;
    REAL uy2 = uy*uy;

    REAL scratch01, scratch02;

    // moments calculation

    m[0] = rho;

    scratch01 = f[1] + f[2] + f[3] + f[4];
    scratch02 = f[5] + f[6] + f[7] + f[8];
    m[1] = -4 * f[0] - scratch01 + 2 * scratch02;
    m[1] += (Se * (rho * (-2.0 + 3 * (ux2 + uy2)) - m[1]));
    m[2] = 4 * f[0] - 2 * scratch01 + scratch02;
    m[2] += (Seps * (rho *(1.0 - 3 * (ux2 + uy2)) - m[2]));

    m[3] = rho*ux;

    m[4] = -2 * (f[1] - f[3]) + f[5] - f[6] - f[7] + f[8];
    m[4] += (Sq * (-rho*ux - m[4]));

    m[5] = rho*uy;

    m[6] = -2 * (f[2] - f[4]) + f[5] + f[6] - f[7] - f[8];
    m[6] += (Sq * (-rho*uy - m[6]));

    m[7] = f[1] - f[2] + f[3] - f[4];
    m[7] += (tao_inv * (rho*(ux2 - uy2) - m[7]));

    m[8] = f[5] - f[6] + f[7] - f[8];
    m[8] += (tao_inv * (rho*ux*uy - m[8]));
}

// a multiple relaxation time collision function from book by Zhaoli GUO
// MRT collisionn operator with bodyforce
/*
@book{
guo2013LBM,
title={Lattice Boltzmann method and its applications in engineering},
author={Guo, Zhaoli and Shu, Chang},
volume={3},
year={2013},
publisher={World Scientific}
}
*/
__device__ __forceinline__ void device_collision_MRT
(
const REAL& rho,
const REAL& ux,
const REAL& uy,
REAL* f,
const REAL& Fx,
const REAL& Fy,
const REAL& tao_inv
)
{
    // MRT relaxation factor in moment space
    // using tao_inv will bring MRT back to BGK model
    const REAL Se = SE; // tao_inv; //
    const REAL Seps = SEPS; // tao_inv; //
    const REAL Sq = SQ; // tao_inv; //

    REAL m[9]; // moments
    // project distribution function to moment space and relax
    device_collision_MRT_f2m_relax(m, f, rho, ux, uy, Se, Seps, Sq, tao_inv);

    // body force
    REAL uFx = Fx*ux;
    REAL uFy = Fy*uy;
    m[1] += (6.0 - Se*3.0) * (uFx + uFy);
    m[2] -= (6.0 - Seps*3.0) * (uFx + uFy);
    m[3] += 0.5*Fx;
    m[4] -= (1.0 - Sq*0.5) * Fx;
    m[5] += 0.5*Fy;
    m[6] -= (1.0 - Sq*0.5) * Fy;
    m[7] += (2.0 - tao_inv) * (uFx - uFy);
    m[8] += (1.0 - tao_inv*0.5) * (ux*Fy + uy*Fx);

    // project back to distribution function
    device_collision_MRT_m2f(f, m);
}

// a multiple relaxation time collision function from book by Zhaoli GUO
// MRT collisionn operator without bodyforce
/*
@book{
guo2013LBM,
title={Lattice Boltzmann method and its applications in engineering},
author={Guo, Zhaoli and Shu, Chang},
volume={3},
year={2013},
publisher={World Scientific}
}
*/
__device__ __forceinline__ void device_collision_MRT
(
const REAL& rho,
const REAL& ux,
const REAL& uy,
REAL* f,
const REAL& tao_inv
)
{
    // MRT relaxation factor in moment space
    // using tao_inv will bring MRT back to BGK model
    const REAL Se = SE; // tao_inv
    const REAL Seps = SEPS; // tao_inv
    const REAL Sq = SQ; // tao_inv

    REAL m[9]; // moments
    // project distribution function to moment space and relax
    device_collision_MRT_f2m_relax(m, f, rho, ux, uy, Se, Seps, Sq, tao_inv);

    // project back to distribution function
    device_collision_MRT_m2f(f, m);
}

// device_streaming:
// there are some magic macros in this function, for your good please do not modify the parameter list
// helper macros for streaming process
#define INDEX(I,J) ((I) + (J)*m)
#ifdef NS_DEBUG
#define STREAMING_TO(ii, jj, iq) do{_f1[(ii) + (jj)*m + iq*mn] = f[iq]; \
printf("f(%i,%i,%i)  streaming  to f(%i,%i,%i)\n", i,j,iq,ii,jj,iq);} while(0)
#define BOUNCE_BACK_AT(iq, iq_inv) do{_f1[idx + (iq_inv)*mn] = f[iq]; \
printf("f(%i,%i,%i) bounce-back to f(%i,%i,%i)\n", i,j,iq,i,j,iq_inv);} while(0)
#else
#define STREAMING_TO(ii, jj, iq) _f1[(ii) + (jj)*m + (iq)*mn] = f[iq]
#define BOUNCE_BACK_AT(iq, iq_inv) _f1[idx + (iq_inv)*mn] = f[iq]
#endif

                                              // these are extral terms from Ladd slip boundary, need to rewrite them in a more proper way 20171115
#define BOUNCE_BACK_AT_1 BOUNCE_BACK_AT(1, 3) - (config.IMPOSING_VELOCITY ? (rho*_ux[INDEX((i+1)%m,j)]*(REAL)(2.0/3.0)) : (REAL)0)
#define BOUNCE_BACK_AT_2 BOUNCE_BACK_AT(2, 4) - (config.IMPOSING_VELOCITY ? (rho*_uy[INDEX(i,(j+1)%n)]*(REAL)(2.0/3.0)) : (REAL)0)
#define BOUNCE_BACK_AT_3 BOUNCE_BACK_AT(3, 1) - (config.IMPOSING_VELOCITY ? (-rho*_ux[INDEX((i-1+m)%m,j)]*(REAL)(2.0/3.0)) : (REAL)0)
#define BOUNCE_BACK_AT_4 BOUNCE_BACK_AT(4, 2) - (config.IMPOSING_VELOCITY ? (-rho*_uy[INDEX(i,(j-1+n)%n)]*(REAL)(2.0/3.0)) : (REAL)0)
#define BOUNCE_BACK_AT_5 BOUNCE_BACK_AT(5, 7) - (config.IMPOSING_VELOCITY ? (rho*(_ux[INDEX((i+1)%m,(j+1)%n)] + _uy[INDEX((i+1)%m,(j+1)%n)])*(REAL)(1.0/6.0)) : (REAL)0)
#define BOUNCE_BACK_AT_6 BOUNCE_BACK_AT(6, 8) - (config.IMPOSING_VELOCITY ? (rho*(-_ux[INDEX((i-1+m)%m,(j+1)%n)] + _uy[INDEX((i-1+m)%m,(j+1)%n)])*(REAL)(1.0/6.0)) : (REAL)0)
#define BOUNCE_BACK_AT_7 BOUNCE_BACK_AT(7, 5) - (config.IMPOSING_VELOCITY ? (rho*(-_ux[INDEX((i-1+m)%m,(j-1+n)%n)] - _uy[INDEX((i-1+m)%m,(j-1+n)%n)])*(REAL)(1.0/6.0)) : (REAL)0)
#define BOUNCE_BACK_AT_8 BOUNCE_BACK_AT(8, 6) - (config.IMPOSING_VELOCITY ? (rho*(_ux[INDEX((i+1)%m,(j-1+n)%n)] - _uy[INDEX((i+1)%m,(j-1+n)%n)])*(REAL)(1.0/6.0)) : (REAL)0)
#define BOUNCE_BACK(i) BOUNCE_BACK_AT_##i
// streaming f(iq) to (ii,jj, iq) if lattice(ii,jj) is not an obstacle, otherwise do bounce-back
#define STREAMING_OR_BOUNCE_BACK(ii, jj, iq) \
if (_eta_idx[INDEX(ii, jj)] >= 0) STREAMING_TO(ii, jj, iq); else BOUNCE_BACK(iq)

__device__ __forceinline__ void device_streaming
(
REAL* __restrict__ _f1,
const REAL* __restrict__ f,
const REAL& rho,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const int* __restrict__ _eta_idx,
const int idx,
const int i,
const int j,
const int m,
const int n,
const int mn,
NS::CONFIG& config
){
    // cx[0] = 0, cy[0] = 0
    _f1[idx] = f[0];

    // cx[1] = 1, cy[1] = 0
    if (i < m - 1){
        STREAMING_OR_BOUNCE_BACK(i + 1, j, 1);
    }
    else{
        if (config.X1_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(1);
        else{
            if (config.X1_BOUNDARY_TYPE == NS::PERIODIC){
                STREAMING_OR_BOUNCE_BACK(0, j, 1);
            }
        }
    }

    // cx[2] = 0, cy[2] = 1
    if (j < n - 1){
        STREAMING_OR_BOUNCE_BACK(i, j + 1, 2);
    }
    else{
        if (config.Y1_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(2);
        else{
            if (config.Y1_BOUNDARY_TYPE == NS::PERIODIC){
                STREAMING_OR_BOUNCE_BACK(i, 0, 2);
            }
        }
    }

    // cx[3] = -1, cy[3] = 0
    if (i > 0){
        STREAMING_OR_BOUNCE_BACK(i - 1, j, 3);
    }
    else{
        if (config.X0_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(3);
        else{
            if (config.X0_BOUNDARY_TYPE == NS::PERIODIC){
                STREAMING_OR_BOUNCE_BACK(m - 1, j, 3);
            }
        }
    }

    // cx[4] = 0, cy[4] = -1
    if (j > 0){
        STREAMING_OR_BOUNCE_BACK(i, j - 1, 4);
    }
    else{
        if (config.Y0_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(4);
        else{
            if (config.Y0_BOUNDARY_TYPE == NS::PERIODIC){
                STREAMING_OR_BOUNCE_BACK(i, n - 1, 4);
            }
        }
    }

    // cx[5] = 1, cy[5] = 1
    if (j < n - 1){
        if (i < m - 1){
            // internal domain
            STREAMING_OR_BOUNCE_BACK(i + 1, j + 1, 5);
        }
        else{
            // X1 boundary
            if (config.X1_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(5);
            else{
                if (config.X1_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(0, j + 1, 5);
                }
            }
        }
    }
    else{
        if (i < m - 1){
            // Y1 boundary
            if (config.Y1_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(5);
            else{
                if (config.Y1_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(i + 1, 0, 5);
                }
            }
        }
        else{
            // X1 && Y1 corner
            if (config.X1_BOUNDARY_TYPE == NS::BOUNCEBACK || config.Y1_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(5);
            else{
                if (config.X1_BOUNDARY_TYPE == NS::PERIODIC && config.Y1_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(0, 0, 5);
                }
            }
        }
    }

    // cx[6] = -1, cy[6] = 1
    if (j < n - 1){
        if (i > 0){
            // internal domain
            STREAMING_OR_BOUNCE_BACK(i - 1, j + 1, 6);
        }
        else{
            // X0 boundary
            if (config.X0_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(6);
            else{
                if (config.X0_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(m - 1, j + 1, 6);
                }
            }
        }
    }
    else{
        if (i > 0){
            // Y1 boundary
            if (config.Y1_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(6);
            else{
                if (config.Y1_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(i - 1, 0, 6);
                }
            }
        }
        else{
            // X0 && Y1 corner
            if (config.X0_BOUNDARY_TYPE == NS::BOUNCEBACK || config.Y1_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(6);
            else{
                if (config.X0_BOUNDARY_TYPE == NS::PERIODIC && config.Y1_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(m - 1, 0, 6);
                }
            }
        }
    }

    // cx[7] = -1, cy[7] = -1
    if (j > 0){
        if (i > 0){
            // internal domain
            STREAMING_OR_BOUNCE_BACK(i - 1, j - 1, 7);
        }
        else{
            // X0 boundary
            if (config.X0_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(7);
            else{
                if (config.X0_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(m - 1, j - 1, 7);
                }
            }
        }
    }
    else{
        if (i > 0){
            // Y0 boundary
            if (config.Y0_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(7);
            else{
                if (config.Y0_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(i - 1, n - 1, 7);
                }
            }
        }
        else{
            // X0 && Y0 corner
            if (config.X0_BOUNDARY_TYPE == NS::BOUNCEBACK || config.Y0_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(7);
            else{
                if (config.X0_BOUNDARY_TYPE == NS::PERIODIC && config.Y0_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(m - 1, n - 1, 7);
                }
            }
        }
    }

    // cx[8] = 1, cy[8] = -1
    if (j > 0){
        if (i < m - 1){
            // internal domain
            STREAMING_OR_BOUNCE_BACK(i + 1, j - 1, 8);
        }
        else{
            // X1 boundary
            if (config.X1_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(8);
            else{
                if (config.X1_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(0, j - 1, 8);
                }
            }
        }
    }
    else{
        if (i < m - 1){
            // Y0 boundary
            if (config.Y0_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(8);
            else{
                if (config.Y0_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(i + 1, n - 1, 8);
                }
            }
        }
        else{
            // X1 && Y0 corner
            if (config.X1_BOUNDARY_TYPE == NS::BOUNCEBACK || config.Y0_BOUNDARY_TYPE == NS::BOUNCEBACK) BOUNCE_BACK(8);
            else{
                if (config.X1_BOUNDARY_TYPE == NS::PERIODIC && config.Y0_BOUNDARY_TYPE == NS::PERIODIC){
                    STREAMING_OR_BOUNCE_BACK(0 , n - 1, 8);
                }
            }
        }
    }
}

__device__ __forceinline__ void device_load_LB_distribution_function(
    REAL* f,
    const REAL* __restrict__ _f,
    const int idx,
    const int mn)
{
#pragma unroll
    for (int iq = 0; iq < Q; ++iq){
        f[iq] = _f[idx + iq * mn];
    }
}

#include "PeskinDeltaFunction.h"
// calculate force from immersed boundary objects
// a cellList and a list of lagrangian entry points are required
__device__ __forceinline__ void device_collect_force_from_immersed_boundary_cell_list
(
REAL& fx,
REAL& fy,
const REAL* __restrict__ * __ibfx,
const REAL* __restrict__ * __ibfy,
const REAL* __restrict__ * __ibx,
const REAL* __restrict__ * __iby,
const int* __restrict__ _cellListId,
const int* __restrict__ _cellListCount,
const int& cellListR, // bucket length
const int& MAX_POINTS, // maximum number of points in a immersed boundary curve
const int& i,
const int& j,
const int& m,
const int& n,
const bool& is_x_periodic,
const bool& is_y_periodic
)
{

    // locate (i,j)'s bucket in cellList
    int ic = i / cellListR;
    int jc = j / cellListR;
    int mc = (m + cellListR - 1) / cellListR;
    int nc = (n + cellListR - 1) / cellListR;

    // collect force (or other vectorial quantity) from all immersed boundary points
    // in this and neighbor buckets in cellList
    for (int iter_jc = -1; iter_jc != 1; ++iter_jc)
    {
        int pos_jc = iter_jc + jc;
        if (is_y_periodic) pos_jc = POSITIVE_MOD(pos_jc, nc);
        if (pos_jc >= 0 & pos_jc < nc)
        {
            for (int iter_ic = -1; iter_ic != 1; ++iter_ic)
            {
                int pos_ic = iter_ic + ic;
                if (is_x_periodic) pos_ic = POSITIVE_MOD(pos_ic, mc);
                if (pos_ic >= 0 & pos_ic < mc)
                {
                    int count = _cellListCount[pos_ic + pos_jc*mc];
                    for (int iter_id = 0; iter_id < count; ++iter_id)
                    {
                        // read immersed point id from _cellListId
                        int id = _cellListId[pos_ic + pos_jc*mc + iter_id*mc*nc];
                        int curve_id = id / MAX_POINTS;
                        id = id % MAX_POINTS;

                        // calculate distance between point x and i
                        REAL x = __ibx[curve_id][id];
                        REAL dx = x;
                        if (is_x_periodic) dx = dx - floor(dx / m)*m;
                        dx = abs(dx - i);
                        dx = min(dx, m - dx); // min is defined in cuda's header __MATH_FUNCTIONS_HPP__

                        // calculate distance between point y and j
                        REAL y = __iby[curve_id][id];
                        REAL dy = y;
                        if (is_y_periodic) dy = dy - floor(dy / n)*n;
                        dy = abs(dy - j);
                        dy = min(dy, n - dy);

                        // add contribution from ibfx and ibfy to fx and fy
                        if (dx < (REAL)2.0 & dy < (REAL)2.0)
                        {
                            REAL lag_fx = __ibfx[curve_id][id];
                            REAL lag_fy = __ibfy[curve_id][id];
                            REAL delta_dx = DELTA1D(dx);
                            REAL delta_dy = DELTA1D(dy);
                            REAL delta_r = delta_dx*delta_dy;
                            fx += (delta_r * lag_fx);
                            fy += (delta_r * lag_fy);
                        }
                    }
                }
            }
        }
    } // end collecting force on grid(i,j)
}

template<bool MRT>
// LBM kernel with bodyforce
__global__ void kernel_LBM
(
NS::CONFIG config,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _rho,
REAL* __restrict__ _f1,
const REAL* __restrict__ _f,
const REAL* __restrict__ _Fx,
const REAL* __restrict__ _Fy,
const int m,
const int n,
const int* _eta_idx, // label
const REAL* __restrict__ tao_inv_list
)
{
    // additional grid is to avoid bank-conflict
    // eta_idx[y][x] < 0 means this lattice is an obstacle

    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;
        REAL tao_inv = tao_inv_list[eta_idx_this];

        REAL f[Q];
        REAL rho, ux, uy;
        REAL Fx, Fy;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        // load macro variables
        rho = _rho[idx];
        ux = _ux[idx];
        uy = _uy[idx];

        // load body force
        Fx = _Fx[idx];
        Fy = _Fy[idx];

        // collision process
        if (MRT)
            device_collision_MRT(rho, ux, uy, f, Fx, Fy, tao_inv);
        else
            device_collision(rho, ux, uy, f, Fx, Fy, tao_inv);

        // advection process
        device_streaming(_f1, f, rho, _ux, _uy, _eta_idx, idx, i, j, m, n, mn, config);

    }
}

template<bool MRT>
// LBM kernel without bodyforce
__global__ void kernel_LBM 
(
NS::CONFIG config,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _rho,
REAL* __restrict__ _f1,
const REAL* __restrict__ _f,
const int m,
const int n,
const int* _eta_idx, // label
const REAL* __restrict__ tao_inv_list
)
{

    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;
        REAL tao_inv = tao_inv_list[eta_idx_this];

        REAL f[Q];
        REAL rho, ux, uy;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        // load macro variables
        rho = _rho[idx];
        ux = _ux[idx];
        uy = _uy[idx];

        if (MRT)
            device_collision_MRT(rho, ux, uy, f, tao_inv);
        else
            device_collision(rho, ux, uy, f, tao_inv);

        // advection process
        device_streaming(_f1, f, rho, _ux, _uy, _eta_idx, idx, i, j, m, n, mn, config);

    }
}

template<bool MRT>
// immersed boundary LBM with body force
__global__ void kernel_IBLBM
(
NS::CONFIG config,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _rho,
REAL* __restrict__ _f1,
const REAL* __restrict__ _f,
const REAL* __restrict__ _Fx,
const REAL* __restrict__ _Fy,
const REAL* __restrict__ _ibFx,
const REAL* __restrict__ _ibFy,
const int m,
const int n,
const int* _eta_idx, // label
const REAL* __restrict__ tao_inv_list
)
{
    // additional grid is to avoid bank-conflict
    // eta_idx[y][x] < 0 means this lattice is an obstacle

    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;
        REAL tao_inv = tao_inv_list[eta_idx_this];

        REAL f[Q];
        REAL rho, ux, uy;
        REAL Fx, Fy;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);
        
        // load macro variables
        rho = _rho[idx];
        ux = _ux[idx];
        uy = _uy[idx];

        // load body force and immersed boundary force
        Fx = _Fx[idx] + _ibFx[idx];
        Fy = _Fy[idx] + _ibFy[idx];

        // collision process
        if (MRT)
            device_collision_MRT(rho, ux, uy, f, Fx, Fy, tao_inv);
        else
            device_collision(rho, ux, uy, f, Fx, Fy, tao_inv);

        // advection process
        device_streaming(_f1, f, rho, _ux, _uy, _eta_idx, idx, i, j, m, n, mn, config);

    }
}

template<bool MRT>
// immersed boundary LBM without body force
__global__ void kernel_IBLBM
(
NS::CONFIG config,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _rho,
REAL* __restrict__ _f1,
const REAL* __restrict__ _f,
const REAL* __restrict__ _ibFx,
const REAL* __restrict__ _ibFy,
const int m,
const int n,
const int* _eta_idx, // label
const REAL* __restrict__ tao_inv_list
)
{

    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;
        REAL tao_inv = tao_inv_list[eta_idx_this];

        REAL f[Q];
        REAL rho, ux, uy;
        REAL Fx, Fy;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        // load macro variables
        rho = _rho[idx];
        ux = _ux[idx];
        uy = _uy[idx];

        // prepare body force for immersed boundary
        Fx = _ibFx[idx];
        Fy = _ibFy[idx];

        // collision process
        // collision process
        if (MRT)
            device_collision_MRT(rho, ux, uy, f, Fx, Fy, tao_inv);
        else
            device_collision(rho, ux, uy, f, Fx, Fy, tao_inv);

        // advection process
        device_streaming(_f1, f, rho, _ux, _uy, _eta_idx, idx, i, j, m, n, mn, config);

    }
}


template<bool MRT>
// immersed boundary LBM with body force
// immersed boundary bodyforce is calculated in this function
__global__ void kernel_IBLBM_with_ib_force_distribution
(
NS::CONFIG config,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _rho,
REAL* __restrict__ _f1,
const REAL* __restrict__ _f,
const REAL* __restrict__ _Fx,
const REAL* __restrict__ _Fy,

// ***********************************************************************
// these parameters are for calculation of immersed boundary body force
const int* __restrict__ _ib_mask,
REAL* __restrict__ _ibFx, // euler fx
REAL* __restrict__ _ibFy, // euler fy
const REAL* __restrict__ * __restrict__ const __ibfx,
const REAL* __restrict__ * __restrict__ const __ibfy,
const REAL* __restrict__ * __restrict__ const __ibx,
const REAL* __restrict__ * __restrict__ const __iby,
const int* __restrict__ _cellListId,
const int* __restrict__ _cellListCount,
const int cellListR, // bucket length
const int MAX_POINTS, // maximum number of points in a immersed boundary curve
// ***********************************************************************

const int m,
const int n,
const int* _eta_idx, // label
const REAL* __restrict__ tao_inv_list
)
{
    // additional grid is to avoid bank-conflict
    // eta_idx[y][x] < 0 means this lattice is an obstacle

    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;
        REAL tao_inv = tao_inv_list[eta_idx_this];

        REAL f[Q];
        REAL rho, ux, uy;
        REAL Fx, Fy;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        // load macro variables
        rho = _rho[idx];
        ux = _ux[idx];
        uy = _uy[idx];

        // calculate immersed boundary force
        REAL ibFx = 0.0;
        REAL ibFy = 0.0;
        int ic = i / cellListR;
        int jc = j / cellListR;
        int mc = (m + cellListR - 1) / cellListR;
        if (_ib_mask[ic + jc*mc] > 0) // distribute force if ib_mask > 0
        {
            _deviceImmersedBoundaryMethod_distributeForce(
                ibFx, ibFy,
                __ibfx, __ibfy,
                __ibx, __iby,
                _cellListId,
                _cellListCount,
                cellListR,
                MAX_POINTS,
                m, n,
                config.X0_BOUNDARY_TYPE == NS::PERIODIC,
                config.Y0_BOUNDARY_TYPE == NS::PERIODIC
                );
        }
        _ibFx[idx] = ibFx;
        _ibFy[idx] = ibFy;

        // load body force and sum up immersed boundary force
        Fx = _Fx[idx] + ibFx;
        Fy = _Fy[idx] + ibFy;

        // collision process
        if (MRT)
            device_collision_MRT(rho, ux, uy, f, Fx, Fy, tao_inv);
        else
            device_collision(rho, ux, uy, f, Fx, Fy, tao_inv);

        // advection process
        device_streaming(_f1, f, rho, _ux, _uy, _eta_idx, idx, i, j, m, n, mn, config);

    }
}

template<bool MRT>
// immersed boundary LBM without body force
// immersed boundary bodyforce is calculated in this function
__global__ void kernel_IBLBM_with_ib_force_distribution
(
NS::CONFIG config,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _rho,
REAL* __restrict__ _f1,
const REAL* __restrict__ _f,

// ***********************************************************************
// these parameters are for calculation of immersed boundary body force
const int* __restrict__ _ib_mask,
REAL* __restrict__ _ibFx, // euler fx
REAL* __restrict__ _ibFy, // euler fy
const REAL* __restrict__ * __restrict__ const __ibfx,
const REAL* __restrict__ * __restrict__ const __ibfy,
const REAL* __restrict__ * __restrict__ const __ibx,
const REAL* __restrict__ * __restrict__ const __iby,
const int* __restrict__ _cellListId,
const int* __restrict__ _cellListCount,
const int cellListR, // bucket length
const int MAX_POINTS, // maximum number of points in a immersed boundary curve
// ***********************************************************************

const int m,
const int n,
const int* _eta_idx, // label
const REAL* __restrict__ tao_inv_list
)
{

    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;
        REAL tao_inv = tao_inv_list[eta_idx_this];

        REAL f[Q];
        REAL rho, ux, uy;
        REAL Fx, Fy;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        // load macro variables
        rho = _rho[idx];
        ux = _ux[idx];
        uy = _uy[idx];

        // calculate immersed boundary force
        REAL ibFx = 0.0;
        REAL ibFy = 0.0;
        int ic = i / cellListR;
        int jc = j / cellListR;
        int mc = (m + cellListR - 1) / cellListR;
        if (_ib_mask[ic + jc*mc] > 0) // distribute force if ib_mask > 0
        {
            _deviceImmersedBoundaryMethod_distributeForce(
                ibFx, ibFy,
                __ibfx, __ibfy,
                __ibx, __iby,
                _cellListId,
                _cellListCount,
                cellListR,
                MAX_POINTS,
                m, n,
                config.X0_BOUNDARY_TYPE == NS::PERIODIC,
                config.Y0_BOUNDARY_TYPE == NS::PERIODIC
                );
        }
        _ibFx[idx] = ibFx;
        _ibFy[idx] = ibFy;
        Fx = ibFx;
        Fy = ibFy;

        // collision process
        if (MRT)
            device_collision_MRT(rho, ux, uy, f, Fx, Fy, tao_inv);
        else
            device_collision(rho, ux, uy, f, Fx, Fy, tao_inv);

        // advection process
        device_streaming(_f1, f, rho, _ux, _uy, _eta_idx, idx, i, j, m, n, mn, config);

    }
}

__global__
void kernel_calculate_macro_variables_LBM
(
REAL* __restrict__ _ux,
REAL* __restrict__ _uy,
REAL* __restrict__ _rho,
const REAL* __restrict__ _f,
const REAL* __restrict__ _Fx,
const REAL* __restrict__ _Fy,
const int m,
const int n,
const int* _eta_idx // label
)
{
    // eta_idx[y][x] < 0 means this lattice is an obstacle
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;

        REAL f[Q];
        REAL Fx, Fy;
        REAL rho;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        // load body force
        Fx = _Fx[idx];
        Fy = _Fy[idx];

        rho = COMPUTE_RHO(f);
        _ux[idx] = COMPUTE_UX_WITH_BODYFORCE(f, Fx, rho);
        _uy[idx] = COMPUTE_UY_WITH_BODYFORCE(f, Fy, rho);
        _rho[idx] = rho;

    }
}

__global__
void kernel_calculate_macro_variables_LBM
(
REAL* __restrict__ _ux,
REAL* __restrict__ _uy,
REAL* __restrict__ _rho,
const REAL* __restrict__ _f,
const int m,
const int n,
const int* _eta_idx // label
)
{
    // eta_idx[y][x] < 0 means this lattice is an obstacle
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;

        REAL f[Q];
        REAL rho;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        rho = COMPUTE_RHO(f);
        _ux[idx] = COMPUTE_UX(f, rho);
        _uy[idx] = COMPUTE_UY(f, rho);
        _rho[idx] = rho;

    }
}

__global__
void kernel_calculate_macro_variables_IBLBM
(
REAL* __restrict__ _ux,
REAL* __restrict__ _uy,
REAL* __restrict__ _rho,
const REAL* __restrict__ _f,
const REAL* __restrict__ _Fx,
const REAL* __restrict__ _Fy,
const REAL* __restrict__ _ibFx,
const REAL* __restrict__ _ibFy,
const int m,
const int n,
const int* _eta_idx // label
)
{
    // eta_idx[y][x] < 0 means this lattice is an obstacle
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;

        REAL f[Q];
        REAL Fx, Fy;
        REAL rho;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        // load body force and immersed boundary force
        Fx = _Fx[idx] + _ibFx[idx];
        Fy = _Fy[idx] + _ibFy[idx];

        rho = COMPUTE_RHO(f);
        _ux[idx] = COMPUTE_UX_WITH_BODYFORCE(f, Fx, rho);
        _uy[idx] = COMPUTE_UY_WITH_BODYFORCE(f, Fy, rho);
        _rho[idx] = rho;

    }
}

__global__
void kernel_calculate_macro_variables_IBLBM
(
REAL* __restrict__ _ux,
REAL* __restrict__ _uy,
REAL* __restrict__ _rho,
const REAL* __restrict__ _f,
const REAL* __restrict__ _ibFx,
const REAL* __restrict__ _ibFy,
const int m,
const int n,
const int* _eta_idx // label
)
{
    // eta_idx[y][x] < 0 means this lattice is an obstacle
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int idx = j*m + i;
    int mn = m*n;

    if (i < m && j < n){

        int eta_idx_this = _eta_idx[idx];
        if (eta_idx_this < 0) return;

        REAL f[Q];
        REAL Fx, Fy;
        REAL rho;

        // load LB distribution function
        device_load_LB_distribution_function(f, _f, idx, mn);

        // load immersed boundary force
        Fx = _ibFx[idx];
        Fy = _ibFy[idx];

        rho = COMPUTE_RHO(f);
        _ux[idx] = COMPUTE_UX_WITH_BODYFORCE(f, Fx, rho);
        _uy[idx] = COMPUTE_UY_WITH_BODYFORCE(f, Fy, rho);
        _rho[idx] = rho;

    }
}

void NSsolver::advanceFlowFieldWithoutImmersedObjects()
{

    // heyheyhey
    // why didn't I calculate macro variables before LBM kernel???
    // because then I would need body force information of previous time step
    // this will make the body force interface too complex for user to handle
    // for current manner of lbm kernel first, macro variable second
    // I can just hold a simple external pointer to body force provided by the user
    // efficiency drop due to this procedure is around 10%

    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    if (nullptr == body_force_x)
    {
        kernel_LBM<false> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(), f.data_ptr(),
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (without body force)
        kernel_calculate_macro_variables_LBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            m, n,
            label
            );
    }
    else
    {
        kernel_LBM<false> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(), uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(), f.data_ptr(),
            body_force_x, body_force_y,
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (with body force)
        kernel_calculate_macro_variables_LBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            body_force_x, body_force_y,
            m, n,
            label
            );

    }

    DA::swap_data_ptr(f, f1);
    ics++;

}

/*
void NSsolver::advanceFlowField()
{

    // calculate immersed boundary bodyforce

    // using only cell list
    ibm.distributeForce();

    // using cell list and neighbor list
    // ibm.distributeForce_neighborlist(ics);

    // heyheyhey
    // why didn't I calculate macro variables before LBM kernel???
    // because then I would need body force information of previous time step
    // this will make the body force interface too complex for user to handle
    // for current manner of "lbm kernel first, macro variable second"
    // I can just hold a simple external pointer to body force provided by the user
    // efficiency drop due to this procedure is around 10%

    // update the flow field and all macro variables (ux, uy and rho)
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    if (nullptr == body_force_x)
    {
        kernel_IBLBM<false> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(), f.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (without body force)
        kernel_calculate_macro_variables_IBLBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label
            );
    }
    else
    {
        kernel_IBLBM<false> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(), f.data_ptr(),
            body_force_x, body_force_y,
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (with body force)
        kernel_calculate_macro_variables_IBLBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            body_force_x, body_force_y,
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label
            );

    }

    DA::swap_data_ptr(f, f1);
    ics++;

}
*/

///*
void NSsolver::advanceFlowField()
{
    // in this version of NSsolver::advanceFlowField()
    // euler immersed boundary force is calculated inside iblbm kernel

    ibm.makeCellList(ics);

    // update the flow field and all macro variables (ux, uy and rho)
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    if (nullptr == body_force_x)
    {
        kernel_IBLBM_with_ib_force_distribution<false> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            f.data_ptr(),
            ibm.cellList.mask.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            ibm.fxEntry,
            ibm.fyEntry,
            ibm.xEntry,
            ibm.yEntry,
            ibm.cellList.id.data_ptr(),
            ibm.cellList.count.data_ptr(),
            ibm.cellList.idr,
            ImmersedObject::MAX_POINTS,
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (without body force)
        kernel_calculate_macro_variables_IBLBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label
            );
    }
    else
    {
        kernel_IBLBM_with_ib_force_distribution<false> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            f.data_ptr(),
            body_force_x,
            body_force_y,
            ibm.cellList.mask.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            ibm.fxEntry,
            ibm.fyEntry,
            ibm.xEntry,
            ibm.yEntry,
            ibm.cellList.id.data_ptr(),
            ibm.cellList.count.data_ptr(),
            ibm.cellList.idr,
            ImmersedObject::MAX_POINTS,
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (with body force)
        kernel_calculate_macro_variables_IBLBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            body_force_x, body_force_y,
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label
            );

    }

    DA::swap_data_ptr(f, f1);
    ics++;

}
//*/

void NSsolver::advanceFlowFieldWithoutImmersedObjects_MRT()
{

    // why didn't I calculate macro variables before LBM kernel???
    // because then I would need body force information of previous time step
    // this will make the body force interface too complex for user to handle
    // for current manner of lbm kernel first, macro variable second
    // I can just hold a simple external pointer to body force provided by the user
    // efficiency drop due to this procedure is around 10%

    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    if (nullptr == body_force_x)
    {
        kernel_LBM<true> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(), f.data_ptr(),
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (without body force)
        kernel_calculate_macro_variables_LBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            m, n,
            label
            );
    }
    else
    {
        kernel_LBM<true> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(), uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(), f.data_ptr(),
            body_force_x, body_force_y,
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (with body force)
        kernel_calculate_macro_variables_LBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            body_force_x, body_force_y,
            m, n,
            label
            );

    }

    DA::swap_data_ptr(f, f1);
    ics++;

}

/*
void NSsolver::advanceFlowField_MRT()
{

    // calculate immersed boundary bodyforce

    // using only cell list
    ibm.distributeForce();

    // using cell list and neighbor list
    // ibm.distributeForce_neighborlist(ics);

    // heyheyhey
    // why didn't I calculate macro variables before LBM kernel???
    // because then I would need body force information of previous time step
    // this will make the body force interface too complex for user to handle
    // for current manner of lbm kernel first, macro variable second
    // I can just hold a simple external pointer to body force provided by the user
    // efficiency drop due to this procedure is around 10%

    // update the flow field and all macro variables (ux, uy and rho)
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    if (nullptr == body_force_x)
    {
        kernel_IBLBM<true> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(), f.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (without body force)
        kernel_calculate_macro_variables_IBLBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label
            );
    }
    else
    {
        kernel_IBLBM<true> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(), f.data_ptr(),
            body_force_x, body_force_y,
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (with body force)
        kernel_calculate_macro_variables_IBLBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            body_force_x, body_force_y,
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label
            );

    }

    DA::swap_data_ptr(f, f1);
    ics++;

}
*/

///*
void NSsolver::advanceFlowField_MRT()
{
    // in this version of NSsolver::advanceFlowField()
    // euler immersed boundary force is calculated inside iblbm kernel

    ibm.makeCellList(ics);

    // update the flow field and all macro variables (ux, uy and rho)
    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    if (nullptr == body_force_x)
    {
        kernel_IBLBM_with_ib_force_distribution<true> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            f.data_ptr(),
            ibm.cellList.mask.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            ibm.fxEntry,
            ibm.fyEntry,
            ibm.xEntry,
            ibm.yEntry,
            ibm.cellList.id.data_ptr(),
            ibm.cellList.count.data_ptr(),
            ibm.cellList.idr,
            ImmersedObject::MAX_POINTS,
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (without body force)
        kernel_calculate_macro_variables_IBLBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label
            );
    }
    else
    {
        kernel_IBLBM_with_ib_force_distribution<true> CUDALAUNCH(grid, block, 0, stream)
            (
            config,
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            f.data_ptr(),
            body_force_x,
            body_force_y,
            ibm.cellList.mask.data_ptr(),
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            ibm.fxEntry,
            ibm.fyEntry,
            ibm.xEntry,
            ibm.yEntry,
            ibm.cellList.id.data_ptr(),
            ibm.cellList.count.data_ptr(),
            ibm.cellList.idr,
            ImmersedObject::MAX_POINTS,
            m, n,
            label,
            tao_inv_list.data_ptr()
            );

        // calculate macro variables (with body force)
        kernel_calculate_macro_variables_IBLBM CUDALAUNCH(grid, block, 0, stream)
            (
            ux.data_ptr(),
            uy.data_ptr(),
            rho.data_ptr(),
            f1.data_ptr(),
            body_force_x, body_force_y,
            ibm.eul_fx.data_ptr(),
            ibm.eul_fy.data_ptr(),
            m, n,
            label
            );

    }

    DA::swap_data_ptr(f, f1);
    ics++;

}