#ifndef _deviceImmersedBoundaryMethod_distributeForce_cuh
#define _deviceImmersedBoundaryMethod_distributeForce_cuh

#include "LBMheader.cuh"
#include "PeskinDeltaFunction.h"

// this is a function separated from ImmersedBoundaryMethod::_device_distributeForce
// it is called by bothe ImmersedBoundaryMethod::distributeForce() and NSsolver::kernel_IBLBM_with_ib_force_distribution

// The separation is not elegant but it helps to avoid Relocatable Device Code (-rdc=true) during linking, which is unfriendly to performance

#define _device_distributeForce_sort_before_calculation
// a device function version of distributeForce, it can be directly called from lbm solver
static __device__ void _deviceImmersedBoundaryMethod_distributeForce
(
REAL& fx,
REAL& fy,
const REAL* __restrict__ * __restrict__ const __ibfx,
const REAL* __restrict__ * __restrict__ const __ibfy,
const REAL* __restrict__ * __restrict__ const __ibx,
const REAL* __restrict__ * __restrict__ const __iby,
const int* __restrict__ _cellListId,
const int* __restrict__ _cellListCount,
const int cellListR, // bucket length
const int MAX_POINTS, // maximum number of points in a immersed boundary curve
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    if (i < m & j < n)
    {
        fx = (REAL)0.0;
        fy = (REAL)0.0;

        // locate (i,j)'s bucket in cellList
        int ic = i / cellListR;
        int jc = j / cellListR;
        int mc = (m + cellListR - 1) / cellListR;
        int nc = (n + cellListR - 1) / cellListR;

        int count_total = 0;
        int count_now = 0;

        int id_list[ImmersedBoundaryMethod::MAX_NUM_LAG_MARKER];

        // construct local neighbour list from cell list
        // iter_jc starts from (-1 - (is_y_periodic&j == 0))
        // since the actual size of last cell can be less than cellListR if (m%cellListR!=0).
        // the same goes for i

        for (int iter_jc = -1 - (is_y_periodic & jc == 0); iter_jc <= 1; ++iter_jc)
        {
            int pos_jc = iter_jc + jc; if (is_y_periodic) pos_jc = (pos_jc % nc + nc) % nc;
            if (pos_jc >= 0 & pos_jc < nc)
            {
                for (int iter_ic = -1 - (is_x_periodic & ic == 0); iter_ic <= 1; ++iter_ic)
                {
                    int pos_ic = iter_ic + ic; if (is_x_periodic) pos_ic = (pos_ic % mc + mc) % mc;
                    if (pos_ic >= 0 & pos_ic < mc)
                    {
                        int count = _cellListCount[pos_ic + pos_jc*mc];
                        count = count < CellList::MAX_POINT_NUMBER_IN_CELL ? count : CellList::MAX_POINT_NUMBER_IN_CELL;
                        if (count == 0) continue;
                        count_total += count;
                        for (int iter_id = 0; iter_id < count; ++iter_id)
                        {
                            int id = _cellListId[pos_ic + pos_jc*mc + iter_id*mc*nc];
                            if (count_now < ImmersedBoundaryMethod::MAX_NUM_LAG_MARKER)
                                id_list[count_now++] = id;
                        }
                    }
                }
            }
        } // end construction of neighbor list

        // calculate force on this grid if count_total > 0
        if (count_total > 0)
        {
            count_total = count_total < ImmersedBoundaryMethod::MAX_NUM_LAG_MARKER ?
            count_total : ImmersedBoundaryMethod::MAX_NUM_LAG_MARKER;
#ifdef _device_distributeForce_sort_before_calculation
            count_now = 0;
#endif
            for (int iter = 0; iter != count_total; ++iter)
            {
                // read immersed point id from _cellListId
                // now this id is global
                int point_id = id_list[iter];

                // if point_id < 0, then this point is a solid boundary
                // it does not involve in immersed object boundary
                if (point_id < 0) continue;

                // calculate point_id and curve_id
                int curve_id = point_id / MAX_POINTS;
                point_id = point_id % MAX_POINTS;

                // calculate distance between point x and i
                REAL dx = __ibx[curve_id][point_id];
                REAL dy = __iby[curve_id][point_id];

                if (is_x_periodic) dx = dx - floor(dx / m)*m;
                dx = abs(dx - i); // abs and min are defined in cuda's header __MATH_FUNCTIONS_HPP__
                dx = min(dx, m - dx);

                if (is_y_periodic) dy = dy - floor(dy / n)*n;
                dy = abs(dy - j);
                dy = min(dy, n - dy);

                // add contribution from ibfx and ibfy to fx and fy
                if (dx < (REAL)2.0 & dy < (REAL)2.0)
                {
#ifdef _device_distributeForce_sort_before_calculation
                    // write valid lagrangian markers into local storage
                    id_list[count_now] = id_list[iter];
                    ++count_now;
#else
                    REAL lag_fx = __ibfx[curve_id][point_id];
                    REAL lag_fy = __ibfy[curve_id][point_id];
                    REAL delta_dx = DELTA1D(dx);
                    REAL delta_dy = DELTA1D(dy);
                    REAL delta_r = delta_dx*delta_dy;

                    fx += (delta_r * lag_fx);
                    fy += (delta_r * lag_fy);
#endif
                }
            }
        }
#ifdef _device_distributeForce_sort_before_calculation
        if (count_now > 0)
        {
            for (int iter = 0; iter != count_now; ++iter)
            {
                // read immersed point id from _cellListId
                // now this id is global
                int point_id = id_list[iter];

                // if point_id < 0, then this point is a solid boundary
                // it does not involve in immersed object boundary
                if (point_id < 0) continue;

                // calculate point_id and curve_id
                int curve_id = point_id / MAX_POINTS;
                point_id = point_id % MAX_POINTS;

                // calculate distance between point x and i
                REAL dx = __ibx[curve_id][point_id];
                REAL dy = __iby[curve_id][point_id];

                if (is_x_periodic) dx = dx - floor(dx / m)*m;
                dx = abs(dx - i); // abs and min are defined in cuda's header __MATH_FUNCTIONS_HPP__
                dx = min(dx, m - dx);

                if (is_y_periodic) dy = dy - floor(dy / n)*n;
                dy = abs(dy - j);
                dy = min(dy, n - dy);
                REAL lag_fx = __ibfx[curve_id][point_id];
                REAL lag_fy = __ibfy[curve_id][point_id];
                REAL delta_dx = DELTA1D(dx);
                REAL delta_dy = DELTA1D(dy);
                REAL delta_r = delta_dx*delta_dy;

                fx += (delta_r * lag_fx);
                fy += (delta_r * lag_fy);
            }
        }
#endif
    }
}

#endif