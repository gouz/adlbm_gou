#include <sstream>
#include "LBMheader.cuh"
#include "RBC_ATP_Release_multi_vesicle.cuh"
// #include "Vesicle_Jamming.cuh"
// #include "TTTS2D.cuh"
// #include "Example.cuh"
// #include "solute_diffusion_in_blood_flow.cuh"
// #include "RBC_flow_local_rheology.cuh"
// #include "Tree2D.cuh"
// #include "RBC_viscosity_contrast_study.cuh"
// #include "Shear_stress_dependent_local_regulation_in_vessel_networks.cuh"
// #include "IBLBM.cuh" // vesicle dynamics

int main(int argc, char *argv[]){
    

    int device_number = DA::showCudaDeviceProperties();
    int device_id = 0;
    if (argc >= 2){
        std::istringstream ss(argv[1]);
        if (!(ss >> device_id)){
            std::cout << "Invalid device number " << argv[1] << std::endl;
            std::cout << "Select device 0 by default" << std::endl;
            device_id = 0;
        }
    }
    if (device_id >= device_number){
        std::cout << "Invalid device number " << device_id << std::endl;
        std::cout << "Select device 0 by default" << std::endl;
        device_id = 0;
    }
    DA::selectCudaDevice(device_id);
    
    // all cases are written here, excutions are managed by config files
	
    RBC_ATP_Release_multi_vesicle atp_release;
    atp_release.red_blood_cell_release_atp_in_straight_channel();
    /*atp_release.red_blood_cell_release_atp_under_poiseuille();
    atp_release.red_blood_cell_release_atp_under_bifurcation();
    vesicle_jamming();
    TTTS2D();
    Tree2D();

    high_reynolds_cavity_flow();
    solute_dispersion_in_flow_around_a_floating_rigid_body();
    galileo_invariance();
    vortex_in_a_box();
    steady_heat_conduction_inside_a_circle_with_constant_advection();
    boundary_export_example();
    round_vesicle_in_static_flow();
    three_leaved_interface_diffusion();
	ATP_dynamics_in_a_vessel();
	*/
	/*
	validation_advection_diffusion_in_irregular_domain(512);
	validation_advection_diffusion_in_irregular_domain(256);
	validation_advection_diffusion_in_irregular_domain(128);
	validation_advection_diffusion_in_irregular_domain(64);
	validation_advection_diffusion_in_irregular_domain(32);
	validation_advection_diffusion_in_irregular_domain(16);
	*/
	// validation_advection_diffusion_in_irregular_domain();

    int is_simulation = 1;
    if (argc > 2)
    {
        std::istringstream arg2(argv[2]);
        if (!(arg2 >> is_simulation))
        {
            is_simulation = 1;
        }
    }

    // solute_diffusion_in_blood_flow(is_simulation);
    // Shear_stress_dependent_local_regulation_in_vessel_networks(is_simulation);
    // RBC_flow_local_rheology();
	// RBC_viscosity_contrast_study();

	/*bool is_arg1_txt = false;
	if (argc >= 2) is_arg1_txt = _IBLBM_GetFileExtension(std::string(argv[1])) == std::string("txt");
	if (is_arg1_txt)
	{
		IBLBM(argv[1]);
	}*/
	
    return 0;
}
