#ifndef Shear_stress_dependent_local_regulation_in_vessel_networks_cuh
#define Shear_stress_dependent_local_regulation_in_vessel_networks_cuh

#include <iostream>
#include <fstream>
#include <cmath>
#include "LBMheader.cuh"

void Shear_stress_dependent_local_regulation_in_vessel_networks_set_boundary
(
REAL* __restrict__ _cin,
REAL* __restrict__ _cex,
short* __restrict__ _ctype_in,
short* __restrict__ _ctype_ex,
const int* __restrict__ _id,
const REAL* __restrict__ _shear_stress,
const REAL* __restrict__ _rho_in,
const REAL k_sigma,
const REAL sigma_c,
const REAL a_infty,
const REAL Ka,
const REAL s0,
const REAL tau_m,
const REAL Da,
const REAL dx,
const REAL dt,
const int maximum_kernel_launch_size,
const int* __restrict__ boundary_size
);


inline void Shear_stress_dependent_local_regulation_in_vessel_networks(const int zero_for_make_mask_only = 0)
{
    ConfigFile cfg("Shear_stress_dependent_local_regulation_in_vessel_networks.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", is_using_initial_flow_field, 0);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "switch", initial_flow_field, std::string("initial_flow_field_afdat"));
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation Shear_stress_dependent_local_regulation_in_vessel_networks not issued\n");
        return;
    }
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Da, 2.36e-10); // ATP diffusivity in plasma [m^2/s]
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", k_sigma, 7e-3); // atp production rate on RBC membrane, [nM/L] * m*s^-1
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", sigma_c, 0.05); // critical APT release shear stress on RBC membrane, Pa
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", a_infty, 100); // ATP concentration at infinitly far, [nM/L]
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", viscosity_physical, 1e-3); // viscosity of plasma [Pa*s]
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", u_capillary_physical, 4.5e-4); // maximum velocity in one of the finest capillary
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Ka, 1.68e-5); // ATP uptake rate (m*s^-1)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", s0, 1e-3); //maximum ATP production rate on EC surface, [nM/L] * m*s^-1
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", tau_m, 1); //characteristic wall shear stress (Pa)


    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", m, 2160);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n, 438);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tend, 100000);

    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", is_red_blood_cells_on, 1);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", viscosity, 0.166666666666666667);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", umax_straight_channel, 0.002);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", W_capillary_characteristic, 28.0); // characteristic width of simulation in simulation units
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", fx, 8.0*viscosity*umax_straight_channel / W_capillary_characteristic / W_capillary_characteristic);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_ves, 2000); // total export frames for vesicle shape
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_flow, 100); // total export frames for velocity field
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_boundary, 100); // total export frames for velocity field
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_lumen_ATP, 2000); // total export frames for atp field at walls
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_wall_ATP, 2000); // total export frames for atp field at walls
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_wall_SS, 2000); // total export frames for shear stress at walls
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_prestep, 1000000);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", R0, 12.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", R0_physical, 3.0e-6); // characteristic radius of RBC, 3e-6m (3.0 um)
    REAL dx = R0_physical / R0;
    printf("dx = %e m\n", dx);
    REAL dt = umax_straight_channel / u_capillary_physical * dx;
    printf("dt = %e s\n", dt);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", tau, 0.7); // reduced area
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kb_physical, 3e-19); // bending modulus of RBC membrane, 3e-19J
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", Ca, -1.0); // capillary number defined with wall shear rate at constriction
    if (Ca < 0)
    {
        printf("Ca is calculated by definition.\n");
        Ca = 
            viscosity_physical * 
            (4.0*u_capillary_physical / (W_capillary_characteristic * dx)) *
            R0_physical*R0_physical*R0_physical /
            kb_physical;
    }
    else
    {
        printf("Ca is fixed from config file. \n");
    }
    printf("Ca = viscosity * shearRate * R0^3 / kb = %f\n", Ca);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kl, 0.3); // vesicle strech modulus (set it adequately large tu ensure membrane inextensibility)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", l0, 0.6667);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", damp, 0.01); // damp factor in relaxation process
    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", is_using_initial_shape, 0); // number of vesicles
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_shape_filename, std::string("initial_vesicle_shape.txt"));
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_position_filename, std::string("initial_vesicle_position.txt"));


    DEFINE_FROM_CONFIGFILE(REAL, cfg, "solute", Pe, -1.0); // peclet number defined as u_max*R0/D
    if (Pe < 0)
    {
        printf("Peclet is calculated by definition.\n");
        Pe = u_capillary_physical * R0_physical / Da;
    }
    else
    {
        printf("Peclet is fixed from config file. \n");
    }
    printf("Pe = u_capillary_physical * R0_physical / Da = %f\n", Pe);
    
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "geometry", vessel_boundary_filename, std::string("Tree_boundary.txt"));


    DeviceArray<REAL> Fx(fx, m, n);
    DeviceArray<REAL> Fy(0.0, m, n);

    // make label
    DeviceArray<int> label_flow(0, m, n);
    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, -(REAL)0.5, -(REAL)0.5, (REAL)1.0, (REAL)1.0, m, n);

    // build a voxelizer to construct label and boundary curves
    LBMGPUvoxelizer* voxelizer = new LBMGPUvoxelizer(m, n, false, false, 100000, 100);
    DataFile vessel_boundary(vessel_boundary_filename.c_str());
    int n_bound = vessel_boundary.get_n() / 3;
    printf("boundary curve number = %i\n", n_bound);
    std::vector< DeviceArray<REAL> > x_bound(n_bound);
    std::vector< DeviceArray<REAL> > y_bound(n_bound);

    for (int i = 0; i != n_bound; ++i)
    {
        DeviceArray<REAL> x_bound_original, y_bound_original;
        x_bound_original = DeviceArray<REAL>
            (
            &vessel_boundary.getData()[i * 3 + 1][0],
            vessel_boundary.getData()[i * 3 + 1].size()
            );
        y_bound_original = DeviceArray<REAL>
            (
            &vessel_boundary.getData()[i * 3 + 2][0],
            vessel_boundary.getData()[i * 3 + 2].size()
            );

        bool is_circle_bound;
        is_circle_bound = std::abs(vessel_boundary.getData()[i * 3][0]) > 1e-5;

        LBMGPUvoxelizer::refine_curve
            (
            x_bound[i],
            y_bound[i],
            x_bound_original.data_ptr(),
            y_bound_original.data_ptr(),
            x_bound_original.size(),
            1.0,
            is_circle_bound
            );

        voxelizer->setBoundaryCurve
            (
            x_bound[i].data_ptr(),
            y_bound[i].data_ptr(),
            x_bound[i].size(),
            is_circle_bound,
            0
            );
    }
    voxelizer->setBoundaryCurveFinished(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, false);
    label_flow = voxelizer->build_filled_tag_map();
    delete voxelizer;
    label_flow = DeviceArray<int>(label_flow == -1) - 1;
    for (int i = 0; i != x_bound.size(); ++i)
    {
        char xname[100];
        char yname[100];
        char xfile[100];
        char yfile[100];
        sprintf(xname, "xb%04d", i);
        sprintf(yname, "yb%04d", i);
        sprintf(xfile, "xb%04d.afdat", i);
        sprintf(yfile, "yb%04d.afdat", i);
        x_bound[i].af_save(xname, xfile);
        y_bound[i].af_save(yname, yfile);
    }
    da_saveAF(label_flow);
    Fx = Fx * DeviceArray<REAL>(label_flow + 1);

    // make a mask map file for random initial positioning of vesicles
    if (zero_for_make_mask_only == 0)
    {
        DeviceArray<int> mask = label_flow;
        mask.transpose();
        mask.save2D("mask.txt");
        printf("mask.txt built\n");
        printf("task done!\n");
        return;
    }


    NSsolver flow(viscosity, m, n, NS::PERIODIC, NS::BOUNCEBACK, Fx.data_ptr(), Fy.data_ptr(), label_flow.data_ptr());

    flow.ibm.xBoundary.af_save("xbound", "xbound.afdat");
    flow.ibm.yBoundary.af_save("ybound", "ybound.afdat");

    REAL umax = 0.0;
    if (is_using_initial_flow_field)
    {
        flow.initializeByFile(initial_flow_field.c_str());
    }
    else
    {
        flow.initializeByDensity();
        //pre-step for initializing flow field
        for (int i = 0; i != flow_prestep; ++i)
        {
            if ((i + 1) % std::max(1, (flow_prestep / 100)) == 0 || i == 0 || i == flow_prestep - 1){
                std::cout << "prestep, iteration = " << i + 1;
                std::vector<REAL> hux(flow.ux.size());
                std::vector<REAL> huy(flow.ux.size());
                flow.ux.host(&hux[0]);
                flow.uy.host(&huy[0]);
                REAL umaxnow = 0.0;
                for (int j = (int)((double)hux.size()*0.49); j != (int)((double)hux.size()*0.51); ++j){
                    REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
                    umaxnow = std::max(umaxnow, uj);
                }
                std::cout << ", u_at_probe = " << umaxnow << std::endl;
                if (std::abs(umaxnow - umax) < 1e-9 && umaxnow >= umax_straight_channel*0.1 && i != 0){
                    printf("u_at_probe reached its maximum, end prestep\n");
                    break;
                }
                umax = umaxnow;
            }
            flow.advanceFlowFieldWithoutImmersedObjects();
        }
        flow.af_save_flow_field(initial_flow_field.c_str());
    }

    // calculate umax
    std::vector<REAL> hux(flow.ux.size());
    std::vector<REAL> huy(flow.ux.size());
    flow.ux.host(&hux[0]);
    flow.uy.host(&huy[0]);
    REAL umaxnow = 0.0;
    for (int j = 0; j != hux.size(); ++j){
        REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
        umaxnow = std::max(umaxnow, uj);
    }
    umax = umaxnow;
    printf("umax = %f\n", umax);
    REAL Re = umax * R0 / viscosity;
    printf("Reynolds number in simulation = umax * R0 / viscosity = %f\n", Re);

    // vesicle bending modulous
    REAL kb = viscosity * R0*R0*R0 / l0 * 16.0 * umax / W_capillary_characteristic / Ca;
    printf("vesicle bending modulous kb (simulation) = %f\n", kb);

    Vesicle ves_template(0.0, 0.0, 0.0, R0, tau, kb, kl, 200.0, l0);
    if (is_using_initial_shape == 0)
    {
        ves_template.relax(10000000, damp);
        ves_template.exportShape(initial_shape_filename.c_str());
    }

    // make vesicles
    DataFile vesicle_initial_position(initial_position_filename.c_str());
    int nves = vesicle_initial_position.get_n(); // total vesicle number
    if (!is_red_blood_cells_on) nves = 0;
    printf("total vesicle number = %i\n", nves);
    std::vector<Vesicle*> vec_vesicles(nves);
    for (int i = 0; i != nves; ++i)
    {
        vec_vesicles[i] = new Vesicle
            (
            vesicle_initial_position.getData()[i][0], // x position
            vesicle_initial_position.getData()[i][1], // y position
            vesicle_initial_position.getData()[i][2], // inclination
            R0, tau, kb, kl, 200.0, l0, initial_shape_filename.c_str(), true);
        flow.immerseAnObject(vec_vesicles[i]);
    }

    flow.immerseObjectFinished();
    printf("%i vesicles' initialization complete.\n", nves);

    REAL D_simu = umax_straight_channel*R0 / Pe;
    printf("D_simulation = umax_straight_channel*R0 / Pe = %f\n", D_simu);

    // make solute (ATP) solver
    const int max_boundary_size_for_solute = 100000;
    ADsolver solute(
        D_simu,
        m, n, max_boundary_size_for_solute,
        flow.ux.data_ptr(),
        flow.uy.data_ptr(),
        nullptr, nullptr,
        AD::PERIODIC,
        AD::PERIODIC);
    
    solute.setInitialConcentration(DeviceArray<REAL>(a_infty * DeviceArray<REAL>(label_flow + 1)));

    DeviceArray<REAL> boundary_shear_stress(0.0, max_boundary_size_for_solute);
    DeviceArray<REAL> boundary_x(0.0, max_boundary_size_for_solute);
    DeviceArray<REAL> boundary_y(0.0, max_boundary_size_for_solute);

    // vesicle position check:
    // malloc on cpu (pinned memory)
    REAL* p_box_vesicles;
    cudaMallocHost((void**)&p_box_vesicles, 4 * sizeof(REAL) * nves, cudaHostAllocDefault);
    std::vector<bool> vesicle_boundary_validity(nves, true);

    std::vector<REAL> h_x_a_infty = { std::ceil(m*0.98)+0.5, std::ceil(m*0.98)+0.5 };
    std::vector<int> h_label_flow(m*n);
    label_flow.host(&h_label_flow[0]);
    REAL y_a_infty_min = n;
    REAL y_a_infty_max = 0;
    for (int i = 0; i != n; ++i)
    {
        int label = h_label_flow[int(std::ceil(m*0.98)) + i];
        if (h_label_flow[int(std::ceil(m*0.98)) + i*m] == 0)
        {
            if (y_a_infty_min > i) y_a_infty_min = i;
            if (y_a_infty_max < i) y_a_infty_max = i;
        }
    }
    y_a_infty_min += 1.5;
    y_a_infty_max -= 1.5;
    std::vector<REAL> h_y_a_infty = { y_a_infty_min, y_a_infty_max };
    DeviceArray<REAL> x_a_infty(&h_x_a_infty[0], 2);
    DeviceArray<REAL> y_a_infty(&h_y_a_infty[0], 2);

    SimpleTimer timer;

    timer.tic();

    for (int i = 0; i != Tend; ++i)
    {

        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->calculateForce();

        if (is_red_blood_cells_on)
        {
            // update vesicle_boundary_validity list
            if (i % 100 == 0)
            {
                for (int ives = 0; ives != nves; ++ives)
                {
                    vec_vesicles[ives]->findBox();
                    cudaMemcpy(p_box_vesicles + ives*4, vec_vesicles[ives]->box.data_ptr(), 4 * sizeof(REAL), cudaMemcpyDeviceToHost);
                    REAL pos_vesicle_front = p_box_vesicles[ives * 4 + 1];
                    pos_vesicle_front = pos_vesicle_front / (REAL)m - std::floor(pos_vesicle_front / (REAL)m);
                    vesicle_boundary_validity[ives] = pos_vesicle_front > 0.95 ? false : true;
                }
            }

            // register vesicle boundary
            for (int ives = 0; ives != nves; ++ives)
                if (vesicle_boundary_validity[ives])
                {
                    solute.registerCurve(vec_vesicles[ives]->get_x(), vec_vesicles[ives]->get_y(), true, 1);
                }
        }

        // register vessel boundary for solute solver (ATP)
        for (int ibound = 0; ibound != x_bound.size(); ++ibound)
        {
            bool is_circular = std::abs(vessel_boundary.getData()[ibound * 3][0]) > 1e-5;
            solute.registerCurve(x_bound[ibound], y_bound[ibound], is_circular, 0);
        }

        // register invisible wall for the effect of influx with constant concentration
        solute.registerCurve(x_a_infty, y_a_infty, false, 2);

        solute.registerCurveFinished();

        // set boundary type
        // solute.boundary.Ctype_ex = 0;
        // solute.boundary.Ctype_in = 0;

        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->sync_operation();


        flow.ibm.add_short_range_repulsive_force();

        if (LBM::getStepStampFlag(i, frames_ves, Tend))
        {
            printf("i = %i, Nstep = %i, t = %es, Tend = %es\n", i, Tend, i*dt, Tend*dt);
        }

        if (LBM::getStepStampFlag(i, frames_ves, Tend))
        {
            printf("i = %i, vesicle shapes snapshot\n", i);
            flow.ibm.export_all_objects_shapes_ascii("sx.afdat", "sy.afdat", i != 0);
            if (i == 0) solute.boundary.exportToFile_ArrayFire("bound0.afdat");

        }

        if (LBM::getStepStampFlag(i, frames_lumen_ATP, Tend))
        {
            printf("i = %i, lumen ATP snapshot\n", i);
            solute.rho.af_save("rho", "ATP.afdat", i != 0);
            //if (i == 0) solute.boundary.exportToFile_ArrayFire("bound0.afdat");

        }

        if (LBM::getStepStampFlag(i, frames_boundary, Tend))
        {
            char bfilename[100];
            sprintf(bfilename, "bound%09d.afdat", i);
            printf("i = %i, boundary snapshot %s\n", i, bfilename);
            solute.boundary.exportToFile_ArrayFire(bfilename);

        }

        if (LBM::getStepStampFlag(i, frames_wall_ATP, Tend))
        {

        }

        if (LBM::getStepStampFlag(i, frames_flow, Tend))
        {
            printf("flow field snapshot\n");
            flow.ux.af_save("ux", "ux.afdat", i != 0);
            flow.uy.af_save("uy", "uy.afdat", i != 0);

            // debug export
            /*
            flow.ibm.eul_fx.af_save("eul_fx", "eul_fx.afdat", i != 0);
            flow.ibm.eul_fy.af_save("eul_fy", "eul_fy.afdat", i != 0);
            auto fabs = sqrt(flow.ibm.eul_fx * flow.ibm.eul_fx + flow.ibm.eul_fy * flow.ibm.eul_fy);
            fabs.af_save("eul_f", "eul_f.afdat", i != 0);
            flow.ibm.cellList.count.af_save("cellListCount", "cellList_count.afdat", i != 0);
            flow.ibm.neighborList.id.af_save("neighbor_id", "neighbor_id.afdat", i != 0);
            flow.ibm.neighborList.count.af_save("neighbor_count", "neighbor_count.afdat", i != 0);
            flow.ibm.neighborList.block_idx.af_save("block_idx", "block_idx.afdat", i != 0, *flow.ibm.neighborList.ptr_block_list_len);
            */
        }

        // set boundary conditions
        solute.calculate_shear_stress_on_boundary(boundary_shear_stress.data_ptr(), viscosity_physical, dt, -1.5);
        solute.computeConcentrationOnBoundary();
        Shear_stress_dependent_local_regulation_in_vessel_networks_set_boundary
            (
            solute.boundary.C_in.data_ptr(),
            solute.boundary.C_ex.data_ptr(),
            solute.boundary.Ctype_in.data_ptr(),
            solute.boundary.Ctype_ex.data_ptr(),
            solute.boundary.id.data_ptr(),
            boundary_shear_stress.data_ptr(),
            solute.boundary.rho_in.data_ptr(),
            k_sigma, sigma_c, a_infty, Ka, s0, tau_m, Da, dx, dt, max_boundary_size_for_solute,
            solute.getBoundarySizeOnDevice().data_ptr()
            );


        if (LBM::getStepStampFlag(i, frames_wall_SS, Tend))
        {
            solute.boundary.getBoundaryCoordinates(boundary_x.data_ptr(), boundary_y.data_ptr());
            boundary_shear_stress.af_save("boundary_shear_stress", "boundary_shear_stress.afdat", i != 0, solute.boundary.size());
            boundary_x.af_save("boundary_x", "boundary_x.afdat", i != 0, solute.boundary.size());
            boundary_y.af_save("boundary_y", "boundary_y.afdat", i != 0, solute.boundary.size());
            solute.boundary.id.af_save("boundary_id", "boundary_id.afdat", i != 0, solute.boundary.size());
        }

        solute.advanceTimeStep();
        flow.advanceFlowField();

        flow.advanceImmersedObjects();

    }
    cudaDeviceSynchronize();
    timer.toc();

}

#endif