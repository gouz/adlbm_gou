#include "Shear_stress_dependent_local_regulation_in_vessel_networks.cuh"

__global__
void kernel_Shear_stress_dependent_local_regulation_in_vessel_networks_set_boundary
(
REAL* __restrict__ _cin,
REAL* __restrict__ _cex,
short* __restrict__ _ctype_in,
short* __restrict__ _ctype_ex,
const int* __restrict__ _id,
const REAL* __restrict__ _shear_stress,
const REAL* __restrict__ _rho_in,
const REAL k_sigma,
const REAL sigma_c,
const REAL a_infty,
const REAL Ka,
const REAL s0,
const REAL tau_m,
const REAL Da,
const REAL dx,
const REAL dt,
const int* __restrict__ _boundary_size
)
{
    int len = *_boundary_size;
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        int id = _id[idx]; // boundary pair id, 1 for vesicle, 0 for vessel wall _cex stores bounday condition values on lumen side
        REAL shear_stress = _shear_stress[idx];

        REAL cin = 0.0;
        if (id == 0) // vessel wall boundary condition
        {
            _ctype_ex[idx] = 1;
            _cex[idx] = 0.0;
            REAL exp_factor = (1.0 - exp(-shear_stress / tau_m));

            cin = -Ka * _rho_in[idx] + s0 * exp_factor*exp_factor*exp_factor;
            cin = cin * dx / Da;
        }

        if (id == 1) // vesicle boundary condition
        {
            _ctype_ex[idx] = 1;
            _cex[idx] = a_infty;
            if (shear_stress > sigma_c)
            {
                cin = k_sigma;
                cin = cin * dx / Da;
            }
        }

        if (id == 2) // invisible wall for the effect of influx with constant concentration
        {
            _ctype_ex[idx] = 1;
            _ctype_in[idx] = 1;
            _cex[idx] = a_infty;
            cin = a_infty;
        }

        _cin[idx] = cin;
    }
}

void Shear_stress_dependent_local_regulation_in_vessel_networks_set_boundary
(
REAL* __restrict__ _cin,
REAL* __restrict__ _cex,
short* __restrict__ _ctype_in,
short* __restrict__ _ctype_ex,
const int* __restrict__ _id,
const REAL* __restrict__ _shear_stress,
const REAL* __restrict__ _rho_in,
const REAL k_sigma,
const REAL sigma_c,
const REAL a_infty,
const REAL Ka,
const REAL s0,
const REAL tau_m,
const REAL Da,
const REAL dx,
const REAL dt,
const int maximum_kernel_launch_size,
const int* __restrict__ _boundary_size
)
{
    const int block_dim = 256;
    int grid_size = (maximum_kernel_launch_size + block_dim - 1) / block_dim;
    kernel_Shear_stress_dependent_local_regulation_in_vessel_networks_set_boundary <<<grid_size, block_dim >>>
        (
        _cin,
        _cex,
        _ctype_in,
        _ctype_ex,
        _id,
        _shear_stress,
        _rho_in,
        k_sigma,
        sigma_c,
        a_infty,
        Ka,
        s0,
        tau_m,
        Da,
        dx,
        dt,
        _boundary_size
        );
}