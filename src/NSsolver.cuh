#ifndef NSsolver_cuh
#define NSsolver_cuh
// ************************************************************************************************
// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

// DeviceArray header
#include "DeviceArray.cuh"

// LBM header
#include "LBMconstant.hpp"

#include "ImmersedBoundaryMethod.cuh"
// if this tag is valid, use default stream for CUDA kernels in NSsolver
#define NS_DEFAULT_STREAM
// ************************************************************************************************

namespace NS
{
    typedef enum
    {
        CUSTOMIZE,  // CUSTOMIZE: solver will not handle boundary condition, add customize kernel
        BOUNCEBACK, // BOUNCEBACK: simple half-way bounce back boundary condition
        PERIODIC,   // PERIODIC: periodic condition
    } BOUNDARY_TYPE;

    struct CONFIG
    {
        BOUNDARY_TYPE X0_BOUNDARY_TYPE;
        BOUNDARY_TYPE X1_BOUNDARY_TYPE;
        BOUNDARY_TYPE Y0_BOUNDARY_TYPE;
        BOUNDARY_TYPE Y1_BOUNDARY_TYPE;
        bool IMPOSING_VELOCITY;
        inline CONFIG() :
            X0_BOUNDARY_TYPE(CUSTOMIZE),
            X1_BOUNDARY_TYPE(CUSTOMIZE),
            Y0_BOUNDARY_TYPE(CUSTOMIZE),
            Y1_BOUNDARY_TYPE(CUSTOMIZE),
            IMPOSING_VELOCITY(false)
        {}
    };
}

class NSsolver
{
public:
    static const int Q = 9;
    REAL eta;
    REAL tao;
    DeviceArray<REAL> eta_list;
    DeviceArray<REAL> tao_inv_list;
    const int m;
    const int n;
    NS::CONFIG config;
    DeviceArray<REAL> rho;
    DeviceArray<REAL> ux;
    DeviceArray<REAL> uy;
    DeviceArray<REAL> f;
    const REAL* body_force_x;
    const REAL* body_force_y;

    // indexing of different eta in different lattices
    // when label[j][i] < 0, the lattice will be treated as an obstacle
    const int* label;
    
    // cuda stream
    cudaStream_t stream;

private:
    int ics; // time step
    DeviceArray<REAL> f1;
    DeviceArray<int> empty_label;
public:
    // immersed boundary method manager
    ImmersedBoundaryMethod ibm;
public:
    inline NSsolver
        (
        const REAL _eta,
        const int _m,
        const int _n,
        const NS::BOUNDARY_TYPE _x_boundary_type = NS::CUSTOMIZE,
        const NS::BOUNDARY_TYPE _y_boundary_type = NS::CUSTOMIZE,
        const REAL* _body_force_x = nullptr,
        const REAL* _body_force_y = nullptr,
        const int* _label = nullptr,
        const REAL MAXIMUM_VELOCITY = 0.1 // the maximum velocity that controls the renew step of neighbor list in immersed boundary method
        ) :
        eta(_eta),
        tao((REAL)1.0 / (_eta * (REAL)3.0 + (REAL)0.5)),
        eta_list(_eta),
        tao_inv_list((REAL)1.0 / (_eta * (REAL)3.0 + (REAL)0.5)),
        m(_m),
        n(_n),
        rho((REAL)0.0, m, n),
        ux((REAL)0.0, m, n),
        uy((REAL)0.0, m, n),
        f((REAL)0.0, m, n, 9),
        ics(0),
        f1((REAL)0.0, m, n, 9),
        empty_label(0, m, n),
        body_force_x(_body_force_x),
        body_force_y(_body_force_y),
        label(_label),
        ibm(_m, _n,
        _x_boundary_type == NS::PERIODIC,
        _y_boundary_type == NS::PERIODIC,
        MAXIMUM_VELOCITY)
    {
        config.X0_BOUNDARY_TYPE = _x_boundary_type;
        config.X1_BOUNDARY_TYPE = _x_boundary_type;
        config.Y0_BOUNDARY_TYPE = _y_boundary_type;
        config.Y1_BOUNDARY_TYPE = _y_boundary_type;

        cudaStreamCreate(&stream);

        if (nullptr == label){
            label = empty_label.data_ptr();
        }

        ibm.make_boundary_from_label(label);

    }
    inline NSsolver
        (
        const REAL* _eta,
        const int n_eta,
        const int _m,
        const int _n,
        const NS::BOUNDARY_TYPE _x_boundary_type = NS::CUSTOMIZE,
        const NS::BOUNDARY_TYPE _y_boundary_type = NS::CUSTOMIZE,
        const REAL* _body_force_x = nullptr,
        const REAL* _body_force_y = nullptr,
        const int* _label = nullptr,
        const REAL MAXIMUM_VELOCITY = 0.1 // the maximum velocity that controls the renew step of neighbor list in immersed boundary method
        ) :
        eta(_eta[0]),
        tao((REAL)1.0 / (_eta[0] * (REAL)3.0 + (REAL)0.5)),
        m(_m),
        n(_n),
        rho((REAL)0.0, m, n),
        ux((REAL)0.0, m, n),
        uy((REAL)0.0, m, n),
        ics(0),
        f((REAL)0.0, m, n, 9),
        f1((REAL)0.0, m, n, 9),
        empty_label(0, m, n),
        body_force_x(_body_force_x),
        body_force_y(_body_force_y),
        label(_label),
        ibm
        (
        _m,
        _n,
        _x_boundary_type == NS::PERIODIC,
        _y_boundary_type == NS::PERIODIC,
        MAXIMUM_VELOCITY
        )
    {
        config.X0_BOUNDARY_TYPE = _x_boundary_type;
        config.X1_BOUNDARY_TYPE = _x_boundary_type;
        config.Y0_BOUNDARY_TYPE = _y_boundary_type;
        config.Y1_BOUNDARY_TYPE = _y_boundary_type;

        cudaStreamCreate(&stream);

        eta_list = DeviceArray<REAL>(_eta, n_eta);
        tao_inv_list = (REAL)1.0 / (eta_list * (REAL)3.0 + (REAL)0.5);

        if (nullptr == label){
            label = empty_label.data_ptr();
        }

        ibm.make_boundary_from_label(label);
    }

    ~NSsolver(){
        cudaStreamDestroy(stream);
    }

    void initializeByDensity(const REAL _rho = (REAL)1.0);
    void initializeByFile(const char* filename);

    // all objects must be immersed into IBM before simulation
    // after this, call immerseObjectFinished()
    inline void immerseAnObject(ImmersedObject* pImObj){ ibm.immerseAnObject(pImObj); }

    // all objects must be immersed into IBM before simulation
    // this method will copy entry points of coordinates, force of all points onto GPU
    inline void immerseObjectFinished(){ ibm.immerseObjectFinished(); }

    void advanceFlowFieldWithoutImmersedObjects();
    void advanceFlowField();
    void advanceFlowFieldWithoutImmersedObjects_MRT();
    void advanceFlowField_MRT();

    inline int getTimeStep(){ return ics; }

    // after flow velocity field get updated. call this function to advect all immersed objects
    // if is_update_position == false, method will only update the velocity of each objects
    // if is_update_position == true, method will update the velocity and position of each objects
    inline void advanceImmersedObjects(const bool is_update_position = true)
    {
        ibm.advect_immersed_objects(ux.data_ptr(), uy.data_ptr(), is_update_position);
    }

    inline void af_save_flow_field(const char* fname = "flow_field.afdat")
    {
        rho.af_save("rho", fname, false);
        ux.af_save("ux", fname, true);
        uy.af_save("uy", fname, true);
        f.af_save("f", fname, true);
        f1.af_save("f1", fname, true);
    }

    // this function will try to develop a steady flow out from current status (e.g. a fixed pressure drop)
    // of the solver the development will take no consideration of the presence of immersed objects
    // user must ensure the convergence of the real solution
    // Nstep: the maximum total step
    // tolerance: accepted maximum velocity difference between two consecutive snapshots
    inline void develop_steady_flow(const int Nstep, const REAL tolerance = 1e-9)
    {
        if (Nstep < 1) return;
        int interval = std::max(1, (Nstep / 100));
        printf("Develop steady flow:\n");
        printf("Maximum step = %i, tolerance = %e\n", Nstep, tolerance);
        DeviceArray<REAL> ux_prev(0.0, m, n);
        DeviceArray<REAL> uy_prev(0.0, m, n);
        for (int i = 0; i != Nstep; ++i)
        {
            if (((i + 1) % interval == 0 || i == 0 || i == Nstep - 1) && i >= (interval - 1))
            {
                std::cout << "step = " << i + 1;
                auto udiff_max = std::sqrt(DA::max((ux - ux_prev) * (ux - ux_prev) + (uy - uy_prev) * (uy - uy_prev)));
                std::cout << ", udiff_max = " << udiff_max;
                std::cout << std::endl;
                if (udiff_max < tolerance)
                {
                    printf("udiff_max < tolerance reached, the flow is considered as well developed.\n");
                    break;
                }
                ux_prev = ux;
                uy_prev = uy;
            }
            advanceFlowFieldWithoutImmersedObjects();
        }
    }
};

#endif