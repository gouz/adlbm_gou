#include "Vesicle.cuh"

#define BSIZE 256
#define ASYNC
#ifdef ASYNC
#define CUDALAUNCH(n) <<<((n) + BSIZE - 1) / BSIZE, BSIZE, 0, cudaStream>>>
#else
#define CUDALAUNCH(n) <<<((n) + BSIZE - 1) / BSIZE, BSIZE>>>
#endif

#include "_deviceReduceOperation.cuh"

/*
algorithm from
% H.J. Sommer III, Ph.D., Professor of Mechanical Engineering, 337 Leonhard Bldg
% The Pennsylvania State University, University Park, PA  16802
% (814)863-8997  FAX (814)865-9693  hjs1-at-psu.edu  www.mne.psu.edu/sommer/
translated from matlab code, this kernel returns "a"
efficiency is not optimized

xm = mean(x); // not used in c++ code
ym = mean(y); // not used in c++ code
x = x - xm; // not used in c++ code
y = y - ym; // not used in c++ code

xp = x( [2:end 1] );
yp = y( [2:end 1] );
a = x.*yp - xp.*y;

A = sum( a ) /2;
*/
template <int blockSize, bool nIsPow2>
__forceinline__ __device__ void device_calculateArea
(
REAL* __restrict__ sdata,
REAL* __restrict__ area_piecewise,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int n
)
{
    int i = threadIdx.x;
    REAL area_this_piese = (REAL)0.0;
    while (i < n)
    {
        int iR = (i + 1) % n;
        //area_this_piese += ((_x[i] - xmean) * (_y[iR] - ymean) - (_x[iR] - xmean) * (_y[i] - ymean));
        area_this_piese += (_x[i] * _y[iR] - _x[iR] * _y[i]);
        i += blockSize;
    }
    area_piecewise[threadIdx.x] = area_this_piese;
    __syncthreads();
    _device_reduce_add<REAL, blockSize, nIsPow2>(area_piecewise, sdata, blockSize, (REAL)0.5);
}

// *******************************************************************
// force calculations are not optimized at all
// copied from zaiyi SHEN's IB-LBM2d Fortran code
// see http://www.theses.fr/2016GREAY037
// this is a fortran style subroutine
// *******************************************************************
// calculate force in a intergrated manner, area and centroid are calculated in this function
template<int blockSize, bool nIsPow2>
// to use this fast method, make sure that thread block size is larger than the vesicle point number
__global__ void kernel_calculateForce_fast
(
REAL* __restrict__ _fx,
REAL* __restrict__ _fy,
REAL* __restrict__ _El,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const REAL s0,
const REAL kb,
const REAL kl,
const REAL ks,
const REAL l0,
const int n
)
{
    __shared__ REAL sdata[blockSize];
    __shared__ REAL area_piecewise[blockSize];
    device_calculateArea<BSIZE, nIsPow2>(sdata, area_piecewise, _x, _y, n);

    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < n){
        REAL s = sdata[0];
        __syncthreads();

        // debug
        // printf("idx = %i * %i + %i = %i, n=%i, area = %f\n", blockDim.x, blockIdx.x, threadIdx.x, idx, n, s);

        REAL kkb1, kkb2, kkb3,
            dcos1dx, dcos2dx, dcos3dx, dcos1dy, dcos2dy, dcos3dy,
            l1, l2,
            costheta1, costheta2, costheta3,
            flx, fbx, fsx, fly, fby, fsy;

        int idxLL = (idx + n - 2) % n;
        int idxL = (idx + n - 1) % n;
        int idxR = (idx + 1) % n;
        int idxRR = (idx + 2) % n;

        // read from global mem
        // position schematic
        /*
        !!!!!!!!!!!x3
        !!!!!!!!!!!!\   } l3, dx3
        !!!!!!!!!!!!!x1
        !!!!!!!!!!!!!!!\   } l1, dx1
        !!!!!!!!!!!!!!!!x
        !!!!!!!!!!!!!!!/   } l2, dx2
        !!!!!!!!!!!!!!x2
        !!!!!!!!!!!!!/   } l4, dx4
        !!!!!!!!!!!!x4
        */

        REAL dx1 = _x[idxR] - _x[idx];      // x1 - x
        REAL dx2 = _x[idx] - _x[idxL];      // x - x2
        REAL dx3 = _x[idxRR] - _x[idxR];    // x3 - x1
        REAL dx4 = _x[idxL] - _x[idxLL];    // x2 - x4
        REAL dy1 = _y[idxR] - _y[idx];      // y1 - y
        REAL dy2 = _y[idx] - _y[idxL];      // y - y2
        REAL dy3 = _y[idxRR] - _y[idxR];    // y3 - y1
        REAL dy4 = _y[idxL] - _y[idxLL];    // y2 - y4

        // length
        l1 = sqrt(dx1*dx1 + dy1*dy1);
        l2 = sqrt(dx2*dx2 + dy2*dy2);
        REAL l1_inv = 1.0 / l1;
        REAL l2_inv = 1.0 / l2;
        REAL l3_inv = 1.0 / sqrt(dx3*dx3 + dy3*dy3);
        REAL l4_inv = 1.0 / sqrt(dx4*dx4 + dy4*dy4);

        REAL temp;

        // trigonometrics
        costheta1 = (dx1*dx2 + dy1*dy2) * l1_inv * l2_inv;
        costheta2 = (dx3*dx1 + dy3*dy1) * l1_inv * l3_inv;
        costheta3 = (dx2*dx4 + dy2*dy4) * l2_inv * l4_inv;

        // trigonometrics derivatives
        temp = 1.0 + costheta1;
        kkb1 = kb / (temp*temp);
        temp = 1.0 + costheta2;
        kkb2 = kb / (temp*temp);
        temp = 1.0 + costheta3;
        kkb3 = kb / (temp*temp);
        REAL l13_inv = l1_inv*l1_inv*l1_inv;
        REAL l23_inv = l2_inv*l2_inv*l2_inv;
        dcos1dx =
            (dx1 - dx2) * l1_inv * l2_inv + (dx1*dx2 + dy1*dy2)*dx1 * l13_inv * l2_inv -
            (dx1*dx2 + dy1*dy2)*dx2 * l1_inv * l23_inv;
        dcos2dx = -dx3 * l3_inv * l1_inv + (dx3*dx1 + dy3*dy1)*dx1 * l3_inv * l13_inv;
        dcos3dx = dx4 * l2_inv * l4_inv - (dx2*dx4 + dy2*dy4)*dx2 * l23_inv * l4_inv;
        dcos1dy =
            (dy1 - dy2) * l1_inv * l2_inv + (dx1*dx2 + dy1*dy2)*dy1 * l13_inv * l2_inv -
            (dx1*dx2 + dy1*dy2)*dy2 * l1_inv * l23_inv;
        dcos2dy = -dy3 * l3_inv * l1_inv + (dx3*dx1 + dy3*dy1)*dy1 * l3_inv * l13_inv;
        dcos3dy = dy4 * l2_inv * l4_inv - (dx2*dx4 + dy2*dy4)*dy2 * l23_inv * l4_inv;

        // streching force
        temp = kl / (l0*l0);
        flx = temp * ((l1 - l0) * dx1 * l1_inv - (l2 - l0) * dx2* l2_inv);
        fly = temp * ((l1 - l0) * dy1 * l1_inv - (l2 - l0) * dy2* l2_inv);

        // bending force
        fbx = kkb1*dcos1dx + kkb2*dcos2dx + kkb3*dcos3dx;
        fby = kkb1*dcos1dy + kkb2*dcos2dy + kkb3*dcos3dy;

        // swelling force
        temp = 0.5 * ks * (s - s0) / (s0*s0);
        fsx = -temp * (dy1 + dy2);
        fsy = temp * (dx1 + dx2);

        _fx[idx] = flx + fbx + fsx;
        _fy[idx] = fly + fby + fsy;

        // streching energy
        _El[idx] = 0.25 * kl * ((l1 - l0) / l0 * (l1 - l0) / l0 + (l2 - l0) / l0 * (l2 - l0) / l0);
    }
}

// works for vesicle with less than BSIZE*4 (defaltly BSIZE=256) points
void Vesicle::calculateForce(){
    if ((x.size()&(x.size() - 1)) == 0)
        kernel_calculateForce_fast<BSIZE, true> CUDALAUNCH(x.size())
        (
        fx.data_ptr(), fy.data_ptr(),
        El.data_ptr(),
        x.data_ptr(), y.data_ptr(),
        s0, kb, kl, ks, l0,
        x.size());
    else
        kernel_calculateForce_fast<BSIZE, false> CUDALAUNCH(x.size())
        (
        fx.data_ptr(), fy.data_ptr(),
        El.data_ptr(),
        x.data_ptr(), y.data_ptr(),
        s0, kb, kl, ks, l0,
        x.size());
}

void Vesicle::calculateForce(const REAL _kb){
    if ((x.size()&(x.size() - 1)) == 0)
        kernel_calculateForce_fast<BSIZE, true> CUDALAUNCH(x.size())
        (
        fx.data_ptr(), fy.data_ptr(),
        El.data_ptr(),
        x.data_ptr(), y.data_ptr(),
        s0, _kb, kl, ks, l0,
        x.size());
    else
        kernel_calculateForce_fast<BSIZE, false> CUDALAUNCH(x.size())
        (
        fx.data_ptr(), fy.data_ptr(),
        El.data_ptr(),
        x.data_ptr(), y.data_ptr(),
        s0, _kb, kl, ks, l0,
        x.size());
}

void Vesicle::calculateForce(const REAL _kb, const REAL _kl, const REAL _ks){
    if ((x.size()&(x.size() - 1)) == 0)
        kernel_calculateForce_fast<BSIZE, true> CUDALAUNCH(x.size())
        (
        fx.data_ptr(), fy.data_ptr(),
        El.data_ptr(),
        x.data_ptr(), y.data_ptr(),
        s0, _kb, _kl, _ks, l0,
        x.size());
    else
        kernel_calculateForce_fast<BSIZE, false> CUDALAUNCH(x.size())
        (
        fx.data_ptr(), fy.data_ptr(),
        El.data_ptr(),
        x.data_ptr(), y.data_ptr(),
        s0, _kb, _kl, _ks, l0,
        x.size());
}

// calculate deflation of vesicle
template<int blockSize, bool nIsPow2>
// to use this fast method, make sure that thread block size is larger than the vesicle point number
__global__ void kernel_deflation_fast
(
REAL* __restrict__ _x,
REAL* __restrict__ _y,
REAL* _s_error, // relative error
const REAL s0,
const REAL l0, 
const int n
)
{
    __shared__ REAL sdata[blockSize];
    __shared__ REAL area_piecewise[blockSize];
    device_calculateArea<BSIZE, nIsPow2>(sdata, area_piecewise, _x, _y, n);

    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < n){
        REAL s = sdata[0];

        int idxL = (idx + n - 1) % n;
        int idxR = (idx + 1) % n;
        REAL nx = _y[idxR] - _y[idxL];
        REAL ny = _x[idxL] - _x[idxR];
        REAL nlength = sqrt(nx*nx + ny*ny);
        nx = nx/nlength;
        ny = ny/nlength;
        __syncthreads();

        REAL dr = (s - s0)/l0/n;
        // DO NOT read and write in one expression
        REAL x = _x[idx] - dr*nx;
        REAL y = _y[idx] - dr*ny;
        _x[idx] = x;
        _y[idx] = y;   

        if (idx == 0)
        {
            _s_error[idx] = abs(s - s0)/s0;
            // printf("relative error = %.17e\n", _s_error[0]);
            // printf("vesicle area = %.4e\n", s);
        }     
    }
}

// deflation of vesicle to ensure volume conservation
// added by Zhe Gou on 20211205
void Vesicle::deflation(){
    DeviceArray<REAL> s_error = DeviceArray<REAL>(1.0, x.size());
    std::vector<REAL> hs_error(x.size());
    int nmax = 10;
    // int imax = 10;
    // printf("deflatioin start\n");

    // deflating iteration
    for (int i = 0; i != nmax; ++i){
        if ((x.size()&(x.size() - 1)) == 0)
            kernel_deflation_fast<BSIZE, true> CUDALAUNCH(x.size())
            (
            x.data_ptr(), y.data_ptr(),
            s_error.data_ptr(), s0, l0, 
            x.size());
        else
            kernel_deflation_fast<BSIZE, false> CUDALAUNCH(x.size())
            (
            x.data_ptr(), y.data_ptr(),
            s_error.data_ptr(), s0, l0, 
            x.size());

        s_error.host(&hs_error[0]);
        if (hs_error[0] < 1e-9)
        {
            // imax = i;
            break;
        }
    }
    // printf("iteration = %i, relative error = %.9e\n", imax, hs_error[0]);
    // printf("deflation finished\n");
}

__global__
void kernel_relax
(
REAL* __restrict__ _x,
REAL* __restrict__ _y,
const REAL* __restrict__ fx,
const REAL* __restrict__ fy,
const REAL damp,
const int n
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < n){
        // DO NOT read and write in one expression.
        REAL x = _x[idx] + fx[idx] * damp;
        REAL y = _y[idx] + fy[idx] * damp;
        _x[idx] = x;
        _y[idx] = y;

    }

}

// relax vesicle shape to force-free
void Vesicle::relax(const int nmax, const REAL damp){
    char file[100] = "shape_relax.afdat";
    printf("vesicle relaxation:\n");
    REAL hfscalarmax_pre = 0.0;
    int interval_save = std::max(1, nmax / 100);
    for (int i = 0; i != nmax; ++i){
        calculateForce();
        if (i % interval_save == 0 || i == nmax - 1){
            DeviceArray<REAL> fscalar = sqrt(fx*fx + fy*fy);
            std::vector<REAL> hfscalar(fscalar.size());
            fscalar.host(&hfscalar[0]);
            REAL hfscalarmax = 0.0;
            for (int ii = 0; ii != fscalar.size(); ++ii){
                hfscalarmax = hfscalarmax > hfscalar[ii] ? hfscalarmax : hfscalar[ii];
            }
            printf("i = %i, f_max = %.17e\n", i, hfscalarmax);

            exportShapeAF(file, i != 0);
            fx.af_save("fx", "fx_relax.afdat", i != 0);
            fy.af_save("fy", "fy_relax.afdat", i != 0);
            if ((hfscalarmax < 1e-8 || std::abs(hfscalarmax - hfscalarmax_pre)<1e-9) && i >0){
                break;
            }
            hfscalarmax_pre = hfscalarmax;
        }
        kernel_relax CUDALAUNCH(x.size())
            (x.data_ptr(), y.data_ptr(), fx.data_ptr(), fy.data_ptr(), damp, x.size());
    }
#ifdef ASYNC
    cudaStreamSynchronize(cudaStream);
#endif
    printf("vesicle relaxation end\n");
}

__global__
void kernel_calculate_ATP_release_rate
(
REAL* __restrict__ rate,
REAL* __restrict__ El, // stretching energy
const int len
)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < len){
        rate[idx] = El[idx] > 4e-7 ? 1e-6 : 0.0;
    }
}

void Vesicle::calculate_ATP_release_rate(){
    kernel_calculate_ATP_release_rate CUDALAUNCH(ATP_release_rate.size())
        (
        ATP_release_rate.data_ptr(),
        El.data_ptr(),
        El.size()
        );
}