#ifndef RBC_viscosity_contrast_study_cuh
#define RBC_viscosity_contrast_study_cuh

#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>

// LBM header
#include "LBMheader.cuh"

inline void RBC_viscosity_contrast_study()
{

    ConfigFile cfg("RBC_viscosity_contrast_study.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation RBC_viscosity_contrast_study not issued\n");
        return;
    }

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", is_MRT, 0);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", R0, 1.0); // vesicle characteristic radius, try not to change this value
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", kappa_b, 1.0); // vesicle bending modulous, try not to change this value
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", viscosity_out, 1.0); // try not to change this value
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", inclination, 0.0); // try not to change this value, initial tilting angle
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Cn, 0.25); // Confinement defined as 2*R0 / Width
    REAL W = 2.0*R0 / Cn;
    printf("W = %f R0\n", W);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", L, W*10.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", tau, 0.6);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", x0ves, 0.0); // normalized initial y position of single vesicle range(-0.5, 0.5)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", y0ves, 0.0); // normalized initial x position of single vesicle range(-0.5, 0.5)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Ca, 80.0); // capillary number who makes ux(y) = Ca/W * y * (W-y)
    REAL umax = 0.25*Ca*W;
    printf("ux(y) = Ca/W * y * (W-y)\n");
    printf("umax = 0.25*Ca*W = %.17f\n", umax);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", lambda, 1.0); // viscosity contrast lambda = viscosity_in / viscosity_out

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", T, 1000.0); // total simulation time (physical unit)

    REAL viscosity_in = lambda * viscosity_out;
    printf("viscosity_in = lambda * viscosity_out = %f\n", viscosity_in);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", viscosity_out_simu, 1.0 / 6.0);
    REAL viscosity_in_simu = lambda * viscosity_out_simu;
    printf("viscosity_in_simu = %f\n", viscosity_in_simu);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", R0_simu, 25.0); // vesicle characteristic radius in simulation
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", l0_simu, 0.6667); // spring length repressenting the vesicle in simulation
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", ks_simu, 400.0); // area energy term (as lagrangian multiplyer)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", kl_simu, 1.2); // streching energy term (as lagrangian multiplyer)
    REAL dx = R0 / R0_simu;
    printf("mesh sized dx = %f\n", dx);

    int m = (int)std::ceil(L*R0 / dx);
    int n = (int)std::ceil(W*R0 / dx);
    printf("m = L * R0 / dx = %i\n", m);
    printf("n = W * R0 / dx = %i\n", n);

    printf("A pseudo Reynolds number (Re_simu = umax * R0 / viscosity_out is used in this simulatoin. \n");
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", Re_simu, 0.1); // pseudo-reynolds number in simulation

    // REAL umax_simu = Re_simu * viscosity_out_simu / R0_simu;
    REAL umax_simu = Re_simu * viscosity_out_simu / R0_simu * 0.25 * W / R0;

    printf("umax_simu = gamma^dot_wall_simu * R0_simu^2 / viscosity_out_simu = %f\n", umax_simu);
    REAL dt = umax_simu / umax * dx;
    printf("dt = umax_simu / umax * dx = %.17f\n", dt);

    int Tend = (int)std::ceil(T / dt);
    printf("Total simulation step Tend = %i\n", Tend);

    // calculate bending modulous kb in simulaiton
    REAL capillary_number = Ca * viscosity_out * R0*R0*R0 / kappa_b;
    REAL kappa_b_simu = 4 * umax_simu / n * viscosity_out_simu * R0_simu*R0_simu*R0_simu / capillary_number;
    REAL kb = 4 * kappa_b_simu / l0_simu;
    printf("kb = %.17f\n", kb);

    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames, 1000); // viscosity contrast lambda = viscosity_in / viscosity_out
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", frames_flowfield, frames / 10.0);

    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_prestep, 2000000);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", use_initial_f_file, 0);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "simulation", initial_f_filename, std::string("initial_flow_field_afdat"));
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n_relax, 1000000);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", damp_relax, 0.5);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "simulation", initial_shape_filename, std::string("initial_vesicle_shape.txt"));
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", use_initial_shape_file, 0);


    // create flow solver
    DeviceArray<int> label_flow(0, m, n);

    std::vector<REAL> viscosity_list({ viscosity_out_simu,viscosity_in_simu });

    DeviceArray<REAL> Fx(8.0 * viscosity_out_simu * umax_simu / n / n, m, n);
    DeviceArray<REAL> Fy(0.0, m, n);

    NSsolver flow(
        &viscosity_list[0], 2,
        m, n,
        NS::PERIODIC,
        NS::BOUNCEBACK,
        Fx.data_ptr(),
        Fy.data_ptr(),
        label_flow.data_ptr(),
        umax_simu*2.0);
    flow.config.IMPOSING_VELOCITY = false;
    if (use_initial_f_file)
    {
        flow.initializeByFile(initial_f_filename.c_str());
    }
    else
    {
        flow.initializeByDensity();
    }

    // create vesicle
    REAL x0ves_simu = (x0ves + 0.5) * m - 0.5;
    REAL y0ves_simu = (y0ves + 0.5) * n - 0.5;
    Vesicle ves(x0ves_simu, y0ves_simu, inclination, R0_simu, tau, kb, kl_simu, ks_simu, l0_simu, use_initial_shape_file ? initial_shape_filename.c_str() : nullptr);
    ves.relax(n_relax, damp_relax);
    ves.exportShape("initial_vesicle_shape.txt");
    ves.updateLabel(label_flow.data_ptr(), m, n, true, false);

    // immerse the vesicle into flow
    flow.immerseAnObject(&ves);
    flow.immerseObjectFinished();


    //pre-step for flow field initialization
    REAL umaxprev = 0.0;
    for (int i = 0; i != flow_prestep; ++i){
        if ((i + 1) % std::max(1, (int)(flow_prestep / (REAL)100)) == 0 || i == 0 || i == Tend - 1){
            std::cout << "prestep, iteration = " << i + 1;
            std::vector<REAL> hux(flow.ux.size());
            std::vector<REAL> huy(flow.ux.size());
            flow.ux.host(&hux[0]);
            flow.uy.host(&huy[0]);
            REAL umaxnow = 0.0;
            for (int j = (int)((double)hux.size()*0.74); j != (int)((double)hux.size()*0.76); ++j){
                REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
                umaxnow = std::max(umaxnow, uj);
            }
            std::cout << ", u_at_probe = " << umaxnow << std::endl;
            if (std::abs(umaxnow - umaxprev) < 1e-9 && umaxnow >= umax_simu*0.1 && i != 0){
                printf("u_at_probe reached its maximum, end prestep\n");
                break;
            }
            umaxprev = umaxnow;
        }
        if (is_MRT) flow.advanceFlowFieldWithoutImmersedObjects_MRT();
        else flow.advanceFlowFieldWithoutImmersedObjects();
    }
    flow.af_save_flow_field("initial_flow_field_afdat");


    // start simulation
    SimpleTimer timer;

    timer.tic();

    for (int i = 0; i != Tend; ++i)
    {
        ves.calculateForce();
        ves.sync_operation();

        if (LBM::getStepStampFlag(i, frames, Tend))
        {
            printf("i = %i, Nstep = %i, Tnow = %f, Tend = %f\n", i, Tend, i*dt, T);

            // save vesicle shape
            ves.get_fx().af_save("lag_fx", "lag_fx.afdat", i != 0);
            ves.get_fy().af_save("lag_fy", "lag_fy.afdat", i != 0);
            ves.exportShapeAF("ves_shape.afdat", i != 0);
            label_flow.af_save("label", "label.afdat", i != 0);
        }

        if (LBM::getStepStampFlag(i, frames_flowfield, Tend))
        {
            // save vesicle shape
            printf("i = %i, write velocity field\n", i);
            DeviceArray<float>(flow.ux).af_save("ux", "ux.afdat", i != 0);
            DeviceArray<float>(flow.uy).af_save("uy", "uy.afdat", i != 0);
        }

        if (is_MRT) flow.advanceFlowField_MRT();
        else flow.advanceFlowField();

        flow.advanceImmersedObjects();
        ves.updateLabel(label_flow.data_ptr(), m, n, true, false);
    }

    timer.toc();

}

#endif