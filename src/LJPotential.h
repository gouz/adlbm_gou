#ifndef LJPotential_h
#define LJPotential_h
//
// a Lennard Jones potential generates an adhesion force : short range repulsive force and long range repulsive force.
// Depletion model
//

//#define LJ_De De //1e-4
//#define LJ_d0 d0
#define LJ_FORCE(d, De, d0) \
    ( \
    (d) > (3.0 * (d0)) ? \
    0.0 : \
    -12.0 * ((De) / (d0)) * pow(((d0) / (d)),7) * (1.0 - pow(((d0) / (d)),6)) \
    )

// LJ potential (with a minus sign)
// added by Zhe Gou on 20201203
#define LJ_POTENTIAL(d, De, d0) \
    ( \
    (d) > (3.0 * (d0)) ? \
    0.0 : \
    (De) * pow(((d0) / (d)),6) * (2.0 - pow(((d0) / (d)),6)) \
    )

#endif

