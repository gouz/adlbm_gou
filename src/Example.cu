#include "Example.cuh"

// cuda launch parameters
#define BSIZE 256
#define CUDALAUNCH(n) <<<((n) + BSIZE - 1) / BSIZE, BSIZE>>>

__global__ void kernel_solute_dispersion_in_flow_around_a_floating_rigid_body_setBoundaryCondition
(
REAL* __restrict__ _C_in,
REAL* __restrict__ _C_ex,
const REAL* __restrict__ _rho_in,
const REAL* __restrict__ _rho_ex,
const REAL k,
const int len
)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        REAL flux = k*(_rho_ex[idx] - _rho_in[idx]);
        flux = flux * (REAL)(flux > 0);
        _C_in[idx] = flux;
        _C_ex[idx] = flux;
    }
}

void solute_dispersion_in_flow_around_a_floating_rigid_body_setBoundaryCondition
(
REAL* __restrict__ _C_in,
REAL* __restrict__ _C_ex,
const REAL* __restrict__ _rho_in,
const REAL* __restrict__ _rho_ex,
const REAL k,
const int len
)
{
    kernel_solute_dispersion_in_flow_around_a_floating_rigid_body_setBoundaryCondition
        CUDALAUNCH(len)
        (_C_in, _C_ex, _rho_in, _rho_ex, k, len);
}

__global__ void kernel_galileo_invariance_set_boundary_condition
(
REAL* __restrict__ _C_in,
REAL* __restrict__ _C_ex,
const REAL* __restrict__ _rho_in,
const REAL* __restrict__ _rho_ex,
const int* __restrict__ _boundary_size,
const REAL k,
const REAL dx,
const int len
)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        int size = *_boundary_size;
        if (idx < size)
        {
            REAL flux = dx*k*(_rho_ex[idx] - _rho_in[idx]);
            _C_in[idx] = flux; // flux;
            _C_ex[idx] = flux; // flux;
        }
    }
}

void galileo_invariance_set_boundary_condition
(
REAL* __restrict__ _C_in,
REAL* __restrict__ _C_ex,
const REAL* __restrict__ _rho_in,
const REAL* __restrict__ _rho_ex,
const int* __restrict__ _boundary_size,
const REAL k,
const REAL dx,
const int len
)
{
    kernel_galileo_invariance_set_boundary_condition
        CUDALAUNCH(len)
        (_C_in, _C_ex, _rho_in, _rho_ex, _boundary_size, k, dx, len);
}


__global__ void kernel_vortex_in_a_box_advect_boundary_curve
(
REAL* __restrict__ _x_numerical,
REAL* __restrict__ _y_numerical,
const int Ntheta,
const REAL dx,
const REAL dt
)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < Ntheta)
    {
        REAL x = _x_numerical[idx];
        REAL y = _y_numerical[idx];

#define CALCULATE_UX(x, y) dt / dx * (-0.5 * (1.0 + cos(PI*(-1.0 + 0.5*dx + (x)*dx))) * sin(PI*(-1.0 + 0.5*dx + (y)*dx)))
#define CALCULATE_UY(x, y) dt / dx * ( 0.5 * (1.0 + cos(PI*(-1.0 + 0.5*dx + (y)*dx))) * sin(PI*(-1.0 + 0.5*dx + (x)*dx)))

        // RK4 method
        REAL ux1 = CALCULATE_UX(x, y);
        REAL uy1 = CALCULATE_UY(x, y);

        REAL ux2 = CALCULATE_UX(x + ux1, y + uy1 * 0.5);
        REAL uy2 = CALCULATE_UY(x + ux1, y + uy1 * 0.5);

        REAL ux3 = CALCULATE_UX(x + ux1, y + uy2 * 0.5);
        REAL uy3 = CALCULATE_UY(x + ux1, y + uy2 * 0.5);

        REAL ux4 = CALCULATE_UX(x + ux1, y + uy3);
        REAL uy4 = CALCULATE_UY(x + ux1, y + uy3);



        _x_numerical[idx] = x + (ux1 + ux2*2.0 + ux3*2.0 + ux4) / 6.0;
        _y_numerical[idx] = y + (uy1 + uy2*2.0 + uy3*2.0 + uy4) / 6.0;
    }
}


void vortex_in_a_box_advect_boundary_curve
(REAL* x_numerical, REAL* y_numerical, const int Ntheta, const REAL dx, const REAL dt)
{
    kernel_vortex_in_a_box_advect_boundary_curve
        CUDALAUNCH(Ntheta + 1)
        (x_numerical, y_numerical, Ntheta, dx, dt);
}


__global__ void kernel_steady_heat_conduction_inside_a_circle_with_constant_advection_set_boundary_condition
(
const int boundary_type,
REAL* __restrict__ _C_ex,
REAL* __restrict__ _C_in,
short* __restrict__ _C_type_ex,
short* __restrict__ _C_type_in,
const REAL* __restrict__ _s,
const REAL k,
const REAL r0,
const REAL dx,
const int Ntheta,
const int launch_size,
const int* __restrict__ _actual_size
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < launch_size)
    {
        int size = *_actual_size;
        if (idx < size)
        {
            REAL theta = 2 * PI * _s[idx] / Ntheta;
            if (boundary_type == 0)
            {
                REAL flux = -dx*k / r0*cos(k*theta);
                _C_ex[idx] = flux;
                _C_in[idx] = 0.0;
            }
            if (boundary_type == 1)
            {
                REAL C_bound = cos(k*theta);
                _C_ex[idx] = C_bound;
                _C_in[idx] = 0.0;
                _C_type_ex[idx] = 1;
                _C_type_in[idx] = 1;
            }
        }
    }
}

void steady_heat_conduction_inside_a_circle_with_constant_advection_set_boundary_condition
(
const int boundary_type,
REAL* __restrict__ _C_ex,
REAL* __restrict__ _C_in,
short* __restrict__ _C_type_ex,
short* __restrict__ _C_type_in,
const REAL* __restrict__ _s,
const REAL k,
const REAL r0,
const REAL dx,
const int Ntheta,
const int launch_size,
const int* __restrict__ _actual_size
)
{
    kernel_steady_heat_conduction_inside_a_circle_with_constant_advection_set_boundary_condition
        CUDALAUNCH(launch_size)
        (boundary_type,_C_ex, _C_in,_C_type_ex,_C_type_in, _s, k, r0, dx, Ntheta, launch_size, _actual_size);
}

__global__
void kernel_three_leaved_interface_diffusion_set_boundary_condition
(
const int boundary_type,
REAL* __restrict__ _C_ex,
REAL* __restrict__ _C_in,
short* __restrict__ _C_type_ex,
short* __restrict__ _C_type_in,
const REAL* __restrict__ _x_numerical,
const REAL* __restrict__ _y_numerical,
const REAL* __restrict__ _nx_numerical,
const REAL* __restrict__ _ny_numerical,
const REAL dn,
const REAL Pe,
const REAL T,
const REAL dx,
const int launch_size,
const int* __restrict__ _actual_size
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < launch_size)
    {
        int size = *_actual_size;
        if (idx < size)
        {
            REAL x = -1.0 + dx*0.5 + dx*_x_numerical[idx];
            REAL y = -1.0 + dx*0.5 + dx*_y_numerical[idx];
            auto phi_exact = PHI_EXACT(x, y, T, Pe);
            if (boundary_type == 0)
            {
                REAL nx = _nx_numerical[idx];
                REAL ny = _ny_numerical[idx];
                auto xn = x + nx*dn;
                auto yn = y + ny*dn;
                auto phi_exact_n = PHI_EXACT(xn, yn, T, Pe);
                _C_in[idx] = (phi_exact_n - phi_exact) / dn * dx;
            }
            if (boundary_type == 1)
            {
                _C_in[idx] = phi_exact;
                _C_type_ex[idx] = 1;
                _C_type_in[idx] = 1;
            }
        }
    }
}

void three_leaved_interface_diffusion_set_boundary_condition
(
const int boundary_type,
REAL* __restrict__ _C_ex,
REAL* __restrict__ _C_in,
short* __restrict__ _C_type_ex,
short* __restrict__ _C_type_in,
const REAL* __restrict__ _x_numerical,
const REAL* __restrict__ _y_numerical,
const REAL* __restrict__ _nx_numerical,
const REAL* __restrict__ _ny_numerical,
const REAL dn,
const REAL Pe,
const REAL T,
const REAL dx,
const int launch_size,
const int* __restrict__ _actual_size
)
{
    kernel_three_leaved_interface_diffusion_set_boundary_condition
        CUDALAUNCH(launch_size)
        (
        boundary_type, _C_ex, _C_in, _C_type_ex, _C_type_in,
        _x_numerical,
        _y_numerical,
        _nx_numerical,
        _ny_numerical,
        dn,
        Pe, T, dx,
        launch_size,
        _actual_size
        );
}

void __global__
kernel_ATP_dynamics_in_a_vessel_set_boundary_condition
(
REAL* __restrict__ _Cin,
const REAL* __restrict__ _rhoin,
const int* __restrict__ _id,
const int* __restrict__ _bd_idx,  // for reading old value of rho, debug
const REAL* __restrict__ _rho_old, // for reading old value of rho, debug
const REAL K1,
const REAL K2,
const REAL c0,
const int launch_size,
const int* __restrict__ _actual_size
)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < launch_size)
    {
        int size = *_actual_size;
        if (idx < size)
        {
            REAL cin = 0.0;

            int id = _id[idx];

            // id = 0: inlet
            // id = 1: outlet
            // id = 2: upper wall
            // id = 3: lower wall where you have the endothelial cells
            if (id == 0) // inlet
            {
                cin = c0;
            }
            if (id == 1 || id == 2) // outlet or upper wall, zero flux condition
            {
                cin = 0.0;
            }
            if (id == 3)
            {
                REAL rhoin = _rhoin[idx];
                // originated as dC/dn=K1C-K2, but yeah, here the normal vector is inversed, no worry.
                cin = K2 - K1 * rhoin;

                // debug, using dirichlet boundary instead.
                // int bd_idx = _bd_idx[idx];
                // int rho_old = _rho_old[bd_idx];
                // cin = 0.5 * (K2 - K1 * rhoin) + rho_old;
            }
            // debug
            // printf("id = %i, cin = %.15e\n", id, cin);
            _Cin[idx] = cin;
            

        }
    }
}

void ATP_dynamics_in_a_vessel_set_boundary_condition
(
REAL* __restrict__ _Cin,
const REAL* __restrict__ _rhoin,
const int* __restrict__ _id,
const int* __restrict__ _bd_idx,  // for reading old value of rho, debug
const REAL* __restrict__ _rho_old, // for reading old value of rho, debug
const REAL K1,
const REAL K2,
const REAL c0,
const int launch_size,
const int* __restrict__ _actual_size
)
{
    kernel_ATP_dynamics_in_a_vessel_set_boundary_condition CUDALAUNCH(launch_size)
        (_Cin, _rhoin, _id, _bd_idx, _rho_old, K1, K2, c0, launch_size, _actual_size);
}


__global__
void kernel_validation_advection_diffusion_in_irregular_domain_set_sorce_term
(
REAL* __restrict__ _source,
const int* __restrict__ _label,
const int N,
const REAL x0,
const REAL x1,
const REAL y0,
const REAL y1,
const REAL omega,
const REAL D,
const REAL t,
const REAL dt,
const REAL dx,
const REAL x0_simu
)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	int j = blockDim.y * blockIdx.y + threadIdx.y;
	if (i < N && j < N)
	{
		// calculate physical position
		// x_simu = (x_phy - x0) / (x1 - x0) * N + x0_simu;
		// x_phy = (x_simu - x0_simu) / N * (x1 - x0) + x0;
		REAL x = (i - x0_simu) / (REAL)N * (x1 - x0) + x0;
		REAL y = (j - x0_simu) / (REAL)N * (y1 - y0) + y0;

		int label = _label[i + j*N];
		REAL S_simu = 0.0;
		// calculate source term
#define SOURCE_TERM_ad_irregular(x,y,omega,t,D) \
( (x)*(x)*(x) + (y)*(y) ) * (omega) * cos((omega)*(t)) + \
(3.0*(x)*(x) - (D) * ( 6.0*(x) + 2.0 )) * sin((omega)*(t))




		if (label == 0)
		{
			REAL s = SOURCE_TERM_ad_irregular(x, y, omega, t, D);
			S_simu = s * dt;
		}

		// write
		_source[i + j*N] = S_simu;
	}
}

void validation_advection_diffusion_in_irregular_domain_set_sorce_term
(
DeviceArray<REAL>& source,
const DeviceArray<int>& label,
const int N,
const REAL x0,
const REAL x1,
const REAL y0,
const REAL y1,
const REAL omega,
const REAL D,
const REAL t,
const REAL dt,
const REAL dx,
const REAL x0_simu
)
{
	const int bsize2d = 32;
	dim3 grid((N + bsize2d - 1) / bsize2d, (N + bsize2d - 1) / bsize2d);
	const dim3 block(bsize2d, bsize2d);
	kernel_validation_advection_diffusion_in_irregular_domain_set_sorce_term <<< grid, block >>>
		(source.data_ptr(), label.data_ptr(), N, x0, x1, y0, y1, omega, D, t, dt, dx, x0_simu);
}

__global__
void kernel_validation_advection_diffusion_in_irregular_domain_set_boundary_condition
(
REAL* __restrict__ _cin,
short * __restrict__ _cin_type,
REAL* __restrict__ _rho,
const REAL* __restrict__ _nx,
const REAL* __restrict__ _ny,
const REAL* __restrict__ _xbound,
const REAL* __restrict__ _ybound,
const int N,
const REAL x0,
const REAL x1,
const REAL y0,
const REAL y1,
const REAL omega,
const REAL D,
const REAL t,
const REAL dt,
const REAL dx,
const REAL x0_simu,
const int boundary_size,
const int boundary_type // 0-Neumann, 1-Dirichlet, 2-Robin
)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < boundary_size)
	{
		// calculate physical position
		// x_simu = (x_phy - x0) / (x1 - x0) * N + x0_simu;
		// x_phy = (x_simu - x0_simu) / N * (x1 - x0) + x0;
		REAL x_simu = _xbound[idx];
		REAL y_simu = _ybound[idx];
		REAL x = (x_simu - x0_simu) / (REAL)N * (x1 - x0) + x0;
		REAL y = (y_simu - x0_simu) / (REAL)N * (y1 - y0) + y0;
		REAL nx = _nx[idx];
		REAL ny = _ny[idx];

		REAL a3;
		short cin_type;
		if (boundary_type == 0) // Neumann
		{
			a3 = (3.0*nx * x*x + 2.0 * ny*y) * sin(omega * t);
			a3 *= dx;
			cin_type = 0;
		}
		if (boundary_type == 1) // Dirichlet
		{
			a3 = (x*x*x + y*y) * sin(omega * t);
			cin_type = 1;
		}
		if (boundary_type == 2) // Robin
		{
			a3 = (x*x*x + y*y + 3.0*nx * x*x + 2.0 * ny*y) * sin(omega * t);
			a3 = a3 - _rho[idx];
			a3 *= dx;
			cin_type = 0;
		}

		// write
		_cin[idx] = a3;
		_cin_type[idx] = cin_type;

	}
}

void validation_advection_diffusion_in_irregular_domain_set_boundary_condition
(
DeviceArray<REAL>& cin,
DeviceArray<short>& cin_type,
const DeviceArray<REAL>& rho,
const DeviceArray<REAL>& nx,
const DeviceArray<REAL>& ny,
const DeviceArray<REAL>& xbound,
const DeviceArray<REAL>& ybound,
const int N,
const REAL x0,
const REAL x1,
const REAL y0,
const REAL y1,
const REAL omega,
const REAL D,
const REAL t,
const REAL dt,
const REAL dx,
const REAL x0_simu,
const int boundary_size,
const int boundary_type
)
{
	kernel_validation_advection_diffusion_in_irregular_domain_set_boundary_condition CUDALAUNCH(boundary_size)
		(
		cin.data_ptr(),
		cin_type.data_ptr(),
		rho.data_ptr(),
		nx.data_ptr(),
		ny.data_ptr(),
		xbound.data_ptr(),
		ybound.data_ptr(),
		N, x0, x1, y0, y1,
		omega, D, t,
		dt, dx, x0_simu, boundary_size, boundary_type);

}