#include "ConfigFile.hpp"

// a simple helper to read BMP file for BMPdataFile class
// http://easybmp.sourceforge.net/
#include "../thirdparty/EasyBMP/EasyBMP.h"

//*************************************************************************************************
/*
Methods for input and output for non-arrayfire properties,
configs, vectors and so on.
Class Chameleon and ConfigFile are from Ren��, see reference:
http://www.adp-gmbh.ch/cpp/config_file.html
http://www.adp-gmbh.ch/cpp/chameleon.html

Thank Rene Nyffenegger
http://stackoverflow.com/users/180275/ren%C3%A9-nyffenegger


Code modyfied by Hengdi ZHANG on April 10, 2016

1. try to return default value when fail to find a match. replace the return of Value method from
const reference to value, default return Chameleon(0.0)
2. add checkValue method, return a bool value which equals to TRUE when it exists.
3. add comment symbol:
!  : fortran
// : c, c++
%  : matlab

*/
//*************************************************************************************************
/*
Chameleon.h

Copyright (C) 2002-2004 Rene Nyffenegger

This source code is provided 'as-is', without any express or implied
warranty. In no event will the author be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this source code must not be misrepresented; you must not
claim that you wrote the original source code. If you use this source code
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original source code.

3. This notice may not be removed or altered from any source distribution.

Rene Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/
//*************************************************************************************************
Chameleon::Chameleon(std::string const& value) {
    value_ = value;
}


Chameleon::Chameleon(const char* c) {
    value_ = c;
}

Chameleon::Chameleon(double d) {
    std::stringstream s;
    s << d;
    value_ = s.str();
}

Chameleon::Chameleon(Chameleon const& other) {
    value_ = other.value_;
}

Chameleon& Chameleon::operator=(Chameleon const& other) {
    value_ = other.value_;
    return *this;
}

Chameleon& Chameleon::operator=(double i) {
    std::stringstream s;
    s << i;
    value_ = s.str();
    return *this;
}

Chameleon& Chameleon::operator=(std::string const& s) {
    value_ = s;
    return *this;
}

Chameleon::operator std::string() const {
    return value_;
}

Chameleon::operator double() const {
    return atof(value_.c_str());
}
//*************************************************************************************************
// trim from Rene Nyffenegger
std::string trim(std::string const& source, char const* delims = " \t\r\n") {
    std::string result(source);
    std::string::size_type index = result.find_last_not_of(delims);
    if (index != std::string::npos)
        result.erase(++index);

    index = result.find_first_not_of(delims);
    if (index != std::string::npos)
        result.erase(0, index);
    else
        result.erase();
    return result;
}

// delete string after first comment
std::string remove_comment(std::string const& source, std::string const& comment_symbol = "//"){
    std::string result(source);
    std::string::size_type index = result.find(comment_symbol);
    if (index != std::string::npos)
        result.erase(result.begin() + index, result.end());
    return result;
}

// Replace all occurrences in a string
// this function is downloaded from page:
// http://stackoverflow.com/questions/2896600/how-to-replace-all-occurrences-of-a-character-in-string
// Thank the answer from Gauthier Boaglio, his homepage:
// http://stackoverflow.com/users/1715716/gauthier-boaglio
// http://www.cefe.cnrs.fr/fr/recherche/ee/eee/797-it/1283-gauthier-boaglio
std::string replace_all(std::string str, const std::string& from = ",", const std::string& to = " ") {
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

ConfigFile::ConfigFile(std::string const& configFile) {
    filename_ = configFile;
    std::ifstream file(configFile.c_str());
    if (!file.is_open()) {
        std::cout << configFile << " open failed" << std::endl;
        return;
    }

    std::cout << "configfile initialized by """ << configFile << """" << std::endl;

    std::string line;
    std::string name;
    std::string value;
    std::string inSection;
    int posEqual;
    while (std::getline(file, line)) {

        // if (line[0] == '#') continue;
        // if (line[0] == ';') continue;
        // Hengdi ZHANG April 10, 2016
        // add "//", "!" "%" as comment symbols
        line = remove_comment(line, "//");  // c++
        line = remove_comment(line, "!");   // Fortran
        line = remove_comment(line, "%");   // matlab
        line = remove_comment(line, "#");   // Python
        line = replace_all(line, ";", " ");
        line = trim(line);
        if (!line.length()) continue;
        if (line[0] == '[') {
            inSection = trim(line.substr(1, line.find(']') - 1));
            continue;
        }

        posEqual = (int)line.find('=');
        name = trim(line.substr(0, posEqual));
        value = trim(line.substr(posEqual + 1));

        content_[inSection + '/' + name] = Chameleon(value);
    }
}

Chameleon ConfigFile::Value(std::string const& section, std::string const& entry) const {

    std::map<std::string, Chameleon>::const_iterator ci = content_.find(section + '/' + entry);

    // code modyfied by Hengdi ZHANG April 10, 2016
    // try to return default value when fail to find a match
    if (ci == content_.end()) { // throw "does not exist";
        std::cout << "Wanrning read " << filename_ << ":" << std::endl;
        std::cout << "  section [" << section << "] " << entry << " does not exist." << std::endl;
        return Chameleon(0.0); // default return 0.0
    }
    return ci->second;
}

// string loader for Linux system
std::string ConfigFile::sValue(std::string const& section, std::string const& entry) const{
    Chameleon ci = this->Value(section, entry);
    return ci.getContent();
}

bool ConfigFile::checkValue(std::string const& section, std::string const& entry) const{
    std::map<std::string, Chameleon>::const_iterator ci = content_.find(section + '/' + entry);
    if (ci == content_.end()) return false;
    return true;
}

Chameleon ConfigFile::Value(std::string const& section, std::string const& entry, double value) {
    if (checkValue(section, entry)) return Value(section, entry);
    content_.insert(std::make_pair(section + '/' + entry, Chameleon(value))).first->second;
    return Chameleon(value);
}

Chameleon ConfigFile::Value(std::string const& section, std::string const& entry, std::string const& value) {
    if (checkValue(section, entry)) return Value(section, entry);
    content_.insert(std::make_pair(section + '/' + entry, Chameleon(value))).first->second;
    return Chameleon(value);
}

void ConfigFile::loadValue(std::string const& section, std::string const& entry, std::string& value){
    if (this->checkValue(section, entry)) value = this->sValue(section, entry);
}

void ConfigFile::loadValue(std::string const& section, std::string const& entry, double& value){
    if (this->checkValue(section, entry)) value = this->Value(section, entry);
}

void ConfigFile::loadValue(std::string const& section, std::string const& entry, float& value){
    if (this->checkValue(section, entry)) value = (float)this->Value(section, entry);
}

void ConfigFile::loadValue(std::string const& section, std::string const& entry, int& value){
    if (this->checkValue(section, entry)) value = (int)this->Value(section, entry);
}

//*************************************************************************************************

DataFile::DataFile(std::string const& filename, bool skip_empty_row) {
    this->filename = filename;
    std::ifstream file(this->filename.c_str());

    n = 0;
    m = 0;

    if (file.fail())
    {
        printf("DataFile reading %s failed.\n", this->filename.c_str());
        return;
    }
    int irow = 0;
    std::string line;
    while (std::getline(file, line)) {

        line = remove_comment(line, "//");  // remove "//", "!", "%" and "#" as they are comment symbols
        line = remove_comment(line, "!"); // Fortran
        line = remove_comment(line, "%"); // matlab
        line = remove_comment(line, "#"); // python, shell
        line = replace_all(line, ",", " "); // replace seperator "," and ";" with " "
        line = replace_all(line, ";", " ");
        line = trim(line);
        bool this_row_is_empty = true;
        std::vector<double> vec_line;
        vec_line.reserve(line.size());
        int m_line = 0;
        while (line.length()>0){
            this_row_is_empty = false;
            std::string::size_type sz = 0;
            double data_temp = std::stod(line, &sz);
            line = trim(line.substr(sz));
            vec_line.push_back(data_temp);
            m_line++;
        }
        if (!(this_row_is_empty && skip_empty_row)){
            this->data.push_back(vec_line);
            n++;
            m = m > m_line ? m : m_line;
        }
    }
}

std::vector< std::vector<double> >& DataFile::getData(){ return this->data; }

void DataFile::transpose(){
    std::vector< std::vector<double> > data_t(m, std::vector<double>(n, 0.0));
    for (int i = 0; i != m; ++i){
        for (int j = 0; j != n; ++j){
            data_t[i][j] = i > data[j].size() - 1 ? 0.0 : data[j][i];
        }
    }
    data = data_t;
    std::swap(m, n);
}

// otsu thresholding method for gray to binary bmp
// simple implementation from github.com/lvfanzai/ (thanks)
// comments in chinese are deleted, sorry
// seems quite slow?
static int findThreshold(BMP frame)
{
    static const int GrayScale = 256;
    int width = frame.TellWidth();
    int height = frame.TellHeight();
    int pixelCount[GrayScale] = { 0 };
    float pixelPro[GrayScale] = { 0 };
    int i, j, pixelSum = width * height, threshold = 0;
    int r = 0, g = 0, b = 0, data = 0;

    for (i = 0; i < height; i++)
    {
        for (j = 0; j < width; j++)
        {
            r = frame(j, i)->Red;
            g = frame(j, i)->Green;
            b = frame(j, i)->Blue;

            data = std::pow(
                (
                std::pow(r, 2.2) * 0.2973 +
                std::pow(g, 2.2)* 0.6274 +
                std::pow(b, 2.2) * 0.0753
                ),
                (1 / 2.2)
                );

            pixelCount[data]++;
        }
    }

    for (i = 0; i < GrayScale; i++)
    {
        pixelPro[i] = (float)pixelCount[i] / pixelSum;
    }

    float w0, w1, u0tmp, u1tmp, u0, u1, deltaTmp, deltaMax = 0;
    for (i = 0; i < GrayScale; i++)
    {
        w0 = w1 = u0tmp = u1tmp = u0 = u1 = deltaTmp = 0;
        for (j = 0; j < GrayScale; j++)
        {
            if (j <= i)
            {
                w0 += pixelPro[j];
                u0tmp += j * pixelPro[j];
            }
            else
            {
                w1 += pixelPro[j];
                u1tmp += j * pixelPro[j];
            }
        }
        u0 = u0tmp / w0;
        u1 = u1tmp / w1;
        deltaTmp = (float)(w0 *w1* pow((u0 - u1), 2));
        if (deltaTmp > deltaMax)
        {
            deltaMax = deltaTmp;
            threshold = i;
        }
    }
    return threshold;
}


BMPdataFile::BMPdataFile(const std::string& filename)
{
    this->filename = filename;
    BMP raw_bmp;
    raw_bmp.ReadFromFile(filename.c_str());
    m = raw_bmp.TellWidth();
    n = raw_bmp.TellHeight();
    R.resize(m*n);
    G.resize(m*n);
    B.resize(m*n);
    A.resize(m*n);
    gray.resize(m*n);
    bin.resize(m*n);

    // calculate threshold for binary image
    int otsu_threshold = findThreshold(raw_bmp);

    for (int i = 0; i != m; ++i)
    {
        for (int j = 0; j != n; ++j)
        {
            auto pixel = raw_bmp(i, j);
            int idx = i + m*(n - 1 - j); // flip y direction
            R[idx] = pixel->Red;
            G[idx] = pixel->Green;
            B[idx] = pixel->Blue;
            A[idx] = pixel->Alpha;
            gray[idx] = (int)std::pow(
                (
                std::pow(pixel->Red, 2.2) * 0.2973 +
                std::pow(pixel->Green, 2.2)* 0.6274 +
                std::pow(pixel->Blue, 2.2) * 0.0753
                ),
                (1 / 2.2)
                );
            bin[idx] = (int)(gray[idx] > otsu_threshold);
        }
    }
}