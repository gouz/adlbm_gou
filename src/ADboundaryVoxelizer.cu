#include "ADboundaryVoxelizer.cuh"

// a lattice volume estimation function using intersection information
template<int N>
// this function estimates the fractional volume with pressence of boundaries
// boundaries as inputs are represented by intersecting points on 4 perpendicular directions on xy coordinates
// N is the refinment level of representitive rays, N=1 will have 8 representitive rays forming velocity lines in D2Q9 model
// for N>1 it will have 8* 2^(N-1) rays in total
// inputs: s1~4: distance from lattice center to intersecting point on 4 perpendicular directions
//         nx, ny 1~4 are the normal direction (outter) for the boundaries
/*
       2
       |
    3--0--1
       |
       4
    be careful: the order is different from that in advection-diffusion lattice boltzmann code in ADmethod.cu
*/
// the volume will be reconstructed using representitive points who are on the rays and intersect with boundaries
__device__ void _device_estimate_lattic_fractional_volume
(
REAL& volume_fraction,
REAL s1,
REAL s2,
REAL s3,
REAL s4,
const REAL nx1,
const REAL nx2,
const REAL nx3,
const REAL nx4,
const REAL ny1,
const REAL ny2,
const REAL ny3,
const REAL ny4
)
{
    // a wrong implementation for simple test:
    s1 = s1 < (REAL)0.5 ? s1 : (REAL)0.5;
    s2 = s2 < (REAL)0.5 ? s2 : (REAL)0.5;
    s3 = s3 < (REAL)0.5 ? s3 : (REAL)0.5;
    s4 = s4 < (REAL)0.5 ? s4 : (REAL)0.5;
    volume_fraction = (s1 + s2 + s3 + s4) * (REAL)0.25;
}

#define BSIZE1D 256

// 2D kernel launch is only for recoverMovingBoundary functions
// small granularity evokes less wasted 8connected calculation
#define BSIZEM 16
#define BSIZEN 8

#ifndef PI
#define PI 3.1415926535897932
#endif

#define EPS ((double)1e-3)
#define EPS2 ((double)1e-6)
#define IS_FLOAT_INTEGER(x, eps) (abs((x) - round(x)) < (eps))
#define DRAG_TO_INTEGER_IF_TOO_CLOSE(x) (abs((x) - round(x)) < EPS2 ? round(x) : (x))
#define INDEX(i, j) ((j) * m + (i))
#define VALID_POSITION(i,j) (!( (((i)>(m-1) |(i)<0) & (!is_x_periodic)) | (((j)>(n-1) |(j)<0) & (!is_y_periodic)) ))
#define POSITIVE_MOD(i, m) ( ( (i)%(m) + (m) ) % (m))

// from NVIDIA book " Jason Sanders, Edward Kandrot CUDA_by_Example"
// Appendix advanced atomics, page 273, it is suggested:
/*
only one thread in the warp can acquire the lock at a time, and we will suffer many a 
headache if we let all 32 threads in the warp contend for the lock simultaneously. 
In this situation, we've found that it's best to do some of the work in software and 
simply walk through each thread in the warp, giving each a chance to acquire the 
data structure's lock, do its work, and subsequently release the lock.
*/
#define WARP_SIZE 32


__device__ void device_lock(int* _mutex){
    while (atomicCAS(_mutex, 0, 1) != 0);
    __threadfence();
}

__device__ void device_unlock(int* _mutex){
    __threadfence();
    atomicExch(_mutex, 0);
}

__forceinline__ __device__ void device_tagPoint
(
const char value,
const int i,
const int j,
const REAL distance,
char* _tag,
int* _mutex,
double* _min_distance,
const int m,
const int n,
const int idx0
)
{
    if (i >= 0 & i < m & j >= 0 & j < n)
    {

        int idx = INDEX(i, j);

        // mutex version
        ///*
        int* mutex = _mutex + idx;
        // fuck atomic operations, they are toooo slow !!
        for (int iwarp = 0; iwarp < WARP_SIZE; ++iwarp)
        {
            if ((idx0 % WARP_SIZE) == iwarp)
            {
                device_lock(mutex);
                if (distance < _min_distance[idx])
                {
                    _tag[idx] = value;
                    _min_distance[idx] = distance;
                }
                device_unlock(mutex);
            }
        }
        //*/

        // an attempt without mutex, dangerous but fast
        /*
        if (distance < _min_distance[idx])
        {
            _tag[idx] = value;
            _min_distance[idx] = distance;
        }
        */
    }
}

__forceinline__ __device__ void device_prepare_search_span_in_one_direction
(
const double& x0_in,
const double& x1_in,
double& dx,
int& is_x_reversed,
int& i0,
int& i1,
int& di,
int& ilen
)
{
    double x0 = x0_in; x0 = IS_FLOAT_INTEGER(x0, EPS2) ? round(x0) : x0;

    double x1 = x1_in;
    bool is_x1_integer = IS_FLOAT_INTEGER(x1_in, EPS2);
    x1 = is_x1_integer ? round(x1) : x1;

    dx = x1 - x0;
    is_x_reversed = (int)(dx < 0);
    di = -2 * is_x_reversed + 1;
    i0 = (1 - is_x_reversed)*(int)ceil(x0) + is_x_reversed*(int)floor(x0);
    i1 = (1 - is_x_reversed)*(int)floor(x1) + is_x_reversed*(int)ceil(x1) - di*(int)is_x1_integer;
    ilen = (int)((i1 - i0)*di >= 0) * (abs(i1 - i0) + 1);
    if (x0 == x1) ilen = 0;

}

template<int DIM>
// calculate corresponding intersecting grid positions from a given gridline
// template parameter DIM: 0 - gridline parallel to x, 1 - gridline parallel to y
// in periodic simulation domain, this function will also shift i_tag into
// the domain by POSITIVE_MOD(i,m)
__forceinline__ __device__ void device_calculate_tag_position_from_gridline
(
const double x0,
const double y0,
const double dx,
const double dy,
const int is_x_reversed,
const int di,
int& i_tag, // gridline position
int& j_tag_in,
int& j_tag_ex,
double& distance_in,
double& distance_ex,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    double y_tag = ((double)i_tag - x0) / dx*dy + y0;
    //y_tag = DRAG_TO_INTEGER_IF_TOO_CLOSE(y_tag);
    
    // if (i_tag, round(y_tag)) is too close to this line, then drag y_tag to round(y_rag)
    // this criteria keeps the symmetricity of x and y ( it's a must )
    REAL y_tag_round = round(y_tag);
    REAL y_tag_remainder = abs(y_tag - y_tag_round);
    y_tag_remainder = abs(dx) > abs(dy) ? y_tag_remainder : abs(dx) / abs(dy) * y_tag_remainder;
    y_tag = y_tag_remainder > EPS2 ? y_tag : y_tag_round;

    if (DIM == 0){
        if (is_x_reversed == 0)
        {
            j_tag_in = (int)floor(y_tag);
            j_tag_ex = j_tag_in + 1;
        }
        else
        {
            j_tag_in = (int)ceil(y_tag);
            j_tag_ex = j_tag_in - 1;
        }
        //printf("i_tag = %i, y_tag = %f, j_tag_in = %i, j_tag_ex = %i\n", i_tag, y_tag, j_tag_in, j_tag_ex);
    }
    if (DIM == 1){
        if (is_x_reversed == 0)
        {
            j_tag_in = (int)ceil(y_tag);
            j_tag_ex = j_tag_in - 1;
        }
        else
        {
            j_tag_in = (int)floor(y_tag);
            j_tag_ex = j_tag_in + 1;
        }
        //printf("j_tag = %i, x_tag = %f, i_tag_in = %i, i_tag_ex = %i\n", i_tag, y_tag, j_tag_in, j_tag_ex);
    }
    distance_in = abs(y_tag - (double)j_tag_in);
    distance_ex = abs(y_tag - (double)j_tag_ex);
    if (is_x_periodic){
        i_tag = POSITIVE_MOD(i_tag, m);
    }
    if (is_y_periodic){
        j_tag_in = POSITIVE_MOD(j_tag_in, n);
        j_tag_ex = POSITIVE_MOD(j_tag_ex, n);
    }
}

__device__ void device_push_vertex_away_from_integer
(
double& x,
double& y,
const double& xL,
const double& yL,
const double& xR,
const double& yR
)
{
    bool is_x_integer = IS_FLOAT_INTEGER(x, EPS);
    bool is_y_integer = IS_FLOAT_INTEGER(y, EPS);
    if (!is_x_integer && !is_y_integer) return;

    // modefied by Zhe Gou on 20211228
    REAL sx = round(x);
    REAL sy = round(y);

    bool is_cross_xR = false; // if there is cross point
    bool is_cross_xL = false;
    REAL syR = y;
    REAL syL = y;
    if (is_x_integer && ((xR - x) * (x - xL) <= 0))
    {
        if (xR - x != 0)
        {
            syR = (yR - y) * (sx - x) / (xR - x) + y;
            if ((yR - syR) * (syR - y) >= 0 && (xR - sx) * (sx - x) >= 0)
                is_cross_xR = true;
        }
        if (xL - x != 0)
        {
            syL = (yL - y) * (sx - x) / (xL - x) + y;
            if ((yL - syL) * (syL - y) >= 0 && (xL - sx) * (sx - x) >= 0)
                is_cross_xL = true;
        }
    }

    bool is_cross_yR = false; // if there is cross point
    bool is_cross_yL = false;
    REAL sxR = x;
    REAL sxL = x;
    if (is_y_integer && ((yR - y) * (y - yL) <= 0))
    {
        if (yR - y != 0)
        {
            sxR = (xR - x) * (sy - y) / (yR - y) + x;
            if ((xR - sxR) * (sxR - x) >= 0 && (yR - sy) * (sy - y) >= 0)
                is_cross_yR = true;
        }
        if (yL - y != 0)
        {
            sxL = (xL - x) * (sy - y) / (yL - y) + x;
            if ((xL - sxL) * (sxL - x) >= 0 && (yL - sy) * (sy - y) >= 0)
                is_cross_yL = true;
        }
    }

    bool is_on_x = is_cross_xR && is_cross_xL && (floor(syR) == floor(syL));
    bool is_on_y = is_cross_yR && is_cross_yL && (floor(sxR) == floor(sxL));
    //bool is_on_x = is_x_integer & ((xR - x) * (x - xL) <= 0);
    //bool is_on_y = is_y_integer & ((yR - y) * (y - yL) <= 0);
    //bool is_on_grid = is_x_integer & is_y_integer;
    if (is_on_x || is_on_y)// || is_on_grid)
    {
        double dxL = x - xL;
        double dyL = y - yL;
        double dxR = xR - x;
        double dyR = yR - y;
        double thetaL = atan2(dyL, dxL); thetaL += (thetaL < 0)*2.0*PI;
        double thetaR = atan2(dyR, dxR); thetaR += (thetaR < 0)*2.0*PI;
        thetaR += (thetaR < thetaL)*2.0*PI;
        double thetaN = (thetaL + thetaR + PI)*0.5;
        x += (cos(thetaN)*EPS*2);
        y += (sin(thetaN)*EPS*2);
    }
    else
    {
        if (IS_FLOAT_INTEGER(x, EPS))
        {
            x += (((int)(x - sx >= 0) - (int)(x - sx < 0))*EPS);
        }
        if (IS_FLOAT_INTEGER(y, EPS))
        {
            y += (((int)(y - sy >= 0) - (int)(y - sy < 0))*EPS);
        }
    }
}

__device__ void device_read_and_push_vertex_away_from_integer
(
REAL& x0,
REAL& y0,
REAL& x1,
REAL& y1,
const int idx0,
const int len,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y
)
{
    int idx1 = (idx0 + 1) % len;
    int idx1R = (idx0 + 2) % len;
    int idx0L = (idx0 - 1 + len) % len;

    x0 = (double)_x[idx0];
    x1 = (double)_x[idx1];
    y0 = (double)_y[idx0];
    y1 = (double)_y[idx1];

    // points for push vertex
    // added by Zhe Gou on 20211228
    double x00 = x0;
    double x11 = x1;
    double y00 = y0;
    double y11 = y1;
    double x0L = (double)_x[idx0L];
    double y0L = (double)_y[idx0L];
    double x1R = (double)_x[idx1R];
    double y1R = (double)_y[idx1R];

    device_push_vertex_away_from_integer(x0, y0, x0L, y0L, x11, y11);
    device_push_vertex_away_from_integer(x1, y1, x00, y00, x1R, y1R);
}

__global__ void kernel_makeTag
(
char* _tag,
int* _mutex,
double* _min_distance,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int len,
const bool is_circle,
const int m,
const int n,
bool is_x_periodic,
bool is_y_periodic
)
{
    int idx0 = blockDim.x*blockIdx.x + threadIdx.x;
    int len_segment = len - (int)(!is_circle);
    if (idx0 < len_segment){
        double x0, y0, x1, y1;
        device_read_and_push_vertex_away_from_integer(x0, y0, x1, y1, idx0, len, _x, _y);

        double dx, dy;
        int i0, i1, di, ilen, is_x_reversed;
        int j0, j1, dj, jlen, is_y_reversed;
        device_prepare_search_span_in_one_direction(x0, x1, dx, is_x_reversed, i0, i1, di, ilen);
        device_prepare_search_span_in_one_direction(y0, y1, dy, is_y_reversed, j0, j1, dj, jlen);

        // tag points along x direction
        for (int i = 0; i != ilen; ++i)
        {
            int i_tag = i0 + di*i;
            int j_tag_in, j_tag_ex;
            double distance_in, distance_ex;
            device_calculate_tag_position_from_gridline<0>(
                x0, y0, dx, dy, is_x_reversed, di,
                i_tag, j_tag_in, j_tag_ex, distance_in, distance_ex,
                m, n, is_x_periodic, is_y_periodic);
            device_tagPoint(-1, i_tag, j_tag_in, distance_in, _tag, _mutex, _min_distance, m, n, idx0);
            device_tagPoint(1, i_tag, j_tag_ex, distance_ex, _tag, _mutex, _min_distance, m, n, idx0);
        }

        // tag points along y direction
        for (int j = 0; j != jlen; ++j)
        {
            int j_tag = j0 + dj*j;
            int i_tag_in, i_tag_ex;
            double distance_in, distance_ex;
            device_calculate_tag_position_from_gridline<1>(
                y0, x0, dy, dx, is_y_reversed, dj,
                j_tag, i_tag_in, i_tag_ex, distance_in, distance_ex,
                n, m, is_y_periodic, is_x_periodic);
            device_tagPoint(-1, i_tag_in, j_tag, distance_in, _tag, _mutex, _min_distance, m, n, idx0);
            device_tagPoint(1, i_tag_ex, j_tag, distance_ex, _tag, _mutex, _min_distance, m, n, idx0);
        }
    }
}


// Return a lattice tag under a 8-connection definition (works on single lattice)
//     This method will be useful for preventing leakage
//     in recover process in moving boundary cases
//
// example on lattice in []:
//     original label     8-connected label
//        -1   1      ==>   -1   1
//       [ 0] -1           [-1] -1
//   in this case, function will return -1, while if the lattice is already 
//   tagged with -1 or 1 it will simply return -1 or 1
// If indice i,j exceeds the domain return 0
__device__ char device_tag_8connected
(
const int& i,
const int& j,
const int& m,
const int& n,
const char* __restrict__ _tag,
const bool& is_x_periodic,
const bool& is_y_periodic
)
{
    int tag = _tag[INDEX(i, j)];
    if (tag != 0) return tag;
    // neighbor position:
    /*
    / 4 3 2 \
    | 5[0]1 |
    \ 6 7 8 /
    */
    // read 8 neighbor tag into array a, a[8]=a[0]
    char a[9];
    const int di[] = { 1, 1, 0, -1, -1, -1, 0, 1 };
    const int dj[] = { 0, 1, 1, 1, 0, -1, -1, -1 };
    bool is_not_all_neighbor_zero = false;
    if (i > 0 & i < m - 1 & j > 0 & j < n - 1)
    {
#pragma unroll
        for (int iter = 0; iter != 8; ++iter)
        {
            // neighbor position
            int in = i + di[iter];
            int jn = j + dj[iter];

            a[iter] = _tag[INDEX(in, jn)];
            is_not_all_neighbor_zero |= (a[iter] != 0);
        }
        a[8] = a[0];
    }
    else // lattices near simulation boundary need special treatment
    {
#pragma unroll
        for (int iter = 0; iter != 8; ++iter)
        {
            // neighbor position
            int in = i + di[iter];
            int jn = j + dj[iter];

            if (VALID_POSITION(in, jn))
            {
                a[iter] = _tag[INDEX(POSITIVE_MOD(in, m), POSITIVE_MOD(jn, n))];
            }
            else
            {
                a[iter] = 0;
            }
            is_not_all_neighbor_zero |= (a[iter] != 0);
        }
        a[8] = a[0];
    }

    if (!is_not_all_neighbor_zero) return (char)0;

    // layer1, loop on 4 corners, tell if a corner is internal, under 8-connection definition
    bool is_internal = false;
    bool is_external = false;
#pragma unroll
    for (int iter = 0; iter != 4; iter++){
        char& b0 = a[iter * 2];
        char& b1 = a[iter * 2 + 1];
        char& b2 = a[iter * 2 + 2];
        //if (b0 == -1 && b1 == 1 && b2 == -1) is_internal = true; // cpu version
        //if (b0 == 1 && b1 == -1 && b2 == 1) is_external = true; // cpu version
        is_internal |= (b0 == -1 & b1 == 1 & b2 == -1); // gpu version
        is_external |= (b0 == 1 & b1 == -1 & b2 == 1); // gpu version
    }

    //if (is_internal && !is_external)return -1; // cpu version
    //if (!is_internal && is_external)return 1; // cpu version
    //return 0; // cpu version
    return (char)((1 - 2 * (int)is_internal) * (int)(is_internal != is_external)); // gpu version

}

__global__ void kernel_makeTag_conn8
(
char* __restrict__ _tag_conn8,
const char* __restrict__ _tag,
const int m,
const int n,
bool is_x_periodic,
bool is_y_periodic
)
{
    int i = blockDim.x*blockIdx.x + threadIdx.x;
    int j = blockDim.y*blockIdx.y + threadIdx.y;
    if (i < m & j < n)
    {
        _tag_conn8[INDEX(i, j)] = device_tag_8connected(i, j, m, n, _tag, is_x_periodic, is_y_periodic);
    }
}


void LBMGPUvoxelizer::makeTag(void)
{
    for (int i = 0; i != curve_records_size; ++i)
    {
        int len = curve_records[i].len;
        kernel_makeTag <<< (len + BSIZE1D - 1) / BSIZE1D, BSIZE1D, 0, cudaStream[curve_records[i].id_cudaStream] >>>
            (
            tag.data_ptr(),
            mutex.data_ptr(),
            min_distance.data_ptr(),
            curve_records[i]._x,
            curve_records[i]._y,
            len,
            curve_records[i].is_circle,
            m, n,
            is_x_periodic, is_y_periodic
            );
    }

    for (int i = 0; i != curve_records_size; ++i){
        cudaStreamSynchronize(cudaStream[curve_records[i].id_cudaStream]);
    }

    dim3 grid((m + 32 - 1) / 32, (n + 16 - 1) / 16);
    dim3 block(32, 16);
    kernel_makeTag_conn8 <<< grid, block>>>
        (
        tag_conn8.data_ptr(),
        tag.data_ptr(),
        m, n,
        is_x_periodic, is_y_periodic
        );
    cudaStreamSynchronize(cudaStreamDefault);
    DA::swap_data_ptr(tag_conn8, tag);

    min_distance = (double)2.0; // min_distance all set back to 2.0 (away from any boundary)
}

__global__ void kernel_fill_boundary_size_on_segment
(
int* __restrict__ _boundary_size_on_segment,
const char* __restrict__ _tag,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int len,
const bool is_circle,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int idx0 = blockDim.x*blockIdx.x + threadIdx.x;
    int len_segment = len - (int)(!is_circle);
    if (idx0 < len_segment){
        double x0, y0, x1, y1;
        device_read_and_push_vertex_away_from_integer(x0, y0, x1, y1, idx0, len, _x, _y);

        double dx, dy;
        int i0, i1, di, ilen, is_x_reversed;
        int j0, j1, dj, jlen, is_y_reversed;
        device_prepare_search_span_in_one_direction(x0, x1, dx, is_x_reversed, i0, i1, di, ilen);
        device_prepare_search_span_in_one_direction(y0, y1, dy, is_y_reversed, j0, j1, dj, jlen);

        // count the number of valid intersecting pairs of boundary grids along this segment
        int count_boundary_pair = 0;

        // count pairs along x direction
        for (int i = 0; i != ilen; ++i)
        {
            int i_tag = i0 + di*i;
            int j_tag_in, j_tag_ex;
            double distance_in, distance_ex;
            device_calculate_tag_position_from_gridline<0>(
                x0, y0, dx, dy, is_x_reversed, di,
                i_tag, j_tag_in, j_tag_ex, distance_in, distance_ex,
                m, n, is_x_periodic, is_y_periodic);

            // make sure this pair of intersecting grids (i_tag,j_tag_in) and (i_tag,j_tag_ex) both
            // lie in the mesh
            if (i_tag >= 0 & i_tag < m & j_tag_in >= 0 & j_tag_in < n & j_tag_ex >= 0 & j_tag_ex < n)
            {
                // this is a valid pair if internal grid is tagged as "non 1"
                // while external grid is tagged as "non -1", add it into the count
                count_boundary_pair += (int)(
                    (_tag[INDEX(i_tag, j_tag_in)] != 1) &
                    (_tag[INDEX(i_tag, j_tag_ex)] != -1));
            }
        }

        // count pairs along y direction
        for (int j = 0; j != jlen; ++j)
        {
            int j_tag = j0 + dj*j;
            int i_tag_in, i_tag_ex;
            double distance_in, distance_ex;
            device_calculate_tag_position_from_gridline<1>(
                y0, x0, dy, dx, is_y_reversed, dj,
                j_tag, i_tag_in, i_tag_ex, distance_in, distance_ex,
                n, m, is_y_periodic, is_x_periodic);

            // make sure this pair of intersecting grids (i_tag,j_tag_in) and (i_tag,j_tag_ex) both
            // lie in the mesh
            if (i_tag_in >= 0 & i_tag_in < m & i_tag_ex >= 0 & i_tag_ex < m & j_tag >= 0 & j_tag < n)
            {
                // this is a valid pair if internal grid is tagged as "non 1"
                // while external grid is tagged as "non -1", add it into the count
                count_boundary_pair += (int)(
                    (_tag[INDEX(i_tag_in, j_tag)] != 1) &
                    (_tag[INDEX(i_tag_ex, j_tag)] != -1));
            }
        }

        _boundary_size_on_segment[idx0] = count_boundary_pair;
    }
}

void LBMGPUvoxelizer::fill_boundary_size_on_segment()
{
    for (int i = 0; i != curve_records_size; ++i)
    {
        int len = curve_records[i].len;
        kernel_fill_boundary_size_on_segment <<< (len + BSIZE1D - 1) / BSIZE1D, BSIZE1D, 0, cudaStream[curve_records[i].id_cudaStream] >>>
            (
            boundary_size_on_segment.data_ptr() + curve_records[i].idx_entry,
            tag.data_ptr(),
            curve_records[i]._x,
            curve_records[i]._y,
            len,
            curve_records[i].is_circle, m, n, is_x_periodic, is_y_periodic);
    }
    for (int i = 0; i != curve_records_size; ++i){
        cudaStreamSynchronize(cudaStream[curve_records[i].id_cudaStream]);
    }
}


// This kernel function fills the boundary information list segment by segment in a curve
// it will be called in fill_boundary_list() in a asynchronous manner.
// (each call from CPU side will invoke a kernel issuing on one single curve)
// the list size and start point is pre-assigned by fill_boundary_size_on_segment()
// and an exclusive prefix of the "count per segment" list.
// The boundary lists are:
// in LBM boundary lists, idx is the index of a distribution function
// at position (i,j) with micro velocity c incoming, namely idx = i + j*m + c*m*n
// in this D2Q5 model, c[1] = [1 0], c[2] = [-1 0], c[3] = [0 1], c[4] = [0 -1]
// s is a lagrangian coordinate that the floor(s) is the index numbering in the
// curve, while s-floor(s) is the relative position inside the segment it lies in
// nx and ny are components of normal vector, offset is the distance of this 
// intersecting point to the corresponding neighbor mesh grid (along c direction).
// boundary_size is the total size of boundary list, will be filled by the thread
// who deals with the last segment line among all the curves in curve_records
__global__ void kernel_fill_boundary_list
(
int* __restrict__ _idx, // boundarylist::idx
int* __restrict__ _id,  // boundarylist::id, the numbering of boundary, used in multiple boundary conditions
double* __restrict__ _offset, // boundarylist::offset, distance between intersecting point and internal neighbor mesh grid.
double* __restrict__ _nx, // boundarylist::nx, normal vector x component
double* __restrict__ _ny, // boundarylist::ny, normal vector y component
double* __restrict__ _s, // boundarylist::s, lagrangian coordiante along the boundary curve
int* __restrict__ _boundary_size, // length of boundarylist
const int* __restrict__ _boundary_entry_from_segment, // pre-assigned entry position of first boundary pair in each segment
const char* __restrict__ _tag, // the tag map, a mesh grid is previously tagged with -1(internal) or 1(external) if it's a boundary grid
const REAL* __restrict__ _x, // x coordinate of the curve
const REAL* __restrict__ _y, // y coordiante of the curve
const int len, // curve length
const bool is_circle, // is this a close curve
const int id, // numbering of boundary set by user
const bool is_last_curve, // true if this is the last curve in curve_records
const int m, // simulation domain size
const int n, // simulation domain size
const bool is_x_periodic, // true if this domain is periodic along x side
const bool is_y_periodic // true if this domain is periodic along y side
)
{
    int idx0 = blockDim.x*blockIdx.x + threadIdx.x;
    int len_segment = len - (int)(!is_circle);

    // write all boundary pairs on this segment into boundary list
    if (idx0 < len_segment)
    {
        int boundary_list_entry = _boundary_entry_from_segment[idx0];

        double x0, y0, x1, y1;
        device_read_and_push_vertex_away_from_integer(x0, y0, x1, y1, idx0, len, _x, _y);

        double dx, dy;
        int i0, i1, di, ilen, is_x_reversed;
        int j0, j1, dj, jlen, is_y_reversed;
        device_prepare_search_span_in_one_direction(x0, x1, dx, is_x_reversed, i0, i1, di, ilen);
        device_prepare_search_span_in_one_direction(y0, y1, dy, is_y_reversed, j0, j1, dj, jlen);

        // calculate normal vector from dx and dy
        double segment_length = sqrt(dx*dx + dy*dy);
        double nx = -dy / segment_length;
        double ny = dx / segment_length;

        // set the micro velocity orientation
        int ci = (7 + di) / 2; // if(di==1) c=4; if(di==-1) c=3; // avoid branching
        int cj = (3 - dj) / 2; // if(dj==1) c=1; if(dj==-1) c=2; // avoid branching

        // count the number of valid intersecting pairs of boundary grids along this segment
        int count_boundary_pair = 0;

        // write boundaries of x direction
        for (int i = 0; i != ilen; ++i)
        {
            int i_tag = i0 + di*i;

            // calculate the lagrangian coordinate of this intersecting point
            //       fractional part           integer part
            double s = ((double)i_tag - x0) / dx + (double)idx0;

            int j_tag_in, j_tag_ex;
            double distance_in, distance_ex;
            device_calculate_tag_position_from_gridline<0>(
                x0, y0, dx, dy, is_x_reversed, di,
                i_tag, j_tag_in, j_tag_ex, distance_in, distance_ex,
                m, n, is_x_periodic, is_y_periodic);

            // make sure this pair of intersecting grids (i_tag,j_tag_in) and (i_tag,j_tag_ex) both
            // lie in the mesh
            if (i_tag >= 0 & i_tag < m & j_tag_in >= 0 & j_tag_in < n & j_tag_ex >= 0 & j_tag_ex < n)
            {
                // this is a valid pair if internal grid is tagged as "non 1"
                // while external grid is tagged as "non -1", thus, add it into the count
                if ((_tag[INDEX(i_tag, j_tag_in)] != 1) & (_tag[INDEX(i_tag, j_tag_ex)] != -1))
                {
                    // now fill boundary info into the boundary lists
                    int idx_boundary_list = boundary_list_entry + count_boundary_pair;
                    _idx[idx_boundary_list] = i_tag + j_tag_in*m + ci*m*n;
                    _offset[idx_boundary_list] = distance_in;
                    _nx[idx_boundary_list] = nx;
                    _ny[idx_boundary_list] = ny;
                    _s[idx_boundary_list] = s;
                    _id[idx_boundary_list] = id;

                    count_boundary_pair += 1;

                }
            }
        }

        // write boundaries of y direction
        for (int j = 0; j != jlen; ++j)
        {
            int j_tag = j0 + dj*j;

            // calculate the lagrangian coordinate of this intersecting point
            //       remainder part          integer part
            double s = ((double)j_tag - y0) / dy + (double)idx0;

            int i_tag_in, i_tag_ex;
            double distance_in, distance_ex;
            device_calculate_tag_position_from_gridline<1>(
                y0, x0, dy, dx, is_y_reversed, dj,
                j_tag, i_tag_in, i_tag_ex, distance_in, distance_ex,
                n, m, is_y_periodic, is_x_periodic);

            // make sure this pair of intersecting grids (i_tag,j_tag_in) and (i_tag,j_tag_ex) both
            // lie in the mesh
            if (i_tag_in >= 0 & i_tag_in < m & i_tag_ex >= 0 & i_tag_ex < m & j_tag >= 0 & j_tag < n)
            {
                // this is a valid pair if internal grid is tagged as "non 1"
                // while external grid is tagged as "non -1", add it into the count
                if ((_tag[INDEX(i_tag_in, j_tag)] != 1) & (_tag[INDEX(i_tag_ex, j_tag)] != -1))
                {
                    // now fill boundary info into the boundary lists
                    int idx_boundary_list = boundary_list_entry + count_boundary_pair;
                    _idx[idx_boundary_list] = i_tag_in + j_tag*m + cj*m*n;
                    _offset[idx_boundary_list] = distance_in;
                    _nx[idx_boundary_list] = nx;
                    _ny[idx_boundary_list] = ny;
                    _s[idx_boundary_list] = s;
                    _id[idx_boundary_list] = id;

                    count_boundary_pair += 1;
                }
            }
        } // end of "write boundaries of y direction"

        // if this is the last segment in last curve, then write the total boundary list length into _boundary_size
        if (is_last_curve)
        {
            if (idx0 == len_segment - 1)
            {
                *_boundary_size = boundary_list_entry + count_boundary_pair;
            }
        }

    } // end of "write all boundary pairs on this segment into boundary list"
}

void LBMGPUvoxelizer::fill_boundary_list
(
int* __restrict__ _idx,
int* __restrict__ _id,
double* __restrict__ _offset,
double* __restrict__ _nx,
double* __restrict__ _ny,
double* __restrict__ _s,
int* __restrict__ _boundary_size
)
{
    for (int i = 0; i != curve_records_size; ++i) // issue filling boundary list on all curves
    {
        auto& curve_record = curve_records[i];
        int len = curve_record.len;
        kernel_fill_boundary_list <<< (len + BSIZE1D - 1) / BSIZE1D, BSIZE1D, 0, cudaStream[curve_records[i].id_cudaStream] >>>
            (
            _idx,
            _id,
            _offset,
            _nx,
            _ny,
            _s,
            _boundary_size,
            boundary_entry_from_segment.data_ptr() + curve_record.idx_entry,
            tag.data_ptr(),
            curve_record._x,
            curve_record._y,
            len,
            curve_record.is_circle,
            curve_record.id_boundary,
            i == curve_records_size - 1,
            m,
            n,
            is_x_periodic,
            is_y_periodic
            );
    } // end "issue filling boundary list on all curves"

    for (int i = 0; i != curve_records_size; ++i){
        cudaStreamSynchronize(cudaStream[curve_records[i].id_cudaStream]);
    } // after synchronization boundary list will be safe to use
}

// this method will scan along the whole tag map to check if a grid (i,j)'s tags in current
// and previous time step are opposite (-1 to 1 or 1 to -1). If so, then "f" and "rho"
// quantity at this position will be extrapolated from its same-sided neighbors.
// the scanning can be accelerated if a boundary grids list (no repeating elements) is
// provided (this is not done yet)
//
// Since grids that need recovery are small but scattered in a block, it is a wast to have
// very few of kernels running heavy task while others are all idle.
// Currently, the tag_8connect check is done by a single thread, as it is a very heavy burden,
// to evenly spread this task into the block is suggested. this modification should be done but
// not planed yet in Nov 2017
__global__ void kernel_recoverMovingBoundary
(
REAL* __restrict__ _f,
REAL* __restrict__ _rho,
const char* __restrict__ _tag1, // tag map of current step
const char* __restrict__ _tag0, // tag map of previous step
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int i = blockDim.x*blockIdx.x + threadIdx.x;
    int j = blockDim.y*blockIdx.y + threadIdx.y;
    
    if (i < m & j < n)
    {
        char tag0 = _tag0[INDEX(i, j)];
        char tag1 = _tag1[INDEX(i, j)];
        if (tag0 * tag1 == -1) //((tag0 == -1 & tag1 == 1) | (tag0 == 1 & tag1 == -1))
        {
            // this position needs extrapolation
            // neibouring points:      weight:    (): branch
            //(6)       (2)      (5)                   
            //   /14    12    16\      /-1    -4    -1\
            //   |    6  4  8   |      |    2  8  2   |
            //(3)| 9  1 [0] 2 10|(1)   |-4  8 [0] 8 -4|
            //   |    5  3  7   |      |    2  8  2   |
            //   \13    11    15/      \-1    -4    -1/
            //(7)      (4)       (8)       
            // a branch is a set of grids in current and previous step on a ray from the
            // center point, a branch is invalid if at least one of the grid has an opposite
            // tag_8connection to tag1
            // only valid branch will be count into the extrapolation
            int din, djn; // relative radial position of a extrapolation branch
            int in1, jn1, in2, jn2;
            REAL f[5] = { (REAL)0.0 };
            REAL rho = (REAL)0.0;
            int w_sum = 0;
            const int branchi[8] = { 1, 0, -1, 0, 1, -1, -1, 1 };
            const int branchj[8] = { 0, 1, 0, -1, 1, 1, -1, -1 };
            const int weight[8] = { 4, 4, 4, 4, 1, 1, 1, 1 };

            
            // sum up along each branch
            for (int iter = 0; iter != 8; ++iter)
            {
                din = branchi[iter];
                djn = branchj[iter];
                in1 = i + din;
                jn1 = j + djn;
                in2 = i + din * 2;
                jn2 = j + djn * 2;
                if (VALID_POSITION(in1, jn1) & VALID_POSITION(in2, jn2))
                {
                    in1 = POSITIVE_MOD(in1, m);
                    in2 = POSITIVE_MOD(in2, m);
                    jn1 = POSITIVE_MOD(jn1, n);
                    jn2 = POSITIVE_MOD(jn2, n);
                    bool is_branch_valid =
                        (device_tag_8connected(in1, jn1, m, n, _tag0, is_x_periodic, is_y_periodic) != -tag1) &&
                        (device_tag_8connected(in2, jn2, m, n, _tag0, is_x_periodic, is_y_periodic) != -tag1) &&
                        (device_tag_8connected(in1, jn1, m, n, _tag1, is_x_periodic, is_y_periodic) != -tag1) &&
                        (device_tag_8connected(in2, jn2, m, n, _tag1, is_x_periodic, is_y_periodic) != -tag1);
                    if (is_branch_valid)
                    {
#pragma unroll
                        for (int iter_f = 0; iter_f != 5; ++iter_f){
                            f[iter_f] += (weight[iter] *(
                                _f[INDEX(in1, jn1) + iter_f*m*n] * 2 -
                                _f[INDEX(in2, jn2) + iter_f*m*n]));
                        }
                        rho += (weight[iter] * (_rho[INDEX(in1, jn1)] * 2 - _rho[INDEX(in2, jn2)]));
                        w_sum += weight[iter];
                    }
                }
            } // end "sum up along each branch"

            // fill exrapolated value back to _f and _rho
            if (w_sum > 0){
#pragma unroll
                for (int iter_f = 0; iter_f != 5; ++iter_f){
                    _f[INDEX(i, j) + iter_f*m*n] = f[iter_f] / (REAL)w_sum;
                }
                _rho[INDEX(i, j)] = rho / (REAL)w_sum;
            }
        } // end "this position needs extrapolation"
    } // if (i < m & j < n)
    
}

#define N_APRON 2
// A faster recoverMovingBoundary kernel that spreads tasks onto all threads
__global__ void kernel_recoverMovingBoundary_withSharedMem 
(
REAL* __restrict__ _f,
REAL* __restrict__ _rho,
const char* __restrict__ _tag1, // tag map of current step
const char* __restrict__ _tag0, // tag map of previous step
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    // prefix / suffix meaning: s - shared array, b - block

    const int ms = BSIZEM + N_APRON * 2;
    const int ns = BSIZEN + N_APRON * 2;

    // tag value under 8 connectivity definition
    //  0: not boundary
    //  1: external
    // -1: internal
    // -2: invalid position
    __shared__ char stag0_8connected[ms * ns];
    __shared__ char stag1_8connected[ms * ns];

    // number of points that needs recover
    __shared__ int nRecover;

    int ib = threadIdx.x; // block idx x
    int jb = threadIdx.y; // block idx y
    int bidx = ib + jb * BSIZEM; // block idx
    int i = blockDim.x*blockIdx.x + threadIdx.x;
    int j = blockDim.y*blockIdx.y + threadIdx.y;

    char tag0, tag1;

    // initialize nRecover
    if (bidx == 0) nRecover = 0; __syncthreads();
    if (i < m & j < n)
    {
        // read tag value (under definition connection-8-neighbor) of current and previous step
        tag0 = _tag0[INDEX(i, j)];
        tag1 = _tag1[INDEX(i, j)];
        /*
        if (true) // (tag0 != tag1) // this lattice might needs to be recovered
        {
            // tag0 * tag1 == -1 means this lattice has for sure flipped in this time step, thus needs to be recovered
            if (tag0 * tag1 == -1)
            {
                atomicAdd(&nRecover, 1);
            }
            else
            {
                // needs to check with 8-connection definition
                tag0 = device_tag_8connected(i, j, m, n, _tag0, is_x_periodic, is_y_periodic);
                tag1 = device_tag_8connected(i, j, m, n, _tag1, is_x_periodic, is_y_periodic);
                if (tag0 * tag1 == -1)
                {
                    printf("(%i,%i) tag 8 connnection works here\n", i, j);
                    atomicAdd(&nRecover, 1);
                }
            }
        }
        */
        if (tag0 * tag1 == -1)
        {
            atomicAdd(&nRecover, 1);
        }

    }
    __syncthreads();

    if (nRecover < 1) return;
    // debug 20171218
    // if (bidx == 0) printf("block(%i, %i), nRecover = %i\n", blockIdx.x, blockIdx.y, nRecover);

    // if nRecover > 0, then this block has lattice that needs recover
    // all threads will build stag_8connected together
    int sidx = bidx;
    while (sidx < ms*ns){
        int is = sidx % ms; // idx in shared array
        int js = sidx / ms; // idx in shared array
        int ig = blockDim.x*blockIdx.x + is - N_APRON; // idx for global fetch
        int jg = blockDim.y*blockIdx.y + js - N_APRON; // idx for global fetch
        if (VALID_POSITION(ig, jg))
        {
            ig = POSITIVE_MOD(ig, m);
            jg = POSITIVE_MOD(jg, n);
            stag0_8connected[sidx] = _tag0[INDEX(ig, jg)];
                //device_tag_8connected(ig, jg, m, n, _tag0, is_x_periodic, is_y_periodic);
            stag1_8connected[sidx] = _tag1[INDEX(ig, jg)];
                //device_tag_8connected(ig, jg, m, n, _tag1, is_x_periodic, is_y_periodic);
        }
        else
        {
            stag0_8connected[sidx] = -2; // a simple marker for invalid position
            stag1_8connected[sidx] = -2; // a simple marker for invalid position
        }
        sidx += (BSIZEM*BSIZEN);
    }
    __syncthreads();

    if (i < m & j < n)
    {

        if (tag0 * tag1 == -1)
        {
            // this position needs extrapolation
            // neibouring points:      weight:    (i): ith branch
            //(6)       (2)      (5)                   
            //   /14    12    16\      /-1    -4    -1\
            //   |    6  4  8   |      |    2  8  2   |
            //(3)| 9  1 [0] 2 10|(1)   |-4  8 [0] 8 -4|
            //   |    5  3  7   |      |    2  8  2   |
            //   \13    11    15/      \-1    -4    -1/
            //(7)      (4)       (8)       
            // a branch is a set of grids in current and previous step on a ray emitted from the
            // center point, a branch is invalid if at least one of the grid has an opposite
            // tag_8connection value to tag1.
            // only valid branch will be count into the extrapolation
            // if a 2nd order extrapolation fails, a 1st order attempt will be done.

            //  neighbors
            const int branchi[8] = { 1, 0, -1, 0, 1, -1, -1, 1 };
            const int branchj[8] = { 0, 1, 0, -1, 1, 1, -1, -1 };
            // extrapolation weights
            const int weight[8] = { 4, 4, 4, 4, 1, 1, 1, 1 };

            int din, djn; // relative radial position of a extrapolation branch
            int in1, jn1, in2, jn2;
            REAL f[5] = { (REAL)0.0 };
            REAL rho = (REAL)0.0;
            int w_sum_1st_order = 0;
            int w_sum_2nd_order = 0;
            bool is_branch_valid_1st_order[8];
            bool is_branch_valid_2nd_order[8];

            // sum up along each branch
            for (int iter = 0; iter != 8; ++iter)
            {
                din = branchi[iter];
                djn = branchj[iter];

                // calculate validity of this branch
                int sidx1 = ib + N_APRON + din + (jb + N_APRON + djn) * ms;
                int sidx2 = ib + N_APRON + din * 2 + (jb + N_APRON + djn * 2) * ms;
                char stag01 = stag0_8connected[sidx1];
                char stag02 = stag0_8connected[sidx2];
                char stag11 = stag1_8connected[sidx1];
                char stag12 = stag1_8connected[sidx2];
                is_branch_valid_1st_order[iter] =
                    ((stag01 != -2) & (stag11 != -2) & (stag01 != -tag1) & (stag11 != -tag1));
                is_branch_valid_2nd_order[iter] = is_branch_valid_1st_order[iter] &
                    ((stag02 != -2) & (stag12 != -2) & (stag02 != -tag1) & (stag12 != -tag1));

                // printf("position(%i, %i), branch (%i, %i) validity = %i\n", i, j, din, djn, is_branch_valid);

                w_sum_2nd_order += weight[iter] * (int)is_branch_valid_2nd_order[iter];
                w_sum_1st_order += weight[iter] * (int)is_branch_valid_1st_order[iter];
            } // end sum up along each branch

            if (w_sum_2nd_order <= 0 && w_sum_1st_order <= 0)
            {
                // debug
                // printf("warning, tag_8conn(%i,%i) = %i has no neighbors for refilling.\n", i, j, (int)tag1);
                return;
            }

            if (w_sum_2nd_order > 0) // 2nd order extrapolation
            // if (false) // debug, use 1st order extrapolation
            {
                // sum up along valid branches
                for (int iter = 0; iter != 8; ++iter)
                {
                    din = branchi[iter];
                    djn = branchj[iter];
                    if (is_branch_valid_2nd_order[iter])
                    {
                        in1 = POSITIVE_MOD(i + din, m);
                        in2 = POSITIVE_MOD(i + din * 2, m);
                        jn1 = POSITIVE_MOD(j + djn, n);
                        jn2 = POSITIVE_MOD(j + djn * 2, n);
                        for (int iter_f = 0; iter_f != 5; ++iter_f)
                        {
                            f[iter_f] += (
                                weight[iter] * (_f[INDEX(in1, jn1) + iter_f*m*n] * 2 - _f[INDEX(in2, jn2) + iter_f*m*n])
                                );
                            rho += f[iter_f];
                        }
                        /* rho += (
                            weight[iter] * (_rho[INDEX(in1, jn1)] * 2 - _rho[INDEX(in2, jn2)])
                            );*/
                    }
                } // end summing up along valid branches

                // fill exrapolated value back to _f and _rho
                for (int iter_f = 0; iter_f != 5; ++iter_f){
                    _f[INDEX(i, j) + iter_f*m*n] = f[iter_f] / w_sum_2nd_order;
                }
                _rho[INDEX(i, j)] = rho / w_sum_2nd_order;
            } //end 2nd order extrapolation
            else
            {
                if (w_sum_1st_order > 0) // 1st order extrapolation
                {
                    // debug
                    // printf("warning, tag_8conn(%i,%i) = %i uses 1st order precision refilling.\n", i, j, (int)tag1);
                    // sum up along valid branches
                    for (int iter = 0; iter != 8; ++iter)
                    {
                        din = branchi[iter];
                        djn = branchj[iter];
                        if (is_branch_valid_1st_order[iter])
                        {
                            in1 = POSITIVE_MOD(i + din, m);
                            jn1 = POSITIVE_MOD(j + djn, n);
                            for (int iter_f = 0; iter_f != 5; ++iter_f)
                            {
                                f[iter_f] += (
                                    weight[iter] * (_f[INDEX(in1, jn1) + iter_f*m*n])
                                    );
                                rho += f[iter_f];
                            }
                            /*rho += (
                                weight[iter] * (_rho[INDEX(in1, jn1)])
                                );*/
                        }
                    } // end summing up along valid branches

                    // fill exrapolated value back to _f and _rho
                    for (int iter_f = 0; iter_f != 5; ++iter_f){
                        _f[INDEX(i, j) + iter_f*m*n] = f[iter_f] / w_sum_1st_order;
                    }
                    _rho[INDEX(i, j)] = rho / w_sum_1st_order;
                } // end 1st order extrapolation
            }
        } // end "this position needs extrapolation"
    } // if (i < m & j < n)

}


// A faster recoverMovingBoundary kernel that spreads tasks onto all threads
// this is a 1st order precision recovering method that conserves the mass
#define N_APRON1 1
__global__ void kernel_recoverMovingBoundary_withSharedMem_mass_conserving
(
REAL* __restrict__ _f,
REAL* __restrict__ _rho,
const char* __restrict__ _tag1, // tag map of current step
const char* __restrict__ _tag0, // tag map of previous step
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    // prefix / suffix meaning: s - shared array, b - block

    const int ms = BSIZEM + N_APRON * 2;
    const int ns = BSIZEN + N_APRON * 2;

    // tag value under 8 connectivity definition
    //  0: not boundary
    //  1: external
    // -1: internal
    // -2: invalid position
    __shared__ char stag0_8connected[ms * ns];
    __shared__ char stag1_8connected[ms * ns];

    // number of points that needs recover
    __shared__ int nRecover;

    int ib = threadIdx.x; // block idx x
    int jb = threadIdx.y; // block idx y
    int bidx = ib + jb * BSIZEM; // block idx
    int i = blockDim.x*blockIdx.x + threadIdx.x;
    int j = blockDim.y*blockIdx.y + threadIdx.y;

    char tag0, tag1;

    // initialize nRecover
    if (bidx == 0) nRecover = 0; __syncthreads();
    if (i < m & j < n)
    {
        // read tag value of current and previous step
        tag0 = _tag0[INDEX(i, j)];
        tag1 = _tag1[INDEX(i, j)];

        // tag0 * tag1 == -1 means this lattice needs to be recovered
        if (tag0 * tag1 == -1) atomicAdd(&nRecover, 1);
    }
    __syncthreads();

    if (nRecover < 1) return;
    // debug 20171218
    // if (bidx == 0) printf("block(%i, %i), nRecover = %i\n", blockIdx.x, blockIdx.y, nRecover);

    // if nRecover > 0, then this block has lattice that needs recover
    // all threads will build stag_8connected together
    int sidx = bidx;
    while (sidx < ms*ns){
        int is = sidx % ms; // idx in shared array
        int js = sidx / ms; // idx in shared array
        int ig = blockDim.x*blockIdx.x + is - N_APRON; // idx for global fetch
        int jg = blockDim.y*blockIdx.y + js - N_APRON; // idx for global fetch
        if (VALID_POSITION(ig, jg))
        {
            ig = POSITIVE_MOD(ig, m);
            jg = POSITIVE_MOD(jg, n);
            stag0_8connected[sidx] =
                device_tag_8connected(ig, jg, m, n, _tag0, is_x_periodic, is_y_periodic);
            stag1_8connected[sidx] =
                device_tag_8connected(ig, jg, m, n, _tag1, is_x_periodic, is_y_periodic);
        }
        else
        {
            stag0_8connected[sidx] = -2; // a simple marker for invalid position
            stag1_8connected[sidx] = -2; // a simple marker for invalid position
        }
        sidx += (BSIZEM*BSIZEN);
    }
    __syncthreads();

    if (i < m & j < n)
    {
        // debug 20171218 display shared array stag1_8connected
        /*
        if (bidx == 0)
        {
            printf("block = (%i,%i), share_tag1:\n", blockDim.x, blockDim.y);
            for (int jj = 0; jj != ns; ++jj)
            {
                for (int ii = 0; ii != ms; ++ii)
                {
                    printf("%3i", stag1_8connected[ii + jj*ms]);
                }
                printf("\n");
            }
        }
        */

        if (tag0 * tag1 == -1)
        {
            // this position needs extrapolation
            // neibouring points:      weight:    (i): ith branch
            //(6)       (2)      (5)                   
            //   /14    12    16\      /-1    -4    -1\
            //   |    6  4  8   |      |    2  8  2   |
            //(3)| 9  1 [0] 2 10|(1)   |-4  8 [0] 8 -4|
            //   |    5  3  7   |      |    2  8  2   |
            //   \13    11    15/      \-1    -4    -1/
            //(7)      (4)       (8)       
            // a branch is a set of grids in current and previous step on a ray emitted from the
            // center point, a branch is invalid if at least one of the grid has an opposite
            // tag_8connection value to tag1.
            // only valid branch will be count into the extrapolation

            // extrapolation weights
            const int branchi[8] = { 1, 0, -1, 0, 1, -1, -1, 1 };
            const int branchj[8] = { 0, 1, 0, -1, 1, 1, -1, -1 };
            const int weight[8] = { 4, 4, 4, 4, 1, 1, 1, 1 };
            int neighbor_validity[8]; // 1: yes, new neighbor, -1:old neighbor, 0: invalid position (typically out of bound)

            REAL w_old_sum = 0;
            REAL w_new_sum = 0;

            // sum up along each branch
            for (int iter = 0; iter != 8; ++iter)
            {
                // calculate validity of this branch
                int sidx1 = ib + N_APRON + branchi[iter] + (jb + N_APRON + branchj[iter]) * ms;
                char stag01 = stag0_8connected[sidx1];
                char stag11 = stag1_8connected[sidx1];
                bool is_branch_valid = (stag01 != -2) & (stag11 != -2);
                bool is_branch_new = (stag01 != -tag1) & (stag11 != -tag1);
                neighbor_validity[iter] =
                    (int)(is_branch_valid & is_branch_new) * 1 +
                    (int)(is_branch_valid & (!is_branch_new)) * (-1);
                w_old_sum += (int)(neighbor_validity[iter] == -1) * weight[iter];
                w_new_sum += (int)(neighbor_validity[iter] == +1) * weight[iter];
            }

            // migrate the mass of this grid into old neighbors
            for (int iter = 0; iter != 8; ++iter)
            {
                if (neighbor_validity[iter] == -1)
                {
                    int in = POSITIVE_MOD(i + branchi[iter], m);
                    int jn = POSITIVE_MOD(j + branchj[iter], n);
                    int idxn = INDEX(in, jn);
                    int idx = INDEX(i, j);
                    REAL portion_migrate = weight[iter] / w_old_sum;
                    for (int ci = 0; ci != 5; ++ci)
                    {
                        _f[idxn + ci*m*n] += _f[idx + ci*m*n] * portion_migrate;
                    }
                    _rho[idxn] += _rho[idx] * portion_migrate;
                }
            }

            // migrate the mass of new neighbors into this grid
            REAL rhotemp = 0.0;
            REAL ftemp[5]; for (int iter = 0; iter != 5; ++iter) ftemp[iter] = 0.0;
            for (int iter = 0; iter != 8; ++iter)
            {
                if (neighbor_validity[iter] == 1)
                {
                    int in = POSITIVE_MOD(i + branchi[iter], m);
                    int jn = POSITIVE_MOD(j + branchj[iter], n);
                    int idxn = INDEX(in, jn);
                    REAL portion_migrate = weight[iter] / w_new_sum;
                    for (int ci = 0; ci != 5; ++ci)
                    {
                        ftemp[ci] += _f[idxn + ci*m*n] * portion_migrate;
                        _f[idxn + ci*m*n] *= (1.0 - portion_migrate);
                    }
                    rhotemp += _rho[idxn] * portion_migrate;
                    _rho[idxn] *= (1.0 - portion_migrate);
                }
            }
            int idx = INDEX(i, j);
            _rho[idx] = rhotemp;
            for (int ci = 0; ci != 5; ++ci)
            {
                _f[idx + ci*m*n] = ftemp[ci];
            }

        } // end "this position needs extrapolation"
    } // if (i < m & j < n)

}

void LBMGPUvoxelizer::recoverMovingBoundary(REAL* __restrict__ _f, REAL* __restrict__ _rho){
    const dim3 blockSize(BSIZEM, BSIZEN);
    dim3 gridSize((m + BSIZEM - 1) / BSIZEM, (n + BSIZEN - 1) / BSIZEN);
    kernel_recoverMovingBoundary_withSharedMem <<< gridSize, blockSize >>>
    //kernel_recoverMovingBoundary_withSharedMem_mass_conserving <<< gridSize, blockSize >>>
        (
        _f,
        _rho,
        tag.data_ptr(),
        tag_previous.data_ptr(),
        m,
        n,
        is_x_periodic,
        is_y_periodic
        );
}

void LBMGPUvoxelizer::setBoundaryCurveFinished
(
int* __restrict__ _idx,
int* __restrict__ _id,
double* __restrict__ _offset,
double* __restrict__ _nx,
double* __restrict__ _ny,
double* __restrict__ _s,
int* __restrict__ _boundary_size,
REAL* __restrict__ _f,
REAL* __restrict__ _rho,
const bool is_write_boundary
)
{

    // step.1 make tag map
    //tag.af_save("tag_ini", "tag_ini.afdat");
    makeTag();
    //debug
    //da_print(DeviceArray<int>(tag));

    // step.2 count boundary size of each segment
    // calculate the exclusive prefix sum as entry index for each segment
    fill_boundary_size_on_segment();
    //da_saveAF(boundary_size_on_segment); //debug
    
    prefixSum(
        boundary_size_on_segment.data_ptr(),
        boundary_entry_from_segment.data_ptr(),
        get_total_segment_size());
    cudaStreamSynchronize(cudaStreamDefault); // prefixSum uses the default cuda stream
    //da_saveAF(boundary_entry_from_segment); //debug
    

    if (is_write_boundary)
    {
        // step.3 fill the boundary list
        fill_boundary_list(_idx, _id, _offset, _nx, _ny, _s, _boundary_size);

        // step.4 recover mesh grids who flipped during this step
        recoverMovingBoundary(_f, _rho);
        cudaStreamSynchronize(cudaStreamDefault);
    }

    // step.5 clear curve_records_size and save tag (swap pointer) into previous step
    DA::swap_data_ptr(tag, tag_previous);
    cudaStreamSynchronize(cudaStreamDefault);
    curve_records_size = 0;
    tag = (char)0;
}

__global__ void kernel_trim_label_by_curve
(
int* __restrict__ _label,
char* __restrict__ _tag,
const int val_in,
const int val_ex,
const int len,
const int m,
const int n
)
{
    int i = blockDim.x*blockIdx.x + threadIdx.x;
    int j = blockDim.y*blockIdx.y + threadIdx.y;
    if (i < m &j < n){
        int idx = INDEX(i, j);
        char tag = _tag[idx];
        if (tag == -1)
            _label[idx] = val_in;
        if (tag == 1)
            _label[idx] = val_ex;
    }
}



void LBMGPUvoxelizer::trim_label_by_curve
(
int* __restrict__ _label,
const int val_in,
const int val_ex,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int len
)
{
    DeviceArray<char> trim_tag((char)0, m, n);
    kernel_makeTag <<< (len + BSIZE1D - 1) / BSIZE1D, BSIZE1D >>>
        (
        trim_tag.data_ptr(),
        mutex.data_ptr(),
        min_distance.data_ptr(),
        _x,
        _y,
        len,
        true,
        m,
        n,
        is_x_periodic,
        is_y_periodic
        );

    const int BSIZEX = 32;
    const int BSIZEY = 16;
    const dim3 blockSize(BSIZEX, BSIZEY);
    dim3 gridSize((m + BSIZEX - 1) / BSIZEX, (n + BSIZEY - 1) / BSIZEY);
    kernel_trim_label_by_curve <<<gridSize, blockSize>>>
        (_label, trim_tag.data_ptr(), val_in, val_ex, len, m, n);
    min_distance = (double)2.0;
    cudaStreamSynchronize(cudaStreamDefault);
}


// 4-connection flood fill cpu recursive kernel for tag map
void flood_fill_cpu_recursive_kernel
(
int* _tag,
const int i,
const int j,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
#define TAG_VAL_IN -1
#define TAG_VAL_EX 1
#define IS_NON_BOUNDARY(val) ((val) != TAG_VAL_EX && (val) != TAG_VAL_IN)
    const int di[4] = { 1, 0, -1, 0 };
    const int dj[4] = { 0, 1, 0, -1 };
    
    if (!VALID_POSITION(i, j)) return;

    int seed = _tag[INDEX(i, j)];
    if (IS_NON_BOUNDARY(seed)) return;

    for (int iter = 0; iter != 4; ++iter)
    {
        int in = i + di[iter];
        int jn = j + dj[iter];
        if (VALID_POSITION(in, jn))
        {
            in = POSITIVE_MOD(in, m);
            jn = POSITIVE_MOD(jn, n);
            int val = _tag[INDEX(in, jn)];
            if (IS_NON_BOUNDARY(val))
            {
                _tag[INDEX(in, jn)] = seed;
                flood_fill_cpu_recursive_kernel(_tag, in, jn, m, n, is_x_periodic, is_y_periodic);
            }
        }
    }
}

// x-connection flood fill cpu recursive kernel for tag map
// added by Zhe Gou on 20211222
void flood_fill_x_cpu_recursive_kernel
(
int* _tag,
const int i,
const int j,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
#define TAG_VAL_IN -1
#define TAG_VAL_EX 1
#define IS_NON_BOUNDARY(val) ((val) != TAG_VAL_EX && (val) != TAG_VAL_IN)
    const int di[2] = { 1, -1 };
    const int dj[2] = { 0, 0 };
    
    if (!VALID_POSITION(i, j)) return;

    int seed = _tag[INDEX(i, j)];
    if (IS_NON_BOUNDARY(seed)) return;

    for (int iter = 0; iter != 2; ++iter)
    {
        int in = i + di[iter];
        int jn = j + dj[iter];
        if (VALID_POSITION(in, jn))
        {
            in = POSITIVE_MOD(in, m);
            jn = POSITIVE_MOD(jn, n);
            int val = _tag[INDEX(in, jn)];
            if (IS_NON_BOUNDARY(val))
            {
                _tag[INDEX(in, jn)] = seed;
                flood_fill_x_cpu_recursive_kernel(_tag, in, jn, m, n, is_x_periodic, is_y_periodic);
            }
        }
    }
}

// y-connection flood fill cpu recursive kernel for tag map
// added by Zhe Gou on 20211222
void flood_fill_y_cpu_recursive_kernel
(
int* _tag,
const int i,
const int j,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
#define TAG_VAL_IN -1
#define TAG_VAL_EX 1
#define IS_NON_BOUNDARY(val) ((val) != TAG_VAL_EX && (val) != TAG_VAL_IN)
    const int di[2] = { 0, 0 };
    const int dj[2] = { 1, -1 };
    
    if (!VALID_POSITION(i, j)) return;

    int seed = _tag[INDEX(i, j)];
    if (IS_NON_BOUNDARY(seed)) return;

    for (int iter = 0; iter != 2; ++iter)
    {
        int in = i + di[iter];
        int jn = j + dj[iter];
        if (VALID_POSITION(in, jn))
        {
            in = POSITIVE_MOD(in, m);
            jn = POSITIVE_MOD(jn, n);
            int val = _tag[INDEX(in, jn)];
            if (IS_NON_BOUNDARY(val))
            {
                _tag[INDEX(in, jn)] = seed;
                flood_fill_y_cpu_recursive_kernel(_tag, in, jn, m, n, is_x_periodic, is_y_periodic);
            }
        }
    }
}

// a simple flood fill algorithm that fills all points in a tag map (defined previously in voxelizer)
// with internal -1 and external 1. Simply implemented with Depth-first search method
// THIS SHIT IS VERY SLOW, DON'T USE IT IN A GPU LOOP
// stack overflow for large geometry
// commented by Zhe Gou on 20211222
/*void flood_fill_cpu
(
int* _tag,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    for (int j = 0; j != n; ++j)
    {
        for (int i = 0; i != m; ++i)
        {
            int val = _tag[INDEX(i, j)];
            if (!IS_NON_BOUNDARY(val))
            {
                flood_fill_cpu_recursive_kernel(_tag, i, j, m, n, is_x_periodic, is_y_periodic);
            }
        }
    }
}*/

// flood fill algorithm
// added by Zhe Gou on 20211222
void flood_fill_cpu
(
int* _tag,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    // flood fill in x direction
    for (int j = 0; j != n; ++j)
    {
        for (int i = 0; i != m; ++i)
        {
            int val = _tag[INDEX(i, j)];
            if (!IS_NON_BOUNDARY(val))
            {
                flood_fill_x_cpu_recursive_kernel(_tag, i, j, m, n, is_x_periodic, is_y_periodic);
            }
        }
    }

    // flood fill in y direction
    for (int j = 0; j != n; ++j)
    {
        for (int i = 0; i != m; ++i)
        {
            int val = _tag[INDEX(i, j)];
            if (!IS_NON_BOUNDARY(val))
            {
                flood_fill_y_cpu_recursive_kernel(_tag, i, j, m, n, is_x_periodic, is_y_periodic);
            }
        }
    }
}

DeviceArray<int> LBMGPUvoxelizer::build_filled_tag_map()
{
    auto temp_tag = DeviceArray<int>(tag_previous);
    std::vector<int> htag(m*n);
    temp_tag.host(&htag[0]);
    flood_fill_cpu(&htag[0], m, n, is_x_periodic, is_y_periodic);
    return DeviceArray<int>(&htag[0], m, n);
}

// a helper function that split boundary curve into segments with a length less than dl
// this function works on cpu, don't use it in a GPU loop
// be careful, this function will change the value of x_refined.data_ptr() and y_refined.data_ptr()
void LBMGPUvoxelizer::refine_curve
(
DeviceArray<REAL>& x_refined,
DeviceArray<REAL>& y_refined,
const REAL* __restrict__ _x_original,
const REAL* __restrict__ _y_original,
const int len,
const REAL dl,
const bool is_circular
)
{
    if (len < 2 + (int)is_circular) return;

    std::vector<REAL> hx_original(len);
    std::vector<REAL> hy_original(len);
    std::vector<REAL> hx_refined(0);
    std::vector<REAL> hy_refined(0);
    cudaMemcpy(&hx_original[0], _x_original, sizeof(REAL)*len, cudaMemcpyDeviceToHost);
    cudaMemcpy(&hy_original[0], _y_original, sizeof(REAL)*len, cudaMemcpyDeviceToHost);
    for (int i = 0; i != len - 1 + (int)is_circular; ++i)
    {
        REAL x0 = hx_original[i];
        REAL x1 = hx_original[(i + 1) % len];
        REAL y0 = hy_original[i];
        REAL y1 = hy_original[(i + 1) % len];
        REAL xs = x1 - x0;
        REAL ys = y1 - y0;
        REAL s = std::sqrt(xs*xs + ys*ys);
        int n = std::ceil(s / dl);
        REAL dx = xs / n;
        REAL dy = ys / n;
        for (int j = 0; j != n; ++j)
        {
            hx_refined.push_back(x0 + dx*j);
            hy_refined.push_back(y0 + dy*j);
        }
    }
    if (!is_circular)
    {
        hx_refined.push_back(hx_original[len - 1]);
        hy_refined.push_back(hy_original[len - 1]);
    }
    x_refined = DeviceArray<REAL>(&hx_refined[0], hx_refined.size());
    y_refined = DeviceArray<REAL>(&hy_refined[0], hy_refined.size());
}