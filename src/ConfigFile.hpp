#ifndef ConfigFile_hpp
#define ConfigFile_hpp
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>

//*************************************************************************************************
/*
Methods for input and output for non-arrayfire properties,
configs, vectors and so on.
Chameleon and ConfigFile ref:
http://www.adp-gmbh.ch/cpp/config_file.html
http://www.adp-gmbh.ch/cpp/chameleon.html

Thank Ren�� Nyffenegger
http://stackoverflow.com/users/180275/ren%C3%A9-nyffenegger

code modyfied by Hengdi ZHANG April 10, 2016
try to return default value when fail to find a match
1. replace the return of Value method from const reference to value

*/
//*************************************************************************************************
/*
Chameleon.h

Copyright (C) 2002-2004 Rene Nyffenegger

This source code is provided 'as-is', without any express or implied
warranty. In no event will the author be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this source code must not be misrepresented; you must not
claim that you wrote the original source code. If you use this source code
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original source code.

3. This notice may not be removed or altered from any source distribution.

Ren�� Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/
//*************************************************************************************************
class Chameleon {
public:
    Chameleon() {};
    Chameleon(const std::string&);
    Chameleon(double);
    Chameleon(const char*);

    Chameleon(const Chameleon&);
    Chameleon& operator=(Chameleon const&);

    Chameleon& operator=(double);
    Chameleon& operator=(std::string const&);

    // Added April 20 2016
    inline std::string getContent() const{ return this->value_; }

public:
    operator std::string() const;
    operator double() const;
private:
    std::string value_;
};
//*************************************************************************************************
// a class who helps define variables from configure file
// use macro DEFINE_FROM_CONFIGFILE to define a variable after creation of an instance
//
// example:
//   **********************************************************
//   in cpp:
//   ConfigFile cfg('config.txt');
//   // (type, name, ConfigFile name, category, default value)
//   DEFINE_FROM_CONFIGFILE(const int, m, cfg, "geometry", 1000);
//   DEFINE_FROM_CONFIGFILE(const double, dt, cfg, "numerical", 1e-4);
//   DEFINE_FROM_CONFIGFILE(const double, dx, cfg, "numerical", 1e-2);
//   // when dealing with string, USE std::string instead of c char*
//   DEFINE_FROM_CONFIGFILE(std::string, curve_file_name, cfg, "numerical", std::string("default.dat"));
//   **********************************************************
//   in config.txt:
//   category tag in brackets
//   [numerical]
//   // comment is formatted from Matlab, C++ or fortran
//   //dt = 0.1 // time step, commented
//   dx = 0.1; % spatial grid size, semicolon is not necessary
//   [geometry]
//   m = 500 ! size of the model, expressions are unrecognizable
//   curve_file_name = curve.txt // make sure file name contains no blank
//   **********************************************************
//   run and display on screen:
//   Load from file, geometry::m = 500
//   Set by default, numerical::dt = 0.0001
//   Load from file, numerical::dx = 0.1
//   Load from file, numerical::curve_file_name = curve.txt
//   **********************************************************
class ConfigFile {
    std::map<std::string, Chameleon> content_;
    std::string filename_;
public:
    ConfigFile(std::string const& configFile);

    Chameleon Value(std::string const& section, std::string const& entry) const;
    bool checkValue(std::string const& section, std::string const& entry) const;

    // string loader under linux
    std::string sValue(std::string const& section, std::string const& entry) const;

    Chameleon Value(std::string const& section, std::string const& entry, double value);
    Chameleon Value(std::string const& section, std::string const& entry, std::string const& value);

    void loadValue(std::string const& section, std::string const& entry, std::string& value);
    void loadValue(std::string const& section, std::string const& entry, double& value);
    void loadValue(std::string const& section, std::string const& entry, float& value);
    void loadValue(std::string const& section, std::string const& entry, int& value);

    template<typename T>
    inline T loadValueOrSetByDefault
        (
        std::string const& section,
        std::string const& entry,
        const T& defalut_value
        )
    {
        T value = defalut_value;
        loadValue(section, entry, value);
        if (this->checkValue(section, entry)){
            std::cout << "Load from file, ";
        }
        else{
            std::cout << "Set by default, ";
        }
        std::cout << section << "::" << entry << " = " << value << std::endl;
        return value;
    }


};

// a macro for defining a variable from config file
#define DEFINE_FROM_CONFIGFILE(type, configFile, sectionName, variable, defaultValue) \
type variable = (configFile).loadValueOrSetByDefault(sectionName, #variable, (type)(defaultValue))

#define READ_FROM_CONFIGFILE(configFile, sectionName, variable, defaultValue) \
variable = (configFile).loadValueOrSetByDefault(sectionName, #variable, (type)(defaultValue))

//*************************************************************************************************
// a simple helper that reads 1-dim vector / 2-dim matrix in ascii format
// not optimized for large matrix, could be slow
class DataFile{
private:
    std::string filename;
    std::vector< std::vector<double> > data;

    int m; // number of columms
    int n; // number of rows
public:
    // create data from file
    // m is decided by the longest line, lines shorter than m will be padded with zeros
    // if skip_empty_row is true, an empty line will be ignored else it will be padded with zeros
    DataFile(const std::string& filename, bool skip_empty_row = true);
    void transpose();
    std::vector< std::vector<double> >& getData();
    inline int get_m(){ return m; }
    inline int get_n(){ return n; }
};

// a simple helper to read 2d bmp into std::vector<int>
class BMPdataFile{
private:
    std::string filename; // file name
    std::vector<int> R; // red
    std::vector<int> G; // green
    std::vector<int> B; // blue
    std::vector<int> A; // alpha
    std::vector<int> gray; // gray-scale data
    std::vector<int> bin; // binary-scale data, calculated by simple OTSU thresholding method

    int m; // width
    int n; // height
public:
    BMPdataFile(const std::string& filename);
    inline std::vector<int>& get_R(){ return R; }
    inline std::vector<int>& get_G(){ return G; }
    inline std::vector<int>& get_B(){ return B; }
    inline std::vector<int>& get_A(){ return A; }
    inline std::vector<int>& get_gray(){ return gray; }
    inline std::vector<int>& get_binary(){ return bin; }
    inline int get_width(){ return m; }
    inline int get_height(){ return n; }

};

#endif