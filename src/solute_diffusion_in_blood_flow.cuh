#ifndef solute_diffusion_in_blood_flow_cuh
#define solute_diffusion_in_blood_flow_cuh

#include <iostream>
#include <fstream>
#include <cmath>
#include "LBMheader.cuh"

inline void solute_diffusion_in_blood_flow(const int zero_for_make_mask_only = 0)
{
    ConfigFile cfg("solute_diffusion_in_blood_flow.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", is_using_initial_flow_field, 0);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "switch", initial_flow_field, std::string("initial_flow_field_afdat"));
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation solute_diffusion_in_blood_flow not issued\n");
        return;
    }
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", m, 1000);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n, 102);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tend, 100000);

    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", is_red_blood_cells_on, 1);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", is_red_blood_cells_transparent_to_solute, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", is_using_analytical_gaussian_boundary, 0); // setting as 0 will omit the arbitrary boundary shape read from vessel_boundary_filename
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", h_thrombosis, 0.6); // height of thrombosis site, normalized by channel width
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", w_thrombosis, 0.6); // width of thrombosis site, normalized by channel width
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", x0_thrombosis, 0.5); // position of thrombosis site, normalized by channel length


    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", viscosity, 0.166666666666666667);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", umax_straight_channel, 0.005);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", W_characteristic, (REAL)(n - 2)); // characteristic width of simulation
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", fx, 8.0*viscosity*umax_straight_channel / W_characteristic / W_characteristic);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_ves, 1000); // total export frames for vesicle shape
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_flow, 100); // total export frames for field information
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_prestep, 1000000);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", R0, 13.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", tau, 0.7); // reduced area
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", Ca, 50.0); // capillary number defined with wall shear rate at constriction
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kl, 0.3); // vesicle strech modulus (set it adequately large tu ensure membrane inextensibility)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", l0, 0.6667);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", damp, 0.01); // damp factor in relaxation process
    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", is_using_initial_shape, 0); // number of vesicles
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_shape_filename, std::string("initial_vesicle_shape.txt"));
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_position_filename, std::string("initial_vesicle_position.txt"));

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "liposome", R0_liposome, 8.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "liposome", tau_liposome, 0.95); // reduced area
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "liposome", Ca_liposome, 0.1); // capillary number defined with wall shear rate at constriction
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "liposome", x0_liposome, (REAL)m * 0.1);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "liposome", y0_liposome, (REAL)(n - 2) * 0.5);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "liposome", damp_liposome, 0.1);
    DEFINE_FROM_CONFIGFILE(int, cfg, "liposome", x_position_liposome_start_release, 200000);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "solute", Pe, 50); // peclet number defined as u_max*R0/D
    DEFINE_FROM_CONFIGFILE(int, cfg, "solute", boundary_type, 0); // vessel boundary type for solute, 1 for dirichlet(instant absorption), 0 for neumann (full barrier)


    DEFINE_FROM_CONFIGFILE(std::string, cfg, "geometry", vessel_boundary_filename, std::string("vessel_boundary.txt"));


    DeviceArray<REAL> Fx(fx, m, n);
    DeviceArray<REAL> Fy(0.0, m, n);

    // make label
    DeviceArray<int> label_flow(0, m, n);
    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, -(REAL)0.5, -(REAL)0.5, (REAL)1.0, (REAL)1.0, m, n);

    // build a voxelizer to help constructing label and boundary curves
    LBMGPUvoxelizer* voxelizer = new LBMGPUvoxelizer(m, n, false, false, 100000, 100);
    DataFile vessel_boundary(vessel_boundary_filename.c_str());
    int n_bound = vessel_boundary.get_n() / 3;
    if (is_using_analytical_gaussian_boundary)
    {
        n_bound = 2;
    }
    printf("boundary curve number = %i\n", n_bound);
    std::vector< DeviceArray<REAL> > x_bound(n_bound);
    std::vector< DeviceArray<REAL> > y_bound(n_bound);

    for (int i = 0; i != n_bound; ++i)
    {
        DeviceArray<REAL> x_bound_original, y_bound_original;
        if (is_using_analytical_gaussian_boundary) // using analytical gaussian hill
        {
            if (i == 0)
            {
                REAL h_th = (REAL)(n - 2)*h_thrombosis;
                REAL w_th = (REAL)(n - 2)*w_thrombosis * 0.5;
                REAL x0_th = (REAL)m * x0_thrombosis;
                x_bound_original = DA::range(-0.5, l0 * 2, std::ceil((REAL)m / (l0 * 2)) + 1);
                y_bound_original = 0.5 + h_th / exp((x_bound_original - x0_th)*(x_bound_original - x0_th) / 2.0 / w_th / w_th);
            }
            if (i == 1)
            {
                std::vector<REAL> x_upper_bound({ m - 0.5, -0.5 });
                std::vector<REAL> y_upper_bound({ n - 1.5, n - 1.5 });
                x_bound_original = DeviceArray<REAL>(&x_upper_bound[0], x_upper_bound.size());
                y_bound_original = DeviceArray<REAL>(&y_upper_bound[0], y_upper_bound.size());

            }
        }
        else // using "vessel_boundary_filename" to initialized the vessel shape
        {
            x_bound_original = DeviceArray<REAL>
                (
                &vessel_boundary.getData()[i * 3 + 1][0],
                vessel_boundary.getData()[i * 3 + 1].size()
                );
            y_bound_original = DeviceArray<REAL>
                (
                &vessel_boundary.getData()[i * 3 + 2][0],
                vessel_boundary.getData()[i * 3 + 2].size()
                );
        }

        bool is_circle_bound;
        if (is_using_analytical_gaussian_boundary)
            is_circle_bound = false;
        else
            is_circle_bound = std::abs(vessel_boundary.getData()[i * 3][0]) > 1e-5;

        LBMGPUvoxelizer::refine_curve
            (
            x_bound[i],
            y_bound[i],
            x_bound_original.data_ptr(),
            y_bound_original.data_ptr(),
            x_bound_original.size(),
            1.0,
            is_circle_bound
            );

        voxelizer->setBoundaryCurve
            (
            x_bound[i].data_ptr(),
            y_bound[i].data_ptr(),
            x_bound[i].size(),
            is_circle_bound,
            0
            );
    }
    voxelizer->setBoundaryCurveFinished(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, false);
    label_flow = voxelizer->build_filled_tag_map();
    delete voxelizer;
    label_flow = -1 * DeviceArray<int>(label_flow == -1);
    x_bound[0].af_save("xb0", "xb0.afdat");
    y_bound[0].af_save("yb0", "yb0.afdat");
    x_bound[1].af_save("xb1", "xb1.afdat");
    y_bound[1].af_save("yb1", "yb1.afdat");
    da_saveAF(label_flow);

    // make a mask map file for random initial positioning of vesicles
    if (zero_for_make_mask_only == 0)
    {
        DeviceArray<int> mask = label_flow;
        mask -= DeviceArray<int>(abs(X - x0_liposome) < R0_liposome + 4.0 && abs(Y - y0_liposome) < R0_liposome + 4.0);
        mask.transpose();
        mask.save2D("mask.txt");
        printf("mask.txt built\n");
        printf("task done!\n");
        return;
    }


    NSsolver flow(viscosity, m, n, NS::PERIODIC, NS::BOUNCEBACK, Fx.data_ptr(), Fy.data_ptr(), label_flow.data_ptr());

    flow.ibm.xBoundary.af_save("xbound", "xbound.afdat");
    flow.ibm.yBoundary.af_save("ybound", "ybound.afdat");

    REAL umax = 0.0;
    if (is_using_initial_flow_field)
    {
        flow.initializeByFile(initial_flow_field.c_str());
    }
    else
    {
        flow.initializeByDensity();
        //pre-step for initializing flow field
        for (int i = 0; i != flow_prestep; ++i)
        {
            if ((i + 1) % std::max(1, (flow_prestep / 100)) == 0 || i == 0 || i == flow_prestep - 1){
                std::cout << "prestep, iteration = " << i + 1;
                std::vector<REAL> hux(flow.ux.size());
                std::vector<REAL> huy(flow.ux.size());
                flow.ux.host(&hux[0]);
                flow.uy.host(&huy[0]);
                REAL umaxnow = 0.0;
                for (int j = (int)((double)hux.size()*0.49); j != (int)((double)hux.size()*0.51); ++j){
                    REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
                    umaxnow = std::max(umaxnow, uj);
                }
                std::cout << ", u_at_probe = " << umaxnow << std::endl;
                if (std::abs(umaxnow - umax) < 1e-9 && umaxnow >= umax_straight_channel*0.1 && i != 0){
                    printf("u_at_probe reached its maximum, end prestep\n");
                    break;
                }
                umax = umaxnow;
            }
            flow.advanceFlowFieldWithoutImmersedObjects();
        }
        flow.af_save_flow_field(initial_flow_field.c_str());
    }

    // calculate umax
    std::vector<REAL> hux(flow.ux.size());
    std::vector<REAL> huy(flow.ux.size());
    flow.ux.host(&hux[0]);
    flow.uy.host(&huy[0]);
    REAL umaxnow = 0.0;
    for (int j = 0; j != hux.size(); ++j){
        REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
        umaxnow = std::max(umaxnow, uj);
    }
    umax = umaxnow;
    printf("umax = %f\n", umax);
    REAL Re = umax * R0 / viscosity;
    printf("Reynolds number in simulation = umax * R0 / viscosity = %f\n", Re);

    // vesicle bending modulous
    REAL kb = viscosity * R0*R0*R0 / l0 * 16.0 * umax / W_characteristic / Ca;
    printf("vesicle bending modulous kb (simulation) = %f\n", kb);

    Vesicle ves_template(0.0, 0.0, 0.0, R0, tau, kb, kl, 200.0, l0);
    if (is_using_initial_shape == 0)
    {
        ves_template.relax(10000000, damp);
        ves_template.exportShape(initial_shape_filename.c_str());
    }

    // make vesicles
    DataFile vesicle_initial_position(initial_position_filename.c_str());
    int nves = vesicle_initial_position.get_n(); // total vesicle number
    if (!is_red_blood_cells_on) nves = 0;
    printf("total vesicle number = %i\n", nves);
    std::vector<Vesicle*> vec_vesicles(nves);
    for (int i = 0; i != nves; ++i)
    {
        vec_vesicles[i] = new Vesicle
            (
            vesicle_initial_position.getData()[i][0], // x position
            vesicle_initial_position.getData()[i][1], // y position
            vesicle_initial_position.getData()[i][2], // inclination
            R0, tau, kb, kl, 200.0, l0, initial_shape_filename.c_str(), true);
        flow.immerseAnObject(vec_vesicles[i]);
    }

    // make a liposome
    REAL kb_liposome = viscosity * R0_liposome*R0_liposome*R0_liposome / l0 * 16.0 * umax / W_characteristic / Ca_liposome;
    printf("liposome bending modulous kb_liposome (simulation) = %f\n", kb_liposome);

    auto* liposome = new Vesicle(x0_liposome, y0_liposome, 0.0, R0_liposome, tau_liposome, kb_liposome, kl, 200.0, l0);
    liposome->relax(10000000, damp_liposome);
    flow.immerseAnObject(liposome);

    flow.immerseObjectFinished();
    printf("%i vesicles' initialization complete.\n", nves);

    REAL D_simu = umax_straight_channel*R0 / Pe;
    printf("D_simulation = %f\n", D_simu);

    // make solute solver
    ADsolver solute(
        D_simu,
        m, n, 60000,
        flow.ux.data_ptr(),
        flow.uy.data_ptr(),
        nullptr, nullptr,
        AD::PERIODIC,
        AD::PERIODIC);

    // set liposone with a constant initial concentration
    DeviceArray<int> c_ini(0, m, n);
    liposome->updateLabel(c_ini.data_ptr(), m, n, true, false, 1, 0);

    solute.setInitialConcentration(DeviceArray<REAL>(c_ini));

    // liposome position check:
    // malloc on cpu (pinned memory)
    REAL* p_box_liposome;
    cudaMallocHost((void**)&p_box_liposome, 4 * sizeof(REAL), cudaHostAllocDefault);
    bool is_liposome_not_yet_releasing = true;

    SimpleTimer timer;

    timer.tic();

    for (int i = 0; i != Tend; ++i)
    {

        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->calculateForce();
        liposome->calculateForce();

        if (!is_red_blood_cells_transparent_to_solute)
            for (int ives = 0; ives != nves; ++ives)
                solute.registerCurve(vec_vesicles[ives]->get_x(), vec_vesicles[ives]->get_y(), true, 0);

        // register vessel boundary
        for (int ibound = 0; ibound != x_bound.size(); ++ibound)
        {
            bool is_circular;
            if (is_using_analytical_gaussian_boundary) is_circular = false;
            else is_circular = std::abs(vessel_boundary.getData()[ibound * 3][0]) > 1e-5;
            solute.registerCurve(x_bound[ibound], y_bound[ibound], is_circular, boundary_type);
        }

        // register liposome boundary
        if (i % 10 == 0 && is_liposome_not_yet_releasing)
        {
            liposome->findBox();
            cudaMemcpy(p_box_liposome, liposome->box.data_ptr(), 4 * sizeof(REAL), cudaMemcpyDeviceToHost);
            if (p_box_liposome[1] > x_position_liposome_start_release)
            {
                is_liposome_not_yet_releasing = false;
            }
        }

        if (is_liposome_not_yet_releasing)
            // the liposome doesn't release drug, build a barrier
            solute.registerCurve(liposome->get_x(), liposome->get_y(), true, 0);

        solute.registerCurveFinished();

        // set boundary type
        solute.boundary.Ctype_ex = solute.boundary.id;

        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->sync_operation();
        liposome->sync_operation();


        flow.ibm.add_short_range_repulsive_force();

        if (LBM::getStepStampFlag(i, frames_ves, Tend))
        {
            printf("i = %i, Tend = %i\n", i, Tend);
        }

        if (LBM::getStepStampFlag(i, frames_ves, Tend))
        {
            printf("vesicle shapes snapshot\n");
            flow.ibm.export_all_objects_shapes_af("sx.afdat", "sy.afdat", i != 0);
            solute.rho.af_save("rho", "rho.afdat", i != 0);
            liposome->El.af_save("El", "EL.afdat", i != 0);
            if (i == 0) solute.boundary.exportToFile_ArrayFire("bound0.afdat");

        }

        if (LBM::getStepStampFlag(i, frames_flow, Tend))
        {
            printf("flow field snapshot\n");
            flow.ux.af_save("ux", "ux.afdat", i != 0);
            flow.uy.af_save("uy", "uy.afdat", i != 0);

            // debug export
            /*
            flow.ibm.eul_fx.af_save("eul_fx", "eul_fx.afdat", i != 0);
            flow.ibm.eul_fy.af_save("eul_fy", "eul_fy.afdat", i != 0);
            auto fabs = sqrt(flow.ibm.eul_fx * flow.ibm.eul_fx + flow.ibm.eul_fy * flow.ibm.eul_fy);
            fabs.af_save("eul_f", "eul_f.afdat", i != 0);
            flow.ibm.cellList.count.af_save("cellListCount", "cellList_count.afdat", i != 0);
            flow.ibm.neighborList.id.af_save("neighbor_id", "neighbor_id.afdat", i != 0);
            flow.ibm.neighborList.count.af_save("neighbor_count", "neighbor_count.afdat", i != 0);
            flow.ibm.neighborList.block_idx.af_save("block_idx", "block_idx.afdat", i != 0, *flow.ibm.neighborList.ptr_block_list_len);
            */
        }

        solute.advanceTimeStep();
        flow.advanceFlowField();

        flow.advanceImmersedObjects();

    }
    cudaDeviceSynchronize();
    timer.toc();


    cudaFreeHost(p_box_liposome);
    delete liposome;

}

#endif