#ifndef LBMglobal_hpp
#define LBMglobal_hpp

// LBM parameters

// precision used in LBM
//#define REAL float
#define REAL double

#define _LBM_MRT_SE 1.1
#define _LBM_MRT_SEPS 1.1
#define _LBM_MRT_SQ 1.1

// [Class].cuh is the header file of class
// [Class].cu contains all implementations and kernel functions
// no cpp wrappers for kernel funcitons
#endif