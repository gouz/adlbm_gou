#ifndef RBC_flow_local_rheology_cuh
#define RBC_flow_local_rheology_cuh

#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>

// random number generator
#include <random>

// LBM header
#include "LBMheader.cuh"


inline void RBC_flow_local_rheology()
{
    ConfigFile cfg("RBC_flow_local_rheology.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation RBC_flow_local_rheology not issued\n");
        return;
    }

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", is_using_initial_flow_field, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", is_using_repulsive_force, 1);
    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", is_using_random_initial_position, 1);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "switch", initial_flow_field, std::string("initial_flow_field_afdat"));

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Hematocrit, 0.81);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Width, 5.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Length, 10.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", R0, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", tau, 0.7);

    // this Capillary number is actually defining the velocity profile ux = Ca*(W-y)*y
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Ca, 1.0);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", viscosity, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", kappa_b, 1.0);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Tend, 10000);

    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames_ves, 1000); // total export frames for vesicle shape
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames_flow, 100); // total export frames for field information
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames_relax, 100); // total export frames for field information
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", R0_simu, 15.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", l0_simu, 0.67); // grid length ratio between vesicle and flow
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", Reynolds_simu, 0.1);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", viscosity_simu, 1.0 / 6.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", ks, 200.0); // area penalty factor 
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", kl, 0.3); // local stretching penalty factor 
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", kb_relax, 0.1); // vesicle modulus for relax only
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", kl_relax, 0.1); // vesicle modulus for relax only
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", ks_relax, 0.1); // vesicle modulus for relax only
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", inclination, 0.0); // inclination
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", a_r0_ratio_template, 3.4); // long diameter for relaxed vesicle at tau=0.7
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", b_r0_ratio_template, 1.2); // long diameter for relaxed vesicle at tau=0.7
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", damp, 0.1); // relaxation damp factor
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", is_using_initial_shape, 0); // use template shape is true
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", flow_prestep, 1000000);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "numerical", initial_shape_filename, std::string("rbc_initial_shape_hint.txt"));
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", nrelax_high_hematocrit, 1000000);

    // specify numerical width "n", modify physical Width if needed
    int n = (int)std::round(R0_simu * Width);
    REAL Width_modified = n / R0_simu;
    if (std::abs(Width_modified - Width) > 1e-9)
    {
        printf("Width is modified from %f into %f.\n", Width, Width_modified);
        Width = Width_modified;
    }

    // calculate total number of vesicles
    int nves = (int)std::round(Hematocrit * Width * Length / (PI*R0*R0));
    printf("Vesicle number: %i\n", nves);
    
    // modify Length to adjust Hematocrit value
    REAL Length_modified = std::round(nves * (PI*R0*R0) / Hematocrit / Width * R0_simu) / R0_simu;
    if (std::abs(Length_modified - Length) > 1e-9)
    {
        printf("Length is modified from %f into %f.\n", Length, Length_modified);
        Length = Length_modified;
    }

    // spacify numerical length "m"
    int m = (int)std::round(R0_simu * Length);

    // calculate actual Hematocrit
    REAL Hematocrit_modified = PI * nves / (Width*Length);
    if (std::abs(Hematocrit_modified - Hematocrit) > 1e-9)
    {
        printf("Hematocrit is modified from %f into %f.\n", Hematocrit, Hematocrit_modified);
        Hematocrit = Hematocrit_modified;
    }

    // simulation box size
    printf("m = %i\n", m);
    printf("n = %i\n", n);

    // calculate u_max and body force in simulation
    // REAL umax_simu = viscosity_simu * Reynolds_simu / R0_simu;
	REAL umax_simu = viscosity_simu * n * Reynolds_simu / 4.0 / R0_simu / R0_simu;
    printf("umax_simu = %f\n", umax_simu);
    REAL fx_simu = 8.0*viscosity_simu*umax_simu / n / n;
    printf("fx_simu = %.17f\n", fx_simu);

    // calculate delta t: dt, delta x: dx
    // dt is derived from umax and umax_simu
    // you can't use viscosity info to get dt, since Re_simu is a fake value
    REAL umax_phy = Ca * (Width*0.5)*(Width*0.5);
    printf("umax = max (Ca * (Width-y) * y) = %f * %f^2 = %f\n", Ca, Width*0.5, umax_phy);
    REAL dx = R0 / R0_simu;
    // REAL dt = viscosity_simu / viscosity * dx*dx; // this is wrong
    REAL dt = umax_simu / umax_phy * dx;
    printf("dx = R0 / R0_simu = %f\n", dx);
    printf("dt = umax_simu / umax * dx = %f\n", dt);
    // calculate total step
    int Nstep = (int)std::ceil(Tend / dt);

    // calculate vesicle kappa_b_simu and spring bending constant kb: kappa_b_simu = (kb*l0)/4
    // define the capillary number as wall shear rate * viscosity * R0^3 / kappa_b
    REAL capillary_number = Ca * Width * viscosity * R0*R0*R0 / kappa_b;
    REAL kappa_b_simu = 4 * umax_simu / n * viscosity_simu * R0_simu*R0_simu*R0_simu / capillary_number;
    REAL kb = 4 * kappa_b_simu / l0_simu;

    // build flow field
    DeviceArray<int> label_flow(0, m, n + 2);
    DeviceArray<int> X, Y;
    DA::meshgrid(X, Y, 0, 0, 1, 1, m, n + 2);
    label_flow = 0 - (DeviceArray<int>(Y == 0) + DeviceArray<int>(Y == n + 1));

    DeviceArray<REAL> Fx(fx_simu, m, n + 2); Fx = Fx * DeviceArray<REAL>(label_flow == 0);
    DeviceArray<REAL> Fy(0.0, m, n + 2);

    NSsolver flow(
        viscosity_simu,
        m,
        n + 2,
        NS::PERIODIC,
        NS::BOUNCEBACK,
        Fx.data_ptr(),
        Fy.data_ptr(),
        label_flow.data_ptr(),
        umax_simu*2.0);
    flow.initializeByDensity();
    flow.ibm.xBoundary.af_save("xbound", "xbound.afdat");
    flow.ibm.yBoundary.af_save("ybound", "ybound.afdat");

    Vesicle ves_template(0.0, 0.0, inclination, R0_simu, tau, kb, kl, ks, l0_simu);
    if (is_using_initial_shape == 0 && !nrelax_high_hematocrit)
    {
        ves_template.relax(10000000, damp);
        ves_template.exportShape(initial_shape_filename.c_str());
    }

    // REAL a = a_r0_ratio_template * R0_simu + 2.0; // not used
    REAL b = b_r0_ratio_template * R0_simu + 2.0;
    int nves_y = std::floor(n / b);
    int nves_x = std::ceil((REAL)nves / (REAL)nves_y);
    REAL ybox = (REAL)n / (REAL)nves_y;
    REAL xbox = (REAL)m / (REAL)nves_x;

    std::vector<Vesicle*> vec_vesicles(nves);
    std::vector<REAL> xpos_list;
    std::vector<REAL> ypos_list;
    for (int i = 0; i != nves; ++i)
    {
        int ipos = i / nves_y;
        int jpos = i % nves_y;
        REAL xpos = ipos * xbox + 0.5*xbox;
        REAL ypos = jpos * ybox + 0.5*ybox + 0.5;
        if (nrelax_high_hematocrit)
        {
            if (is_using_random_initial_position)
            {
                std::uniform_real_distribution<double> unif_x(0 + 2, m - 1 - 2);
                std::default_random_engine re_x;
                std::uniform_real_distribution<double> unif_y(0 + 2, n + 1 - 2);
                std::default_random_engine re_y;
                xpos = unif_x(re_x);
                ypos = unif_y(re_y);

            }
            vec_vesicles[i] = new Vesicle(
                xpos, ypos, inclination, R0_simu, tau, kb, kl, ks, l0_simu,
                nullptr, true,0.5);
        }
        else
        {
            vec_vesicles[i] = new Vesicle(
                xpos, ypos, inclination, R0_simu, tau, kb, kl, ks, l0_simu,
                initial_shape_filename.c_str(), true);
        }
        flow.immerseAnObject(vec_vesicles[i]);
        printf("Vesicle No. %i is added at (%f,%f) in simulation box.\n", i, xpos, ypos);
    }
    flow.immerseObjectFinished();
    auto Fx_temp = Fx;
    Fx *= 0.0;


    SimpleTimer timer;

    timer.tic();

    if (nrelax_high_hematocrit)
    {
        printf("Vesicle relaxation at high concentration:\n");
        for (int i = 0; i != nrelax_high_hematocrit; ++i)
        {
            if (i < 0.8*nrelax_high_hematocrit)
            {
                for (int ives = 0; ives != nves; ++ives)
                    vec_vesicles[ives]->calculateForce(kb_relax, kl_relax, ks_relax);
            } else
            {
                for (int ives = 0; ives != nves; ++ives)
                    vec_vesicles[ives]->calculateForce(kb, kl, ks);
            }
            for (int ives = 0; ives != nves; ++ives)
                vec_vesicles[ives]->sync_operation();
            if (is_using_repulsive_force) flow.ibm.add_short_range_repulsive_force();

            if (LBM::getStepStampFlag(i, frames_relax, nrelax_high_hematocrit))
            {
                printf("i = %i, nrelax = %i\n", i, nrelax_high_hematocrit);
                printf("unrelaxed vesicle shapes snapshot\n");
                flow.ibm.export_all_objects_shapes_af("sx_relax.afdat", "sy_relax.afdat", i != 0);
                // printf("flow field snapshot\n");
                // flow.ux.af_save("ux", "ux.afdat", i != 0);
                // flow.uy.af_save("uy", "uy.afdat", i != 0);

            }
            flow.advanceFlowField();
            flow.advanceImmersedObjects();

            if (i % 1000 == 0)
            {
                flow.initializeByDensity();
            }

        }
        cudaDeviceSynchronize();
    }
    Fx = Fx_temp;
    flow.body_force_x = Fx.data_ptr(); //dirty!
    printf("%i vesicles initialization complete.\n", nves);

    // initialize flow field if needed
    REAL umax = 0.0; // actual umax in flow initialization
    if (is_using_initial_flow_field)
    {
        flow.initializeByFile(initial_flow_field.c_str());
    }
    else
    {
        flow.initializeByDensity();
        //pre-step for initializing flow field
        for (int i = 0; i != flow_prestep; ++i)
        {
            if ((i + 1) % std::max(1, (flow_prestep / 100)) == 0 || i == 0 || i == flow_prestep - 1){
                std::cout << "prestep, iteration = " << i + 1;
                std::vector<REAL> hux(flow.ux.size());
                std::vector<REAL> huy(flow.ux.size());
                flow.ux.host(&hux[0]);
                flow.uy.host(&huy[0]);
                REAL umaxnow = 0.0;
                for (int j = (int)((double)hux.size()*0.49); j != (int)((double)hux.size()*0.51); ++j){
                    REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
                    umaxnow = std::max(umaxnow, uj);
                }
                std::cout << ", u_at_probe = " << umaxnow << std::endl;
                if (std::abs(umaxnow - umax) < 1e-9 && umaxnow >= umax_simu*0.1 && i != 0){
                    printf("u_at_probe reached its maximum, end prestep\n");
                    break;
                }
                umax = umaxnow;
            }
            flow.advanceFlowFieldWithoutImmersedObjects();
        }
        flow.af_save_flow_field(initial_flow_field.c_str());
    }

    // start the simulaiton

    for (int i = 0; i != Nstep; ++i)
    {

        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->calculateForce();
        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->sync_operation();
        if (is_using_repulsive_force) flow.ibm.add_short_range_repulsive_force();

        if (LBM::getStepStampFlag(i, frames_ves, Nstep))
        {
            printf("i = %i, Nstep = %i, Tnow = %f, Tend = %f\n", i, Nstep, i*dt, Tend);
        }

        if (LBM::getStepStampFlag(i, frames_ves, Nstep))
        {
			printf("vesicle shapes snapshot\n");

			char fname[100];
			sprintf(fname, "shape_%09d.dat", i);
			flow.ibm.export_all_immersed_object_shapes(fname, 0.5, -0.5, dx, dx);
			flow.ibm.export_all_objects_shapes_af("sx.afdat", "sy.afdat", i != 0);

        }

        if (LBM::getStepStampFlag(i, frames_flow, Nstep))
        {
            printf("flow field snapshot\n");
            // flow.ux.af_save("ux", "ux.afdat", i != 0 || nrelax_high_hematocrit != 0);
            // flow.uy.af_save("uy", "uy.afdat", i != 0 || nrelax_high_hematocrit != 0);
			std::vector<REAL> hux(m*(n + 2), 0.0);
			std::vector<REAL> ux_mean(n + 2, 0.0);
			std::vector<REAL> yaxis(n + 2);

			flow.ux.host(&hux[0]);
			for (int j = 0; j != n + 2; ++j)
			{
				yaxis[j] = -0.5 + j*dx;
			}
			yaxis[0] = 0.0;
			yaxis[n+1] = dx*n;

			for (int j = 0; j != n + 2; ++j)
			{
				yaxis[j] = -0.5 + j*dx;
				for (int ii = 0; ii != m; ++ii)
				{
					ux_mean[j] += hux[j*m + ii];
				}
				ux_mean[j] /= (REAL)m;
			}
			char fname[100];
			sprintf(fname, "uxmean_%09d.dat", i);
			std::ofstream output(fname, std::ofstream::trunc);
			output.precision(7);
			output << std::scientific;
			for (int j = 0; j != n + 2; ++j)
			{
				output << std::scientific;
				output << yaxis[j] << " " << ux_mean[j] << std::endl;
			}
        }

        flow.advanceFlowField();
        flow.advanceImmersedObjects();
    }
    cudaDeviceSynchronize();
    timer.toc();


}

#endif // RBC_flow_local_rheology_cuh