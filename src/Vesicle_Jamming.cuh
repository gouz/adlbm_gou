﻿#ifndef Vesicle_Jamming_cuh
#define Vesicle_Jamming_cuh
#include <iostream>
#include <fstream>
#include <cmath>
#include "LBMheader.cuh"

inline void vesicle_jamming()
{
    ConfigFile cfg("vesicle_jamming.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", is_using_initial_flow_field, 0);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "switch", initial_flow_field, std::string("initial_flow_field_afdat"));
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation vesicle_jamming not issued\n");
        return;
    }
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", m, 1001);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n, 201);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tend, 100000);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", viscosity, 0.166666666666666667);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", umax_straight_channel, 0.03);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", fx, 8.0*viscosity*umax_straight_channel / (n - 1) / (n - 1));
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_ves, 1000); // total export frames for vesicle shape
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_flow, 1000); // total export frames for field information
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", R0, 13.0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_prestep, 1000000);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "geometry", L, 4.0*R0); // constriction length
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "geometry", W, 1.6*R0); // constriction width
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "geometry", x0_constriction, 0.5*(m-1)); // constriction centroid x position in the channel

    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", nves, 100); // number of vesicles
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", tau, 0.7); // reduced area
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", Ca, 1.0); // capillary number defined with wall shear rate at constriction
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kl, 0.3); // vesicle strech modulus (set it adequately large tu ensure membrane inextensibility)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", l0, 0.6667);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", a_box, 52.0); // box size for initialization
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", b_box, 21.0); // box size for initialization
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", damp, 0.01); // box size for initialization

    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", is_using_initial_shape, 0); // number of vesicles
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_shape_filename, std::string("rbc_initial_shape_hint.txt"));

    DeviceArray<REAL> Fx(fx, m, n);
    DeviceArray<REAL> Fy(0.0, m, n);

    // make label
    DeviceArray<int> label_flow(0, m, n);
    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, -(REAL)0.5, -(REAL)0.5, (REAL)1.0, (REAL)1.0, m, n);

    REAL xc0 = x0_constriction - 0.5*L;
    REAL xc1 = x0_constriction + 0.5*L;
    REAL yc0 = 0.5*(n - 1) - 0.5*W;
    REAL yc1 = 0.5*(n - 1) + 0.5*W;
    auto constriction_shape =
        (
        (Y < yc0) &&
        (Y < (X - xc0 + yc0)) &&
        (Y < (-X + xc1 + yc0))
        ) ||
        (
        (Y > yc1) &&
        (Y >(-X + xc0 + yc1)) &&
        (Y >(X - xc1 + yc1))
        );
    label_flow -= ((Y > (REAL)(n - 2)) || (Y < 0.0) || constriction_shape);
    da_saveAF(label_flow);


    NSsolver flow(viscosity, m, n, NS::PERIODIC, NS::BOUNCEBACK, Fx.data_ptr(), Fy.data_ptr(),label_flow.data_ptr());

    // debug
    flow.ibm.xBoundary.af_save("xbound", "xbound.afdat");
    flow.ibm.yBoundary.af_save("ybound", "ybound.afdat");

    REAL umax = 0.0;
    if (is_using_initial_flow_field)
    {
        flow.initializeByFile(initial_flow_field.c_str());
    }
    else
    {
        flow.initializeByDensity();
        //pre-step for initializing flow field
        for (int i = 0; i != flow_prestep; ++i)
        {
            if ((i + 1) % std::max(1, (flow_prestep / 100)) == 0 || i == 0 || i == flow_prestep - 1){
                std::cout << "prestep, iteration = " << i + 1;
                std::vector<REAL> hux(flow.ux.size());
                std::vector<REAL> huy(flow.ux.size());
                flow.ux.host(&hux[0]);
                flow.uy.host(&huy[0]);
                REAL umaxnow = 0.0;
                for (int j = (int)((double)hux.size()*0.49); j != (int)((double)hux.size()*0.51); ++j){
                    REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
                    umaxnow = std::max(umaxnow, uj);
                }
                std::cout << ", u_at_probe = " << umaxnow << std::endl;
                if (std::abs(umaxnow - umax) < 1e-9 && umaxnow >= umax_straight_channel*0.1 && i != 0){
                    printf("u_at_probe reached its maximum, end prestep\n");
                    break;
                }
                umax = umaxnow;
            }
            flow.advanceFlowFieldWithoutImmersedObjects();
        }
        flow.af_save_flow_field(initial_flow_field.c_str());
    }

    // calculate umax
    std::vector<REAL> hux(flow.ux.size());
    std::vector<REAL> huy(flow.ux.size());
    flow.ux.host(&hux[0]);
    flow.uy.host(&huy[0]);
    REAL umaxnow = 0.0;
    for (int j = (int)((double)hux.size()*0.49); j != (int)((double)hux.size()*0.51); ++j){
        REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
        umaxnow = std::max(umaxnow, uj);
    }
    umax = umaxnow;

    // vesicle bending modulous
    REAL kb = viscosity * R0*R0*R0 / l0 * 16.0 * umax / W / Ca;
    printf("vesicle bending modulous kb (simulation) = %f\n", kb);

    Vesicle ves_template(0.0, 0.0, 0.0, R0, tau, kb, kl, 200.0, l0);
    if (is_using_initial_shape == 0)
    {
        ves_template.relax(10000000, damp);
        ves_template.exportShape(initial_shape_filename.c_str());
    }

    // make vesicles
    int nVes = (int)std::floor((REAL)(n - 1) / b_box);
    int mVes = (int)std::ceil((REAL)nves / (REAL)nVes);
    REAL b_box_actual = (REAL)(n - 1) / nVes;
    std::vector<Vesicle*> vec_vesicles(nves);
    for (int i = 0; i != nves; ++i)
    {
        int im = i / nVes;
        int in = i % nVes;
        REAL ypos = b_box_actual * (in + 0.5);
        REAL xpos = a_box * (im + 0.5);

        vec_vesicles[i] = new Vesicle(xpos, ypos, 0.0, R0, tau, kb, kl, 200.0, l0, initial_shape_filename.c_str(), true);
        flow.immerseAnObject(vec_vesicles[i]);
    }
    printf("%i vesicles' initialization complete.\n", nves);
    flow.immerseObjectFinished();

    SimpleTimer timer;

    timer.tic();

    for (int i = 0; i != Tend; ++i)
    {

        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->calculateForce();
        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->sync_operation();
        // flow.ibm.add_short_range_repulsive_force();

        if (LBM::getStepStampFlag(i, frames_ves, Tend))
        {
            printf("i = %i, Tend = %i\n", i, Tend);
        }

        if (LBM::getStepStampFlag(i, frames_ves, Tend))
        {
            printf("vesicle shapes snapshot\n");
            flow.ibm.export_all_objects_shapes_af("sx.afdat", "sy.afdat", i != 0);
        }

        if (LBM::getStepStampFlag(i, frames_flow, Tend))
        {
            printf("flow field snapshot\n");
            flow.ux.af_save("ux", "ux.afdat", i != 0);
            flow.uy.af_save("uy", "uy.afdat", i != 0);

            // debug export
            ///*
            flow.ibm.eul_fx.af_save("eul_fx", "eul_fx.afdat", i != 0);
            flow.ibm.eul_fy.af_save("eul_fy", "eul_fy.afdat", i != 0);
            auto fabs = sqrt(flow.ibm.eul_fx * flow.ibm.eul_fx + flow.ibm.eul_fy * flow.ibm.eul_fy);
            fabs.af_save("eul_f", "eul_f.afdat", i != 0);
            flow.ibm.cellList.count.af_save("cellListCount", "cellList_count.afdat", i != 0);
            flow.ibm.neighborList.id.af_save("neighbor_id", "neighbor_id.afdat", i != 0);
            flow.ibm.neighborList.count.af_save("neighbor_count", "neighbor_count.afdat", i != 0);
            flow.ibm.neighborList.block_idx.af_save("block_idx", "block_idx.afdat", i != 0, *flow.ibm.neighborList.ptr_block_list_len);
            //*/
        }

        flow.advanceFlowField();
        flow.advanceImmersedObjects();
    }
    cudaDeviceSynchronize();
    timer.toc();
}

#endif