// Calculate ATP released by multiple RBCs
// Based on RBC_ATP_Release.cuh
// Modified by Zhe Gou at 20191103

// Try to fix 'updatelabel' function for multi-vesicle problem
// Modified by Zhe Gou at 20191120

#ifndef RBC_ATP_Release_multi_vesicle_cuh
#define RBC_ATP_Release_multi_vesicle_cuh
#include <iostream>
#include <fstream>
#include <cmath>
#include "LBMheader.cuh"

class RBC_ATP_Release_multi_vesicle{
private:

    void set_boundary
        (
        DeviceArray<REAL>& cin,
        DeviceArray<REAL>& cex,
        DeviceArray<REAL>& shear_stress,
        DeviceArray<REAL>& curvature_change,
        const int maximum_kernel_launch_size,
        const DeviceArray<int>& boundary_size,
        const DeviceArray<int>& bid,
        const REAL cc_critical, // critical curvature change value, 200 /(um*s)
        const REAL cc_slope, // CFTR-ATP release amplification / cc value
        const REAL ss_critical, // shear stress criteria, Px1 protein
        const REAL dx,
        const REAL dt,
        const REAL k // phenomenological coefficient, k_sigma
        );

    void interpolate_curvature_change
        (
        DeviceArray<REAL>& curvature_change,
        DeviceArray<REAL>& s,
        DeviceArray<int>& bid,
        const DeviceArray<REAL>& ves_curvature_change,
        const int maximum_kernel_launch_size,
        const DeviceArray<int>& boundary_size,
        const int nves // total vesicle number
        );

    // calculate_curvature_change
    // a sav-gal filter along time is used with a stencil of n_history size.
    // i_history indicates that i_history th row in "curvature" stores the current
    // curvature data. previous steps are at its relative position (in a loop manner)
    void calculate_curvature_change
        (
        DeviceArray<REAL>& ves_curvature_change,
        const DeviceArray<REAL>& curvature,
        const int vesicle_size,
        const int n_history,
        const int i_history,
        const REAL dt
        );
    REAL calculate_shear_stress
        (
        const REAL* __restrict__ _ux,
        const int* __restrict__ _label,
        const REAL viscosity_in,
        const REAL viscosity_out,
        const REAL dt,
        const int m,
        const int n
        );

public:
    inline RBC_ATP_Release_multi_vesicle(){};
    inline ~RBC_ATP_Release_multi_vesicle(){};
public:
    // this is a combination of shear flow and poiseuille flow condition
    inline void red_blood_cell_release_atp_in_straight_channel(){

        ConfigFile cfg("red_blood_cell_release_atp_in_straight_channel.txt");

        DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
        if (flag_run == 0)
        {
            printf("flag_run==0, simulation red_blood_cell_release_atp_in_straight_channel not issued\n");
            return;
        }

        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_vesicle_on, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_collision_MRT, 0);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_using_repulsive_force, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_vesicle_relax, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_ATP_on, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_viscosity_labeling, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_x_periodic, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_y_periodic, 0);

        // is_shear_flow==1: this is a shear flow with simple geometry(a long straight periodic channel)
        // is_shear_flow==0: this is a pressure driven flow (typically a poiseuille flow)
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_shear_flow, 1);

        // "_phys" refers to physical unit, "_simu" refers to LB unit
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", viscosity_cyto_phys, 0.012); // red blood cell cytoplasmic viscosity, 12 mPa*s
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", viscosity_contrast, 1.0);
        REAL viscosity_solvent_phys = viscosity_cyto_phys / viscosity_contrast;
        printf("viscosity_solvent_phys = %f Pa*s\n", viscosity_solvent_phys);
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", umax_phys, 800.0e-6); // maximum velocity in physical units, m/s
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", pressure_drop_inclination_phys, 0.0); // the angle between pressure drop direction and x axis, unit: degree
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Datp_phys, (REAL)2.36e-10);

        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", cc_critical_phys, (REAL)2.0e8); // critical curvature change value, 200 /(um*s)
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", cc_slope_phys, (REAL)1.5e-9); // CFTR-ATP release amplification / cc value
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", ss_critical_phys, (REAL)0.03); // Px1 shear stress criteria, 0.01 Pa
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", k_sigma_phys, (REAL)7e-3); // phenomenological coefficient for Px1, k_sigma, [nM/L] * m*s^-1
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", dx_phys, 0.2e-6);

        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", m, 1001); // length
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n, 201); // width
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tend, 2000000);
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tbeg_export, 0); // export starts from this time step
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames, 322); // total export frames for field information
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_boundary, frames / 20); // total export frames for boundary information
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_prestep, 100000);
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", flow_prestep_tolerance, 1e-9);
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", use_initial_f_file, 0);
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "simulation", initial_f_filename, std::string("initial_flow_field_afdat"));
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", use_initial_ad_file, 0);
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "simulation", initial_ad_filename, std::string("initial_ad_file_afdat"));

        // in LB units, this is a fake value in simulation, which makes Re = 0.1 approximately
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", viscosity_simu, 0.166666666666666667);
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", umax_simu, 0.02);
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n_history, 1000);
        
        // calculate shear rate
        REAL shear_rate_phys = umax_phys / (n - 1) / dx_phys; // physical unit
        REAL shear_rate_simu = umax_simu / (n - 1); // LB unit
        if (!is_shear_flow)
        {
            shear_rate_phys = 4 * shear_rate_phys;
            shear_rate_simu = 4 * shear_rate_simu;
        }
        
        // calculate dt in physical unit
        REAL dt_phys = umax_simu / umax_phys * dx_phys; 
        
        // calculate dissipation rate in LB unit
        REAL D_simu = Datp_phys / dx_phys / dx_phys * dt_phys;
        printf("dt_phys = %e\n", dt_phys);
        printf("Datp_LB = %.17f\n", D_simu);

        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", inclination_phys, 0.0); // vesicle initial inclination angle
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", R0ves_simu, 13.0); // characteristic radius
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", tau, 0.693); // reduced area
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kb_simu, 1.0); // bending modulus
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kl_simu, 0.3); // local stretching penalty factor
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", ks_simu, 200.0); // area penalty factor
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", l0, 0.667); // grid length ratio between vesicle and flow
        DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", n_relax, 1000000); // vesicle relaxation steps
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", damp_relax, 0.5); // vesicel relaxation damping factor
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_shape_filename, std::string("initial_vesicle_shape.txt"));
        DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", use_initial_shape_file, 0);

        // this list specifies the initial position, inclination, tau and kb etc. of vesicles.
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", vesicle_initialization_filename,
            std::string("vesicle_initialization.txt"));

        // Peclet number:
        REAL Pe = shear_rate_simu * R0ves_simu*R0ves_simu / D_simu; 
        REAL Pe_simu = umax_simu / D_simu;
        printf("Pe = shear_rate_simu * R0_simu^2 / D_simu = %f\n", Pe);
        printf("Pe_simu = umax_simu / D_simu = %f\n", Pe_simu);
        printf("Pe_simu should be at the order of ~10, at least less than 100!\n");

        std::ofstream shear_stress_file;
        shear_stress_file.open("shear_stress.dat", std::ios::trunc);
        shear_stress_file.close();

        // Reynolds number:
        REAL Re_simu = shear_rate_simu * R0ves_simu*R0ves_simu / viscosity_simu;
        printf("An artificial Reynolds number is set to approach the stokes flow:\n");
        printf("Re_simu = shear_rate_simu * R0_simu^2 / viscosity_simu = %f\n", Re_simu);
        
        // capillary number: 
        REAL kappa_simu = (REAL)0.25 * kb_simu * l0;
        REAL Ca = viscosity_simu * shear_rate_simu * R0ves_simu*R0ves_simu*R0ves_simu / kappa_simu;
        printf("Ca = viscosity_simu * R0_simu^3 * shear_rate_simu / kappa_simu = %.17f\n", Ca);

        // channel confinement
        REAL confinement = 2.0 * R0ves_simu / (n - 1);
        printf("channel confinement = %f\n", confinement);        

        // a flag to label internal zone of vesicle as 1
        const bool do_viscosity_labeling = is_viscosity_labeling;

        // label_flow has its value equal to -1 in solid lattices, 0 in flow lattice, 1 in vesicle with viscosity contrast
        DeviceArray<int> label_flow(0, m, n);
        DeviceArray<REAL> X, Y;
        DA::meshgrid(X, Y, -(REAL)0.5, -(REAL)0.5, (REAL)1.0 / (m - 1), (REAL)1.0 / (n - 1), m, n);
        label_flow -= (abs(Y) > 0.4999); // top and but edge are tagged for imposing shear flow

        da_saveAF(label_flow);
        da_saveAF(X);
        da_saveAF(Y);

        // calculate the body force in simulation
        REAL f_body_simu = 8.0 * viscosity_simu * umax_simu / (n - 1) / (n - 1); // LB unit
        REAL f_body_phys = 8.0 * viscosity_solvent_phys * umax_phys / ((n - 1)*dx_phys) / ((n - 1)*dx_phys); // physical unit
        REAL fx_body_simu = std::cos(pressure_drop_inclination_phys / 180.0 * PI) * f_body_simu;
        REAL fy_body_simu = std::sin(pressure_drop_inclination_phys / 180.0 * PI) * f_body_simu;

        // body force field, initialized as an empty array on GPU, with data_prt = nullptr
        DeviceArray<REAL> Fx_simu;
        DeviceArray<REAL> Fy_simu;
        if (!is_shear_flow)
        {
            // if the flow is not a shear flow, then it is considered as driven by a pressure drop (body force)
            Fx_simu = DeviceArray<REAL>(fx_body_simu, m, n);
            Fy_simu = DeviceArray<REAL>(fy_body_simu, m, n); 
            printf("pressure drop per unit = %f, inclination = %f degree\n", f_body_phys, 
                   pressure_drop_inclination_phys);
        }

        // create flow solver
        std::vector<REAL> viscosity_list({ viscosity_simu, viscosity_simu*viscosity_contrast });
        NSsolver flow(
            &viscosity_list[0], 2,
            m, n,
            is_x_periodic ? NS::PERIODIC : NS::BOUNCEBACK,
            is_y_periodic ? NS::PERIODIC : NS::BOUNCEBACK,
            Fx_simu.data_ptr(),
            Fy_simu.data_ptr(),
            label_flow.data_ptr(),
            umax_simu*2.0);
        
        // initialize flow field or read initial flow field from file
        if (use_initial_f_file)
        {
            flow.initializeByFile(initial_f_filename.c_str());
            printf("Initialize flow using initial_f_file \n");
        }
        else
        {
            flow.initializeByDensity();
            flow.config.IMPOSING_VELOCITY = (is_shear_flow > 0);
            
            // impose velocity boundary as a shear flow
            if (is_shear_flow)
            {
                flow.ux = Y*umax_simu;
                flow.uy = 0.0;
            }

            //pre-step for initializing flow field
            printf("\n");
            flow.develop_steady_flow(flow_prestep, flow_prestep_tolerance);
            flow.af_save_flow_field(initial_f_filename.c_str());
        }
        flow.ux.af_save("ux_ini", "ux_ini.afdat"); // for validation

        // create vesicles by using vesicle template or initial shape file
        DataFile vesicle_initial_position(vesicle_initialization_filename.c_str());
        DataFile ves_initial_shape(initial_shape_filename.c_str());
        int nves = 0; // total vesicle number
        int npoint = 0; // total point number of one vesicle
        if (use_initial_shape_file == 0)
        {
            nves = vesicle_initial_position.get_n(); 
        }
        else
        {
            ves_initial_shape.transpose();
            nves = ves_initial_shape.get_n() / 2;
            npoint = ves_initial_shape.get_m();
        }
        if (!is_vesicle_on) nves = 0;
        printf("total vesicle number = %i\n", nves);

        std::vector< Vesicle* > vesicles(nves);
        if (use_initial_shape_file == 0)
        {
            // create vesicle template and relaxation
            Vesicle ves_template(0.0, 0.0, 0.0, R0ves_simu, tau, 
                kb_simu, kl_simu, ks_simu, l0);
            ves_template.relax(n_relax, damp_relax);
            ves_template.exportShape(initial_shape_filename.c_str());

            for (int ives = 0; ives != nves; ++ives)
            {
                // load values from vesicle initialization file
                REAL xpos = (m - 1) / 2;
                REAL ypos = (n - 1) / 2;
                REAL inclination = inclination_phys;
                auto& ves_ini_data_this = vesicle_initial_position.getData()[ives];
                int len_ves_ini_data_this = ves_ini_data_this.size(); // total initial parameter number
                if (len_ves_ini_data_this > 0) xpos = ves_ini_data_this[0];
                if (len_ves_ini_data_this > 1) ypos = ves_ini_data_this[1];
                if (len_ves_ini_data_this > 2) inclination = ves_ini_data_this[2];
                vesicles[ives] = new Vesicle
                    (
                    xpos, // x position
                    ypos, // y position
                    inclination, // inclination
                    R0ves_simu, tau, kb_simu, kl_simu, ks_simu, l0, 
                    initial_shape_filename.c_str(), true);

                // viscosity labeling
                if (do_viscosity_labeling) 
                    vesicles[ives]->updateLabel(label_flow.data_ptr(), m, n, true, false);

                // immerse the vesicle into flow
                flow.immerseAnObject(vesicles[ives]);
                if (ives == 0) 
                    printf("Add vesicle(s) into flow:\n");
                printf("No.%i: Position = (%.2f,%.2f), Inclination = %.2f, \n", 
                    ives, xpos, ypos, inclination);
                printf("       N_point = %d\n", vesicles[ives]->size());
            }
        }
        else
        {
            // loud values from vesicle initial shape file
            for (int ives = 0; ives != nves; ++ives)
            {
                vesicles[ives] = new Vesicle
                    (
                    &ves_initial_shape.getData()[ives*2][0],
                    &ves_initial_shape.getData()[ives*2 + 1][0],
                    npoint, R0ves_simu, tau, 
                    kb_simu, kl_simu, ks_simu,
                    l0, true);

                // viscosity labeling
                if (do_viscosity_labeling) 
                    vesicles[ives]->updateLabel(label_flow.data_ptr(), m, n, true, false);

                // immerse the vesicle into flow
                flow.immerseAnObject(vesicles[ives]);
                if (ives == 0) 
                    printf("Add vesicle(s) into flow:\n");
                printf("No.%i, \n", ives);
                printf("       N_point = %d\n", vesicles[ives]->size());
            }
        }

        // call immerseObjectFinished() when you finished immersing all vesicles into the flow
        flow.immerseObjectFinished();
        printf("%i vesicles' initialization complete.\n", nves);

        // advection diffusion solver and its boundary
        ADsolver diffusion
            (
            D_simu, // diffusion coefficient
            m, n, // mesh size
            nves*512, // maximum boundary pair size
            flow.ux.data_ptr(), // advecting velocity x
            flow.uy.data_ptr(), // advecting velocity y
            nullptr, nullptr, // source term and diffusion coefficient index map
            is_x_periodic ? AD::PERIODIC : AD::BOUNCEBACK, // x boundary type
            is_y_periodic ? AD::PERIODIC : AD::BOUNCEBACK // y boundary type
            );

        // trim the label in order to make it the same in NS and AD solver
        for (int ives = 0; ives != nves; ++ives)
        {
            diffusion.boundary.voxelizer.trim_label_by_curve
                (
                label_flow.data_ptr(),
                0, 1,
                vesicles[ives]->get_x().data_ptr(),
                vesicles[ives]->get_y().data_ptr(),
                vesicles[ives]->size()
                );
        }
        diffusion.setInitialConcentration(DeviceArray<REAL>(label_flow > 0)*0.0);
        // initialize AD field from file
        if (use_initial_ad_file)
        {
            diffusion.initializeByFile(initial_ad_filename.c_str());
            printf("Initialize AD field using initial_ad_file \n");
        }
        diffusion.rho.af_save("rhoini", "rhoini.afdat");

        // set vesicle boundary
        for (int ives = 0; ives != nves; ++ives)
        {
            diffusion.registerCurve(vesicles[ives]->get_x(), vesicles[ives]->get_y(), 
                true, ives);
        }
        diffusion.registerCurveFinished();
        diffusion.computeConcentrationOnBoundary();        

        // shear_stress along the membrane
        DeviceArray<REAL> shear_stress((REAL)0.0, diffusion.boundary.BOUNDARY_LIST_CAPACITY);

        // curvature of current or previous steps on the membrane
        DeviceArray<REAL> curvature((REAL)0.0, vesicles[0]->size()*nves, n_history);
        DeviceArray<REAL> curvature_export((REAL)0.0, vesicles[0]->size()*nves);
        DeviceArray<REAL> ves_curvature_change((REAL)0.0, vesicles[0]->size()*nves);
        for (int ives = 0; ives != nves; ++ives)
        {
            vesicles[ives]->calculate_curvature(
                curvature_export.data_ptr() + ives*vesicles[ives]->size(), l0, dx_phys);
        }
        for (int i = 0; i != n_history; ++i)
        {
            for (int ives = 0; ives != nves; ++ives)
                vesicles[ives]->calculate_curvature(
                    curvature.data_ptr() + (ives + i*nves)*vesicles[ives]->size(), 
                    l0, dx_phys);
        }        
        calculate_curvature_change(ves_curvature_change, curvature, 
            vesicles[0]->size()*nves, n_history, 0, dt_phys);
        
        // curvature change (time varient) on the membrane along boundary list
        DeviceArray<REAL> curvature_change((REAL)0.0, diffusion.boundary.BOUNDARY_LIST_CAPACITY);

        // start timer
        SimpleTimer timer;
        timer.tic();

        // start time loop
        for (int i = 0; i != Tend; ++i)
        {
            // *********************************************************************
            // vesicle dynamics
            if (is_vesicle_on)
            {
                // set label to zero
                if (do_viscosity_labeling) // modified by Zhe Gou at 20191122
                {
                    label_flow.reset(0);
                }

                for (int ives = 0; ives != nves; ++ives)
                {
                    // calculate vesicle force
                    vesicles[ives]->calculateForce();

                    // calculate curvature, store the data at i%n_history th row
                    vesicles[ives]->calculate_curvature(
                        curvature.data_ptr() + (ives + (i%n_history)*nves)*vesicles[ives]->size(),
                        vesicles[ives]->get_l0(), dx_phys);

                    // calculate curvature, store for out put
                    /*vesicles[ives]->calculate_curvature(
                            curvature_export.data_ptr() + ives*vesicles[ives]->size(), 
                            l0, dx_phys);*/

                    // update vesicle label
                    if (do_viscosity_labeling)
                        vesicles[ives]->updateLabel(label_flow.data_ptr(), m, n, true, false);
                }
            }

            // *********************************************************************
            // exporting and screen display
            if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
            {
                std::cout << "i = " << i << ", Tend = " << Tend << std::endl;
            
                std::cout << "i = " << i << ", write frame" << std::endl;

                // save vesicle shape
                printf("vesicle shapes snapshot\n");
                flow.ibm.export_all_objects_shapes_af("sx.afdat", "sy.afdat", i != 0);

                // save flow field
                printf("flow field snapshot\n");
                flow.ux.af_save("ux", "ux.afdat", i != 0);
                flow.uy.af_save("uy", "uy.afdat", i != 0);
                flow.rho.af_save("density", "density.afdat", i != 0);
                flow.af_save_flow_field("last_frame.afdat");
                label_flow.af_save("label", "label.afdat", i != 0);

                // calculate and save total shear stress
                REAL sigma_mean = calculate_shear_stress(
                    flow.ux.data_ptr(), flow.label,
                    viscosity_cyto_phys, viscosity_solvent_phys,
                    dt_phys, m, n);
                shear_stress_file.open("shear_stress.dat", std::ios::app);
                shear_stress_file << i*dt_phys << " " << sigma_mean << std::endl;
                shear_stress_file.close();

            }

            if (is_vesicle_on)
            {
                // sync calculation of vesicles
                for (int ives = 0; ives != nves; ++ives)
                {
                    vesicles[ives]->sync_operation();
                }            
            
                // add repulsive force                
                if (is_using_repulsive_force)
                    flow.ibm.add_short_range_repulsive_force();
            }

            // *********************************************************************
            // diffusion of ATP
            if (is_vesicle_on && is_ATP_on)
            {
                // calculate time-averaged curvature change rate in Lagrangian form
                calculate_curvature_change(ves_curvature_change, curvature, 
                    vesicles[0]->size()*nves, n_history, i%n_history, dt_phys);
                
                // register vesicles
                for (int ives = 0; ives != nves; ++ives)
                {
                    diffusion.registerCurve(vesicles[ives]->get_x(), vesicles[ives]->get_y(), 
                        true, ives);
                }
                diffusion.registerCurveFinished(); // contains "cudaStreamSynchronize"
                
                // diffusion.computeConcentrationOnBoundary();
                
                // calculate membrane shear stress in Eulerian form
                diffusion.calculate_shear_stress_on_boundary(
                    shear_stress.data_ptr(), viscosity_solvent_phys, 
                    dt_phys, (REAL)(-1.5));

                // interpolate vesicle curvature change to Eulerian form
                interpolate_curvature_change
                    (
                    curvature_change,
                    diffusion.boundary.s,
                    diffusion.boundary.id,
                    ves_curvature_change,
                    diffusion.boundary.kernel_launch_size,
                    diffusion.boundary.boundary_size,
                    nves
                    );

                if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
                {

                    // get boundary size
                    int diffusion_boundary_size = diffusion.boundary.size();

                    // save shear stress on membrane
                    shear_stress.af_save(
                        "membrane_shear_stress_euler",
                        "membrane_shear_stress_euler.afdat",
                        i != 0,
                        diffusion_boundary_size);
                    auto normalized_membrane_shear_rate = shear_stress / viscosity_solvent_phys / shear_rate_phys;
                    /*normalized_membrane_shear_rate.af_save(
                        "normalized_membrane_shear_rate_euler",
                        "normalized_membrane_shear_rate_euler.afdat",
                        i != 0,
                        diffusion_boundary_size);*/

                    // save membrane curvature
                    //curvature_export.af_save("curvature", "curvature.afdat", i != 0);
                    
                    // save membrane curvature change
                    ves_curvature_change.af_save("curvature_change", 
                        "curvature_change.afdat", i != 0);

                    // save curvature change value on boundary intersecting points of membrane and lattice lines
                    curvature_change.af_save(
                        "curvature_change_euler",
                        "curvature_change_euler.afdat",
                        i != 0, diffusion_boundary_size);

                    DeviceArray<REAL> x_ves(0.0, diffusion.boundary.size());
                    DeviceArray<REAL> y_ves(0.0, diffusion.boundary.size());
                    diffusion.boundary.getBoundaryCoordinates(x_ves.data_ptr(), y_ves.data_ptr());
                    //x_ves.af_save("x_ves", "x_ves_euler.afdat", i != 0);
                    //y_ves.af_save("y_ves", "y_ves_euler.afdat", i != 0);
                    diffusion.boundary.s.af_save("ves_s_lag", "ves_s_lag.afdat", 
                        i != 0, diffusion.boundary.size());
                    diffusion.boundary.id.af_save("ves_id", "ves_id.afdat", 
                        i != 0, diffusion.boundary.size());

                    // export solute field
                    diffusion.rho.af_save("rho", "rho.afdat", i != 0);
                    diffusion.af_save_ad_field("ad_last_frame.afdat");

                    // export data for boundary structure, for matlab post process script
                    if (LBM::getStepStampFlag(i, frames_boundary, Tend, Tbeg_export))
                    {
                        char boundary_file_name[100];
                        sprintf(boundary_file_name, "bound%08d.afdat", i);
                        diffusion.boundary.exportToFile_ArrayFire(boundary_file_name);
                        //char vesicle_shape_file_name[100];
                        //sprintf(vesicle_shape_file_name, "shape%08d.afdat", i);
                        //ves.exportShapeAF(vesicle_shape_file_name);
                    }
                }
                set_boundary(
                    diffusion.boundary.C_in,
                    diffusion.boundary.C_ex,
                    shear_stress,
                    curvature_change,
                    diffusion.boundary.kernel_launch_size,
                    diffusion.getBoundarySizeOnDevice(),
                    diffusion.boundary.id,
                    cc_critical_phys,
                    cc_slope_phys,
                    ss_critical_phys,
                    dx_phys,
                    dt_phys,
                    k_sigma_phys
                    );

                diffusion.advanceTimeStep();
            }

            // advance flow
            if (is_collision_MRT) flow.advanceFlowField_MRT();
            else flow.advanceFlowField();

            // convect immersed objects
            if (is_vesicle_on)
            {

                /*if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
                {
                    // save vesicle shape
                    for (int ives = 0; ives != nves; ++ives)
                    {
                        char vesicle_fx_number[100];
                        sprintf(vesicle_fx_number, "lag_fx_%06d.afdat", ives);
                        vesicles[ives]->get_fx().af_save("lag_fx", vesicle_fx_number, i != 0);

                        char vesicle_fy_number[100];
                        sprintf(vesicle_fy_number, "lag_fy_%06d.afdat", ives);
                        vesicles[ives]->get_fy().af_save("lag_fy", vesicle_fy_number, i != 0);

                        char vesicle_shape_number[100];
                        sprintf(vesicle_shape_number, "ves_shape_%06d.afdat", ives);
                        vesicles[ives]->exportShapeAF(vesicle_shape_number, i != 0);
                    }
                }*/

                flow.advanceImmersedObjects();

                if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
                {
                    // flow.ibm.eul_fx.af_save("eul_fx", "eul_fx.afdat", i != 0);
                    // flow.ibm.eul_fy.af_save("eul_fy", "eul_fy.afdat", i != 0);
                }
            }
        }
        cudaDeviceSynchronize();
        timer.toc();
    }

};

#endif
