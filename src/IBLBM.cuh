#ifndef IBLBM_cuh
#define IBLBM_cuh

#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>

// random number generator
#include <random>

// LBM header
#include "LBMheader.cuh"

inline std::string _IBLBM_GetFileExtension(const std::string& FileName)
{
    if (FileName.find_last_of(".") != std::string::npos)
        return FileName.substr(FileName.find_last_of(".") + 1);
    return "";
}

inline void IBLBM(const char* config_file = "../IBLBM.txt")
{
    // ****************************************************************************************************
    // read config
    printf("IBLBM, ");
    ConfigFile cfg(config_file);

    DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_using_initial_flow_field, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_using_MRT_LBM, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_using_repulsive_force, 1);
    DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_using_adhesion_force, 1);
    DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_using_random_initial_position, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_x_periodic, 1);
	DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_y_periodic, 0);
	DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_using_fast_relaxation_without_interaction, 0);

    // is_shear_flow==1: this is a shear flow with simple geometry(a long straight periodic channel)
    // is_shear_flow==0: this is a pressure driven flow (typically a poiseuille flow)
    //                   with simple or specified geometry (specified from a bmp file)
    DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_shear_flow, 1);

    // this flag work only when is_shear_flow==0
    // if is_externally_specified_geometry==0, the flow will be in a simple periodic channel
    // if is_externally_specified_geometry==1, the flow will be in a geometry specified from a bmp file
    DEFINE_FROM_CONFIGFILE(int, cfg, "flag", is_externally_specified_geometry, 0);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "geometry", Width, 5.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "geometry", Length, 10.0);

    // by using "geometry.bmp", the simulation mesh will be specified by size of the bmp file, rather than by Width and Length
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "geometry", geometry_filename, std::string("geometry.bmp"));

    // specify if geometry file is a bmp or txt.
    // enum, 0 for bmp, 1 for txt or general ascii formats
    auto is_geometry_file_bmp = _IBLBM_GetFileExtension(std::string(geometry_filename)) == std::string("bmp");

    DEFINE_FROM_CONFIGFILE(std::string, cfg, "flow", initial_flow_field, std::string("initial_flow_field_afdat"));

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "flow", viscosity, 1.0); // 

    // in shear flow, shear rate = u_characteristic / width
    // in poiseuille flow, pressure drop per unit length (body force f_b) generates
    // a Poiseuille profile with its maximum velocity as u_characteristic
    // f_b = 8 * viscosity * u_characteristic / width_characteristic^2
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "flow", u_characteristic, 1.0);

    // the angle between pressure drop (body force) direction and x axis, unit: degree
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "flow", pressure_drop_inclination, 0.0);

    // characteristic channel width, this is used when the geometry is complex
    // with u_characteristic together one will have a body force f_b = 8 * viscosity * u_characteristic / width_characteristic^2
    // if it's prescribed as a negative value, it will be set as width by default.
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "flow", width_characteristic, -1.0);
    if (width_characteristic <= 0)
    {
        width_characteristic = Width;
        printf("width_characteristic = %f is set by default\n", width_characteristic);
    }

    // the viscosity ratio between internal and external fluids
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "flow", viscosity_contrast, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "flow", Tend, 1000); // total time (not total time step)
    DEFINE_FROM_CONFIGFILE(int, cfg, "flow", frames_flow, 100); // total export frames for field information

    // when is_using_random_initial_position==1
    // vesicles will be generated with default properties (specified below)
    // and random positions and inclinations, satisfying the hematocrit
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", Hematocrit, 0.15);

    // this list specifies the initial position, inclination, tau and kb etc. of vesicles.
    // it is valid only when is_using_random_initial_position==0
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", vesicle_initialization_filename,
        std::string("vesicle_initialization.txt"));

    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", frames_ves, 1000); // total export frames for vesicle shape
    // default radius for vesicle
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", R0_default, 1.0); // characteristic radius, physical unit, default
    // default reduced area for vesicle
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", tau_default, 0.7); // reduced area, default
    // default bending rigidity for vesicle
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kappa_bending_default, 1.0); // bending modulous, physical unit, default
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kappa_area_default, 10000.0); // area penalty factor, physical unit, default
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kappa_stretching_default, 10000.0); // local stretching penalty factor, physical unit, default
    // default inclination for vesicle
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", inclination_default, 0.0);


    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", mesh_number, 15.0); // mesh size for unit length, dx = 1.0 / mesh_number
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", l0_simu, 0.67); // grid length ratio between vesicle and flow (numerical units)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u_characteristic_simu, 0.001); // simulation velocity, related to Reynolds numberu

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", viscosity_simu, 1.0 / 6.0);
    
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", flow_prestep, 1000000);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", flow_prestep_tolerance, 1e-9);

    // vesicles will relax from a small pre-initial shape to full relaxed shape if this value is larger than zero
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", vesicle_relaxation_nmax, 10000000);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", vesicle_relaxation_tolerance_umax, 1e-4);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", vesicle_relaxation_frames, 1000); // total export frames for field information


    // ****************************************************************************************************
    // specify numerical length / width m and n, modify physical Width if needed
    // specify numerical label_geometry, a [m*n] matrix that valued 0 at fluid points, 
    // 1 (or 0) at vesicle internal points, -1 at solid points
    // if the geometry is specified by a bmp file, then m,n will be the size of bmp file
    // otherwise, m,n will be calculated from width, length and mesh_number
    int m, n;

    if (is_shear_flow)
    {
        is_x_periodic = 1;
        is_y_periodic = 0;
        printf("\nThis is a shear flow with simple channel geometry\nImposing: \n");
        printf("is_x_periodic = 1 \n");
        printf("is_y_periodic = 0 \n");
    }

    // x offset for boundary coordinates
    // x^(phy) = ( x^(simu) + x0 ) / mesh_number
    // x^(simu) = x^(phy) * mesh_number - x0
    REAL x0 = is_x_periodic ? 0.5 : -0.5;

    // y offset for boundary coordinates
    // y^(phy) = ( y^(simu) + y0 ) / mesh_number
    // y^(simu) = y^(phy) * mesh_number - y0
    REAL y0 = is_y_periodic ? 0.5 : -0.5;

    DeviceArray<int> label_geometry;
    if (is_shear_flow == 0 && is_externally_specified_geometry >= 1 && is_geometry_file_bmp)
    {
        // m,n are read from the size of input bmp file
        // label_geometry is read from the bmp file too
        printf("\nmesh size (m,n) is specified by the size of %s\n", geometry_filename.c_str());
        BMPdataFile geometry_data(geometry_filename);
        int m = geometry_data.get_width();
        int n = geometry_data.get_height();
        label_geometry = DeviceArray<int>(&geometry_data.get_binary()[0], m, n);
        label_geometry = -label_geometry;
    }
    else
    {
        // m,n are calculated from length / width and mesh_number.
        printf("\nmesh size (m,n) is specified from length, width and mesh_number\n");
        n = (int)std::round(mesh_number * Width);
        REAL mesh_number_modified = n / Width;
        if (std::abs(mesh_number_modified - mesh_number) > 1e-6)
        {
            printf("In order to match mesh_number = Width / n\n"); 
            printf("mesh_number is modified from %f into %f.\n", mesh_number, mesh_number_modified);
            mesh_number = mesh_number_modified;
        }
        m = (int)std::round(mesh_number * Length);
        REAL Length_modified = m / mesh_number;
        if (std::abs(Length_modified - Length) > 1e-6)
        {
            printf("In order to match Length = m / mesh_number\n");
            printf("Length is modified from %f into %f.\n", Length, Length_modified);
            Length = Length_modified;
        }
        if (is_x_periodic == 0) m += 2;
        if (is_y_periodic == 0) n += 2;

        // build label_geometry
        if (is_shear_flow == 0 && is_externally_specified_geometry >= 1 && (!is_geometry_file_bmp))
        {
            // label_geometry is built from boundary curve file
            label_geometry = LBM::build_pixelated_geometry_map_from_boundary_curve_file
                (
                geometry_filename.c_str(), m, n, -x0, -y0, mesh_number, mesh_number
                );
        }
        else
        {
            // label_geometry is build as a long straight channel (with edges at non-periodic directions)
            auto label_edge = DeviceArray<bool>(false, m, n);
            DeviceArray<REAL> X, Y;
            DA::meshgrid(X, Y, x0, y0, 1.0, 1.0, m, n);
            X = X / mesh_number;
            Y = Y / mesh_number;
            if (!is_x_periodic)
            {
                label_edge = label_edge || (X <= 0.0);
                label_edge = label_edge || (X >= Length);
            }
            if (!is_y_periodic)
            {
                label_edge = label_edge || (Y <= 0.0);
                label_edge = label_edge || (Y >= Width);
            }
            label_geometry = DeviceArray<int>(0, m, n) - DeviceArray<int>(label_edge);
        }
    }

    // simulation box size [m*n]
    printf("mesh size:\n");
    printf("m = %i\n", m);
    printf("n = %i\n", n);


    // ****************************************************************************************************
    // calculate Reynolds number

    // for stokes flow, Re_simu should be smaller than 0.1, the smaller the more precise (but slower)
    // Re_simu = shear_rate_wall_simu * mesh_numbering^2 / viscosity_simu
    // when is_shear_flow == 1:
    //     shear_rate_wall_simu = u_characteristic_simu / width_characteristic_simu
    // when is_shear_flow == 0:
    //     shear_rate_wall_simu = 4 * u_characteristic_simu / width_characteristic_sim
    REAL width_characteristic_simu = width_characteristic * mesh_number;

    // if the simulation is for Stokes flow, it is better to have this value at the magnitude of 0.1
    REAL shear_rate_wall_simu = u_characteristic_simu / width_characteristic_simu;
    if (!is_shear_flow) shear_rate_wall_simu *= 4.0;
    REAL Reynolds_simu = shear_rate_wall_simu * mesh_number * mesh_number / viscosity_simu;
    printf("\nReynolds number in simulation\nRe_simu = shear_rate_wall_simu * mesh_number^2 / viscosity_simu = %.2f\n", Reynolds_simu);


    // ****************************************************************************************************
    // calculate the body force in simulation
    REAL f_body_simu = 8.0*viscosity_simu*u_characteristic_simu / width_characteristic_simu / width_characteristic_simu;
    REAL f_body = 8.0 * viscosity * u_characteristic / width_characteristic / width_characteristic;
    REAL fx_body_simu = std::cos(pressure_drop_inclination / 180.0 * PI) * f_body_simu;
    REAL fy_body_simu = std::sin(pressure_drop_inclination / 180.0 * PI) * f_body_simu;

    DeviceArray<REAL> Fx_simu; // body force field, initialized as an empty array on GPU, with data_ptr = nullptr
    DeviceArray<REAL> Fy_simu; // body force field, initialized as an empty array on GPU, with data_ptr = nullptr
    if (!is_shear_flow)
    {
        // if the flow is not a shear flow, then it is considered as driven by a pressure drop (body force)
        Fx_simu = DeviceArray<REAL>(fx_body_simu, m, n) * DeviceArray<REAL>(label_geometry + 1);
        Fy_simu = DeviceArray<REAL>(fy_body_simu, m, n) * DeviceArray<REAL>(label_geometry + 1); // "fx_body_simu" changed to "fy_body_simu" (20190930)
        printf("pressure drop per unit = %f, inclination = %f degree\n", f_body, pressure_drop_inclination);
    }


    // ****************************************************************************************************
    // calculate:
    // delta t: dt;
    // delta x: dx;
    // total time step for simulation: Nstep;
    // dt is derived from u_characteristic and u_characteristic_simu
    REAL dx = 1.0 / mesh_number;
    REAL dt = u_characteristic_simu / u_characteristic / mesh_number;
    printf("dx = 1 / mesh_number = 1 / %f = %e\n", mesh_number, dx);
    printf("dt = u_characteristic / u_characteristic_simu * dx = %e\n", dt);
    // calculate total step
    int Nstep = (int)std::ceil(Tend / dt);
    printf("\nTotal simulation step = ceil(Tend / dt) = ceil(%f / %f) = %i\n", Tend, dt, Nstep);

    // ****************************************************************************************************
    // build flow field
    std::vector<REAL> h_viscosity_simu_list({ viscosity_simu, viscosity_simu *viscosity_contrast });

    // label_flow has its value equal to -1 in solid lattices, 0 in flow lattice, 1 in vesicle with viscosity contrast
    auto label_flow = label_geometry;

    // construct flow solver
    NSsolver flow(
        &h_viscosity_simu_list[0],2,
        m,
        n,
        is_x_periodic ? NS::PERIODIC : NS::BOUNCEBACK,
        is_y_periodic ? NS::PERIODIC : NS::BOUNCEBACK,
        Fx_simu.data_ptr(), // body force, if the flow is a shear flow, this pointer should be set as a nullptr
        Fy_simu.data_ptr(), // body force, if the flow is a shear flow, this pointer should be set as a nullptr
        label_flow.data_ptr(),
        u_characteristic_simu*2.0);
    flow.initializeByDensity();

    // export boundary geometry
    DeviceArray<REAL>* boundary_geometry_export = new DeviceArray<REAL>();
    *boundary_geometry_export = DA::join(
        (flow.ibm.xBoundary + x0) / mesh_number,    // position of boundary solid points in physical units
        (flow.ibm.yBoundary + y0) / mesh_number);   // 
    boundary_geometry_export->reshape(flow.ibm.xBoundary.size(), 2);
	boundary_geometry_export->transpose();
    boundary_geometry_export->save2D("pixelate_boundary.dat", 6);
    flow.ibm.xBoundary.af_save("xbound", "xbound.afdat");
    flow.ibm.yBoundary.af_save("ybound", "ybound.afdat");


    // ****************************************************************************************************
    // generate vesicles by using initial config file or default values
    int nves; // the total number of vesicles 
    DataFile* ptr_vesicle_initialization_data = nullptr;

    // calculate nves
    // read initialization data from file if needed
    if (is_using_random_initial_position)
    {
        // using random initial positions and default properties
        // nves is calculated from geometry area and default characteristic radius.
        REAL area_geometry = DA::sum(label_geometry + 1) / (mesh_number*mesh_number);
        REAL area_vesicle_default = PI*R0_default*R0_default;
        nves = (int)std::round(area_geometry * Hematocrit / area_vesicle_default);
    }
    if (!is_using_random_initial_position)
    {
        // vesicles initialized from initial config file
        ptr_vesicle_initialization_data = new DataFile(vesicle_initialization_filename.c_str());
        nves = ptr_vesicle_initialization_data->get_n();
    }

    std::vector< Vesicle* > vesicles(nves, nullptr);
    std::vector< bool > vesicles_viscosity_contrast_flag(nves, false);
    std::vector< REAL > kb_simu_relax(nves, 0.0);

    // generate vesicles, then immerse them into the fluid
    if (is_using_random_initial_position)
    {
        // using random initial positions and default properties
        // nves is calculated from geometry area and default characteristic radius.
        // this function is under construction
    }
    if (!is_using_random_initial_position)
    {
        // read and calculate initial configurations from ptr_vesicle_initialization_data
        // then immerse vesicles into flow
        for (int i = 0; i != nves; ++i)
        {
            // load values from configure file
            REAL xpos = Length * 0.5;
            REAL ypos = Width * 0.5;
            REAL inclination = inclination_default;
            REAL r0 = R0_default;
            REAL tau = tau_default;
            REAL kappa_bending = kappa_bending_default;
            REAL kappa_stretching = kappa_stretching_default;
            REAL kappa_area = kappa_area_default;
            bool vesicles_viscosity_contrast_flag_this = false;
            auto& ves_ini_data_this = ptr_vesicle_initialization_data->getData()[i];
            // load data from current vesicle initialization data
            int len_ves_ini_data_this = ves_ini_data_this.size();
            if (len_ves_ini_data_this > 0) xpos = ves_ini_data_this[0];
            if (len_ves_ini_data_this > 1) ypos = ves_ini_data_this[1];
            if (len_ves_ini_data_this > 2) inclination = ves_ini_data_this[2];
            inclination = inclination / 180.0 * PI;
            if (len_ves_ini_data_this > 3) r0 = ves_ini_data_this[3];
            if (len_ves_ini_data_this > 4) tau = ves_ini_data_this[4];
            if (len_ves_ini_data_this > 5) kappa_bending = ves_ini_data_this[5];
            if (len_ves_ini_data_this > 6) vesicles_viscosity_contrast_flag_this = ves_ini_data_this[6] > 0;
            vesicles_viscosity_contrast_flag[i] = vesicles_viscosity_contrast_flag_this;
            if (len_ves_ini_data_this > 7) kappa_stretching = ves_ini_data_this[7];
            if (len_ves_ini_data_this > 8) kappa_area = ves_ini_data_this[8];

            // calculate corresponding values in simulation units
            REAL xpos_simu = xpos * mesh_number - x0;
            REAL ypos_simu = ypos * mesh_number - y0;
            REAL inclination_simu = inclination;
            REAL r0_simu = r0 * mesh_number;
            REAL tau_simu = tau;

            // The calculation of kappa_bending_simu is to find a kb in simulation that generates identical
            // capillary number corresponds to physical system
            REAL capillary_number = u_characteristic / width_characteristic * viscosity * r0*r0*r0 / kappa_bending;
            REAL kappa_bending_simu =
                u_characteristic_simu / width_characteristic_simu *
                viscosity_simu * r0_simu*r0_simu*r0_simu / capillary_number;

            // for the calculation of kb_simu, please check Eqn. (18) in:
            /*
            @article{shen2017interaction,
              title={Interaction and rheology of vesicle suspensions in confined shear flow},
              author={Shen, Zaiyi and Farutin, Alexander and Thi{\'e}baud, Marine and Misbah, Chaouqi},
              journal={Physical Review Fluids},
              volume={2},
              number={10},
              pages={103101},
              year={2017},
              publisher={APS}
            }
            */
            REAL kb_simu = 4 * kappa_bending_simu / l0_simu;

            // prepare a kb for relax too.
            REAL capillary_number_relax = 1; // fixed at 1.0
            REAL kappa_bending_relax_simu =
                u_characteristic_simu / width_characteristic_simu *
                viscosity_simu * r0_simu*r0_simu*r0_simu / capillary_number_relax;
            kb_simu_relax[i] = 4 * kappa_bending_relax_simu / l0_simu;

            // similar implementation for kappa_streching and kappa_area:
            // for kappa streching:
            REAL capillary_number_streching = u_characteristic / width_characteristic * viscosity * r0 / kappa_stretching;
            REAL kappa_streaching_simu =
                u_characteristic_simu / width_characteristic_simu *
                viscosity_simu * r0_simu / capillary_number_streching;
            REAL kl_simu = kappa_streaching_simu * l0_simu;
            // for kappa area:
            REAL capillary_number_area = u_characteristic / width_characteristic * viscosity * r0*r0 / kappa_area;
            REAL ks_simu =
                u_characteristic_simu / width_characteristic_simu *
                viscosity_simu * r0_simu*r0_simu / capillary_number_area;

            // make vesicle. All the parameters are in LBM unit
            vesicles[i] = new Vesicle(
                xpos_simu, ypos_simu, inclination_simu, r0_simu, // position x, y, inclination, characteristic radius
                tau_simu, kb_simu, kl_simu, ks_simu, l0_simu, // reduced area, bending modulus, streaching modulus, area conservator, dx_lag / dx_euler
                nullptr, // file name for the initial shape curve, nullptr means the shape is generated by default seed.
                true, // is_silent = true: don't dump numerical properties of vesicle to the screen
                0.5); // original size amplification. use 0.5 to adapt high concentration cases.

            // immerse it into flow
            flow.immerseAnObject(vesicles[i]);
            if (i == 0) printf("Add vesicle(s) into flow:\n");
            printf("No.%i: Position=(%.2f,%.2f), R0=%.2f, tau=%4.2f, kappa_bending=%.2f\n",
                i, xpos, ypos, r0, tau, kappa_bending);
            printf("       kb_simu=%.3e, kl_simu=%.3e, ks_simu=%.3e\n", kb_simu, kl_simu, ks_simu);
            printf("       N_point = %d\n", vesicles[i]->get_x().size());
        }
    }

    // call immerseObjectFinished() when you finished immersing all vesicles into the flow.
    flow.immerseObjectFinished();

    // relax the immersed objects in a quiescent flow
    SimpleTimer timer;

    timer.tic();

    if (vesicle_relaxation_nmax && !is_using_fast_relaxation_without_interaction)
    {

        // this is a dirty hack that prevents body force (pressure drop effect) during relaxation.
        // part 1. set the force to nullptr before relaxation
        flow.body_force_x = nullptr;
        flow.body_force_y = nullptr;

        printf("\nVesicle relaxation:\n");

        printf("umax_tolerance = %f\n", vesicle_relaxation_tolerance_umax);
        printf("vesicle_relaxation_nmax = %d\n", vesicle_relaxation_nmax);
        for (int i = 0; i != vesicle_relaxation_nmax; ++i)
        {
            if (i < 1.0 * vesicle_relaxation_nmax)
            {
                for (int ives = 0; ives != nves; ++ives)
                {
                    //vesicles[ives]->calculateForce(kb_relax, kl_relax, ks_relax);
                    vesicles[ives]->calculateForce(kb_simu_relax[ives]);
                }
            } else
            {
                // debug, using physical (which is milder) relax parameters
                for (int ives = 0; ives != nves; ++ives)
                    vesicles[ives]->calculateForce();
            }
            for (int ives = 0; ives != nves; ++ives)
                vesicles[ives]->sync_operation();
            if (is_using_repulsive_force) flow.ibm.add_short_range_repulsive_force();
            if (is_using_adhesion_force)
            {
                // add adhesion force here
                // unfinished
            }

            if (LBM::getStepStampFlag(i, vesicle_relaxation_frames, vesicle_relaxation_nmax))
            {

                // debug export
                flow.ibm.export_all_objects_shapes_af("sx_relax.afdat", "sy_relax.afdat", i != 0);
                
                char fname[100];
                sprintf(fname, "shape_relax_%08d.dat", i);
                flow.ibm.export_all_immersed_object_shapes(fname, x0, y0, 1.0 / mesh_number, 1.0 / mesh_number);
                
                // flow.ux.af_save("ux", "ux.afdat", i != 0);
                // flow.uy.af_save("uy", "uy.afdat", i != 0);
                printf("relaxation step = %i", i);

                // calculate maximum vesicle velocity during relaxation
                REAL umax_ves = 0.0;
                for (int ives = 0; ives != nves; ++ives)
                {
                    
                    const auto& uxves = vesicles[ives]->ux;
                    const auto& uyves = vesicles[ives]->uy;
                    auto usves = sqrt(uxves * uxves + uyves * uyves) * (dx / dt);
                    std::vector<REAL> h_usves(usves.size(), 0.0);
                    usves.host(&h_usves[0]);
                    REAL umax_this = *std::max_element(&h_usves[0], (&h_usves[0]) + h_usves.size());
                    if (umax_this > umax_ves)
                        umax_ves = umax_this;
                }
                printf(", umax_vesicle / u_flow = %e\n", umax_ves / u_characteristic);

                // termination
                if (i > 0 && (umax_ves / u_characteristic) < vesicle_relaxation_tolerance_umax)
                {
                    printf("umax_vesicle < tolerance reached, the vesicles are considered as well relaxed.\n");
                    break;
                }
                else
                {
                    if (i == vesicle_relaxation_nmax - 1)
                    {
                        printf("Vesicle relaxation terminated by reaching maximum time step.\n");
                    }
                }

            }
            flow.advanceFlowField();
            flow.advanceImmersedObjects();

            if (i % 10 == 0)
            {
                flow.initializeByDensity();
            }

        }
        cudaDeviceSynchronize();
        printf("%i vesicles initialization complete.\n", nves);
    }

    // this is a dirty hack that prevents body force (pressure drop effect) during relaxation.
    // part 2. set the force back to F_simu after relaxation
    flow.body_force_x = Fx_simu.data_ptr();
    flow.body_force_y = Fy_simu.data_ptr();

	if (is_using_fast_relaxation_without_interaction)
	{
		for (int i = 0; i != vesicles.size(); ++i)
		{
			vesicles[i]->relax(10000000, 0.1);
		}
	}


    // initialize flow field or read initial flow field from file
    if (is_using_initial_flow_field)
    {
        flow.initializeByFile(initial_flow_field.c_str());
    }
    else
    {
        flow.initializeByDensity();

        if (is_shear_flow > 0)
        {
            // if it is a shear flow, the velocity on the upper/lower wall
            // should be imposed as (+/-)u_characteristic_simu/2
            flow.config.IMPOSING_VELOCITY = is_shear_flow > 0;
            DeviceArray<REAL> Y = DA::range(-0.5, 1.0 / (n - 1), n);
            Y.transpose();
            Y = DA::tile(Y, m);
            flow.ux = Y * u_characteristic_simu;
            flow.uy = 0.0;
        }

        //pre-step for initializing flow field
        printf("\n");
        flow.develop_steady_flow(flow_prestep, flow_prestep_tolerance);
        flow.af_save_flow_field(initial_flow_field.c_str());
    }

    // start the simulaiton

    for (int i = 0; i != Nstep; ++i)
    {
        // calculate vesicle force based on membrane force
        for (int ives = 0; ives != nves; ++ives)
            vesicles[ives]->calculateForce();
        for (int ives = 0; ives != nves; ++ives)
            vesicles[ives]->sync_operation();

        // add repulsive force if needed.
        if (is_using_repulsive_force) flow.ibm.add_short_range_repulsive_force();

        if (is_using_adhesion_force)
        {
            // under construction
            // add the long range adhesion force between ves-ves and ves-wall
        }

        if (LBM::getStepStampFlag(i, frames_ves, Nstep))
        {
            printf("i = %i, Nstep = %i, Tnow = %f, Tend = %f\n", i, Nstep, i*dt, Tend);
        }

        if (LBM::getStepStampFlag(i, frames_ves, Nstep))
        {
            printf("vesicle shapes snapshot\n");
            flow.ibm.export_all_objects_shapes_af("sx.afdat", "sy.afdat", i != 0);
            char fname[100];
            sprintf(fname, "shape_%08d.dat", i);
            flow.ibm.export_all_immersed_object_shapes(fname, x0, y0, 1.0 / mesh_number, 1.0 / mesh_number);
        }

        if (LBM::getStepStampFlag(i, frames_flow, Nstep))
        {
            printf("flow field snapshot\n");
            flow.ux.af_save("ux", "ux.afdat", i != 0);
            flow.uy.af_save("uy", "uy.afdat", i != 0);
            char fname_ux[100];
            char fname_uy[100];
            sprintf(fname_ux, "ux_%08d.dat", i);
            sprintf(fname_uy, "uy_%08d.dat", i);
            (flow.ux*dx / dt).save2D(fname_ux, 6);
            (flow.uy*dx / dt).save2D(fname_uy, 6);
        }

        flow.advanceFlowField();
        flow.advanceImmersedObjects();
    }
    cudaDeviceSynchronize();
    timer.toc();

}

#endif // RBC_flow_local_rheology_cuh
