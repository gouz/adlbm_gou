#ifndef LBMheader_cuh
#define LBMheader_cuh

// ************************************************************************************************

// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

// DeviceArray header
#include "DeviceArray.cuh"

// LBM related header
#include "LBMconstant.hpp"

#include "ADsolver.cuh"
#include "NSsolver.cuh"

#include "ImmersedBoundaryMethod.cuh"
#include "Vesicle.cuh"
//#include "NBeadsSwimmer.cuh"

// helper header
#include "ConfigFile.hpp"

// std
#include<time.h>
#include <cmath>

class SimpleTimer{
public:
    double time_begin;
    double time_end;
    double time_elapsed;
public:
    inline void tic(){ time_begin = std::clock(); }
    inline void toc_silent()
    {
        time_end = std::clock();
        time_elapsed = (double)(time_end - time_begin) / (double)CLOCKS_PER_SEC;
    }
    inline void toc()
    {
        toc_silent();
        std::cout << "Elapsed time = " << time_elapsed << " seconds" << std::endl;
    }
};


namespace LBM
{
    inline bool getStepStampFlag(const int i, int frames, int Tend, int Tbeg = 0)
    {
        if (frames <= 0) return false;
        frames = frames > 0 ? frames : 1;
        Tbeg = Tbeg > 0 ? Tbeg : 0;
        Tend = Tend > Tbeg ? Tend : Tbeg;
        return
            (i>=Tbeg) &&
            ((i - Tbeg) % std::max(1, ((Tend - Tbeg) / frames)) == 0) ||
            (i == Tbeg) ||
            (i == Tend - 1);
    }

    // build a pixelated 2D array representing the simulation geometry from a ascii file
    // the input ascii file (exemplified by geometry.txt) has a format like:
    //   +----------------------------------+
    //   | // geometry.txt                  |
    //   | // it has N curves in total      |
    //   |                                  |
    //   | // curve0                        |
    //   | 0 // 0 for non-circular curve    |
    //   | x0 x1 ... xn                     |
    //   | y0 y1 ... yn //split by Spacebar |
    //   |                                  |
    //   | // curve1                        |
    //   | 1 // 1 for circular curve        |
    //   | x0, x1, ... xn                   |
    //   | y0, y1, ... yn // or by comma    |
    //   |                                  |
    //   | ... // other curves              |
    //   |                                  |
    //   | // curveN                        |
    //   | 1 // 1 for circular curve        |
    //   | x0 x1 ... xn                     |
    //   | y0 y1 ... yn                     |
    //   +----------------------------------+
    //
    // The curve direction matters:
    // =============================  
    //                                
    //      (x4,y4)     (xn,yn)       
    //            +----->             
    //   (x3,y3) /                    
    //          +                     
    //          |                     
    // [solid]  |      [fluid]        
    //          +                     
    //   (x2,y2) \                    
    //            \                   
    //             +---------<        
    //       (x1,y1)         (x0,y0)  
    //                                
    // =============================  
    //
    // fluid points will be tagged as  0
    // solid points will be tagged as -1
    // Before building the map, curves will be rescaled via:
    //     x = x * xscale + x_offset
    //     y = y * yscale + y_offset
    // a DeviceArray<int> type 2d array will be returned
    inline DeviceArray<int> build_pixelated_geometry_map_from_boundary_curve_file
        (
        const char* fname,
        const int m,
        const int n,
        const REAL x_offset = 0,
        const REAL y_offset = 0,
        const REAL xscale = 1,
        const REAL yscale = 1
        )
    {
        DataFile boundary_curves(fname);
        printf("Build pixelated geometry map from boundary curve file %s\n", fname);
        int n_bound = boundary_curves.get_n() / 3;
        printf("boundary curve number = %i\n", n_bound);

        std::vector< DeviceArray<REAL> > x_bound(n_bound);
        std::vector< DeviceArray<REAL> > y_bound(n_bound);
        std::vector< bool > is_circle_bound(n_bound);
        int total_segment_number = 0;

        for (int i = 0; i != n_bound; ++i)
        {
            DeviceArray<REAL> x_bound_original, y_bound_original;
            x_bound_original = DeviceArray<REAL>
                (
                &boundary_curves.getData()[i * 3 + 1][0],
                boundary_curves.getData()[i * 3 + 1].size()
                );
            y_bound_original = DeviceArray<REAL>
                (
                &boundary_curves.getData()[i * 3 + 2][0],
                boundary_curves.getData()[i * 3 + 2].size()
                );

            x_bound_original = x_bound_original * xscale + x_offset;
            y_bound_original = y_bound_original * yscale + y_offset;

            bool is_circle_bound_this;
            is_circle_bound_this = std::abs(boundary_curves.getData()[i * 3][0]) > 1e-5;
            is_circle_bound[i] = is_circle_bound_this;

            LBMGPUvoxelizer::refine_curve
                (
                x_bound[i],
                y_bound[i],
                x_bound_original.data_ptr(),
                y_bound_original.data_ptr(),
                x_bound_original.size(),
                1.0,
                is_circle_bound_this
                );

            total_segment_number += x_bound[i].size();
        }

        LBMGPUvoxelizer* voxelizer = new LBMGPUvoxelizer(m, n, false, false, total_segment_number * 2, n_bound * 2);

        for (int i = 0; i != n_bound; ++i)
        {
            voxelizer->setBoundaryCurve
                (
                x_bound[i].data_ptr(),
                y_bound[i].data_ptr(),
                x_bound[i].size(),
                is_circle_bound[i],
                0
                );
        }
        voxelizer->setBoundaryCurveFinished(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, false);
        auto geometry_map = voxelizer->build_filled_tag_map();
        delete voxelizer;
        geometry_map = DeviceArray<int>(geometry_map == -1) - 1;
        return geometry_map;
    }
}
#endif