#include "ImmersedBoundaryMethod.cuh"

#include "_deviceImmersedBoundaryMethod_distributeForce.cuh"

#include <fstream>

// ************************************************************************************************
// atomicAdd from cuda website
// http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#atomic-functions
// for Tesla K80, cuda capacity 3.7, implement should be done by ourselves
// for Kepler with cuda capa > 6.0, it's already implemented in cuda header
// #include <cuda.h>
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600

#else
static __forceinline__ __device__ double atomicAdd(double *address, double val) {
    unsigned long long int* address_as_ull = (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    if (val == 0.0)
        return __longlong_as_double(old);
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed, __double_as_longlong(val + __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}
#endif
// ************************************************************************************************

#ifndef PI
#define PI 3.141592656589793238464338327950
#endif

// cuda launch parameters dor 1D
#define BSIZE 256
#define CUDALAUNCH(n) <<<((n) + BSIZE - 1) / BSIZE, BSIZE>>>
#define CUDALAUNCH_ASYNC(n, stream) <<<((n) + BSIZE - 1) / BSIZE, BSIZE, 0, stream>>>

// cuda launch for 2D
// block size
#define BSIZEM 32
#define BSIZEN 16

#define DEFINE_LAUNCH_PARAMETER(grid, block, m, n) \
dim3 grid((m + BSIZEM - 1) / BSIZEM, (n + BSIZEN - 1) / BSIZEN); \
const dim3 block(BSIZEM, BSIZEN)
#define CUDALAUNCH2D(grid, block)  <<<grid, block>>>

#define POSITIVE_MOD(i,n) (((i) % (n)) + (n)) % (n)

// ********************************************************************
// a peskin delta function macro defined for immersed boundary method
#include "PeskinDeltaFunction.h"
// ********************************************************************

// ********************************************************************
// a morse potential is defined for repulsive force between ibObjs and
// solid walls
#include "MorsePotential.h"
// ********************************************************************

// ********************************************************************
// a Lennard Jones potential is defined for attractive at long range
// and repulsive at short range
#include "LJPotential.h"
// ********************************************************************

template<bool is_shuffle_position>
__global__ void kernel_advect_immersed_objects
(
REAL* __restrict__ lag_ux,
REAL* __restrict__ lag_uy,
REAL* __restrict__ lag_x,
REAL* __restrict__ lag_y,
const REAL* __restrict__ eul_ux,
const REAL* __restrict__ eul_uy,
const int m,
const int n,
const int len_obj,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < len_obj){
        REAL x = lag_x[idx];
        REAL y = lag_y[idx];
        int _i = (int)floor(x);
        int i0 = _i - 1;
        int i1 = _i + 2;
        int _j = (int)floor(y);
        int j0 = _j - 1;
        int j1 = _j + 2;

        REAL delta_dx[4];
#pragma unroll
        for (int i = i0; i <= i1; ++i){
            REAL dx = abs((REAL)i - x);
            delta_dx[i - i0] = DELTA1D(dx);
        }

        REAL delta_dy[4];
#pragma unroll
        for (int j = j0; j <= j1; ++j){
            REAL dy = abs((REAL)j - y);
            delta_dy[j - j0] = DELTA1D(dy);
        }

        REAL ux_ij = 0.0;
        REAL uy_ij = 0.0;
#pragma unroll
        for (int j = j0; j <= j1; ++j){
            REAL delta_dy_j = delta_dy[j - j0];
            int j_box;
            if ((j < 0 || j >= n) && (!is_y_periodic)) continue;
            else j_box = POSITIVE_MOD(j, n);
#pragma unroll
            for (int i = i0; i <= i1; ++i){
                REAL delta_dx_i = delta_dx[i - i0];
                int i_box;
                if ((i < 0 || i >= m) && (!is_x_periodic)) continue;
                else i_box = POSITIVE_MOD(i, m);
                REAL delta_r = delta_dy_j * delta_dx_i;
                int idx_box = j_box*m + i_box;
                ux_ij += delta_r * eul_ux[idx_box];
                uy_ij += delta_r * eul_uy[idx_box];
            }
        }

        if (is_shuffle_position){
            lag_x[idx] = x + ux_ij;
            lag_y[idx] = y + uy_ij;
        }
        lag_ux[idx] = ux_ij;
        lag_uy[idx] = uy_ij;
    }
}

// advect all immersed objects
// if is_shuffle_position == false, function will only write velocity into immersedObjects[i]->ux and uy
// but will not advect the object directly, this is useful when dealing with a slipping boundary
// or mass point.
// If so, you advect your imObj with this velocity and other properties by your self 
void ImmersedBoundaryMethod::advect_immersed_objects
(
const REAL* __restrict__ eul_ux,
const REAL* __restrict__ eul_uy,
const bool is_shuffle_position
)
{
    int nObj = immersedObjects.size();
    if (nObj < 1) return;

    for (int i = 0; i != nObj; ++i){

        int len = immersedObjects[i]->size();
        if (is_shuffle_position){
            kernel_advect_immersed_objects<true> CUDALAUNCH_ASYNC(len, immersedObjects[i]->cudaStream)
                (
                immersedObjects[i]->ux.data_ptr(),
                immersedObjects[i]->uy.data_ptr(),
                immersedObjects[i]->get_x().data_ptr(),
                immersedObjects[i]->get_y().data_ptr(),
                eul_ux,
                eul_uy,
                m, n,
                immersedObjects[i]->size(),
                is_x_periodic,
                is_y_periodic
                );
        }
        else{
            kernel_advect_immersed_objects<false> CUDALAUNCH_ASYNC(len, immersedObjects[i]->cudaStream)
                (
                immersedObjects[i]->ux.data_ptr(),
                immersedObjects[i]->uy.data_ptr(),
                immersedObjects[i]->get_x().data_ptr(),
                immersedObjects[i]->get_y().data_ptr(),
                eul_ux,
                eul_uy,
                m, n,
                immersedObjects[i]->size(),
                is_x_periodic,
                is_y_periodic
                );
        }
    }

    for (int i = 0; i != nObj; ++i){
        immersedObjects[i]->sync_operation();
    }
}

__global__
void kernel_distribute_variable
(
REAL* __restrict__ eul_var,
const REAL* __restrict__ lag_var, // variable that you want to distribute
const REAL* __restrict__ lag_x,
const REAL* __restrict__ lag_y,
const int m,
const int n,
const int len,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < len){
        REAL x = lag_x[idx];
        REAL y = lag_y[idx];
        int _i = (int)floor(x);
        int i0 = _i - 1;
        int i1 = _i + 2;
        int _j = (int)floor(y);
        int j0 = _j - 1;
        int j1 = _j + 2;

        REAL delta_dx[4];
#pragma unroll
        for (int i = i0; i <= i1; ++i){
            REAL dx = abs((REAL)i - x);
            delta_dx[i - i0] = DELTA1D(dx);
        }

        REAL delta_dy[4];
#pragma unroll
        for (int j = j0; j <= j1; ++j){
            REAL dy = abs((REAL)j - y);
            delta_dy[j - j0] = DELTA1D(dy);
        }

#pragma unroll
        for (int j = j0; j <= j1; ++j){
            REAL delta_dy_j = delta_dy[j - j0];
            int j_box;
            if ((j < 0 || j >= n) && (!is_y_periodic)) continue;
            else j_box = ((j % n) + n) % n;
#pragma unroll
            for (int i = i0; i <= i1; ++i){
                REAL delta_dx_i = delta_dx[i - i0];
                int i_box;
                if ((i < 0 || i >= m) && (!is_x_periodic)) continue;
                else i_box = ((i % m) + m) % m;
                REAL delta_r = delta_dy_j * delta_dx_i;
                atomicAdd(eul_var + (j_box*m + i_box), delta_r * lag_var[idx]);
            }
        }
    }
}

void ImmersedBoundaryMethod::distribute_variable
(
REAL* __restrict__ eul_var,
const REAL* __restrict__ lag_var, // variable that you want to distribute
const REAL* __restrict__ lag_x,
const REAL* __restrict__ lag_y,
const int len
)
{
    kernel_distribute_variable CUDALAUNCH(len)
        (eul_var, lag_var, lag_x, lag_y, m, n, len, is_x_periodic, is_y_periodic);
}

__global__
void kernel_collect_variable
(
const REAL* __restrict__ eul_var,
REAL* __restrict__ lag_var, // variable that you want to collect
const REAL* __restrict__ lag_x,
const REAL* __restrict__ lag_y,
const int m,
const int n,
const int len,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < len){
        REAL x = lag_x[idx];
        REAL y = lag_y[idx];
        int _i = (int)floor(x);
        int i0 = _i - 1;
        int i1 = _i + 2;
        int _j = (int)floor(y);
        int j0 = _j - 1;
        int j1 = _j + 2;

        REAL delta_dx[4];
#pragma unroll
        for (int i = i0; i <= i1; ++i){
            REAL dx = abs((REAL)i - x);
            delta_dx[i - i0] = DELTA1D(dx);
        }

        REAL delta_dy[4];
#pragma unroll
        for (int j = j0; j <= j1; ++j){
            REAL dy = abs((REAL)j - y);
            delta_dy[j - j0] = DELTA1D(dy);
        }

        REAL var = (REAL)0.0;
#pragma unroll
        for (int j = j0; j <= j1; ++j){
            REAL delta_dy_j = delta_dy[j - j0];
            int j_box;
            if ((j < 0 || j >= n) && (!is_y_periodic)) continue;
            else j_box = ((j % n) + n) % n;
#pragma unroll
            for (int i = i0; i <= i1; ++i){
                REAL delta_dx_i = delta_dx[i - i0];
                int i_box;
                if ((i < 0 || i >= m) && (!is_x_periodic)) continue;
                else i_box = ((i % m) + m) % m;
                REAL delta_r = delta_dy_j * delta_dx_i;
                int idx_box = j_box*m + i_box;
                var += delta_r * eul_var[idx_box];
            }
        }
        lag_var[idx] = var;
    }
}

void ImmersedBoundaryMethod::collect_variable
(
const REAL* __restrict__ eul_var,
REAL* __restrict__ lag_var, // variable that you want to distribute
const REAL* __restrict__ lag_x,
const REAL* __restrict__ lag_y,
const int len
)
{
    kernel_collect_variable CUDALAUNCH(len)
        (eul_var, lag_var, lag_x, lag_y, m, n, len, is_x_periodic, is_y_periodic);
}



__global__ void kernel_distributeForce
(
REAL* __restrict__ _fx,
REAL* __restrict__ _fy,
const REAL* __restrict__ * __restrict__ const __ibfx,
const REAL* __restrict__ * __restrict__ const __ibfy,
const REAL* __restrict__ * __restrict__ const __ibx,
const REAL* __restrict__ * __restrict__ const __iby,
const int* __restrict__ _cellListId,
const int* __restrict__ _cellListCount,
const int cellListR, // bucket length
const int MAX_POINTS, // maximum number of points in a immersed boundary curve
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    REAL fx = 0.0;
    REAL fy = 0.0;
    _deviceImmersedBoundaryMethod_distributeForce
        (
        fx, fy,
        __ibfx, __ibfy,
        __ibx, __iby,
        _cellListId,
        _cellListCount,
        cellListR,
        MAX_POINTS,
        m, n,
        is_x_periodic, is_y_periodic
        );
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    if (i < m & j < n)
    {
        _fx[i + j*m] = fx;
        _fy[i + j*m] = fy;
    }
}


/*
__global__ void kernel_distributeForce
(
REAL* __restrict__ _fx,
REAL* __restrict__ _fy,
const REAL* __restrict__ * __restrict__ const __ibfx,
const REAL* __restrict__ * __restrict__ const  __ibfy,
const REAL* __restrict__ * __restrict__ const  __ibx,
const REAL* __restrict__ * __restrict__ const  __iby,
const int* __restrict__ _cellListId,
const int* __restrict__ _cellListCount,
const int cellListR, // bucket length
const int MAX_POINTS, // maximum number of points in a immersed boundary curve
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    if (i < m & j < n)
    {
        REAL fx = (REAL)0.0;
        REAL fy = (REAL)0.0;

        // locate (i,j)'s bucket in cellList
        int ic = i / cellListR;
        int jc = j / cellListR;
        int mc = (m + cellListR - 1) / cellListR;
        int nc = (n + cellListR - 1) / cellListR;

        int count_total = 0;
        int count_now = 0;
        int id_list[16];

        // construct local neighbour list from cell list
        // iter_jc starts from (-1 - (is_y_periodic&j == 0))
        // since the actual size of last cell can be less than cellListR if (m%cellListR!=0).
        // the same goes for i

        for (int iter_jc = -1 - (is_y_periodic & jc == 0); iter_jc <= 1; ++iter_jc)
        {
            int pos_jc = iter_jc + jc; if (is_y_periodic) pos_jc = POSITIVE_MOD(pos_jc, nc);
            if (pos_jc >= 0 & pos_jc < nc)
            {
                for (int iter_ic = -1 - (is_x_periodic & ic == 0); iter_ic <= 1; ++iter_ic)
                {
                    int pos_ic = iter_ic + ic; if (is_x_periodic) pos_ic = POSITIVE_MOD(pos_ic, mc);
                    if (pos_ic >= 0 & pos_ic < mc)
                    {
                        int count = _cellListCount[pos_ic + pos_jc*mc];
                        if (count == 0) continue;
                        count_total += count;
                        for (int iter_id = 0; iter_id < count; ++iter_id)
                        {
                            int id = _cellListId[pos_ic + pos_jc*mc + iter_id*mc*nc];
                            id_list[count_now++] = id;
                        }
                    }
                }
            }
        } // end construction of neighbor list

        // calculate force on this grid if count_total > 0
        if (count_total > 0)
        {

            for (int iter = 0; iter != count_total; ++iter)
            {
                // read immersed point id from _cellListId
                // now this id is global
                int point_id = id_list[iter];

                // if point_id < 0, then this point is a solid boundary
                // it does not involve in immersed object boundary
                if (point_id < 0) continue;

                // calculate point_id and curve_id
                int curve_id = point_id / MAX_POINTS;
                point_id = point_id % MAX_POINTS;

                // calculate distance between point x and i
                REAL dx = __ibx[curve_id][point_id];
                REAL dy = __iby[curve_id][point_id];

                if (is_x_periodic) dx = dx - floor(dx / m)*m;
                dx = abs(dx - i); // no worry, abs and min are defined in cuda's header __MATH_FUNCTIONS_HPP__
                dx = min(dx, m - dx);

                if (is_y_periodic) dy = dy - floor(dy / n)*n;
                dy = abs(dy - j);
                dy = min(dy, n - dy);

                // add contribution from ibfx and ibfy to fx and fy
                if (dx < (REAL)2.0 & dy < (REAL)2.0)
                {
                    REAL lag_fx = __ibfx[curve_id][point_id];
                    REAL lag_fy = __ibfy[curve_id][point_id];
                    REAL delta_dx = DELTA1D(dx);
                    REAL delta_dy = DELTA1D(dy);
                    REAL delta_r = delta_dx*delta_dy;

                    fx += (delta_r * lag_fx);
                    fy += (delta_r * lag_fy);

                }
            }
        }
        _fx[i + j*m] = fx;
        _fy[i + j*m] = fy;
    }
}
*/
void ImmersedBoundaryMethod::distributeForce()
{
    if (this->immersedObjects.size() < 1)
    {
        return;
    }

    updateCellList(cellList);

    DEFINE_LAUNCH_PARAMETER(grid, block, m, n);
    kernel_distributeForce CUDALAUNCH2D(grid, block)
        (
        eul_fx.data_ptr(),
        eul_fy.data_ptr(),
        fxEntry,
        fyEntry,
        xEntry,
        yEntry,
        cellList.id.data_ptr(),
        cellList.count.data_ptr(),
        CellList::idr,
        ImmersedObject::MAX_POINTS,
        m,
        n,
        is_x_periodic,
        is_y_periodic
        );

}


void ImmersedBoundaryMethod::export_all_objects_shapes_af
(
const char* filename_sx,
const char* filename_sy,
const bool append
)
{
    int size_max = 0;
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        int size_this = immersedObjects[i]->size();
        size_max = size_max > size_this ? size_max : size_this;
    }
    DeviceArray<REAL> sx(0.0, size_max, immersedObjects.size());
    DeviceArray<REAL> sy(0.0, size_max, immersedObjects.size());
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        cudaMemcpy
            (
            sx.data_ptr() + i*size_max,
            immersedObjects[i]->get_x().data_ptr(),
            immersedObjects[i]->size()*sizeof(REAL), cudaMemcpyDeviceToDevice
            );
        cudaMemcpy
            (
            sy.data_ptr() + i*size_max,
            immersedObjects[i]->get_y().data_ptr(),
            immersedObjects[i]->size()*sizeof(REAL), cudaMemcpyDeviceToDevice
            );
    }
    sx.af_save("sx", filename_sx, append);
    sy.af_save("sy", filename_sy, append);
}

// export all immersed objects' shapes as txt format (ascii)
void ImmersedBoundaryMethod::export_all_objects_shapes_ascii
(
const char* filename_sx,
const char* filename_sy,
const int precision
)
{
    int size_max = 0;
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        int size_this = immersedObjects[i]->size();
        size_max = size_max > size_this ? size_max : size_this;
    }
    DeviceArray<REAL> sx(0.0, size_max, immersedObjects.size());
    DeviceArray<REAL> sy(0.0, size_max, immersedObjects.size());
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        cudaMemcpy
            (
            sx.data_ptr() + i*size_max,
            immersedObjects[i]->get_x().data_ptr(),
            immersedObjects[i]->size()*sizeof(REAL), cudaMemcpyDeviceToDevice
            );
        cudaMemcpy
            (
            sy.data_ptr() + i*size_max,
            immersedObjects[i]->get_y().data_ptr(),
            immersedObjects[i]->size()*sizeof(REAL), cudaMemcpyDeviceToDevice
            );
    }
    sx.save2D(filename_sx, 7);
    sy.save2D(filename_sy, 7);
}

void ImmersedBoundaryMethod::export_all_immersed_object_shapes(
    const char* file_name,
    const REAL x0,
    const REAL y0,
    const REAL dx,
    const REAL dy,
    const int precision)
{
    int size_total = 0;
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        size_total += immersedObjects[i]->size();
    }

    std::vector<REAL> x(size_total);
    std::vector<REAL> y(size_total);
    std::vector<int> id(size_total);

    int idx0 = 0;
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        if (immersedObjects[i]->size() > 0)
        {
            immersedObjects[i]->get_x().host(&x[idx0]);
            immersedObjects[i]->get_y().host(&y[idx0]);
            for (int idx = idx0; idx != idx0 + immersedObjects[i]->size(); ++idx)
            {
                id[idx] = i;
            }
            idx0 += immersedObjects[i]->size();
        }
    }

    std::ofstream output(file_name, std::ofstream::trunc);
    output.precision(precision);
    output << std::scientific;
	int id_prev = 0;
    for (int i = 0; i != size_total; ++i)
    {
		if (id[i] != id_prev) output << std::endl;
		id_prev = id[i];
        output << std::scientific;
        output << (x[i] + x0) * dx << " " << (y[i] + y0) * dy << " ";
        output << id[i];
        if (i != size_total - 1) output << std::endl;
    }


}

void ImmersedBoundaryMethod::make_boundary_from_label(const int* __restrict__ _label)
{
    std::vector<int> hlabel(m*n);
    std::vector<REAL> hxBoundary; hxBoundary.reserve(m*n);
    std::vector<REAL> hyBoundary; hyBoundary.reserve(m*n);
    cudaMemcpy(&hlabel[0], _label, m*n*sizeof(int), cudaMemcpyDeviceToHost);
    for (int iter = 0; iter != m*n; ++iter)
    {
        if (hlabel[iter] >= 0) continue;
        int i = iter % m;
        int j = iter / m;
        bool isBoundary = false;
        for (int jj = j - 1; jj <= j + 1; ++jj)
        {
            int jpos = jj; if (is_y_periodic) jpos = POSITIVE_MOD(jpos, n);
            if (jpos<0 || jpos>n - 1) continue;
            for (int ii = i - 1; ii <= i + 1; ++ii)
            {
                int ipos = ii; if (is_x_periodic) ipos = POSITIVE_MOD(ipos, m);
                if (ipos<0 || ipos>m - 1) continue;
                if (hlabel[ipos + jpos*m] >= 0) isBoundary = true;
            }
        }

        if (isBoundary)
        {
            hxBoundary.push_back((REAL)i);
            hyBoundary.push_back((REAL)j);
        }
    }
    xBoundary = DeviceArray<REAL>(&hxBoundary[0], hxBoundary.size());
    yBoundary = DeviceArray<REAL>(&hyBoundary[0], hxBoundary.size());
    cellList_boundary.registerCurve(xBoundary.data_ptr(), yBoundary.data_ptr(), xBoundary.size(), -m*n);

    // build neighbor list for boundary
    neighborList_boundary.update_block_list(cellList_boundary);
    neighborList_boundary.update(cellList_boundary);
}

// make boundary from DeviceArray
// added by Zhe Gou on 20211223
void ImmersedBoundaryMethod::make_boundary_from_array(
const DeviceArray<REAL>& x_bound_full, 
const DeviceArray<REAL>& y_bound_full
)
{
    // push boundary points
    xBoundary = x_bound_full;
    yBoundary = y_bound_full;
    
    // update cellList for boundary
    cellList_boundary.clear();
    cellList_boundary.registerCurve(xBoundary.data_ptr(), yBoundary.data_ptr(), xBoundary.size(), -m*n);

    // build neighbor list for boundary
    neighborList_boundary.update_block_list(cellList_boundary);
    neighborList_boundary.update(cellList_boundary);
}

__device__ __forceinline__ REAL device_positiveMod(const REAL& x, const int& m)
{
    return x - floor(x / m) * m;
}

__global__
void kernel_add_short_range_repulsive_force
(
REAL* __restrict__ _fx,
REAL* __restrict__ _fy,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int len,
const int id_obj,
const REAL* __restrict__ * __restrict__ const  __ibx, // ib for immersed boundary
const REAL* __restrict__ * __restrict__ const  __iby,
const int* __restrict__ _cellListId,
const int* __restrict__ _cellListCount,
const int* __restrict__ _cellListId_boundary,
const int* __restrict__ _cellListCount_boundary,
const REAL* __restrict__ _sbx, // sb for solid boundary
const REAL* __restrict__ _sby,
const int cellListR, // bucket length
const int MAX_POINTS, // maximum number of points in a immersed boundary curve
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        // find its bucket
        REAL x = _x[idx];
        REAL y = _y[idx];
        if (is_x_periodic) x = device_positiveMod(x, m);
        if (is_y_periodic) y = device_positiveMod(y, n);

        int mc = (m + cellListR - 1) / cellListR;
        int nc = (n + cellListR - 1) / cellListR;
        int ic = (int)floor(x / cellListR);
        int jc = (int)floor(y / cellListR);

        const int COUNT_TOTAL_MAX = 32;
        int count_total = 0;
        int count_now = 0;
        int id_list[COUNT_TOTAL_MAX];
        // construct local neighbour list from cell list
        // iter_jc starts from (-1 - (is_y_periodic&j == 0))
        // since the actual size of last cell can be less than cellListR if (m%cellListR!=0).
        // the same goes for i

        // scan immersed objects
        for (int iter_jc = -1 - (is_y_periodic & jc == 0); iter_jc <= 1; ++iter_jc)
        {
            int pos_jc = iter_jc + jc; if (is_y_periodic) pos_jc = POSITIVE_MOD(pos_jc, nc);
            if (pos_jc >= 0 & pos_jc < nc)
            {
                for (int iter_ic = -1 - (is_x_periodic & ic == 0); iter_ic <= 1; ++iter_ic)
                {
                    int pos_ic = iter_ic + ic; if (is_x_periodic) pos_ic = POSITIVE_MOD(pos_ic, mc);
                    if (pos_ic >= 0 & pos_ic < mc)
                    {
                        int count = _cellListCount[pos_ic + pos_jc*mc];
                        count = count < CellList::MAX_POINT_NUMBER_IN_CELL ? count : CellList::MAX_POINT_NUMBER_IN_CELL;
                        if (count == 0) continue;
                        count_total += count;
                        for (int iter_id = 0; iter_id < count; ++iter_id)
                        {
                            int id = _cellListId[pos_ic + pos_jc*mc + iter_id*mc*nc];
                            if (count_now < COUNT_TOTAL_MAX)
                                id_list[count_now++] = id;
                        }
                    }
                }
            }
        }
        // scan solid boundary
        for (int iter_jc = -1 - (is_y_periodic & jc == 0); iter_jc <= 1; ++iter_jc)
        {
            int pos_jc = iter_jc + jc; if (is_y_periodic) pos_jc = POSITIVE_MOD(pos_jc, nc);
            if (pos_jc >= 0 & pos_jc < nc)
            {
                for (int iter_ic = -1 - (is_x_periodic & ic == 0); iter_ic <= 1; ++iter_ic)
                {
                    int pos_ic = iter_ic + ic; if (is_x_periodic) pos_ic = POSITIVE_MOD(pos_ic, mc);
                    if (pos_ic >= 0 & pos_ic < mc)
                    {
                        int count = _cellListCount_boundary[pos_ic + pos_jc*mc];
                        count = count < CellList::MAX_POINT_NUMBER_IN_CELL ? count : CellList::MAX_POINT_NUMBER_IN_CELL;
                        if (count == 0) continue;
                        count_total += count;
                        for (int iter_id = 0; iter_id < count; ++iter_id)
                        {
                            int id = _cellListId_boundary[pos_ic + pos_jc*mc + iter_id*mc*nc];
                            if (count_now < COUNT_TOTAL_MAX)
                                id_list[count_now++] = id;
                        }
                    }
                }
            }
        }
        // end construction of neighbor list

        // calculate repulsive force on this point if count_total > 0
        if (count_total > 0)
        {
            
            count_total = count_total < COUNT_TOTAL_MAX ? count_total : COUNT_TOTAL_MAX;

            REAL fx_rep = (REAL)0.0;
            REAL fy_rep = (REAL)0.0;

            for (int iter = 0; iter != count_total; ++iter)
            {

                // read immersed point id from _cellListId
                // now this id is global
                int point_id = id_list[iter];

                // if point_id < 0, then this point is a solid boundary
                // it does not involve in immersed object boundary
                REAL xp, yp, dx, dy;
                int curve_id;
                if (point_id < 0)
                {
                    point_id = point_id + m*n;
                    curve_id = -1; // -1 is the obj label for all solid boundary points
                    xp = _sbx[point_id];
                    yp = _sby[point_id];

                    // exclude duplicate points
                    // added by Zhe Gou on 20211228
                    if (is_x_periodic & (xp < 0))
                        continue;
                    if (is_y_periodic & (yp < 0))
                        continue;
                }
                else
                {
                    // calculate point_id and curve_id
                    curve_id = point_id / MAX_POINTS;
                    point_id = point_id % MAX_POINTS;

                    // calculate distance between point x and i
                    xp = __ibx[curve_id][point_id];
                    yp = __iby[curve_id][point_id];
                }

                if (is_x_periodic)
                {
                    xp = device_positiveMod(xp, m);
                    dx = abs(x - xp);
                    if ((x > xp) & (dx > (m - dx)))
                        xp += m;
                    else if ((x < xp) & (dx >(m - dx)))
                        xp -= m;
                }
                dx = x - xp;

                if (is_y_periodic)
                {
                    yp = device_positiveMod(yp, n);
                    dy = abs(y - yp);
                    if ((y > yp) & (dy > (n - dy)))
                        yp += n;
                    else if ((y < yp) & (dy >(n - dy)))
                        yp -= n;
                }
                dy = y - yp;

                // add repulsive force
                REAL r = sqrt(dx*dx + dy*dy);
                // a point contributes the repulsive force if it is close enough
                // and not from the same immersed object
                if (r < (REAL)2.0 & (id_obj!= curve_id))
                {
                    REAL morse_force = MORSE_FORCE(r);
                    fx_rep += (morse_force * (dx / r));
                    fy_rep += (morse_force * (dy / r));
                    /*
                    printf("id_obj=%3i, idx=%3i at (%7.2f, %7.2f) is repulsed by id_obj=%3i, idx=%3i, at (%7.2f, %7.2f) with force (%7.5e, %7.5e), r = %7.4f\n",
                        id_obj, idx, x, y, curve_id, point_id, xp, yp, (morse_force * (dx / r)), (morse_force * (dy / r)), r);
                    */
                }
            }

            _fx[idx] += fx_rep;
            _fy[idx] += fy_rep;

        }

    }
}

// add repulsive force to all immersed objects
void ImmersedBoundaryMethod::add_short_range_repulsive_force()
{
    if (immersedObjects.size() < 1) return;
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        int len = immersedObjects[i]->size();
        kernel_add_short_range_repulsive_force CUDALAUNCH_ASYNC(len, immersedObjects[i]->cudaStream)
            (
            immersedObjects[i]->get_fx().data_ptr(),
            immersedObjects[i]->get_fy().data_ptr(),
            immersedObjects[i]->get_x().data_ptr(),
            immersedObjects[i]->get_y().data_ptr(),
            len,
            i,
            xEntry,
            yEntry,
            cellList.id.data_ptr(),
            cellList.count.data_ptr(),
            cellList_boundary.id.data_ptr(),
            cellList_boundary.count.data_ptr(),
            xBoundary.data_ptr(),
            yBoundary.data_ptr(),
            CellList::idr,
            ImmersedObject::MAX_POINTS,
            m,
            n,
            is_x_periodic,
            is_y_periodic
            );
    }
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        immersedObjects[i]->sync_operation();
    }
}

///////////////////////////////// Lennard-Jones Potential //////////////////////////////  function added by Mehdi 
// modified searching rage
// modified periodic boundary treatment
// Zhe Gou, 20201017
// modified LJ force calculation
// Zhe Gou, 20201204
__global__
void kernel_add_adhesion_force
(
REAL* __restrict__ _fx,
REAL* __restrict__ _fy,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const REAL* __restrict__ _curvature, // curvature for immersed boundary
const int len,
const int id_obj,
const REAL* __restrict__ * __restrict__ const  __ibx, // ib for immersed boundary
const REAL* __restrict__ * __restrict__ const  __iby,
const int* __restrict__ _cellListId,
const int* __restrict__ _cellListCount,
const int* __restrict__ _cellListId_boundary,
const int* __restrict__ _cellListCount_boundary,
const REAL* __restrict__ _sbx, // sb for solid boundary
const REAL* __restrict__ _sby,
const int cellListR, // bucket length
const int MAX_POINTS, // maximum number of points in a immersed boundary curve
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic,
const REAL De,
const REAL d0,
const REAL l0,
const REAL dx_phys, // grid size in physical unit
const int is_vesicle_adhesion,
const int is_boundary_adhesion
)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        // find its bucket
        REAL x = _x[idx];
        REAL y = _y[idx];
        if (is_x_periodic) x = device_positiveMod(x, m);
        if (is_y_periodic) y = device_positiveMod(y, n);

        int mc = (m + cellListR - 1) / cellListR;
        int nc = (n + cellListR - 1) / cellListR;
        int ic = (int)floor(x / cellListR);
        int jc = (int)floor(y / cellListR);

        // scan range for buckets
        // added by Zhe Gou at 20201018
        const int scan_range = (int)ceil(d0*3.0 / cellListR); 
        const int COUNT_TOTAL_MAX = 1000;
        int count_total = 0;
        int count_now = 0;
        int id_list[COUNT_TOTAL_MAX];
        // construct local neighbour list from cell list
        // iter_jc starts from (-1 - (is_y_periodic&j == 0))
        // since the actual size of last cell can be less than cellListR if (m%cellListR!=0).
        // the same goes for i

        // scan immersed objects
        for (int iter_jc = -scan_range - (is_y_periodic & jc == 0); iter_jc <= scan_range; ++iter_jc)
        {
            int pos_jc = iter_jc + jc; if (is_y_periodic) pos_jc = POSITIVE_MOD(pos_jc, nc);
            if (pos_jc >= 0 & pos_jc < nc)
            {
                for (int iter_ic = -scan_range - (is_x_periodic & ic == 0); iter_ic <= scan_range; ++iter_ic)
                {
                    int pos_ic = iter_ic + ic; if (is_x_periodic) pos_ic = POSITIVE_MOD(pos_ic, mc);
                    if (pos_ic >= 0 & pos_ic < mc)
                    {
                        int count = _cellListCount[pos_ic + pos_jc*mc];
                        count = count < CellList::MAX_POINT_NUMBER_IN_CELL ? count : CellList::MAX_POINT_NUMBER_IN_CELL;
                        if (count == 0) continue;
                        count_total += count;
                        for (int iter_id = 0; iter_id < count; ++iter_id)
                        {
                            int id = _cellListId[pos_ic + pos_jc*mc + iter_id*mc*nc];
                            if (count_now < COUNT_TOTAL_MAX)
                                id_list[count_now++] = id;
                        }
                    }
                }
            }
        }

        // scan solid boundary
        for (int iter_jc = -scan_range - (is_y_periodic & jc == 0); iter_jc <= scan_range; ++iter_jc)
        {
            int pos_jc = iter_jc + jc; if (is_y_periodic) pos_jc = POSITIVE_MOD(pos_jc, nc);
            if (pos_jc >= 0 & pos_jc < nc)
            {
                for (int iter_ic = -scan_range - (is_x_periodic & ic == 0); iter_ic <= scan_range; ++iter_ic)
                {
                    int pos_ic = iter_ic + ic; if (is_x_periodic) pos_ic = POSITIVE_MOD(pos_ic, mc);
                    if (pos_ic >= 0 & pos_ic < mc)
                    {
                        int count = _cellListCount_boundary[pos_ic + pos_jc*mc];
                        count = count < CellList::MAX_POINT_NUMBER_IN_CELL ? count : CellList::MAX_POINT_NUMBER_IN_CELL;
                        if (count == 0) continue;
                        count_total += count;
                        for (int iter_id = 0; iter_id < count; ++iter_id)
                        {
                            int id = _cellListId_boundary[pos_ic + pos_jc*mc + iter_id*mc*nc];
                            if (count_now < COUNT_TOTAL_MAX)
                                id_list[count_now++] = id;
                        }
                    }
                }
            }
        }
					
        // end construction of neighbor list
        if (count_total > 0)
        {
            
            count_total = count_total < COUNT_TOTAL_MAX ? count_total : COUNT_TOTAL_MAX;

            REAL fx_adh = (REAL)0.0;
            REAL fy_adh = (REAL)0.0;

            for (int iter = 0; iter != count_total; ++iter)
            {

                // read immersed point id from _cellListId
                // now this id is global
                int point_id = id_list[iter];

                // if point_id < 0, then this point is a solid boundary
                // it does not involve in immersed object boundary
                REAL xp, yp, dx, dy;
                int curve_id;
                if (point_id < 0)
                {
                    point_id = point_id + m*n; // modified by Zhe Gou on 20201019
                    curve_id = -1; // -1 is the obj label for all solid boundary points
                    xp = _sbx[point_id];
                    yp = _sby[point_id];

                    // exclude duplicate points
                    // added by Zhe Gou on 20211228
                    if (is_x_periodic & (xp < 0))
                        continue;
                    if (is_y_periodic & (yp < 0))
                        continue;
                }
                else
                {
                    // calculate point_id and curve_id
                    curve_id = point_id / MAX_POINTS;
                    point_id = point_id % MAX_POINTS;

                    // calculate distance between point x and i
                    xp = __ibx[curve_id][point_id];
                    yp = __iby[curve_id][point_id];
                }

                if (is_x_periodic)
                {
                    xp = device_positiveMod(xp, m);
                    dx = abs(x - xp);
                    if ((x > xp) & (dx > (m - dx)))
                        xp += m;
                    else if ((x < xp) & (dx >(m - dx)))
                        xp -= m;
                }
                dx = x - xp;

                if (is_y_periodic)
                {
                    yp = device_positiveMod(yp, n);
                    dy = abs(y - yp);
                    if ((y > yp) & (dy > (n - dy)))
                        yp += n;
                    else if ((y < yp) & (dy >(n - dy)))
                        yp -= n;
                }
                dy = y - yp;

                REAL r = sqrt(dx*dx + dy*dy);
                if ((r < (REAL)3.0*d0) & (id_obj != curve_id))
                {
                    if ((!is_vesicle_adhesion) & (curve_id != -1) & (r > d0))
                    {
                        // repulsive only
                        continue;
                    }
                    
                    if ((!is_boundary_adhesion) & (curve_id == -1) & (r > d0))
                    {
                        // repulsive only
                        continue;
                    }
                    
                    // corrected LJ force
                    // modified by Zhe Gou on 20201204

                    // outward normal vector
                    int idxL = (idx + len - 1) % len;
                    int idxR = (idx + 1) % len;
                    REAL tx = _x[idxR] - _x[idxL];
                    REAL ty = _y[idxR] - _y[idxL];
                    REAL nx = ty;
                    REAL ny = -tx;
                    REAL n_length = sqrt(nx*nx + ny*ny);
                    nx /= n_length;
                    ny /= n_length; 

                    REAL curvature_this = _curvature[idx] * dx_phys; // transfer to LB unit
                    REAL LJ_force = LJ_FORCE(r, De, d0) * (nx*dx/r + ny*dy/r) 
                                  + curvature_this * LJ_POTENTIAL(r, De, d0);   
                    if (curve_id != -1)
                        LJ_force *= (l0*l0);   
                    else
                        LJ_force *= l0;      
                    fx_adh += (LJ_force * nx);
                    fy_adh += (LJ_force * ny);
                }
            }

            _fx[idx] += fx_adh;
            _fy[idx] += fy_adh;

        }

    }
}

// add adhesion force to all immersed objects
void ImmersedBoundaryMethod::add_adhesion_force
(
DeviceArray<REAL>& curvature,
const REAL De, 
const REAL d0, 
const REAL l0,
const REAL dx_phys, // grid size in physical unit
const int is_vesicle_adhesion,
const int is_boundary_adhesion
)
{
    if (immersedObjects.size() < 1) return;
    
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        int len = immersedObjects[i]->size();
        kernel_add_adhesion_force CUDALAUNCH_ASYNC(len, immersedObjects[i]->cudaStream)
            (
            immersedObjects[i]->get_fx().data_ptr(),
            immersedObjects[i]->get_fy().data_ptr(),
            immersedObjects[i]->get_x().data_ptr(),
            immersedObjects[i]->get_y().data_ptr(),
            curvature.data_ptr() + i*len,
            len,
            i,
            xEntry,
            yEntry,
            cellList.id.data_ptr(),
            cellList.count.data_ptr(),
            cellList_boundary.id.data_ptr(),
            cellList_boundary.count.data_ptr(),
            xBoundary.data_ptr(),
            yBoundary.data_ptr(),
            CellList::idr,
            ImmersedObject::MAX_POINTS,
            m,
            n,
            is_x_periodic,
            is_y_periodic,
	        De,
	        d0,
            l0,
            dx_phys,
            is_vesicle_adhesion,
            is_boundary_adhesion
            );
    }
    for (int i = 0; i != immersedObjects.size(); ++i)
    {
        immersedObjects[i]->sync_operation();
    }
}

void ImmersedBoundaryMethod::update_neighbor_list(int i)
{
    if (i % neighborList.N_update_neighbor_list == 0)
    {
        updateCellList(cellList);
        if (i % neighborList.N_update_block_list == 0)
        {
            neighborList.update_block_list(cellList);
        }
        neighborList.update(cellList);
    }
}


__global__
// launch by block ((M_BLOCK*cellListR) * (N_BLOCK*cellListR))
// from _block_list, in a 1D manner
void kernel_distributeForce_neighborlist
(
REAL* __restrict__ _fx,
REAL* __restrict__ _fy,
const REAL* __restrict__ * __restrict__ const __ibfx,
const REAL* __restrict__ * __restrict__ const  __ibfy,
const REAL* __restrict__ * __restrict__ const  __ibx,
const REAL* __restrict__ * __restrict__ const  __iby,
const int* __restrict__ _block_list,
const int* __restrict__ _neighborList_id, // _id[pos + order * (block_list_capacity*M_BLOCK*N_BLOCK)]
const int* __restrict__ _neighborList_count,
const int cellListR, // bucket length
const int MAX_POINTS, // maximum number of points in a immersed boundary curve
const int block_list_capacity, // CAPACITY!!! NOT len!!!! size(id) = (block_list_capacity*M_BLOCK*N_BLOCK, MAX_POINT_NUMBER_IN_CELL)
const int m,
const int n,
const int mb,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int block_idx = _block_list[blockIdx.x];
    int ib = block_idx % mb;
    int jb = block_idx / mb;
    const int m_bsize = NeighborList::M_BLOCK*cellListR;
    const int n_bsize = NeighborList::N_BLOCK*cellListR;
    int i = ib*m_bsize + threadIdx.x % m_bsize;
    int j = jb*n_bsize + threadIdx.x / m_bsize;

    // position in _id
    int pos = blockDim.x * blockIdx.x + threadIdx.x / (cellListR*cellListR);
    int count_total = _neighborList_count[pos];

    REAL fx = 0.0;
    REAL fy = 0.0;

    // calculate force on this grid if count_total > 0
    if (count_total > 0)
    {
        for (int iter = 0; iter != count_total; ++iter)
        {

            // read immersed point id from _cellListId
            // now this id is global
            int point_id = _neighborList_id[pos + block_list_capacity*(NeighborList::M_BLOCK*NeighborList::N_BLOCK)*iter];

            // if point_id < 0, then this point is a solid boundary
            // it does not involve in immersed object boundary
            if (point_id < 0) continue;

            // calculate point_id and curve_id
            int curve_id = point_id / MAX_POINTS;
            point_id = point_id % MAX_POINTS;

            // calculate distance between point x and i
            REAL dx = __ibx[curve_id][point_id];
            REAL dy = __iby[curve_id][point_id];

            if (is_x_periodic) dx = dx - floor(dx / m)*m;
            dx = abs(dx - i); // no worry, abs and min are defined in cuda's header __MATH_FUNCTIONS_HPP__
            dx = min(dx, m - dx);

            if (is_y_periodic) dy = dy - floor(dy / n)*n;
            dy = abs(dy - j);
            dy = min(dy, n - dy);

            // add contribution from ibfx and ibfy to fx and fy
            if (dx < (REAL)2.0 & dy < (REAL)2.0)
            {
                REAL lag_fx = __ibfx[curve_id][point_id];
                REAL lag_fy = __ibfy[curve_id][point_id];
                REAL delta_dx = DELTA1D(dx);
                REAL delta_dy = DELTA1D(dy);
                REAL delta_r = delta_dx*delta_dy;

                fx += (delta_r * lag_fx);
                fy += (delta_r * lag_fy);

            }
        }
    }
    _fx[i + j*m] = fx;
    _fy[i + j*m] = fy;
}

void ImmersedBoundaryMethod::distributeForce_neighborlist(int i)
{

    update_neighbor_list(i);

    const int block_size = NeighborList::M_BLOCK*CellList::idr * NeighborList::N_BLOCK*CellList::idr;
    kernel_distributeForce_neighborlist <<<*neighborList.ptr_block_list_len, block_size >>>
        (
        eul_fx.data_ptr(),
        eul_fy.data_ptr(),
        fxEntry,
        fyEntry,
        xEntry,
        yEntry,
        neighborList.block_list.data_ptr(),
        neighborList.id.data_ptr(),
        neighborList.count.data_ptr(),
        CellList::idr,
        ImmersedObject::MAX_POINTS,
        neighborList.block_list_capacity,
        m,
        n,
        neighborList.mb,
        is_x_periodic,
        is_y_periodic);
}