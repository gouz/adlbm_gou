#ifndef ADboundary_cuh
#define ADboundary_cuh

// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

#include "DeviceArray.cuh"

#include "ADmethod.cuh"
#include "ADboundaryVoxelizer.cuh"

// precision
#ifndef REAL
#define REAL double
#endif

class ADBoundary{
public:
    const int m;  // mesh size x
    const int n;  // mesh size y
    const bool is_x_periodic;
    const bool is_y_periodic;
private:

public:
    // gpu voxelizer
    LBMGPUvoxelizer voxelizer;

    // Geometry
    DeviceArray<int> idx;       // index of internal boundary
    DeviceArray<REAL> offset;   // internal boundary offset position on an intersecting velocity
    DeviceArray<REAL> nx;       // normal x component
    DeviceArray<REAL> ny;       // normal y component
    DeviceArray<REAL> s;        // lagrangian length coordinate
    DeviceArray<int> id;        // custom boundary ID
    DeviceArray<int> boundary_size; // boundary size

    // kernel launch relevent to boundary treatment will use this value
    // make sure this is larger than boundary_size on device
    int kernel_launch_size;

    // the assigned size of DeviceArrays of boundary properties
    int BOUNDARY_LIST_CAPACITY;

    DeviceArray<REAL> rho_in;   // concentration values on internal boundary
    DeviceArray<REAL> rho_ex;   // concentration values on internal boundary

    // *************************************************************************
    // boundary condition n * grad(rho) == C (Neumann), or rho == C (Dirichlet)
    DeviceArray<REAL> C_in; // internal boundary condition values
    DeviceArray<REAL> C_ex; // external boundary condition values

    // internal (left hand side of boundary vector) boundary type:
    // value 0: neumann boundary condition (default)
    // value 1: dirichlet boundary condition
    // other: no meaning, use default
    DeviceArray<short> Ctype_in;

    // external (right hand side of boundary vector) boundary type:
    // value 0: neumann boundary condition (default)
    // value 1: dirichlet boundary condition
    // other: no meaning, use default
    DeviceArray<short> Ctype_ex;
    // *************************************************************************

public:
    // this method will trigger cuda mem copy, since boundary size is stored on GPU
    inline int size() const
    {
        int hbsize;
        boundary_size.host(&hbsize);
        cudaStreamSynchronize(cudaStreamDefault);
        return hbsize;
    }

    inline int capacity(){ return BOUNDARY_LIST_CAPACITY; }

    inline void clear(){
        idx.clear();
        offset.clear();
        nx.clear();
        ny.clear();
        s.clear();
        rho_in.clear();
        rho_ex.clear();
        C_in.clear();
        C_ex.clear();
        Ctype_in.clear();
        Ctype_ex.clear();
        id.clear();
        boundary_size.clear();
        kernel_launch_size = 0;
        BOUNDARY_LIST_CAPACITY = 0;
    }

    // BOUNDARY_LIST_CAPACITY shouldn't be resized during the simulation
    inline ADBoundary
        (
        const int _m,
        const int _n,
        const int _kernel_launch_size,
        const int _is_x_periodic,
        const int _is_y_periodic,
        const int _BOUNDARY_LIST_CAPACITY = 0
        ) :
        m(_m),
        n(_n),
        is_x_periodic(_is_x_periodic),
        is_y_periodic(_is_y_periodic),
        voxelizer(_m, _n, _is_x_periodic, _is_y_periodic, _m*_n, _m*_n), // very large MAX_SEGMENT and MAX_RECORD,
        kernel_launch_size(_kernel_launch_size),                         //  if the application is memory bounded,
        BOUNDARY_LIST_CAPACITY(_BOUNDARY_LIST_CAPACITY)                  //         consider reduce these numbers.
    {

        if (BOUNDARY_LIST_CAPACITY == 0) BOUNDARY_LIST_CAPACITY = m*n / 2;

        idx = DeviceArray<int>(0, BOUNDARY_LIST_CAPACITY);
        id = DeviceArray<int>(0, BOUNDARY_LIST_CAPACITY);
        offset = DeviceArray<REAL>(0.0, BOUNDARY_LIST_CAPACITY);
        nx = DeviceArray<REAL>(0.0, BOUNDARY_LIST_CAPACITY);
        ny = DeviceArray<REAL>(0.0, BOUNDARY_LIST_CAPACITY);
        s = DeviceArray<REAL>(0.0, BOUNDARY_LIST_CAPACITY);
        rho_in = DeviceArray<REAL>(0.0, BOUNDARY_LIST_CAPACITY);
        rho_ex = DeviceArray<REAL>(0.0, BOUNDARY_LIST_CAPACITY);
        C_in = DeviceArray<REAL>(0.0, BOUNDARY_LIST_CAPACITY);
        C_ex = DeviceArray<REAL>(0.0, BOUNDARY_LIST_CAPACITY);
        Ctype_in = DeviceArray<short>((short)0, BOUNDARY_LIST_CAPACITY);
        Ctype_ex = DeviceArray<short>((short)0, BOUNDARY_LIST_CAPACITY);
        boundary_size = DeviceArray<int>(0, 1);
    }

    inline void registerCurve
        (
        const REAL* __restrict__ __x,
        const REAL* __restrict__ __y,
        const int _len,
        const bool _is_circle,
        const int _id_boundary = 0 // different boundary id can use different boundary conditions
        )
    {
        voxelizer.setBoundaryCurve(__x, __y, _len, _is_circle, _id_boundary);
    }

    inline void registerCurveFinished(REAL* __restrict__ _f, REAL* __restrict__ _rho)
    {
        voxelizer.setBoundaryCurveFinished
            (
            idx.data_ptr(),
            id.data_ptr(),
            offset.data_ptr(),
            nx.data_ptr(),
            ny.data_ptr(),
            s.data_ptr(),
            boundary_size.data_ptr(),
            _f,
            _rho
            );

    }

    void computeConcentrationOnBoundary(const ADmethod& method, const REAL* _f);
    void computeConcentrationOnBoundary(const ADmethod& method, const REAL* _f, const REAL* _ux, const REAL* _uy);

    void computeConcentrationOnBoundary(const ADmethod& method, const REAL* _f, const int* _Didx);
    void computeConcentrationOnBoundary(const ADmethod& method, const REAL* _f, const REAL* _ux, const REAL* _uy, const int* _Didx);

    void setAllBoundaryConditions(const ADmethod& method, REAL* _f, REAL* _f0);
    void setAllBoundaryConditions(const ADmethod& method, REAL* _f, REAL* _f0, const int* _Didx);
    void setAllBoundaryConditions(const ADmethod& method, REAL* _f, REAL* _f0, const REAL* _ux, const REAL* _uy);
    void setAllBoundaryConditions(const ADmethod& method, REAL* _f, REAL* _f0, const REAL* _ux, const REAL* _uy, const int* _Didx);
    
    // export boundary information to file in the format of ARRAYFIRE (GOOGLE IT)
    void exportToFile_ArrayFire(const char fname[]) const;

    // return boundary coordinates, (x,y), make sure you have allocated enough memory
    void getBoundaryCoordinates(REAL* _x, REAL* _y);

    // calculate shear stress along the boundary with a reference layer distance
    void calculate_boundary_shear_stress(
        REAL* shear_stress,
        const REAL* _ux,
        const REAL* _uy,
        const REAL dx_probe,
        const REAL mu, // dynamics viscosity
        const bool is_x_periodic,
        const bool is_y_periodic,
        const REAL dt);
};

#endif