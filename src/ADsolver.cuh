#ifndef ADsolver_cuh
#define ADsolver_cuh
// ************************************************************************************************
// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

// DeviceArray header
#include "DeviceArray.cuh"

#include "LBMconstant.hpp"

#include "ADmethod.cuh"
#include "ADboundary.cuh"
// ************************************************************************************************
class ADsolver
{
private:
    ADmethod method; // advection-diffusion-reaction solver without boundary
public:
    AD::CONFIG& config;
    ADBoundary boundary; // deformable or static boundary, moving bounrary curves should be registered at each time step
    // these are pointers to device memory! // BE VERY CARFUL TO USE THEM!
    
    const REAL* ux;       // device pointer to x velocity (lattice unit)
    const REAL* uy;       // device pointer to  y velocity (lattice unit)
    const REAL* source;   // device pointer to  reaction source term

    // device pointer to label for different diffusion coefficient in different lattices
    // negative value means this lattice is masked
    const int* Didx;
    
public:
    const int m;    // x mesh
    const int n;    // y mesh
    int ics;        // current time step
    // LBmethod for this ADsolver, including functions such as feq, time step advance etc.
    DeviceArray<REAL> rho;  // macro concentration rho
    DeviceArray<REAL> f;    // time-step-current Distribution function
private:
    DeviceArray<REAL> f1;   // time-step-foward Distribution function

public:
    inline ADsolver
        (
        const REAL _D,
        const int _m,
        const int _n,
        const int _boundary_list_size_upper_limit,
        const REAL* _ux = nullptr,
        const REAL* _uy = nullptr,
        const REAL* _source = nullptr,
        const int* _Didx = nullptr,
        const AD::BOUNDARY_TYPE _X_CONFIG = AD::PERIODIC,
        const AD::BOUNDARY_TYPE _Y_CONFIG = AD::BOUNCEBACK
        ) :
        m(_m),
        n(_n),
        method(_D),
        config(method.config),
        ics(0),
        boundary(_m, _n, _boundary_list_size_upper_limit, _X_CONFIG == AD::PERIODIC, _Y_CONFIG == AD::PERIODIC),
        rho(0.0, _m, _n),
        f(0.0, _m, _n, ADmethod::Q),
        f1(0.0, _m, _n, ADmethod::Q),
        ux(_ux),
        uy(_uy),
        source(_source),
        Didx(_Didx)
    {
        config.X_BOUNDARY_TYPE = _X_CONFIG;
        config.Y_BOUNDARY_TYPE = _Y_CONFIG;
    }

    inline ADsolver
        (
        const REAL* const _D,
        const int _nD,
        const int _m,
        const int _n,
        const int _boundary_list_size_upper_limit,
        const REAL* _ux = nullptr,
        const REAL* _uy = nullptr,
        const REAL* _source = nullptr,
        const int* _Didx = nullptr,
        const AD::BOUNDARY_TYPE _X_CONFIG = AD::PERIODIC,
        const AD::BOUNDARY_TYPE _Y_CONFIG = AD::BOUNCEBACK
        ) :
        m(_m), n(_n),
        method(_D, _nD),
        config(method.config),
        boundary(_m, _n, _boundary_list_size_upper_limit, _X_CONFIG == AD::PERIODIC, _Y_CONFIG == AD::PERIODIC),
        ics(0),
        rho(0.0, _m, _n),
        f(0.0, _m, _n, ADmethod::Q),
        f1(0.0, _m, _n, ADmethod::Q),
        ux(_ux),
        uy(_uy),
        source(_source),
        Didx(_Didx)
    {
        config.X_BOUNDARY_TYPE = _X_CONFIG;
        config.Y_BOUNDARY_TYPE = _Y_CONFIG;
    }
    inline void reset_diffusion_coefficient(const REAL DiffCoeff = 1.0 / 6.0){
        method.reset_diffusion_coefficient(DiffCoeff);
    }
    inline void reset_diffusion_coefficient(const REAL* DiffCoeff, const int n){
        method.reset_diffusion_coefficient(DiffCoeff, n);
    }

// ************************************************************************************************
    inline ~ADsolver()
    {

    }

    inline const int& getMeshSize_x() const { return m; }
    inline const int& getMeshSize_y() const { return n; }
    inline int getKernelLaunchSizeForBoundary() const { return boundary.kernel_launch_size; }

    // boundary size is stored on GPU, on array DeviceArray<int> boundary.boundary_size
    inline const DeviceArray<int>& getBoundarySizeOnDevice() const { return boundary.boundary_size; }
    // boundary size is stored on GPU, on array DeviceArray<int> boundary.boundary_size
    inline const int* getBoundarySizePointer() const { return boundary.boundary_size.data_ptr(); }
// ************************************************************************************************
    inline void setInitialConcentration(const REAL* _rho)
    {
        cudaError_t eror = cudaMemcpy(rho.data_ptr(), _rho, rho.size() * sizeof(REAL), cudaMemcpyDeviceToDevice);
        method.feq(f.data_ptr(), rho.data_ptr(), m, n);
        f1 = f;
    }

    inline void setInitialConcentration(const REAL _rho)
    {
        rho = _rho;
        method.feq(f.data_ptr(), rho.data_ptr(), m, n);
        f1 = f;
    }

    inline void setInitialConcentration(const DeviceArray<REAL>& _rho)
    {
        rho = _rho;
        if (nullptr == this->ux){
            method.feq(f.data_ptr(), _rho.data_ptr(), m, n);
        }
        else{
            method.feq(f.data_ptr(), _rho.data_ptr(), ux, uy, m, n);
        }
        f1 = f;
    }
// ************************************************************************************************
    // set a new boundary curve in the calculation domain
    // Lattice unit is used, calculation domain is [0:m-1] * [0:n-1]
    inline void registerCurve
        (
        const REAL* __restrict__ __x,
        const REAL* __restrict__ __y,
        const int _len,
        const bool _is_circle,
        const int _id_boundary = 0 // different boundary id can use different boundary conditions
        )
    {
        boundary.registerCurve(__x, __y, _len, _is_circle, _id_boundary);
    }

    inline void registerCurve
        (
        const DeviceArray<REAL>& x,
        const DeviceArray<REAL>& y,
        const bool _is_circle,
        const int _id_boundary = 0
        )
    {
        boundary.registerCurve(x.data_ptr(), y.data_ptr(), x.size(), _is_circle, _id_boundary);
    }

    // this function must be called after all boundary curves are set
    // it will build boundary pixels and their relevent information
    // if registerCurveFinished() is called for multiple times
    // the boundary manager will consider this is a moving boundary simulation
    // then it will try to recover internal/external flipped lattices
    // DO NOT destroy the curve data before calling this function!!!!
    inline void registerCurveFinished()
    {
        boundary.registerCurveFinished(f.data_ptr(), rho.data_ptr());
    }
// ************************************************************************************************
    // if you need concentration on the boundary to set your boundary condition, call this function
    // if advection is considered, this function must be called before time advancing and after boundary geometry renewed.
    inline void computeConcentrationOnBoundary()
    {
        if (nullptr == ux)
        {
            if (nullptr == Didx)
            {
                boundary.computeConcentrationOnBoundary(method, f.data_ptr());
            }
            else
            {
                boundary.computeConcentrationOnBoundary(method, f.data_ptr(), Didx);
            }
        }
        else
        {
            if (nullptr == Didx)
            {
                boundary.computeConcentrationOnBoundary(
                    method, f.data_ptr(),
                    ux,
                    uy);
            }
            else
            {
                boundary.computeConcentrationOnBoundary(
                    method, f.data_ptr(),
                    ux,
                    uy,
                    Didx);
            }
        }

    }
// ************************************************************************************************
    // ADsolver::advanceTimeStep() will
    // 1. call ADmethod::advance()
    // 2. call ADboundary::setAllBoundaryConditions()
    // 3. swap new / old distribution function array pointer
    // 4. time step count ics++
    // PS: CALL ADsolver::computeConcentrationOnBoundary() before this if ADVECTION is considered!
    inline void advanceTimeStep_MRT()
    {
        if (nullptr == ux)
        {
            if (nullptr == source)
            {
                if (nullptr == Didx)
                {
                    // pure duffusion, homogeneous diffusion coefficient
                    method.advance_diffusion_MRT(
                        rho.data_ptr(),
                        f1.data_ptr(),
                        f.data_ptr(),
                        m, n);
                    boundary.setAllBoundaryConditions
                        (
                        method,
                        f1.data_ptr(),
                        f.data_ptr()
                        );
                }
                else
                {
                    // pure duffusion, inhomogeneous diffusion coefficient
                    method.advance_diffusion_MRT(
                        rho.data_ptr(),
                        f1.data_ptr(),
                        f.data_ptr(),
                        Didx,
                        m, n);
                    boundary.setAllBoundaryConditions(
                        method,
                        f1.data_ptr(),
                        f.data_ptr(),
                        Didx);
                }
            }
            else
            {
                if (nullptr == Didx)
                {
                    // diffusion reaction, homogeneous diffusion coefficient
                    method.advance_diffusion_reaction_MRT(
                        rho.data_ptr(),
                        f1.data_ptr(),
                        f.data_ptr(),
                        source,
                        m, n);
                    boundary.setAllBoundaryConditions(
                        method,
                        f1.data_ptr(),
                        f.data_ptr());
                }
                else
                {
                    // diffusion reaction, inhomogeneous diffusion coefficient
                    method.advance_diffusion_reaction_MRT(
                        rho.data_ptr(),
                        f1.data_ptr(),
                        f.data_ptr(),
                        source,
                        Didx,
                        m, n);
                    boundary.setAllBoundaryConditions(
                        method,
                        f1.data_ptr(),
                        f.data_ptr(),
                        Didx);
                }
            }
        }
        else
        {
            if (nullptr == source)
            {
                if (nullptr == Didx)
                {
                    // advection duffusion, homogeneous diffusion coefficient
                    method.advance_advection_diffusion_MRT(
                        rho.data_ptr(),
                        f1.data_ptr(),
                        f.data_ptr(),
                        ux,
                        uy,
                        m, n);
                    boundary.setAllBoundaryConditions(
                        method,
                        f1.data_ptr(),
                        f.data_ptr(),
                        ux,
                        uy);
                }
                else
                {
                    // advection duffusion, inhomogeneous diffusion coefficient
                    method.advance_advection_diffusion_MRT(
                        rho.data_ptr(),
                        f1.data_ptr(),
                        f.data_ptr(),
                        ux,
                        uy,
                        Didx,
                        m, n);
                    boundary.setAllBoundaryConditions(
                        method,
                        f1.data_ptr(),
                        f.data_ptr(),
                        ux,
                        uy,
                        Didx);
                }
            }
            else
            {
                if (nullptr == Didx)
                {
                    // advection duffusion reaction, homogeneous diffusion coefficient
                    method.advance_advection_diffusion_reaction_MRT(
                        rho.data_ptr(),
                        f1.data_ptr(),
                        f.data_ptr(),
                        ux,
                        uy,
                        source,
                        m, n);
                    boundary.setAllBoundaryConditions(
                        method,
                        f1.data_ptr(),
                        f.data_ptr(),
                        ux,
                        uy);
                }
                else
                {
                    // advection duffusion reaction, inhomogeneous diffusion coefficient
                    method.advance_advection_diffusion_reaction_MRT(
                        rho.data_ptr(),
                        f1.data_ptr(),
                        f.data_ptr(),
                        ux,
                        uy,
                        source,
                        Didx,
                        m, n);
                    boundary.setAllBoundaryConditions(
                        method,
                        f1.data_ptr(),
                        f.data_ptr(),
                        ux,
                        uy,
                        Didx);
                }
            }
        }
        // after advancing time step and setting boundary condition
        // swap new and old fields
        DA::swap_data_ptr(f, f1);
        ics++;
    }
// ************************************************************************************************
    // ADsolver::advanceTimeStep() will
    // 1. call ADmethod::advance()
    // 2. call ADboundary::setAllBoundaryConditions()
    // 3. swap new / old distribution function array pointer
    // 4. time step count ics++
    // PS: CALL ADsolver::computeConcentrationOnBoundary() before this if ADVECTION is considered!
    // mode: 0 advance concentration field and then boundaries
    // mode: 1 advance only concentration field
    // mode: 2 advance only boundaries
    inline void advanceTimeStep(const int mode = 0)
    {
        if (nullptr == ux)
        {
            if (nullptr == source)
            {
                if (nullptr == Didx)
                {
                    // pure duffusion, homogeneous diffusion coefficient
                    if (mode != 2)
                    {
                        method.advance_diffusion(
                            rho.data_ptr(),
                            f1.data_ptr(),
                            f.data_ptr(),
                            m, n);
                    }

                    if (mode != 1)
                    {
                        cudaStreamSynchronize(method.stream);
                        boundary.setAllBoundaryConditions(
                            method,
                            f1.data_ptr(),
                            f.data_ptr());
                    }
                }
                else
                {
                    // pure duffusion, inhomogeneous diffusion coefficient
                    if (mode != 2)
                    {
                        method.advance_diffusion(
                            rho.data_ptr(),
                            f1.data_ptr(),
                            f.data_ptr(),
                            Didx,
                            m, n);
                    }

                    if (mode != 1)
                    {
                        cudaStreamSynchronize(method.stream);
                        boundary.setAllBoundaryConditions(
                            method,
                            f1.data_ptr(),
                            f.data_ptr(),
                            Didx);
                    }
                }
            }
            else
            {
                if (nullptr == Didx)
                {
                    // diffusion reaction, homogeneous diffusion coefficient
                    if (mode != 2)
                    {
                        method.advance_diffusion_reaction(
                            rho.data_ptr(),
                            f1.data_ptr(),
                            f.data_ptr(),
                            source,
                            m, n);
                    }

                    if (mode != 1)
                    {
                        cudaStreamSynchronize(method.stream);
                        boundary.setAllBoundaryConditions(
                            method,
                            f1.data_ptr(),
                            f.data_ptr());
                    }
                }
                else
                {
                    // diffusion reaction, inhomogeneous diffusion coefficient
                    if (mode != 2)
                    {
                        method.advance_diffusion_reaction(
                            rho.data_ptr(),
                            f1.data_ptr(),
                            f.data_ptr(),
                            source,
                            Didx,
                            m, n);
                    }

                    if (mode != 1)
                    {
                        cudaStreamSynchronize(method.stream);
                        boundary.setAllBoundaryConditions(
                            method,
                            f1.data_ptr(),
                            f.data_ptr(),
                            Didx);
                    }
                }
            }
        }
        else
        {
            if (nullptr == source)
            {
                if (nullptr == Didx)
                {
                    // advection duffusion, homogeneous diffusion coefficient
                    if (mode != 2)
                    {
                        method.advance_advection_diffusion(
                            rho.data_ptr(),
                            f1.data_ptr(),
                            f.data_ptr(),
                            ux,
                            uy,
                            m, n);
                    }

                    if (mode != 1)
                    {
                        cudaStreamSynchronize(method.stream);
                        boundary.setAllBoundaryConditions(
                            method,
                            f1.data_ptr(),
                            f.data_ptr(),
                            ux,
                            uy);
                    }
                }
                else
                {
                    // advection duffusion, inhomogeneous diffusion coefficient
                    if (mode != 2)
                    {
                        method.advance_advection_diffusion(
                            rho.data_ptr(),
                            f1.data_ptr(),
                            f.data_ptr(),
                            ux,
                            uy,
                            Didx,
                            m, n);
                    }

                    if (mode != 1)
                    {
                        cudaStreamSynchronize(method.stream);
                        boundary.setAllBoundaryConditions(
                            method,
                            f1.data_ptr(),
                            f.data_ptr(),
                            ux,
                            uy,
                            Didx);
                    }
                }
            }
            else
            {
                if (nullptr == Didx)
                {
                    // advection duffusion reaction, homogeneous diffusion coefficient
                    if (mode != 2)
                    {
                        method.advance_advection_diffusion_reaction(
                            rho.data_ptr(),
                            f1.data_ptr(),
                            f.data_ptr(),
                            ux,
                            uy,
                            source,
                            m, n);
                    }

                    if (mode != 1)
                    {
                        cudaStreamSynchronize(method.stream);
                        boundary.setAllBoundaryConditions(
                            method,
                            f1.data_ptr(),
                            f.data_ptr(),
                            ux,
                            uy);
                    }
                }
                else
                {
                    // advection duffusion reaction, inhomogeneous diffusion coefficient
                    if (mode != 2)
                    {
                        method.advance_advection_diffusion_reaction(
                            rho.data_ptr(),
                            f1.data_ptr(),
                            f.data_ptr(),
                            ux,
                            uy,
                            source,
                            Didx,
                            m, n);
                    }

                    if (mode != 1)
                    {
                        cudaStreamSynchronize(method.stream);
                        boundary.setAllBoundaryConditions(
                            method,
                            f1.data_ptr(),
                            f.data_ptr(),
                            ux,
                            uy,
                            Didx);
                    }
                }
            }
        }

        // after advancing time step and setting boundary condition
        // swap new and old fields
        DA::swap_data_ptr(f, f1);
        ics++;
    }
// ************************************************************************************************
    inline void calculate_shear_stress_on_boundary
        (
        REAL* __restrict__ _shear_stress, // user defined device array to store shear stress value
        const REAL viscosity = 1.0, // fluid dynamic viscosity
        const REAL dt = 1.0, // dt for fluid solver (scalar)
        const REAL dn = 1.5 // diff(u_tangential, normal) will be approximate by a finite difference with dn*dx length
        )
    {
        boundary.calculate_boundary_shear_stress(
            _shear_stress,
            ux,
            uy,
            dn,
            viscosity,
            config.X_BOUNDARY_TYPE == AD::PERIODIC,
            config.Y_BOUNDARY_TYPE == AD::PERIODIC, dt);
    }

// ************************************************************************************************
    // save AD field for initialization
    // added by Zhe Gou, 20200319
    inline void af_save_ad_field(const char* fname = "ad_field.afdat")
    {
        rho.af_save("rho", fname, false);
        f.af_save("f", fname, true);
        f1.af_save("f1", fname, true);
    }
// ************************************************************************************************
    // read flow field from file exported from af_save_ad_field()
    // file must be formated in ArrayFire binary files
    // added by Zhe Gou, 20200319
    void initializeByFile(const char* f_filename)
    {
        const int Q = ADmethod::Q;
        auto rho_temp = DA::readArray<REAL>(f_filename, 0);
        auto f_temp = DA::readArray<REAL>(f_filename, 1);
        auto f1_temp = DA::readArray<REAL>(f_filename, 2);
        bool size_validity;

        size_validity = rho_temp.dim(0) == m && rho_temp.dim(1) == n 
                     && rho_temp.dim(2) == 1 && rho_temp.dim(3) == 1;
        assert(size_validity);
        size_validity = f_temp.dim(0) == m && f_temp.dim(1) == n 
                     && f_temp.dim(2) == Q && f_temp.dim(3) == 1;
        assert(size_validity);
        size_validity = f1_temp.dim(0) == m && f1_temp.dim(1) == n 
                     && f1_temp.dim(2) == Q && f1_temp.dim(3) == 1;
        assert(size_validity);

        rho = rho_temp;
        f = f_temp;
        f1 = f1_temp;
    }

// ************************************************************************************************
    // reset concentration inside vesicle to 0
    // added by Zhe Gou, 20211208
    inline void reset(const DeviceArray<int>& label)
    {
        method.reset( 
            rho.data_ptr(), 
            f1.data_ptr(), 
            f.data_ptr(), 
            label.data_ptr(),
            m, n);
    }
};
#endif
