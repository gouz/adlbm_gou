#ifndef Example_cuh
#define Example_cuh

#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include "LBMheader.cuh"

inline void boundary_export_example()
{
    ConfigFile cfg("boundary_export_example.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, boundary_export_example not issued\n");
        return;
    }
    DEFINE_FROM_CONFIGFILE(int, cfg, "parameters", m, 20);
    DEFINE_FROM_CONFIGFILE(int, cfg, "parameters", n, 20);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "parameters", boundary_filename, std::string("boundary.txt"));

    ADsolver solver(1.0 / 6.0, m, n, m*n, nullptr, nullptr, nullptr, nullptr, AD::BOUNCEBACK, AD::BOUNCEBACK);
    
    DataFile boundary_data(boundary_filename.c_str());
    int n_bound = boundary_data.get_n() / 3;

    std::vector< DeviceArray<REAL> > x_bound(n_bound);
    std::vector< DeviceArray<REAL> > y_bound(n_bound);

    for (int i = 0; i != n_bound; ++i)
    {
        x_bound[i] = DeviceArray<REAL>
            (
            &boundary_data.getData()[i * 3 + 1][0],
            boundary_data.getData()[i * 3 + 1].size()
            );
        y_bound[i] = DeviceArray<REAL>
            (
            &boundary_data.getData()[i * 3 + 2][0],
            boundary_data.getData()[i * 3 + 2].size()
            );
        bool is_circle_bound = std::abs(boundary_data.getData()[i * 3][0]) > 1e-5;
        solver.registerCurve(x_bound[i], y_bound[i], is_circle_bound, 0);
        auto curve = DA::join(x_bound[i], y_bound[i]);
        curve.reshape(x_bound[i].dim(0), 2);
        char curve_file_name[100];
        sprintf(curve_file_name, "curve%08d.afdat", i);
        curve.af_save(curve_file_name, curve_file_name);
    }
    solver.registerCurveFinished();
    solver.boundary.exportToFile_ArrayFire("bound0.afdat");
}

inline void high_reynolds_cavity_flow()
{
    ConfigFile cfg("high_reynolds_cavity_flow.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation high_reynolds_cavity_flow not issued\n");
        return;
    }
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", is_MRT, 1);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", m, 256);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n, 256);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", Re, 1e5);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", rho, 2.7);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", u_wall, 1e-2);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tend, 2000000);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tbeg_export, 0); // export starts from this time step
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames, 200); // total export frames for field information
    REAL viscosity = u_wall*m / Re;
    printf("viscosity = %f\n", viscosity);

    DeviceArray<int> X, Y;
    DA::meshgrid(X, Y, 0, 0, 1, 1, m, n);
    DeviceArray<int> label = DeviceArray<int>(
        ((Y < n - 1) && (Y > 0) && (X < m - 1) && (X > 0)) ||
        ((Y > n - 2) && (Y < n - 1))) - 1; // closed
    da_saveAF(label);
    NSsolver flow(viscosity, m, n, NS::PERIODIC, NS::BOUNCEBACK, nullptr, nullptr, label.data_ptr());
    flow.config.IMPOSING_VELOCITY = true;
    flow.initializeByDensity(rho);

    flow.ux = DeviceArray<REAL>((Y == n - 1))*u_wall;
    flow.uy = 0.0;

    // start simulation
    // start timer
    SimpleTimer timer;
    timer.tic();
    for (int i = 0; i != Tend; ++i)
    {
        if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
        {
            std::cout << "i = " << i + 1 << ", write frame" << std::endl;
            // save flow field
            flow.ux.af_save("ux", "ux.afdat", i != 0);
            flow.uy.af_save("uy", "uy.afdat", i != 0);
            flow.rho.af_save("rho", "rho.afdat", i != 0);
        }

        // advance flow
        if (is_MRT) flow.advanceFlowFieldWithoutImmersedObjects_MRT();
        else flow.advanceFlowFieldWithoutImmersedObjects();
    }
    timer.toc();
}






void solute_dispersion_in_flow_around_a_floating_rigid_body_setBoundaryCondition
(
REAL* __restrict__ _C_in,
REAL* __restrict__ _C_ex,
const REAL* __restrict__ _rho_in,
const REAL* __restrict__ _rho_ex,
const REAL k,
const int len
);

inline void solute_dispersion_in_flow_around_a_floating_rigid_body()
{
    ConfigFile cfg("solute_dispersion_in_flow_around_a_floating_rigid_body.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation solute_dispersion_in_flow_around_a_floating_rigid_body not issued\n");
        return;
    }

    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", is_MRT, 1);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", m, 1024);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n, 256);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", nTheta, 360);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", rho, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", x0_cylinder, (int)(0.2*m));
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", y0_cylinder, (int)(0.5*n));
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", umax, 1e-2);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", R_cylinder, (int)(0.2*n));
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", k, 1e-4);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", Re, 200);
    REAL viscosity = umax * R_cylinder / Re;
    printf("viscosity = umax * R_cylinder / Re = %f\n", viscosity);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", Pe, 200);
    REAL diffusion_coefficient = umax * R_cylinder / Pe;
    printf("solute diffusion coefficient = umax * R_cylinder / Pe = %f\n", diffusion_coefficient);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_prestep, 1000000);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tend, 1000000);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tbeg_export, 0); // export starts from this time step
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames, 100); // total export frames for field information

    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, 0.0, 0.0, 1.0, 1.0, m, n);

    DeviceArray<int> label = (X - x0_cylinder)*(X - x0_cylinder) + (Y - y0_cylinder)*(Y - y0_cylinder) > R_cylinder*R_cylinder;
    label = label - 1;
    DeviceArray<REAL> Fx(8.0*viscosity*umax / (n - 1) / (n - 1), m, n);
    DeviceArray<REAL> Fy(0.0, m, n);
    da_saveAF(Fx);
    da_saveAF(Fy);
    DeviceArray<REAL> X_cylinder = x0_cylinder + cos(DA::range(0.0, PI*2.0 / nTheta, nTheta)) * (R_cylinder - 1.0 * 0);
    DeviceArray<REAL> Y_cylinder = y0_cylinder + sin(DA::range(0.0, PI*2.0 / nTheta, nTheta)) * (R_cylinder - 1.0 * 0);
    DeviceArray<REAL> concentration0
        (
        (X - x0_cylinder)*(X - x0_cylinder) +
        (Y - y0_cylinder)*(Y - y0_cylinder) <
        (R_cylinder - 1.0 * 0)*(R_cylinder - 1.0 * 0)
        );

    // create NSsolver
    NSsolver flow(
        viscosity,
        m, n,
        NS::PERIODIC,
        NS::BOUNCEBACK,
        Fx.data_ptr(),
        Fy.data_ptr(),
        label.data_ptr(),
        umax * 2);
    flow.initializeByDensity();

    // create Advection-Diffusion solver
    ADsolver solute(
        diffusion_coefficient,
        m, n,
        (int)(R_cylinder*PI*5.0),
        flow.ux.data_ptr(),
        flow.uy.data_ptr(),
        nullptr,
        nullptr,
        AD::PERIODIC,
        AD::BOUNCEBACK);
    solute.setInitialConcentration(concentration0);
    solute.registerCurve(X_cylinder, Y_cylinder, true, 0);
    solute.registerCurveFinished();
    int bound_size = solute.boundary.size();
    printf("boundary size = %i\n", bound_size);
    solute.boundary.exportToFile_ArrayFire("bound0.afdat");
    auto curve0 = DA::join(X_cylinder, Y_cylinder);
    curve0.reshape(X_cylinder.dim(0), 2);
    da_saveAF(curve0);

    solute.computeConcentrationOnBoundary();

    //pre-step for flow field initialization
    REAL umaxprev = 0.0;

    for (int i = 0; i != flow_prestep; ++i){
        if ((i + 1) % std::max(1, (flow_prestep / frames)) == 0 || i == 0 || i == Tend - 1){
            std::cout << "prestep, iteration = " << i + 1;
            std::vector<REAL> hux(flow.ux.size());
            std::vector<REAL> huy(flow.ux.size());
            flow.ux.host(&hux[0]);
            flow.uy.host(&huy[0]);
            REAL umaxnow = 0.0;
            for (int j = (int)((double)hux.size()*0.45); j != (int)((double)hux.size()*0.55); ++j){
                REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
                umaxnow = std::max(umaxnow, uj);
            }
            std::cout << ", u_at_probe = " << umaxnow << std::endl;
            if (std::abs(umaxnow - umaxprev) < 1e-4 && umaxnow >= umax*0.1 && i != 0){
                printf("u_at_probe reached its maximum, end prestep\n");
                break;
            }
            umaxprev = umaxnow;
        }
        flow.advanceFlowFieldWithoutImmersedObjects_MRT();
    }
    SimpleTimer timer;
    timer.tic();
    for (int i = 0; i != Tend; ++i)
    {

        // export to files
        if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
        {
            std::cout << "i = " << i + 1 << ", write frame" << std::endl;
            // save flow field
            flow.ux.af_save("ux", "ux.afdat", i != 0);
            flow.uy.af_save("uy", "uy.afdat", i != 0);
            solute.rho.af_save("rho", "rho.afdat", i != 0);
        }

        // set boundary condition for cylinder surface
        solute.computeConcentrationOnBoundary();
        solute_dispersion_in_flow_around_a_floating_rigid_body_setBoundaryCondition
            (
            solute.boundary.C_in.data_ptr(),
            solute.boundary.C_ex.data_ptr(),
            solute.boundary.rho_in.data_ptr(),
            solute.boundary.rho_ex.data_ptr(),
            k,
            bound_size
            );
        
        // advance
        solute.advanceTimeStep_MRT();
        flow.advanceFlowFieldWithoutImmersedObjects_MRT();
    }
    timer.toc();
}

void galileo_invariance_set_boundary_condition
(
REAL* __restrict__ _C_in,
REAL* __restrict__ _C_ex,
const REAL* __restrict__ _rho_in,
const REAL* __restrict__ _rho_ex,
const int* __restrict__ _boundary_size,
const REAL k,
const REAL dx,
const int len
);

inline void galileo_invariance()
{
    ConfigFile cfg("galileo_invariance.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation galileo_invariance not issued\n");
        return;
    }

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", D, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", T, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", k, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", r0, 0.8);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", x0, -4.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", x1, 4.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", y0, -1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", y1, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u, 8.0); // velocity amplitude
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u_theta, 0.0); // velocity direction angle
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u_simu, 1e-3);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", D_simu, 1.0 / 6.0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Ntheta, 360);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", N, 64);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames, 100);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Tend, -1);
    REAL dx = (y1 - y0) / N;
    int m = (int)(std::round((x1 - x0) / (y1 - y0) * N));
    int n = N;
    printf("m = %i\n", m);
    printf("n = %i\n", n);
    printf("dx = %f\n", dx);
    REAL dt;
    if (std::abs(u) >= 1e-9 && std::abs(u_simu) >= 1e-9)
    {
        dt = u_simu * dx / u;
        D_simu = D*dt / dx / dx;
    }
    else
    {
        dt = D_simu / D * dx * dx;
        u_simu = u * dt / dx;
    }
    printf("dt = %f\n", dt);
    printf("D_simu = %f\n", D_simu);


    if (Tend < 0)
    {
        Tend = (int)std::ceil(T / dt);
    }
    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, x0 + dx / 2, y0 + dx / 2, dx, dx, m, n);
    REAL ux_simu = u_simu*std::cos(u_theta);
    REAL uy_simu = u_simu*std::sin(u_theta);
    printf("ux_simu = %f\n", ux_simu);
    printf("uy_simu = %f\n", uy_simu);
    DeviceArray<REAL> ux(ux_simu, X.dim(0), X.dim(1));
    DeviceArray<REAL> uy(uy_simu, X.dim(0), X.dim(1));

    DeviceArray<REAL> theta = DA::range(0.0, 2 * PI / Ntheta, Ntheta);
    auto curve_x_phy = r0 * cos(theta);
    auto curve_y_phy = r0 * sin(theta);
    auto curve_x = (curve_x_phy - x0) / (x1 - x0) * (REAL)X.dim(0) - 0.5;
    auto curve_y = (curve_y_phy - y0) / (y1 - y0) * (REAL)Y.dim(1) - 0.5;

    ADsolver c(D_simu, m, n, 20 * N, ux.data_ptr(), uy.data_ptr(), nullptr, nullptr, AD::PERIODIC, AD::PERIODIC);

    auto int_c_ini = DeviceArray<int>(X*X + Y*Y < r0*r0);
    c.boundary.voxelizer.trim_label_by_curve(int_c_ini.data_ptr(), 0, 1, curve_x.data_ptr(), curve_y.data_ptr(), curve_x.size());
    c.setInitialConcentration(DeviceArray<REAL>(int_c_ini));


    SimpleTimer timer;
    timer.tic();

    for (int i = 0; i != Tend; ++i){

        if (std::abs(u) >= 1e-9 || i == 0)
        {
            c.registerCurve(curve_x, curve_y, true, 0);
            c.registerCurveFinished();
        }

        c.computeConcentrationOnBoundary();

        galileo_invariance_set_boundary_condition
            (
            c.boundary.C_in.data_ptr(),
            c.boundary.C_ex.data_ptr(),
            c.boundary.rho_in.data_ptr(),
            c.boundary.rho_ex.data_ptr(),
            c.boundary.boundary_size.data_ptr(),
            k, dx,
            c.getKernelLaunchSizeForBoundary()
            );

        /*
        auto flux = (dx*k)*(c.boundary.rho_ex - c.boundary.rho_in);
        c.boundary.C_in = flux;
        c.boundary.C_ex = flux;
        */

        // export to files
        if (LBM::getStepStampFlag(i, frames, Tend))
        {
            std::cout << "i = " << i + 1 << "/" << Tend << ", write frame" << std::endl;
            // save flow field
            c.rho.af_save("rho", "rho.afdat", i != 0);
            char boundary_file_name[100];
            sprintf(boundary_file_name, "bound%08d.afdat", i);
            c.boundary.exportToFile_ArrayFire(boundary_file_name);
            char curve_file_name[100];
            sprintf(curve_file_name, "curve%08d.afdat", i);
            auto curve0 = DA::join(curve_x, curve_y);
            curve0.reshape(curve_x.size(), 2);
            curve0.af_save("curve", curve_file_name);
            DeviceArray<int> temp = c.boundary.voxelizer.tag_previous;
            temp.af_save("tag", "tag.afdat", i != 0);
        }

        c.advanceTimeStep();
        curve_x += ux_simu;
        curve_y += uy_simu;

    }
    timer.toc();
    




}

void vortex_in_a_box_advect_boundary_curve
(REAL* x_numerical, REAL* y_numerical, const int Ntheta, const REAL dx, const REAL dt);

inline void vortex_in_a_box()
{

    ConfigFile cfg("vortex_in_a_box.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation vortex_in_a_box not issued\n");
        return;
    }

    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", N, 64);
    REAL dx = 2.0 / N;
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Ntheta, 360);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "boundary", r0, 0.3);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "boundary", x0, 0.1);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "boundary", y0, 0.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Tstop, 4.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Peclet, 100.0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames, -1);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Tend, -1);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", D_limit, 1.0/6.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u_limit, 0.05);
    DEFINE_FROM_CONFIGFILE(int, cfg, "model", is_advection, 1); // switch on/off advection term
    DEFINE_FROM_CONFIGFILE(int, cfg, "model", is_bounded, 1); // switch on/off advection term
    DEFINE_FROM_CONFIGFILE(int, cfg, "model", is_using_final_boundary, 0); // switch on/off advection term

    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, -1.0 + dx / 2, -1.0 + dx / 2, dx, dx, N, N);
    auto R2 = (X - x0)*(X - x0) + (Y - y0) * (Y - y0);
    auto R = sqrt(R2);
    auto zone0 = DeviceArray<REAL>(R2 < r0*r0);
    auto zone1 = DeviceArray<REAL>((R2 >= r0*r0) && (R2 < (2.5*r0)*(2.5*r0)));
    auto zone2 = DeviceArray<REAL>(R2 >= (2.5*r0)*(2.5*r0));
    DeviceArray<REAL> theta = DA::range(0.0, 2.0*PI / Ntheta, Ntheta);
    auto x = r0*cos(theta) + x0;
    auto y = r0*sin(theta) + y0;
    REAL C0_mean = DA::sum(0.5 + 0.4*cos(PI*x)*cos(PI*y)) / (Ntheta);
    auto rho0 =
        zone0 * 0.0 +
        zone1 * (C0_mean + (0.4*cos(PI*X)*cos(PI*Y) + 0.5 - C0_mean) * 0.5*(1.0 - cos((R - r0)*PI / (1.5*r0)))) +
        zone2 * (0.5 + 0.4*cos(PI*X)*cos(PI*Y));
    // rho0 = zone0 * 0.0 + zone1 * 1.0 + zone2 * 1.0; // debug 20170706

    auto ux = -0.5 * (1.0 + cos(PI*X))*sin(PI*Y);
    auto uy = 0.5 * (1.0 + cos(PI*Y))*sin(PI*X);

    /*
    // debug 20170706
    da_saveAF(X);
    da_saveAF(Y);
    da_saveAF(x);
    da_saveAF(y);
    da_saveAF(R2);
    da_saveAF(R);
    da_saveAF(zone0);
    da_saveAF(zone1);
    da_saveAF(zone2);
    da_saveAF(rho0);
    da_saveAF(theta);
    */

    REAL D_LB = D_limit;
    REAL u_LB = Peclet * dx * D_LB;
    if (u_LB > u_limit){
        u_LB = u_limit;
        D_LB = u_LB / dx / Peclet;
    }
    REAL dt = u_LB * dx;

    if (Tend < 0){
        Tend = (int)std::ceil(Tstop / dt);
    }
    if (frames < 0){
        frames = (int)std::ceil(Tstop) * 50;
    }
    printf("simulation config:\n");
    printf("c0_mean = %e\n", C0_mean);
    printf("D_LB = %e\n", D_LB);
    printf("u_LB = %e\n", u_LB);
    printf("N = %i\n", N);
    printf("dx = %e\n", dx);
    printf("dt = %e\n", dt);
    printf("Tend = %i\n", Tend);
    printf("Export total frames = %i\n", frames);
    auto ux_numerical = ux * (dt / dx);
    auto uy_numerical = uy * (dt / dx);

    if (!is_advection){
        ux_numerical = 0.0;
        uy_numerical = 0.0;
    }

    auto us_numerical = sqrt(ux_numerical*ux_numerical + uy_numerical*uy_numerical);
    std::vector<REAL> host_us_numerical(N*N);
    us_numerical.host(&host_us_numerical[0]);

    printf("maximum velocity in simulation = %f\n", 
        *std::max_element
        (
        &host_us_numerical[0], &host_us_numerical[0] + host_us_numerical.size()
        )
        );

    /*
    // debug 20170718
    da_saveAF(ux_numerical);
    da_saveAF(uy_numerical);
    */

    ADsolver c(
        D_LB, N, N, N * 20,
        ux_numerical.data_ptr(),
        uy_numerical.data_ptr(),
        nullptr,
        nullptr,
        AD::PERIODIC,
        AD::PERIODIC
        );
    
    // delta function
    /*
    auto temp_c_ini = DeviceArray<REAL>
        (
        abs(X - x0) < dx &&
        abs(Y - y0) < dx
        );
    temp_c_ini /= DA::sum(temp_c_ini);
    */

    // gaussian hill
    auto rg0 = 0.3*r0;
    auto temp_c_ini = DeviceArray<REAL>(exp(-R2 / (2.0 * rg0 * rg0)) / PI / (2.0 * rg0 * rg0));
    temp_c_ini = temp_c_ini * DeviceArray<REAL>(R2 < r0*r0);

    c.setInitialConcentration(rho0*0.0 + temp_c_ini);
    auto x_numerical = 0.5*(x + 1.0 - 0.5*dx)*(REAL)N;
    auto y_numerical = 0.5*(y + 1.0 - 0.5*dx)*(REAL)N;

    if (is_bounded)
    {
        c.registerCurve(x_numerical, y_numerical, true, 0);
        c.registerCurveFinished();
        c.boundary.exportToFile_ArrayFire("bound0.afdat");
        auto curve0 = DA::join(x_numerical, y_numerical);
        curve0.reshape(x.size(), 2);
        da_saveAF(curve0);
        c.boundary.Ctype_ex = 0;
        c.boundary.Ctype_in = 0;
        c.boundary.C_ex = 0.0;
        c.boundary.C_in = 0.0;
    }

    if (is_using_final_boundary)
    {
        if (is_bounded)
        {
            for (int i = 0; i != Tend; ++i)
                vortex_in_a_box_advect_boundary_curve(
                x_numerical.data_ptr(),
                y_numerical.data_ptr(),
                Ntheta, dx, dt);
        }
    }


    SimpleTimer timer;
    timer.tic();

    for (int i = 0; i != Tend; ++i){

        if (is_bounded){
            if (is_using_final_boundary && i != 0)
            {

            }
            else
            {
                vortex_in_a_box_advect_boundary_curve(x_numerical.data_ptr(), y_numerical.data_ptr(), Ntheta, dx, dt);
                c.registerCurve(x_numerical, y_numerical, true, 0);
                c.registerCurveFinished();
            }
            c.boundary.Ctype_ex = 0;
            c.boundary.Ctype_in = 0;
            c.boundary.C_ex = 0.0;
            c.boundary.C_in = 0.0;

        }

        c.advanceTimeStep();
        if ((i + 1) % std::max(1, (Tend / frames)) == 0 || i == 0 || i == Tend - 1){
            std::cout << "i = " << i + 1 << ", Tend = " << Tend << std::endl;
        }
        if ((i >= 0 && (i + 1) % std::max(1, (Tend / frames)) == 0) || i == 0 || i == Tend - 1)
        {
            std::cout << "i = " << i + 1 << ", write frame" << std::endl;
            c.rho.af_save("rho", "rho.afdat", i != 0);
            ux_numerical.af_save("ux", "ux.afdat", i != 0);
            uy_numerical.af_save("uy", "uy.afdat", i != 0);
            if (is_bounded){
                char boundary_file_name[100];
                sprintf(boundary_file_name, "bound%08d.afdat", i);
                c.boundary.exportToFile_ArrayFire(boundary_file_name);
                char curve_file_name[100];
                sprintf(curve_file_name, "curve%08d.afdat", i);
                auto curve0 = DA::join(x_numerical, y_numerical);
                curve0.reshape(x_numerical.size(), 2);
                curve0.af_save("curve", curve_file_name);
                DeviceArray<int> temp = c.boundary.voxelizer.tag_previous;
                temp.af_save("tag", "tag.afdat", i != 0);
            }
        }
    }

    cudaDeviceSynchronize();
    timer.toc();
}


void steady_heat_conduction_inside_a_circle_with_constant_advection_set_boundary_condition
(
const int boundary_type,
REAL* __restrict__ _C_ex,
REAL* __restrict__ _C_in,
short* __restrict__ _C_type_ex,
short* __restrict__ _C_type_in,
const REAL* __restrict__ _s,
const REAL k,
const REAL r0,
const REAL dx,
const int Ntheta,
const int launch_size,
const int* __restrict__ _actual_size
);

// case from Huang 2016 JCP Chp 4.3
// div(D grad(C)) = 0, r>=0, r<r0, theta>=0,theta<2pi
// n * grad(C) = k/r0 cos(k theta) at r=r0
// exact solution: (r/r0)^kcos(k theta)
// A moving coordinate with a constant velocity is used to check the moving boundary (keep it static!)
inline void steady_heat_conduction_inside_a_circle_with_constant_advection()
{
    ConfigFile cfg("steady_heat_conduction_inside_a_circle_with_constant_advection.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation steady_heat_conduction_inside_a_circle_with_constant_advection\
                not issued\n");
        return;
    }
    
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "model", k, 4.0); // default value 4.0 from that paper
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "model", r0, 0.4); // default value 0.4 from that paper, the value must be less than 1.0
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "model", T, 1.0); // default value 1.0, physical simulation time
    DEFINE_FROM_CONFIGFILE(int, cfg, "model", boundary_type, 0); // boundary type, 1 for dirichlet, 0 for neumann

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", N, 16);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", D, 1.0 / 6.0); // diffusion coefficient
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u, 0.0); // velocity amplitude
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u_theta, 0.0); // velocity direction angle
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Ntheta, 360); // velocity direction angle
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames, 100);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Tend, -1);
    REAL x0 = -1;
    REAL x1 = 1;
    REAL dx = (x1 - x0) / N;
    printf("dx = %f\n", dx);
    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, x0 + dx / 2, x0 + dx / 2, dx, dx, N, N);
    REAL ux_scalar = u*std::cos(u_theta);
    REAL uy_scalar = u*std::sin(u_theta);
    DeviceArray<REAL> ux(ux_scalar, N, N);
    DeviceArray<REAL> uy(uy_scalar, N, N);

    DeviceArray<REAL> theta = DA::range(0.0, 2 * PI / Ntheta, Ntheta);
    auto curve_x_phy = r0 * cos(theta);
    auto curve_y_phy = r0 * sin(theta);
    auto curve_x = (curve_x_phy + 1.0) * (REAL)N*0.5 - 0.5;
    auto curve_y = (curve_y_phy + 1.0) * (REAL)N*0.5 - 0.5;

    ADsolver c(D, N, N, Ntheta * 2, ux.data_ptr(), uy.data_ptr(), nullptr, nullptr, AD::PERIODIC, AD::PERIODIC);
    c.setInitialConcentration(0.0);
    if (Tend <= 0)
    {
        Tend = T / D / dx / dx;
    }

    SimpleTimer timer;
    timer.tic();

    for (int i = 0; i != Tend; ++i){

        if (std::abs(u) >= 1e-9 || i == 0)
        {
            c.registerCurve(curve_x, curve_y, true, 0);
            c.registerCurveFinished();
        }

        steady_heat_conduction_inside_a_circle_with_constant_advection_set_boundary_condition
            (
            boundary_type,
            c.boundary.C_ex.data_ptr(),
            c.boundary.C_in.data_ptr(),
            c.boundary.Ctype_ex.data_ptr(),
            c.boundary.Ctype_in.data_ptr(),
            c.boundary.s.data_ptr(),
            k, r0, dx, Ntheta,
            c.getKernelLaunchSizeForBoundary(),
            c.boundary.boundary_size.data_ptr()
            );

        // export to files
        if (LBM::getStepStampFlag(i, frames, Tend))
        {
            std::cout << "i = " << i + 1 << "/" << Tend << ", write frame" << std::endl;
            // save flow field
            c.rho.af_save("rho", "rho.afdat", i != 0);
            char boundary_file_name[100];
            sprintf(boundary_file_name, "bound%08d.afdat", i);
            c.boundary.exportToFile_ArrayFire(boundary_file_name);
            char curve_file_name[100];
            sprintf(curve_file_name, "curve%08d.afdat", i);
            auto curve0 = DA::join(curve_x, curve_y);
            curve0.reshape(curve_x.size(), 2);
            curve0.af_save("curve", curve_file_name);
            DeviceArray<int> temp = c.boundary.voxelizer.tag_previous;
            temp.af_save("tag", "tag.afdat", i != 0);
        }

        c.advanceTimeStep();
        curve_x += ux_scalar;
        curve_y += uy_scalar;

    }
    timer.toc();
}

inline void round_vesicle_in_static_flow()
{

    ConfigFile cfg("round_vesicle_in_static_flow.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation round_vesicle_in_static_flow not issued\n");
        return;
    }
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", m, 200);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", n, 200);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", viscosity, 1.0 / 6.0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", R0, 20);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", x0ves, m / 2);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", y0ves, n / 2);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", tau, 1.0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kb, 0.05);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kl, 0.3);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", l0, 0.6667);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", damp, 0.1); // damp factor in relaxation process
    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", n_relax, 1000000);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_shape_filename, std::string("initial_vesicle_shape.txt"));
    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", use_initial_shape_file, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Tend, 10000);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames, 200);

    Vesicle vesicle(x0ves, y0ves, 0.0, R0, tau, kb, kl, 200.0, l0, use_initial_shape_file ? initial_shape_filename.c_str() : nullptr);
    if (use_initial_shape_file <= 0)
    {
        vesicle.relax(n_relax, damp);
        vesicle.exportShape(initial_shape_filename.c_str());
    }

    DeviceArray<REAL> Fx(0.0, m, n);
    DeviceArray<REAL> Fy(0.0, m, n);
    DeviceArray<int> label_flow(0, m, n);

    NSsolver flow(viscosity, m, n, NS::PERIODIC, NS::BOUNCEBACK, Fx.data_ptr(), Fy.data_ptr(), label_flow.data_ptr());
    flow.initializeByDensity(1.0);
    flow.immerseAnObject(&vesicle);
    flow.immerseObjectFinished();

    SimpleTimer timer;

    timer.tic();

    for (int i = 0; i != Tend; ++i)
    {
        vesicle.calculateForce();


        if (LBM::getStepStampFlag(i, frames, Tend))
        {
            std::cout << "i = " << i + 1 << ", write frame" << std::endl;
            // save flow field
            flow.ux.af_save("ux", "ux.afdat", i != 0);
            flow.uy.af_save("uy", "uy.afdat", i != 0);
            flow.rho.af_save("density", "density.afdat", i != 0);
            vesicle.exportShapeAF("ves_shape.afdat", i != 0);
        }

        flow.advanceFlowField();
        flow.advanceImmersedObjects();

    }
    timer.toc();

}


// exact solution for three_leaved_interface_diffusion
#define PHI_EXACT(x,y,t, Pe) ( 1.0 + (0.5*exp(-2.0*PI*PI*(t)/(Pe))) * cos(PI*(x))*cos(PI*(y)) )
#define PHI_EXACT_CPU(x,y,t, Pe) ( 1.0 + (0.5*std::exp(-2.0*PI*PI*(t)/(Pe))) * cos(PI*(x))*cos(PI*(y)) )
void three_leaved_interface_diffusion_set_boundary_condition
(
const int boundary_type,
REAL* __restrict__ _C_ex,
REAL* __restrict__ _C_in,
short* __restrict__ _C_type_ex,
short* __restrict__ _C_type_in,
const REAL* __restrict__ _x_numerical,
const REAL* __restrict__ _y_numerical,
const REAL* __restrict__ _nx_numerical,
const REAL* __restrict__ _ny_numerical,
const REAL dn,
const REAL Pe,
const REAL T,
const REAL dx,
const int launch_size,
const int* __restrict__ _actual_size
);
/*
@article{hu2018coupled,
title={A coupled immersed boundary and immersed interface method for interfacial flows with soluble surfactant},
author={Hu, Wei-Fan and Lai, Ming-Chih and Misbah, Chaouqi},
journal={Computers \& Fluids},
volume={168},
pages={201--215},
year={2018},
publisher={Elsevier}
}
*/
inline void three_leaved_interface_diffusion()
{
    ConfigFile cfg("three_leaved_interface_diffusion.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation three_leaved_interface_diffusion not issued\n");
        return;
    }

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "model", Pe, 10.0); // default value 10 from paper
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "model", T, 1.0); // default value 1 from paper
    DEFINE_FROM_CONFIGFILE(int, cfg, "model", boundary_type, 0); // boundary type, 1 for dirichlet, 0 for neumann

    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", N, 16);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", dn, 1e-4); // a simple numerical approach to get dcdn
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", D, 1.0 / 6.0); // diffusion coefficient
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u, 0.0); // velocity amplitude
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", u_theta, 0.0); // velocity direction angle
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Ntheta, 3600); // velocity direction angle
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames, 100);
    DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", Tend, -1);
    REAL x0 = -1;
    REAL x1 = 1;
    REAL dx = (x1 - x0) / N;
    printf("dx = %f\n", dx);
    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, x0 + dx / 2, x0 + dx / 2, dx, dx, N, N);
    REAL ux_scalar = u*std::cos(u_theta);
    REAL uy_scalar = u*std::sin(u_theta);
    DeviceArray<REAL> ux(ux_scalar, N, N);
    DeviceArray<REAL> uy(uy_scalar, N, N);

    DeviceArray<REAL> theta = DA::range(0.0, 2 * PI / Ntheta, Ntheta);
    auto r_phy = 0.1 * cos(3.0*theta) + 0.4;
    auto curve_x_phy = r_phy*cos(theta);
    auto curve_y_phy = r_phy*sin(theta);
    auto curve_x = (curve_x_phy + 1.0) * (REAL)N*0.5 - 0.5;
    auto curve_y = (curve_y_phy + 1.0) * (REAL)N*0.5 - 0.5;

    // use voxelizer to generate a tag map.
    LBMGPUvoxelizer* voxelizer = new LBMGPUvoxelizer(N, N, false, false, Ntheta * 2, 10);
    voxelizer->setBoundaryCurve(curve_x.data_ptr(), curve_y.data_ptr(), curve_x.size(), true);
    voxelizer->setBoundaryCurveFinished(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, false);
    auto label = DeviceArray<REAL>(voxelizer->build_filled_tag_map() < 0);
    delete voxelizer;

    REAL dt = Pe*D*dx*dx;
    printf("dt = Pe * D_numerical * dx*dx = %f\n", dt);

    if (Tend <= 0)
    {
        Tend = (int)std::ceil(T / dt) + 1;
        dt = T / (Tend - 1);
        D = dt / Pe / dx / dx;
        printf("modified dt = %f\n", dt);
        printf("modified D = %f\n", D);
    }

    ADsolver c(D, N, N, Ntheta * 2, ux.data_ptr(), uy.data_ptr(), nullptr, nullptr, AD::PERIODIC, AD::PERIODIC);
    auto c_ini = PHI_EXACT_CPU(X, Y, 0.0, Pe);
    auto temp = cos(PI*(X))*cos(PI*(Y));
    da_saveAF(temp);
    c.setInitialConcentration(c_ini * label);

    DeviceArray<REAL> x_numerical_coordinate(0.0, N*N);
    DeviceArray<REAL> y_numerical_coordinate(0.0, N*N);
    SimpleTimer timer;
    timer.tic();

    for (int i = 0; i != Tend; ++i){

        if (std::abs(u) >= 1e-9 || i == 0)
        {
            c.registerCurve(curve_x, curve_y, true, 0);
            c.registerCurveFinished();
            c.boundary.getBoundaryCoordinates(x_numerical_coordinate.data_ptr(), y_numerical_coordinate.data_ptr());
        }

        three_leaved_interface_diffusion_set_boundary_condition
            (
            boundary_type,
            c.boundary.C_ex.data_ptr(),
            c.boundary.C_in.data_ptr(),
            c.boundary.Ctype_ex.data_ptr(),
            c.boundary.Ctype_in.data_ptr(),
            x_numerical_coordinate.data_ptr(),
            y_numerical_coordinate.data_ptr(),
            c.boundary.nx.data_ptr(),
            c.boundary.ny.data_ptr(),
            dn,
            Pe, i*dt, dx,
            c.getKernelLaunchSizeForBoundary(),
            c.boundary.boundary_size.data_ptr()
            );

        // export to files
        if (LBM::getStepStampFlag(i, frames, Tend))
        {
            std::cout << "T = " << i*dt << ", i = " << i + 1 << "/" << Tend << ", write frame" << std::endl;
            // save flow field
            c.rho.af_save("rho", "rho.afdat", i != 0);
            if (i == 0)
            {
                char boundary_file_name[100];
                sprintf(boundary_file_name, "bound%08d.afdat", i);
                c.boundary.exportToFile_ArrayFire(boundary_file_name);
                char curve_file_name[100];
                sprintf(curve_file_name, "curve%08d.afdat", i);
                auto curve0 = DA::join(curve_x, curve_y);
                curve0.reshape(curve_x.size(), 2);
                curve0.af_save("curve", curve_file_name);
                DeviceArray<int> temp = c.boundary.voxelizer.tag_previous;
                temp.af_save("tag", "tag.afdat", i != 0);
            }
        }

        c.advanceTimeStep();
        curve_x += ux_scalar;
        curve_y += uy_scalar;

    }
    timer.toc();
    auto rho_final_exact = PHI_EXACT_CPU(X, Y, 1.0, Pe) * label;
    auto rho_final_numerical = c.rho;
    da_saveAF(rho_final_exact);
    da_saveAF(rho_final_numerical);

    std::vector<REAL> rho_exa(N*N, 0.0); rho_final_exact.host(&rho_exa[0]);
    std::vector<REAL> rho_num(N*N, 0.0); rho_final_numerical.host(&rho_num[0]);

    // calculate L2 Error
    REAL sum_exa2 = 0.0;
    REAL sum_dif2 = 0.0;
    for (int i = 0; i != N*N; ++i)
    {
        sum_dif2 += ((rho_num[i] - rho_exa[i]) * (rho_num[i] - rho_exa[i]));
        sum_exa2 += (rho_exa[i] * rho_exa[i]);
    }
    REAL E2 = std::sqrt(sum_dif2 / sum_exa2);

    // calculate L_inf Error
    REAL max_dif = 0.0;
    for (int i = 0; i != N*N; ++i)
    {
        auto temp_dif = std::abs(rho_num[i] - rho_exa[i]);
        max_dif = max_dif > temp_dif ? max_dif : temp_dif;
    }

    // write into ASCII file
    // boundary_type N tau E2 Emax
    std::ofstream data_convergence("data_convergence.txt", std::ios_base::app | std::ios_base::out);
    data_convergence
        << std::setprecision(1) << boundary_type << " "
        << std::setprecision(5) << N << " "
        << std::setprecision(5) << D * 3 + 0.5 << " "
        << std::setprecision(17) << E2 << " "
        << std::setprecision(17) << max_dif
        << std::endl;

}

// this case is from Abdul Barakat's paper in 2001
// Provided by Brenna Hogan <brenbrenhogan@gmail.com>

void ATP_dynamics_in_a_vessel_set_boundary_condition
(
REAL* __restrict__ _Cin,
const REAL* __restrict__ _rhoin,
const int* __restrict__ _id,
const int* __restrict__ _bd_idx,  // for reading old value of rho, debug
const REAL* __restrict__ _rho_old, // for reading old value of rho, debug
const REAL K1,
const REAL K2,
const REAL c0,
const int launch_size,
const int* __restrict__ _actual_size
);

inline void ATP_dynamics_in_a_vessel()
{

    ConfigFile cfg("ATP_dynamics_in_a_vessel.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    if (flag_run == 0)
    {
        printf("flag_run==0, simulation ATP_dynamics_in_a_vessel not issued\n");
        return;
    }

    DEFINE_FROM_CONFIGFILE(int, cfg, "parameters", W, 200); // channel width
    DEFINE_FROM_CONFIGFILE(int, cfg, "parameters", L, 2000); // channel length
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameters", umax, 0.004166667); // umax for the Poiseuille profile
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameters", D, 3.93e-5); // diffusivity
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameters", K1, 1.43); // dc/dn = K1 C - K2
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameters", K2, 4.30E-10); // dc/dn = K1 C - K2
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameters", c0, 5.07E-11); // inlet concentration value of ATP
    DEFINE_FROM_CONFIGFILE(int, cfg, "parameters", Tend, 1e6); // total time step
    DEFINE_FROM_CONFIGFILE(int, cfg, "parameters", frames, 200); // total time step

    REAL Pe = umax * W / D;
    printf("Peclet number = umax * W / D = %f\n", Pe);

    // build velocity field
    DeviceArray<REAL> X, Y;
    DA::meshgrid(X, Y, -0.5, -0.5, 1.0, 1.0, L + 2, W + 2);
    auto ux = (1.0 - (Y / (REAL)W - 0.5) * (Y / (REAL)W - 0.5)) * umax;
    ux = ux * DeviceArray<REAL>(ux > 0.0);
    auto uy = ux * 0.0;

    // build boundaries
    // inlet boundary
    REAL x0 = 0.5;
    REAL x1 = L + 2 - 0.5;
    REAL y0 = 0.5;
    REAL y1 = W + 2 - 0.5;

    std::vector<REAL> x_inlet_h = { x0, x0 };
    std::vector<REAL> y_inlet_h = { y0, y1 };
    std::vector<REAL> x_upper_h = { x0, x1 };
    std::vector<REAL> y_upper_h = { y1, y1 };
    std::vector<REAL> x_outlet_h = { x1, x1 };
    std::vector<REAL> y_outlet_h = { y1, y0 };
    std::vector<REAL> x_lower_h = { x1, x0 };
    std::vector<REAL> y_lower_h = { y0, y0 };

    DeviceArray<REAL> x_inlet(&x_inlet_h[0], x_inlet_h.size());
    DeviceArray<REAL> y_inlet(&y_inlet_h[0], y_inlet_h.size());
    DeviceArray<REAL> x_upper(&x_upper_h[0], x_upper_h.size());
    DeviceArray<REAL> y_upper(&y_upper_h[0], y_upper_h.size());
    DeviceArray<REAL> x_outlet(&x_outlet_h[0], x_outlet_h.size());
    DeviceArray<REAL> y_outlet(&y_outlet_h[0], y_outlet_h.size());
    DeviceArray<REAL> x_lower(&x_lower_h[0], x_lower_h.size());
    DeviceArray<REAL> y_lower(&y_lower_h[0], y_lower_h.size());

    ADsolver ATP(
        D,
        L + 2,
        W + 2,
        (L + W + 4) * 4,
        ux.data_ptr(),
        uy.data_ptr(),
        nullptr,
        nullptr,
        AD::PERIODIC,
        AD::PERIODIC);

    // initialization for concentration field
    ATP.setInitialConcentration(0.0);

    // register boundary curves
    // 0: inlet
    // 1: outlet
    // 2: upper wall
    // 3: lower wall where you have the endothelial cells
    ATP.registerCurve(x_inlet, y_inlet, false, 0);
    ATP.registerCurve(x_outlet, y_outlet, false, 1);
    ATP.registerCurve(x_upper, y_upper, false, 2);
    ATP.registerCurve(x_lower, y_lower, false, 3);
    ATP.registerCurveFinished();
    ATP.boundary.exportToFile_ArrayFire("bound0.afdat");

    // specify boundary type
    ATP.boundary.Ctype_ex = 0; // ghost layers are all zero-flux boundary
    // inlet is a Dirichlet boundary condition
    ATP.boundary.Ctype_in = DeviceArray<short>(ATP.boundary.id == 0);

    SimpleTimer timer;
    timer.tic();

    for (int i = 0; i != Tend; ++i)
    {
        ATP.computeConcentrationOnBoundary();
        ATP_dynamics_in_a_vessel_set_boundary_condition
            (
            ATP.boundary.C_in.data_ptr(),
            ATP.boundary.rho_in.data_ptr(),
            ATP.boundary.id.data_ptr(),
            ATP.boundary.idx.data_ptr(),
            ATP.rho.data_ptr(),
            K1,
            K2,
            c0,
            ATP.getKernelLaunchSizeForBoundary(),
            ATP.boundary.boundary_size.data_ptr()
            );
        if (LBM::getStepStampFlag(i, frames, Tend))
        {
            printf("i = %i, Tend = %i, write ATP field to rho.afdat\n", i, Tend);
            ATP.rho.af_save("rho", "rho.afdat", i != 0);
        }
        ATP.advanceTimeStep();
    }
    cudaDeviceSynchronize();
    timer.toc();
}


void validation_advection_diffusion_in_irregular_domain_set_sorce_term
(
DeviceArray<REAL>& source,
const DeviceArray<int>& label,
const int N,
const REAL x0,
const REAL x1,
const REAL y0,
const REAL y1,
const REAL omega,
const REAL D,
const REAL t,
const REAL dt,
const REAL dx,
const REAL x0_simu
);

void validation_advection_diffusion_in_irregular_domain_set_boundary_condition
(
DeviceArray<REAL>& cin,
DeviceArray<short>& cin_type,
const DeviceArray<REAL>& rho,
const DeviceArray<REAL>& nx,
const DeviceArray<REAL>& ny,
const DeviceArray<REAL>& xbound,
const DeviceArray<REAL>& ybound,
const int N,
const REAL x0,
const REAL x1,
const REAL y0,
const REAL y1,
const REAL omega,
const REAL D,
const REAL t,
const REAL dt,
const REAL dx,
const REAL x0_simu,
const int boundary_size,
const int boundary_type
);

// this validation is taken from Huang et al 2016, see section 4.5
/*
@article{huang2016secondorderrobin,
title={Second-order curved boundary treatments of the lattice Boltzmann method for convection--diffusion equations},
author={Huang, Juntao and Hu, Zexi and Yong, Wen-An},
journal={J. Comput. Phys},
volume={310},
pages={26--44},
year={2016},
publisher={Elsevier}
}
*/
inline void validation_advection_diffusion_in_irregular_domain(const int _N = -1)
{

	ConfigFile cfg("validation_advection_diffusion_in_irregular_domain.txt");

	DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
	if (flag_run == 0)
	{
		printf("flag_run==0, simulation validation_advection_diffusion_in_irregular_domain not issued\n");
		return;
	}

	DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", N, 16);
	if (_N > 0){
		N = _N;
		printf("N is reset from argument as %d\n", N);
	}
	DEFINE_FROM_CONFIGFILE(int, cfg, "numerical", frames, 100);
	DEFINE_FROM_CONFIGFILE(REAL, cfg, "numerical", D_simu, 1.0 / 6.0);
	DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameter", D, 1.0);
	DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameter", ux, 1.0);
	DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameter", uy, 0.0);
	DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameter", omega, 1.0);
	DEFINE_FROM_CONFIGFILE(REAL, cfg, "parameter", T, 1.0);
	DEFINE_FROM_CONFIGFILE(int, cfg, "parameter", boundary_type, 0); // 0-Neumann, 1-Dirichlet, 2-Robin

	std::string str_bd_type;
	switch (boundary_type)
	{
	case 0:
		str_bd_type = "Neumann"; break;
	case 1:
		str_bd_type = "Dirichlet"; break;
	case 2:
		str_bd_type = "Robin"; break;
	default:
		break;
	}
	printf("boundary type: %s\n", str_bd_type.c_str());

	const REAL x0 = -3;
	const REAL x1 = 3;
	const REAL y0 = -2;
	const REAL y1 = 4;
	REAL dx = (x1 - x0) / N;

	// x_simu = (x_phy - x0) / (x1 - x0) * N + x0_simu;
	// x_phy = (x_simu - x0_simu) / N * (x1 - x0) + x0;
	const REAL x0_simu = -0.5;

	DataFile curve("irregular_boundary.txt");
	int curve_len = curve.get_m();
	auto h_curve_phy_x = curve.getData()[1];
	auto h_curve_phy_y = curve.getData()[2];
	DeviceArray<REAL> curve_phy_x(&h_curve_phy_x[0], h_curve_phy_x.size());
	DeviceArray<REAL> curve_phy_y(&h_curve_phy_y[0], h_curve_phy_y.size());
	auto curve_simu_x = (curve_phy_x - x0) / (x1 - x0) * (REAL)N + x0_simu;
	auto curve_simu_y = (curve_phy_y - y0) / (y1 - y0) * (REAL)N + x0_simu;

	// build label for calculation domain (internal zone of the irregular boundary)
	auto label = LBM::build_pixelated_geometry_map_from_boundary_curve_file(
		"irregular_boundary.txt",
		N,
		N,
		x0_simu - x0*N / (x1 - x0),
		x0_simu - y0*N / (y1 - y0),
		N / (x1 - x0),
		N / (y1 - y0));

	REAL dt = dx*dx * D_simu / D;
	int Nstep = (int)std::ceil(T / dt);
	dt = T / Nstep;
	D_simu = dt / dx / dx * D;
	printf("D_simu = %f\n", D_simu);
	printf("tau = %f\n", 3.0 * D_simu + 0.5);
	printf("dx = %f\n", dx);
	printf("dt = %f\n", dt);
	printf("Nstep = %d\n", Nstep);

	REAL ux_simu = ux * dt / dx;
	REAL uy_simu = uy * dt / dx;
	DeviceArray<REAL> UX(ux_simu, N, N);
	DeviceArray<REAL> UY(uy_simu, N, N);
	printf("ux_simu = %f\n", ux_simu);
	printf("uy_simu = %f\n", uy_simu);
	DeviceArray<REAL> S(0.0, N, N);

	ADsolver solver(D_simu, N, N, N*20, UX.data_ptr(), UY.data_ptr(), S.data_ptr(), nullptr, AD::PERIODIC, AD::PERIODIC);
	solver.setInitialConcentration(0.0);

	// register boundary
	solver.registerCurve(curve_simu_x, curve_simu_y, true, 0);
	solver.registerCurveFinished();
	auto curve_simu = DA::join(curve_simu_x, curve_simu_y);
	curve_simu.reshape(curve_simu_x.size(), 2);
	curve_simu.af_save("curve0", "curve0.afdat");
	solver.boundary.exportToFile_ArrayFire("bound0.afdat");

	// get boundary coordinates, normal directions
	auto size_boundary = solver.boundary.size();
	DeviceArray<REAL> xbound(0.0, size_boundary);
	DeviceArray<REAL> ybound(0.0, size_boundary);
	printf("boundary pairs size = %d\n", size_boundary);
	solver.boundary.getBoundaryCoordinates(xbound.data_ptr(), ybound.data_ptr());
	da_saveAF(xbound);
	da_saveAF(ybound);
	auto tag = DeviceArray<int>(solver.boundary.voxelizer.tag);
	auto tag_prev = DeviceArray<int>(solver.boundary.voxelizer.tag_previous);
	auto tag_connect8 = DeviceArray<int>(solver.boundary.voxelizer.tag_conn8);
	da_saveAF(tag);
	da_saveAF(tag_prev);
	da_saveAF(tag_connect8);


	SimpleTimer timer;
	timer.tic();
	for (int i = 0; i != Nstep; ++i)
	{


		if (LBM::getStepStampFlag(i, frames, Nstep))
		{
			printf("i = %d, Nstep = %d\n", i, Nstep);
			solver.rho.af_save("rho", "rho.afdat", i != 0);
		}


		// set source term
		validation_advection_diffusion_in_irregular_domain_set_sorce_term(S, label, N, x0, x1, y0, y1, omega, D, i*dt, dt, dx, x0_simu);

		// set boundary condition
		solver.computeConcentrationOnBoundary();
		validation_advection_diffusion_in_irregular_domain_set_boundary_condition(
			solver.boundary.C_in,
			solver.boundary.Ctype_in,
			solver.boundary.rho_in,
			solver.boundary.nx,
			solver.boundary.ny,
			xbound, ybound,
			N, x0, x1, y0, y1,
			omega, D, i*dt, dt,
			dx, x0_simu,
			size_boundary,
			boundary_type);

		// update field
		solver.advanceTimeStep();

	}
	timer.toc();

	DeviceArray<REAL> X, Y;
	DA::meshgrid(X, Y, -x0_simu / N*(x1 - x0) + x0, -x0_simu / N*(y1 - y0) + y0, dx, dx, N, N);
	auto rho_analytical = (X*X*X + Y*Y) * std::sin(omega * T);
	rho_analytical = rho_analytical * DeviceArray<REAL>(label==0);
	auto rho_numerical = solver.rho;

	REAL E_l2 = sqrt(
		DA::sum((rho_numerical - rho_analytical) * (rho_numerical - rho_analytical)) /
		DA::sum(rho_analytical*rho_analytical)
		);

	printf("dx =%.15e\n", dx);
	printf("N = %d, E_l2 =%.15e\n", N, E_l2);
	da_saveAF(rho_numerical);
	da_saveAF(rho_analytical);
	da_saveAF(X);
	da_saveAF(Y);

}

#endif
