#ifndef MorsePotential_h
#define MorsePotential_h
//
// a Morse potential generates a repulsive force
// 2017 Zaiyi Shen, Alexander Farutin, Marine Thiebaud, and Chaouqi Misbah
// Interaction and rheology of vesicle suspensions in confined shear flow
//

#define MORSE_De 1e-4 //1e-4
#define MORSE_beta 2.0
#define MORSE_d0 2.5 // ensure at least 2 lattice points between boundaries
#define MORSE_FORCE(d) \
    ( \
    (d) > MORSE_d0 ? \
    0.0 : \
    MORSE_De * (2.0*MORSE_beta) * exp(MORSE_beta * (MORSE_d0 - (d))) * (exp(MORSE_beta * (MORSE_d0 - (d))) - 1.0) \
    )

#endif