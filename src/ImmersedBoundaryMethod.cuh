#ifndef ImmersedBoundaryMethod_cuh
#define ImmersedBoundaryMethod_cuh
// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

// DeviceArray header
#include "DeviceArray.cuh"

// LBM constant
#include "LBMconstant.hpp"

// immersed object header
#include "ImmersedObject.cuh"

// interface for LBM kernel in NSsolver
// not used! cause I don't know how nvcc will deal with this __restrict__ qualifier wrapped in a struct
/*
namespace IBM
{
    struct config
    {
        REAL* __restrict__ _fx;
        REAL* __restrict__ _fy;
        const REAL* __restrict__ * __restrict__ const __ibfx;
        const REAL* __restrict__ * __restrict__ const __ibfy;
        const REAL* __restrict__ * __restrict__ const __ibx;
        const REAL* __restrict__ * __restrict__ const __iby;
        const int* __restrict__ _cellListId;
        const int* __restrict__ _cellListCount;
        const int cellListR; // bucket length
        const int MAX_POINTS; // maximum number of points in a immersed boundary curve
        inline config
            (
            REAL* __restrict__ _fx,
            REAL* __restrict__ _fy,
            const REAL* __restrict__ * __restrict__ const __ibfx,
            const REAL* __restrict__ * __restrict__ const __ibfy,
            const REAL* __restrict__ * __restrict__ const __ibx,
            const REAL* __restrict__ * __restrict__ const __iby,
            const int* __restrict__ _cellListId,
            const int* __restrict__ _cellListCount,
            const int cellListR, // bucket length
            const int MAX_POINTS // maximum number of points in a immersed boundary curve
            ) :
            _fx(_fx),
            _fy(_fy),
            __ibfx(__ibfx),
            __ibfy(__ibfy),
            __ibx(__ibx),
            __iby(__iby),
            _cellListId(_cellListId),
            _cellListCount(_cellListCount),
            cellListR(cellListR),
            MAX_POINTS(MAX_POINTS)
        {

        }
    };
}
*/

// ImmersedBoundaryMethod does two things:
//   1. distribute force on a lagrangian curve to an eulerian field
//   2. interpolate the velocity on a lagrangian curve from eulerian field
//
//   it requires immersed object solver to provide a geometry shape and force,
//   as well it requires fluid solver to provide cartician force field pointer and
//   other indicating data (obstacle, is_periodic etc.)
//   raw implementation by atomicAdd, very SLOW!
class ImmersedBoundaryMethod{
public:
    static const int MAX_NUM_LAG_MARKER = 16; //maximum number of lagrangian marker inside a lattice
    int m;
    int n;
    bool is_x_periodic;
    bool is_y_periodic;
    CellList cellList;
    CellList cellList_boundary; // cellList for solid boundary
    NeighborList neighborList;
    NeighborList neighborList_boundary; // cellList for solid boundary
    std::vector<ImmersedObject*> immersedObjects;
    DeviceArray<REAL> eul_fx;
    DeviceArray<REAL> eul_fy;
    const REAL** xEntry;
    const REAL** yEntry;
    const REAL** fxEntry;
    const REAL** fyEntry;

    // coordiantes arrays to store boundary points extracted from label list.
    // all points that valued -1 (solid grid) but neighbor to a fluid grid
    // will be pushed into them
    DeviceArray<REAL> xBoundary;
    DeviceArray<REAL> yBoundary;
public:
    inline ImmersedBoundaryMethod
        (
        const int _m,
        const int _n,
        const bool _is_x_periodic = true,
        const bool _is_y_periodic = false,
        const REAL umax = 0.1
        ) :
        m(_m),
        n(_n),
        is_x_periodic(_is_x_periodic),
        is_y_periodic(_is_y_periodic),
        cellList(_m, _n, _is_x_periodic, _is_y_periodic, umax),
        cellList_boundary(_m, _n, _is_x_periodic, _is_y_periodic, umax),
        neighborList(cellList, umax),
        neighborList_boundary(cellList, 0.0),
        immersedObjects(0),
        xEntry(nullptr),
        yEntry(nullptr),
        fxEntry(nullptr),
        fyEntry(nullptr)
    {
        eul_fx = DeviceArray<REAL>((REAL)0.0, m, n);
        eul_fy = DeviceArray<REAL>((REAL)0.0, m, n);
    }

    // all objects must be immersed into IBM before simulation
    // after this, call immerseObjectFinished()
    inline void immerseAnObject(ImmersedObject* pImObj){ immersedObjects.push_back(pImObj); }

    // all objects must be immersed into IBM before simulation
    // this method will copy entry points of coordinates, force of all ibObjs points onto GPU
    // and it will construct the cell list by ibObjs' initial positions
    inline void immerseObjectFinished()
    {
        /*
        // this is necessary. fix it later
        if (immersedObjects.size() == 0) // seems this is not a full remedy
        {
            return;
        }
        */
        std::vector<const REAL*> hxEntry(immersedObjects.size());
        std::vector<const REAL*> hyEntry(immersedObjects.size());
        std::vector<const REAL*> hfxEntry(immersedObjects.size());
        std::vector<const REAL*> hfyEntry(immersedObjects.size());
        for (int i = 0; i != immersedObjects.size(); ++i)
        {
            hxEntry[i] = immersedObjects[i]->get_x().data_ptr();
            hyEntry[i] = immersedObjects[i]->get_y().data_ptr();
            hfxEntry[i] = immersedObjects[i]->get_fx().data_ptr();
            hfyEntry[i] = immersedObjects[i]->get_fy().data_ptr();
        }
        cudaMalloc((void***)&xEntry, immersedObjects.size()*sizeof(REAL*));
        cudaMalloc((void***)&yEntry, immersedObjects.size()*sizeof(REAL*));
        cudaMalloc((void***)&fxEntry, immersedObjects.size()*sizeof(REAL*));
        cudaMalloc((void***)&fyEntry, immersedObjects.size()*sizeof(REAL*));
        cudaMemcpy(xEntry, &hxEntry[0], immersedObjects.size()*sizeof(REAL*), cudaMemcpyHostToDevice);
        cudaMemcpy(yEntry, &hyEntry[0], immersedObjects.size()*sizeof(REAL*), cudaMemcpyHostToDevice);
        cudaMemcpy(fxEntry, &hfxEntry[0], immersedObjects.size()*sizeof(REAL*), cudaMemcpyHostToDevice);
        cudaMemcpy(fyEntry, &hfyEntry[0], immersedObjects.size()*sizeof(REAL*), cudaMemcpyHostToDevice);

        // using cell list to calculate distributed force term
        cellList.clear();
        for (int i = 0; i != immersedObjects.size(); ++i){
            cellList.registerCurve(
                immersedObjects[i]->get_x().data_ptr(),
                immersedObjects[i]->get_y().data_ptr(),
                immersedObjects[i]->size(),
                ImmersedObject::MAX_POINTS*i,
                cudaStreamDefault);
        }
        // cellList.build_mask();
        cudaStreamSynchronize(cudaStreamDefault);
    }
    inline ~ImmersedBoundaryMethod(){
        if (nullptr != xEntry) cudaFree(xEntry);
        if (nullptr != yEntry) cudaFree(yEntry);
        if (nullptr != fxEntry) cudaFree(fxEntry);
        if (nullptr != fyEntry) cudaFree(fyEntry);
    }

    // update cellList before advance flow field
    // this must be done for every loop in the simulation
    // [[ recently I didn't implement "update" method for CellList
    // thus this is only a reconstruction of cellList ]]
private:
    inline void updateCellList(CellList& cellList)
    {
        cellList.clear();
        for (int i = 0; i != immersedObjects.size(); ++i)
        {
            cellList.registerCurve(
                immersedObjects[i]->get_x().data_ptr(),
                immersedObjects[i]->get_y().data_ptr(),
                immersedObjects[i]->size(),
                ImmersedObject::MAX_POINTS*i,
                immersedObjects[i]->cudaStream);
        }
        for (int i = 0; i != immersedObjects.size(); ++i)
        {
            immersedObjects[i]->sync_operation();
        }
    }
public:
    void distributeForce();

    // i is the current time step. this function will make a cell list for _device_distributeForce() which will be called in NSsolver
    inline void makeCellList(const int i)
    {
        if (i%cellList.N_update == 0)
        {
            this->cellList.build_mask();
        }
        updateCellList(cellList);
    }

private:
    void update_neighbor_list(int i); // i is the current time step

public:
    void distributeForce_neighborlist(int i);

    // advect immersed objects
    // objects are moving toward new positions
    // the force erected on the flow field in old time step is erased
    // this method should be called after advanceing flow time step
    //
    // if is_shuffle_position == false, function will only write velocity into pImObj->ux and uy
    // but will not advect the object directly, this is useful when dealing with a slipping boundary
    // or mass point.
    // In these cases you advect your imObj with this velocity and other properties by your self 
    void advect_immersed_objects(
        const REAL* __restrict__ eul_ux,
        const REAL* __restrict__ eul_uy,
        const bool is_shuffle_position = true);

    // distribute a lagrangian variable to eulerian mesh
    void distribute_variable(
        REAL* __restrict__ eul_var,
        const REAL* __restrict__ lag_var, // variable that you want to distribute
        const REAL* __restrict__ lag_x,
        const REAL* __restrict__ lag_y,
        const int len
        );

    // collect a lagrangian variable from eulerian mesh
    void collect_variable(
        const REAL* __restrict__ eul_var,
        REAL* __restrict__ lag_var, // variable that you want to distribute
        const REAL* __restrict__ lag_x,
        const REAL* __restrict__ lag_y,
        const int len
        );

    // export all immersed objects' shapes as arrayfire format binary files
    void export_all_objects_shapes_af(const char* filename_sx = "allShapes_x.afdat", const char* filename_sy = "allShapes_y.afdat", const bool append = false);

    // export all immersed objects' shapes as txt format (ascii)
    void export_all_objects_shapes_ascii(
        const char* filename_sx = "allShapes_x.afdat",
        const char* filename_sy = "allShapes_y.afdat",
        const int precision = 8);

    // export all immersed objects' shapes at a particular step into a ascii dat file
    void export_all_immersed_object_shapes(
        const char* file_name = "immersed_object_shapes.dat",
        const REAL x0 = 0.0,
        const REAL y0 = 0.0,
        const REAL dx = 1.0,
        const REAL dy = 1.0,
        const int precision = 6);

    // push boundary points into xBoundary and yBoundary
    void make_boundary_from_label(const int* __restrict__ _label);

    // make boundary from DeviceArray
    // added by Zhe Gou on 20211223
    void make_boundary_from_array(
        const DeviceArray<REAL>& x_bound_full, 
        const DeviceArray<REAL>& y_bound_full);

    // add short range repulsive force to all immersed objects
    void add_short_range_repulsive_force();

    // add adhesion force due to Lennard Jones potential
    void add_adhesion_force(
        DeviceArray<REAL>& curvature,
        const REAL De, 
        const REAL d0, 
        const REAL l0,
        const REAL dx_phys, // grid size in physical unit
        const int is_vesicle_adhesion,
        const int is_boundary_adhesion
        );
};
#endif