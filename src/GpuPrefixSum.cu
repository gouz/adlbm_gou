//#include "GpuPrefixSum.cuh"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cstdio>
#include <math.h>
//#include "DeviceArray.cuh"

// block dimension
#define BLOCK_DIM_MIN 8
#define BLOCK_DIM_MAX 1024

// macros to avoid bank conflict
#define NUM_BANKS 16
#define LOG_NUM_BANKS 4
#define CONFLICT_FREE_OFFSET(n) (((n) >> NUM_BANKS) + ((n) >> (2 * LOG_NUM_BANKS)))

template<typename T, int BLOCK_DIM>
// calculate prefix sum block by block, if gridDim.x >1, then write block sum into g_blockSum
__global__ void kernelPrefixSumPerBlock(const T* __restrict__ g_idata, T* g_odata, const int n, T* g_blockSum)
{
    __shared__ T temp[BLOCK_DIM * 2 + CONFLICT_FREE_OFFSET(BLOCK_DIM * 2 - 1)]; // assigned at invocation
    int thid = threadIdx.x;
    int offset = 1;

    // A
    // load input into shared memory
    int ai = thid;
    int bi = ai + BLOCK_DIM;
    int gai = blockDim.x * blockIdx.x * 2 + ai;
    int gbi = blockDim.x * blockIdx.x * 2 + bi;
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);
    temp[ai + bankOffsetA] = gai < n ? g_idata[gai] : (T)0;
    temp[bi + bankOffsetB] = gbi < n ? g_idata[gbi] : (T)0;

    // build sum in place up the tree
    for (int d = BLOCK_DIM; d > 0; d >>= 1)
    {
        __syncthreads();
        if (thid < d)
        {
            // B
            int ai = offset*(2 * thid + 1) - 1;
            int bi = offset*(2 * thid + 2) - 1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            temp[bi] += temp[ai];
        }
        offset *= 2;
    }

    // C
    // clear the last element
    if (thid == 0) { temp[BLOCK_DIM*2 - 1 + CONFLICT_FREE_OFFSET(BLOCK_DIM*2 - 1)] = 0; }

    // traverse down tree & build scan
    for (int d = 1; d < BLOCK_DIM * 2; d *= 2)
    {
        offset >>= 1;
        __syncthreads();
        if (thid < d)
        {
            // D
            int ai = offset*(2 * thid + 1) - 1;
            int bi = offset*(2 * thid + 2) - 1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);

            T t = temp[ai];
            temp[ai] = temp[bi];
            temp[bi] += t;
        }
    }
    __syncthreads();

    // E
    // write results to device memory
    if (gai < n) g_odata[gai] = temp[ai + bankOffsetA];
    if (gbi < n) g_odata[gbi] = temp[bi + bankOffsetB];

    // write block sum into scratch
    if (gridDim.x > 1)
        if (thid == 0)
        {
            // ii: g_idata[ii] is the last element in this block
            // it: temp[it] is the last block prefix sum in shared array temp
            int ii = (blockIdx.x + 1)*BLOCK_DIM * 2 - 1;
            ii = ii < n ? ii : n - 1;
            int it = ii < n - 1 ? BLOCK_DIM * 2 - 1 : (n - 1) % (BLOCK_DIM * 2);
            g_blockSum[blockIdx.x] = g_idata[ii] + temp[it + CONFLICT_FREE_OFFSET(it)];
        }
    // debug export
    /*
    if (thid == 0){
        for (int i = 0; i != BLOCK_DIM * 2 + CONFLICT_FREE_OFFSET(BLOCK_DIM * 2 - 1); ++i){
            printf("block[%i], temp[%i+%i] = %f\n", blockIdx.x, i, CONFLICT_FREE_OFFSET(i), temp[i + CONFLICT_FREE_OFFSET(i)]);
        }
    }
    */
}

// since kernelPrefixSumPerBlock() does prefix (exclusive scan) block by block
// to calculate prefix sum along array with a length larger than block_dim, a block prefix sum shoud be added back to the array
template<typename T>
__global__ void kernelPrefixSumBackwardAddition(const T* __restrict__ g_PrefixSumOfBlockSum, T* g_odata, const int n){
    int idx1 = blockDim.x * blockIdx.x * 2 + threadIdx.x;
    int idx2 = blockDim.x * (blockIdx.x * 2 + 1) + threadIdx.x;
    if (idx1 < n){
        g_odata[idx1] += g_PrefixSumOfBlockSum[blockIdx.x];
        //printf("g_odata[%i] = %f, g_PrefixSumOfBlockSum[%i] = %f\n", idx1, g_odata[idx1], blockIdx.x, g_PrefixSumOfBlockSum[blockIdx.x]);
    }
    if (idx2 < n){
        g_odata[idx2] += g_PrefixSumOfBlockSum[blockIdx.x];
        //printf("g_odata[%i] = %f, g_PrefixSumOfBlockSum[%i] = %f\n", idx2, g_odata[idx2], blockIdx.x, g_PrefixSumOfBlockSum[blockIdx.x]);
    }
}

static inline int nextPow2(int x)
{
    if (x < 0)
        return 0;
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return x + 1;
}

template<typename T>
void gpuPrefixSumPerBlockTemplate(const T* X, T* Y, const int n, T* blockSum){

    int BLOCK_DIM = nextPow2(n) >> 1;
    BLOCK_DIM = BLOCK_DIM >= BLOCK_DIM_MIN ? BLOCK_DIM : BLOCK_DIM_MIN;
    BLOCK_DIM = BLOCK_DIM <= BLOCK_DIM_MAX ? BLOCK_DIM : BLOCK_DIM_MAX;
    int grid_size = ((n + 1) / 2 + BLOCK_DIM - 1) / BLOCK_DIM;

    // debug
    //printf("n = %i, nPow2 = %i, BLOCK_DIM = %i, grid_size = %i\n", n, nextPow2(n), BLOCK_DIM, grid_size);

    switch (BLOCK_DIM)
    {
    case(2) :
        kernelPrefixSumPerBlock<T, 2> <<<grid_size, 2 >>>
        (X, Y, n, blockSum);
        break;
    case(4) :
        kernelPrefixSumPerBlock<T, 4> <<<grid_size, 4 >>>
        (X, Y, n, blockSum);
        break;
    case(8) :
        kernelPrefixSumPerBlock<T, 8> <<<grid_size, 8 >>>
        (X, Y, n, blockSum);
        break;
    case(16) :
        kernelPrefixSumPerBlock<T, 16> <<<grid_size, 16 >>>
        (X, Y, n, blockSum);
        break;
    case(32) :
        kernelPrefixSumPerBlock<T, 32> <<<grid_size, 32 >>>
        (X, Y, n, blockSum);
        break;
    case(64) :
        kernelPrefixSumPerBlock<T, 64> <<<grid_size, 64 >>>
        (X, Y, n, blockSum);
        break;
    case(128) :
        kernelPrefixSumPerBlock<T, 128> <<<grid_size, 128 >>>
        (X, Y, n, blockSum);
        break;
    case(256) :
        kernelPrefixSumPerBlock<T, 256> <<<grid_size, 256 >>>
        (X, Y, n, blockSum);
        break;
    case(1024) :
        kernelPrefixSumPerBlock<T, 1024> <<<grid_size, 1024 >>>
        (X, Y, n, blockSum);
        break;
    default: // set BLOCK_DIM = 512 by default
        kernelPrefixSumPerBlock<T, 512> <<<grid_size, 512 >>>
        (X, Y, n, blockSum);
        break;
    }
}

template<typename T>
void gpuPrefixSumBackwardAdditionTemplate(const T* g_PrefixSumOfBlockSum, T* g_odata, const int n){
    int grid_size = (n + BLOCK_DIM_MAX * 2 - 1) / (BLOCK_DIM_MAX * 2);
    kernelPrefixSumBackwardAddition <<<grid_size, BLOCK_DIM_MAX >>>
        (g_PrefixSumOfBlockSum, g_odata, n);
}

// warppers for templates, explicit instantiation:

void _gpuPrefixSumPerBlock(const float* X, float* Y, const int n, float* blockSum){
    gpuPrefixSumPerBlockTemplate(X, Y, n, blockSum);
}

void _gpuPrefixSumPerBlock(const double* X, double* Y, const int n, double* blockSum){
    gpuPrefixSumPerBlockTemplate(X, Y, n, blockSum);
}

void _gpuPrefixSumPerBlock(const int* X, int* Y, const int n, int* blockSum){
    gpuPrefixSumPerBlockTemplate(X, Y, n, blockSum);
}

void _gpuPrefixSumBackwardAddition(const float* g_PrefixSumOfBlockSum, float* g_odata, const int n){
    gpuPrefixSumBackwardAdditionTemplate(g_PrefixSumOfBlockSum, g_odata, n);
}

void _gpuPrefixSumBackwardAddition(const double* g_PrefixSumOfBlockSum, double* g_odata, const int n){
    gpuPrefixSumBackwardAdditionTemplate(g_PrefixSumOfBlockSum, g_odata, n);
}

void _gpuPrefixSumBackwardAddition(const int* g_PrefixSumOfBlockSum, int* g_odata, const int n){
    gpuPrefixSumBackwardAdditionTemplate(g_PrefixSumOfBlockSum, g_odata, n);
}