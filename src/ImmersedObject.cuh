#ifndef ImmersedObject_cuh
#define ImmersedObject_cuh
// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

// DeviceArray header
#include "DeviceArray.cuh"

// GpuPrefixSum
#include "GpuPrefixSum.cuh"

// LBM constant
#include "LBMconstant.hpp"

// An algorithm that calculates particle short-range interaction with O(n) complexity
// and conducts immersed boundary force distribution with out float atomic addition
// will need a cellList and verlet data structure.
//
// See: Fast N-Body Simulation with CUDA, Chapter 31
// http://developer.download.nvidia.com/compute/cuda/1.1-Beta/x86_64_website/projects/nbody/doc/nbody_gems3_ch31.pdf
//
// this is a naive implementation, but it works fine when particles are quite evenly
// distributed (the maximum number of particles in a cell will never exceed a certain
// value. This is just the case of immersed boundary in LBM method)
//
class CellList{
public: // private: debug 20171204
    static const int idr = 2; // cell size // 2 // if neighbor list is used, then pls set idr=1, MAX_POINT_NUMBER_IN_CELL=8 will be enough
    static const int MAX_POINT_NUMBER_IN_CELL = 64; // maximum lagrangian marker in a single cell // 64
    static const int MASK_GHOST_THICKNESS = 1;

    const int m;
    const int n;
    const int mc;
    const int nc;
    const bool is_x_periodic;
    const bool is_y_periodic;
    int N_update;
    DeviceArray<int> count;
    DeviceArray<int> id;

    // a map for cells who has its 'count' value or its neighbors' 'count' values larger than 0
    // function ImmersedBoundaryMethod::distributeForce() needs this mask to identify if one 
    // Euler point needs to consider immersed objects
    // 1 for valid (this cell needs to deal with immersed boundary), 0 for non-valid
    DeviceArray<int> mask;
public:
    inline CellList
        (
        const int _m,
        const int _n,
        const bool _is_x_periodic,
        const bool _is_y_periodic,
        const REAL umax
        ):
        m(_m),
        n(_n),
        mc((_m + idr - 1) / idr),
        nc((_n + idr - 1) / idr),
        is_x_periodic(_is_x_periodic),
        is_y_periodic(_is_y_periodic)
    {
        if (umax > 0.0)
        {
            N_update = std::ceil(idr * MASK_GHOST_THICKNESS / umax);
        }
        else
        {
            N_update = 1e9;
        }
        count = DeviceArray<int>(0, mc, nc);
        id = DeviceArray<int>(0, mc, nc, MAX_POINT_NUMBER_IN_CELL);
        mask = DeviceArray<int>(0, mc, nc);
    }

    // register a curve to CellList
    // this function allows asynchronous operation, user should do sync stream manually
    void registerCurve(
        const REAL* __restrict__ _x,
        const REAL* __restrict__ _y,
        const int len,
        const int id0 = 0,
        const cudaStream_t& cudaStream = cudaStreamDefault);
    inline void clear()
    {
        count = 0;
    }

    void build_mask();

};

// 20180221 a (Verlet) neighbor list contains a list of neighbor points id
class NeighborList
{
public:
    static const int MAX_NEIGHBOR_LIST_LENGTH = 64;

    // M_BLOCK is the X coarse granularity of sparse neighbor list, it is also the block size
    static const int M_BLOCK = 16;

    // N_BLOCK is the Y coarse granularity of sparse neighbor list, it is also the block size
    static const int N_BLOCK = 16;

    const int N_update_neighbor_list; // time step interval for update neighbor list
    const int N_update_block_list; // time step interval for update block list

    // simulation domain is divided into blocks sized [M_BLOCK * N_BLOCK]

    const int m;
    const int n;
    const int mc; // total cell number along x
    const int nc; // total cell number along y
    const int mb; // total block number along x
    const int nb; // total block number along y
    const bool is_x_periodic;
    const bool is_y_periodic;

    int block_list_capacity; // maximum number of blocks
    int PROTECTION_LAYER_THICKNESS; // unit: cell size -- CellList::idr
    int CUTOFF_DISTANCE; // cut-off distance of short-range interaction, unit: unit: cell size
    int* ptr_block_list_len; // current total number of blocks, pinned memory, managed by cuda

    GpuPrefixSum<int> prefixSum;

    // a [mb] * [nb] 2d array that stores total number of ib points in this block 
    DeviceArray<int> block_count;

    //  block_flag =  (block_count != 0)
    DeviceArray<int> block_flag;

    // a [mb] * [nb] 2d array, non-zero block will have a value corresponds to its index in block_list
    DeviceArray<int> block_idx;

    // a [block_list_capacity] 1d array that stores index of this block in block_flag
    DeviceArray<int> block_list;
    DeviceArray<int> device_block_list_len;
    int * h_block_list; // host copy of block_list, pinned memory, managed by cuda

    // the main data content of neighbor list, dimension:
    // [block_list_capacity * M_BLOCK * N_BLOCK] * [MAX_NEIGHBOR_LIST_LENGTH]
    DeviceArray<int> id;
    DeviceArray<int> count;

public:
    inline ~NeighborList()
    {
        cudaFreeHost(ptr_block_list_len);
        cudaFreeHost(h_block_list);
    }
    // naive constructor
    inline NeighborList
        (
        const int _N_update_neighbor_list,
        const int _N_update_block_list,
        const int _block_list_capacity,
        const int _m,
        const int _n,
        const int idr, // cell size = 2 by defalut
        const bool _is_x_periodic,
        const bool _is_y_periodic,
        const int _PROTECTION_LAYER_THICKNESS = 1, // unit: cell size
        const int _CUTOFF_DISTANCE = 2 // unit: mesh
        ) :
        N_update_neighbor_list(_N_update_neighbor_list),
        N_update_block_list(_N_update_block_list),
        m(_m),
        n(_n),
        mc((_m + idr - 1) / idr),
        nc((_n + idr - 1) / idr),
        mb((_m + idr * M_BLOCK - 1) / (idr * M_BLOCK)),
        nb((_n + idr * N_BLOCK - 1) / (idr * N_BLOCK)),
        is_x_periodic(_is_x_periodic),
        is_y_periodic(_is_y_periodic),
        block_list_capacity(_block_list_capacity),
        PROTECTION_LAYER_THICKNESS(_PROTECTION_LAYER_THICKNESS),
        CUTOFF_DISTANCE(_CUTOFF_DISTANCE),
        prefixSum(mb*nb)
    {
        block_count = DeviceArray<int>(0, mb, nb);
        block_flag = DeviceArray<int>(0, mb, nb);
        block_idx = DeviceArray<int>(0, mb, nb);
        block_list = DeviceArray<int>(0, block_list_capacity);
        device_block_list_len = DeviceArray<int>(0, 1);

        cudaError_t cudaStatus =
            cudaHostAlloc((void**)&h_block_list, block_list_capacity*sizeof(int), cudaHostAllocDefault);
        cudaStatus = cudaHostAlloc((void**)&ptr_block_list_len, 1*sizeof(int), cudaHostAllocDefault);
        

        for (int i = 0; i != block_list_capacity; ++i) h_block_list[i] = 0;

        id = DeviceArray<int>(0, block_list_capacity * M_BLOCK * N_BLOCK, MAX_NEIGHBOR_LIST_LENGTH);
        count = DeviceArray<int>(0, block_list_capacity * M_BLOCK * N_BLOCK);
    }

    // constructor with CellList
    inline NeighborList
        (
        const CellList& cellList,
        const REAL umax = 0.1,
        const int _PROTECTION_LAYER_THICKNESS = 1, // unit: cell size
        const int _CUTOFF_DISTANCE = 2 // unit: mesh
        ) :
        N_update_neighbor_list((int)std::floor(_PROTECTION_LAYER_THICKNESS * CellList::idr/ umax)),
        N_update_block_list((int)std::floor((REAL)(std::min(M_BLOCK, N_BLOCK)*CellList::idr) / umax)),
        m(cellList.m),
        n(cellList.n),
        mc((cellList.m + CellList::idr - 1) / CellList::idr),
        nc((cellList.n + CellList::idr - 1) / CellList::idr),
        mb((cellList.m + CellList::idr * M_BLOCK - 1) / (CellList::idr * M_BLOCK)),
        nb((cellList.n + CellList::idr * N_BLOCK - 1) / (CellList::idr * N_BLOCK)),
        is_x_periodic(cellList.is_x_periodic),
        is_y_periodic(cellList.is_y_periodic),
        block_list_capacity(0),
        PROTECTION_LAYER_THICKNESS(_PROTECTION_LAYER_THICKNESS),
        CUTOFF_DISTANCE(_CUTOFF_DISTANCE),
        prefixSum(mb*nb)
    {
        block_list_capacity = mb*nb; // a sufficiently large number
        block_count = DeviceArray<int>(0, mb, nb);
        block_flag = DeviceArray<int>(0, mb, nb);
        block_idx = DeviceArray<int>(0, mb, nb);
        block_list = DeviceArray<int>(0, block_list_capacity);
        device_block_list_len = DeviceArray<int>(0, 1);

        cudaError_t cudaStatus =
            cudaHostAlloc((void**)&h_block_list, block_list_capacity*sizeof(int), cudaHostAllocDefault);
        for (int i = 0; i != block_list_capacity; ++i) h_block_list[i] = 0;
        cudaStatus = cudaHostAlloc((void**)&ptr_block_list_len, 1 * sizeof(int), cudaHostAllocDefault);

        id = DeviceArray<int>(0, block_list_capacity * M_BLOCK * N_BLOCK, MAX_NEIGHBOR_LIST_LENGTH);
        count = DeviceArray<int>(0, block_list_capacity * M_BLOCK * N_BLOCK);
    }

    void update_block_list(const CellList& cellList);
    void update(const CellList& cellList);
};

// basic class for immersed objects
// derivatives must implement calculateForce()
// get_XXX functions allows user to modify all properties, be careful
class ImmersedObject{
public:
    // the MAX_POINTS is used to calculate a unique ID of a point (who belongs to one of ibObj) among all immersedObjects
    // E.g. assume in the simulation you have all immersedObject in imObj[n]
    // the ith point in jth obj will be marked as id = i+j*MAX_POINT
    // the id will be store in cellList in order to derive back its position in imObj[n]
    // this number doesn't involve with real memory assignment
    static const int MAX_POINTS = 10000;
protected:
    DeviceArray<REAL> x;    // x coordinate
    DeviceArray<REAL> y;    // y coordinate
    DeviceArray<REAL> x_prev; // x coordinate in previous step
    DeviceArray<REAL> y_prev; // y coordinate
    DeviceArray<REAL> fx;   // x force
    DeviceArray<REAL> fy;   // y force
public:
    DeviceArray<REAL> box;  // result of findBox() will be written at here with format [xmin, xmax, ymin, ymax]
public:
    DeviceArray<REAL> ux;   // ux
    DeviceArray<REAL> uy;   // uy
    cudaStream_t cudaStream;

public:
    inline ImmersedObject
        (
        const DeviceArray<REAL>& _x = DeviceArray<REAL>(),
        const DeviceArray<REAL>& _y = DeviceArray<REAL>()
        ) : x(_x), y(_y)
    {
        fx = DeviceArray<REAL>(0.0, x.size());
        fy = DeviceArray<REAL>(0.0, x.size());
        box = DeviceArray<REAL>(0.0, 4);
        ux = DeviceArray<REAL>(0.0, x.size());
        uy = DeviceArray<REAL>(0.0, x.size());
        cudaStreamCreate(&cudaStream);
    }

    inline ImmersedObject
        (
        const int n
        ) : x(0.0, n), y(0.0, n), x_prev(0.0, n), y_prev(0.0, n)
    {
        fx = DeviceArray<REAL>(0.0, n);
        fy = DeviceArray<REAL>(0.0, n);
        box = DeviceArray<REAL>(0.0, 4);
        ux = DeviceArray<REAL>(0.0, n);
        uy = DeviceArray<REAL>(0.0, n);
    }

    inline ~ImmersedObject(){
        cudaStreamDestroy(cudaStream);
    }
    inline const DeviceArray<REAL>& get_x()const{ return x; }
    inline const DeviceArray<REAL>& get_y()const{ return y; }
    inline DeviceArray<REAL>& get_x_non_const(){ return x; }
    inline DeviceArray<REAL>& get_y_non_const(){ return y; }
    inline const DeviceArray<REAL>& get_x_prev()const{ return x_prev; }
    inline const DeviceArray<REAL>& get_y_prev()const{ return y_prev; }
    inline DeviceArray<REAL>& get_x_prev_non_const(){ return x_prev; }
    inline DeviceArray<REAL>& get_y_prev_non_const(){ return y_prev; }
    inline const DeviceArray<REAL>& get_fx()const{ return fx; }
    inline const DeviceArray<REAL>& get_fy()const{ return fy; }
    virtual void calculateForce() = 0;

    // ascii format export
    inline void exportShape(const char* filename = "shape.dat"){
        DeviceArray<REAL> dd = DA::join(x, y);
        dd.reshape(x.size(), 2);
        dd.transpose();
        dd.save2D(filename);
    }
    // arrayfire format export
    inline void exportShapeAF(const char* filename = "shape.afdat", const bool append = false) const{
        DeviceArray<REAL> dd = DA::join(x, y);
        dd.reshape(x.size(), 2);
        dd.af_save("shape", filename, append);
    }

    // find the smallist box that overlapes this object. result will be written in box member
    void findBox();
    inline void showBox(){ findBox(); da_print(box); }

    // tag all points inside the immersed object as a given value valin, tag periphery points outside as valex
    void updateLabel
        (int* _label,
        const int m,
        const int n,
        const bool x_periodic,
        const bool y_periorid,
        const int valin = 1, const int valex = 0);

    void calculate_curvature(REAL* __restrict__ _curvature, const REAL dx);
    void calculate_curvature(REAL* __restrict__ _curvature, const REAL l0, const REAL dx);
    inline int size(){ return x.size(); }
    inline void sync_operation(){ cudaStreamSynchronize(cudaStream); } // warpper for cudaStreamSynchronize



};

#endif