//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//             bug bug free free amitabha
//
#include "ADboundary.cuh"

// ************************************************************************************************
// alias for kernel argument
static const int Q = ADmethod::Q; // D2Q5 model
// ************************************************************************************************
// DEFINE cuda launch pattern
// #ifdef AD_DEFAULT_STREAM
// #define CUDALAUNCH(grid, block, sharedMemSize, streamID) <<<grid, block>>>
// #else
// #define CUDALAUNCH(grid, block, sharedMemSize, streamID) <<<grid, block, sharedMemSize, streamID>>>
// #endif

// enforce default stream 201808
#define CUDALAUNCH(grid, block, sharedMemSize, streamID) <<<grid, block>>>

// ************************************************************************************************

// block size
#define BSIZE 256

// positive modulo for periodic boundary
#define POSITIVE_MOD(i,n) (((i) % (n)) + (n)) % (n)

// Dirichlet Condition
#define DIRICHLET_CONDITION(f_inv, C) ((C) * (REAL)(0.333333333333333333) - (f_inv))

// Neumann Condition : normal * grad(rho) = C
// 
// D: diffusion coefficient
#define NEUMANN_CONDITION(f_inv, C, D) ((D*C) + (f_inv))

// D: diffusion coefficient
// uf: u_factor = 1.0 / (1.0 - 3.0*uc), when uc==0, u_factor=1
//     uc is the projection of macro velocity to boundary micro velocity c
#define NEUMANN_CONDITION_WITH_ADVECTION(f_inv, C, D, uf) ((D*C) * (uf) + (f_inv) * ((REAL)2.0 * (uf) - (REAL)1.0))
// #define NEUMANN_CONDITION_WITH_ADVECTION(f_inv, C, D, uf) ((D*C) + (f_inv)) // debug


// device functions
// compute global index
__device__ __forceinline__ void device_compute_index
(
int& mn,
int& cin,
int& idxin_rho,
int& idxex_rho,
const int idxin,
const int m,
const int n){
    mn = m*n;
    cin = idxin / mn;
    idxin_rho = idxin % mn;
    int jin = idxin_rho / m; jin = POSITIVE_MOD(jin, n);
    int iin = idxin_rho % m; iin = POSITIVE_MOD(iin, m);
    int jex = jin + (int)(cin == 4) - (int)(cin == 3); jex = POSITIVE_MOD(jex, n);
    int iex = iin + (int)(cin == 2) - (int)(cin == 1); iex = POSITIVE_MOD(iex, m);
    idxex_rho = jex * m + iex;
}

// ************************************************************************************************

__global__
void kernel_computeConcentrationOnBoundary
(
REAL* __restrict__ _rho_in,
REAL* __restrict__ _rho_ex,
const int* __restrict__ _idxin,
const REAL* __restrict__ _f,
const REAL* __restrict__ _offset,
const int m,
const int n,
const int* __restrict__ _len,
const REAL tao_inv
)
{
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {
        int mn, cin, idxin_rho, idxex_rho;
        int idxin = _idxin[ik];
        device_compute_index(mn, cin, idxin_rho, idxex_rho, idxin, m, n);
		if (cin == 0) return;

        REAL fin[Q];
        REAL fex[Q];
        REAL rho_in_lattice = (REAL)0.0;
        REAL rho_ex_lattice = (REAL)0.0;

#pragma unroll
        for (int iq = 0; iq != Q; iq++)
        {
            fin[iq] = _f[idxin_rho + mn*iq];
            rho_in_lattice += fin[iq];
        }
#pragma unroll
        for (int iq = 0; iq != Q; iq++)
        {
            fex[iq] = _f[idxex_rho + mn*iq];
            rho_ex_lattice += fex[iq];
        }
        REAL offsetin = _offset[ik];
        REAL offsetex = (REAL)1.0 - offsetin;

        REAL fin0, fin1, fex0, fex1;
        if (cin == 1)
        {
            fin0 = fin[2];
            fin1 = fin[1];
            fex0 = fex[1];
            fex1 = fex[2];
        }
        if (cin == 2)
        {
            fin0 = fin[1];
            fin1 = fin[2];
            fex0 = fex[2];
            fex1 = fex[1];
        }
        if (cin == 3)
        {
            fin0 = fin[4];
            fin1 = fin[3];
            fex0 = fex[3];
            fex1 = fex[4];
        }
        if (cin == 4)
        {
            fin0 = fin[3];
            fin1 = fin[4];
            fex0 = fex[4];
            fex1 = fex[3];
        }

        REAL rho_in_temp = rho_in_lattice + offsetin * (fin1 - fin0) * (REAL)3.0 * tao_inv;
        if (rho_in_temp < 0.0)
        {
            //if (rho_in_lattice < 0.0)
                rho_in_temp = 0.0;
            //else
            //    rho_in_temp = rho_in_lattice;
        }
        _rho_in[ik] = rho_in_temp;

        REAL rho_ex_temp = rho_ex_lattice + offsetex * (fex1 - fex0) * (REAL)3.0 * tao_inv;
        if (rho_ex_temp < 0.0)
        {
            //if (rho_ex_lattice < 0.0)
                rho_ex_temp = 0.0;
            //else
            //    rho_ex_temp = rho_ex_lattice;
        }
        _rho_ex[ik] = rho_ex_temp;
    }
}

__global__
void kernel_computeConcentrationOnBoundary
(
REAL* __restrict__ _rho_in,
REAL* __restrict__ _rho_ex,
const int* __restrict__ _idxin,
const REAL* __restrict__ _f,
const REAL* __restrict__ _offset,
const int m,
const int n,
const int* __restrict__ _len,
const REAL* __restrict__ tao_inv_list,
const int* __restrict__ _Didx
)
{
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {

        int mn, cin, idxin_rho, idxex_rho;
        int idxin = _idxin[ik];
		device_compute_index(mn, cin, idxin_rho, idxex_rho, idxin, m, n);
		if (cin == 0) return;

        int Didx_in = _Didx[idxin_rho];
        int Didx_ex = _Didx[idxex_rho];

        if (Didx_in < 0 && Didx_ex < 0) return;
        REAL tao_inv_in = tao_inv_list[Didx_in];
        REAL tao_inv_ex = tao_inv_list[Didx_ex];

        REAL fin[Q];
        REAL fex[Q];
        REAL rho_in_lattice = (REAL)0.0;
        REAL rho_ex_lattice = (REAL)0.0;

#pragma unroll
        for (int iq = 0; iq != Q; iq++)
        {
            fin[iq] = _f[idxin_rho + mn*iq];
            rho_in_lattice += fin[iq];
        }
#pragma unroll
        for (int iq = 0; iq != Q; iq++)
        {
            fex[iq] = _f[idxex_rho + mn*iq];
            rho_ex_lattice += fex[iq];
        }
        REAL offsetin = _offset[ik];
        REAL offsetex = (REAL)1.0 - offsetin;

        REAL fin0, fin1, fex0, fex1;
        if (cin == 1)
        {
            fin0 = fin[2];
            fin1 = fin[1];
            fex0 = fex[1];
            fex1 = fex[2];
        }
        if (cin == 2)
        {
            fin0 = fin[1];
            fin1 = fin[2];
            fex0 = fex[2];
            fex1 = fex[1];
        }
        if (cin == 3)
        {
            fin0 = fin[4];
            fin1 = fin[3];
            fex0 = fex[3];
            fex1 = fex[4];
        }
        if (cin == 4)
        {
            fin0 = fin[3];
            fin1 = fin[4];
            fex0 = fex[4];
            fex1 = fex[3];
        }

        REAL rho_in_temp = rho_in_lattice + offsetin * (fin1 - fin0) * (REAL)3.0 * tao_inv_in;
        if (rho_in_temp < 0.0)
        {
            //if (rho_in_lattice < 0.0)
                rho_in_temp = 0.0;
            //else
            //    rho_in_temp = rho_in_lattice;
        }
        _rho_in[ik] = rho_in_temp;

        REAL rho_ex_temp = rho_ex_lattice + offsetex * (fex1 - fex0) * (REAL)3.0 * tao_inv_ex;
        if (rho_ex_temp < 0.0)
        {
            //if (rho_ex_lattice < 0.0)
                rho_ex_temp = 0.0;
            //else
            //    rho_ex_temp = rho_ex_lattice;
        }
        _rho_ex[ik] = rho_ex_temp;
    }
}

__global__
void kernel_computeConcentrationOnBoundary
(
REAL* __restrict__ _rho_in,
REAL* __restrict__ _rho_ex,
const int* __restrict__ _idxin,
const REAL* __restrict__ _f,
const REAL* __restrict__ _offset,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const int m,
const int n,
const int* __restrict__ _len,
const REAL tao_inv
)
{
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {
        int mn, cin, idxin_rho, idxex_rho;
        int idxin = _idxin[ik];
		device_compute_index(mn, cin, idxin_rho, idxex_rho, idxin, m, n);
		if (cin == 0) return;

        REAL fin[Q];
        REAL fex[Q];
        REAL rho_in_lattice = (REAL)0.0;
        REAL rho_ex_lattice = (REAL)0.0;

#pragma unroll
        for (int iq = 0; iq != Q; iq++)
        {
            fin[iq] = _f[idxin_rho + mn*iq];
            rho_in_lattice += fin[iq];
        }
#pragma unroll
        for (int iq = 0; iq != Q; iq++)
        {
            fex[iq] = _f[idxex_rho + mn*iq];
            rho_ex_lattice += fex[iq];
        }
        REAL offsetin = _offset[ik];
        REAL offsetex = (REAL)1.0 - offsetin;

        REAL fin0, fin1, fex0, fex1, uin, uex;
        if (cin == 1)
        {
            fin0 = fin[2];
            fin1 = fin[1];
            fex0 = fex[1];
            fex1 = fex[2];
            uin = -_ux[idxin_rho];
            uex = +_ux[idxex_rho];
        }
        if (cin == 2)
        {
            fin0 = fin[1];
            fin1 = fin[2];
            fex0 = fex[2];
            fex1 = fex[1];
            uin = +_ux[idxin_rho];
            uex = -_ux[idxex_rho];
        }
        if (cin == 3)
        {
            fin0 = fin[4];
            fin1 = fin[3];
            fex0 = fex[3];
            fex1 = fex[4];
            uin = -_uy[idxin_rho];
            uex = +_uy[idxex_rho];
        }
        if (cin == 4)
        {
            fin0 = fin[3];
            fin1 = fin[4];
            fex0 = fex[4];
            fex1 = fex[3];
            uin = +_uy[idxin_rho];
            uex = -_uy[idxex_rho];
        }
        
        REAL rho_in_temp = rho_in_lattice + offsetin * (fin1 - fin0 + rho_in_lattice * uin) * (REAL)3.0 * tao_inv;
        if (rho_in_temp < 0.0)
        {
            //if (rho_in_lattice < 0.0)
                rho_in_temp = 0.0;
            //else
            //    rho_in_temp = rho_in_lattice;
        }
        _rho_in[ik] = rho_in_temp;

        REAL rho_ex_temp = rho_ex_lattice + offsetex * (fex1 - fex0 + rho_ex_lattice * uex) * (REAL)3.0 * tao_inv;
        if (rho_ex_temp < 0.0)
        {
            //if (rho_ex_lattice < 0.0)
                rho_ex_temp = 0.0;
            //else
            //    rho_ex_temp = rho_ex_lattice;
        }
        _rho_ex[ik] = rho_ex_temp;
    }
}

__global__
void kernel_computeConcentrationOnBoundary
(
REAL* __restrict__ _rho_in,
REAL* __restrict__ _rho_ex,
const int* __restrict__ _idxin,
const REAL* __restrict__ _f,
const REAL* __restrict__ _offset,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const int m,
const int n,
const int* __restrict__ _len,
const REAL* __restrict__ tao_inv_list,
const int* __restrict__ _Didx
)
{
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {
        int mn, cin, idxin_rho, idxex_rho;
        int idxin = _idxin[ik];
		device_compute_index(mn, cin, idxin_rho, idxex_rho, idxin, m, n);
		if (cin == 0) return;

        int Didx_in = _Didx[idxin_rho];
        int Didx_ex = _Didx[idxex_rho];

        if (Didx_in < 0 && Didx_ex < 0) return;
        REAL tao_inv_in = tao_inv_list[Didx_in];
        REAL tao_inv_ex = tao_inv_list[Didx_ex];

        REAL fin[Q];
        REAL fex[Q];
        REAL rho_in_lattice = (REAL)0.0;
        REAL rho_ex_lattice = (REAL)0.0;

#pragma unroll
        for (int iq = 0; iq != Q; iq++)
        {
            fin[iq] = _f[idxin_rho + mn*iq];
            rho_in_lattice += fin[iq];
        }
#pragma unroll
        for (int iq = 0; iq != Q; iq++)
        {
            fex[iq] = _f[idxex_rho + mn*iq];
            rho_ex_lattice += fex[iq];
        }
        REAL offsetin = _offset[ik];
        REAL offsetex = (REAL)1.0 - offsetin;

        REAL fin0, fin1, fex0, fex1, uin, uex;
        if (cin == 1)
        {
            fin0 = fin[2];
            fin1 = fin[1];
            fex0 = fex[1];
            fex1 = fex[2];
            uin = -_ux[idxin_rho];
            uex = +_ux[idxex_rho];
        }
        if (cin == 2)
        {
            fin0 = fin[1];
            fin1 = fin[2];
            fex0 = fex[2];
            fex1 = fex[1];
            uin = +_ux[idxin_rho];
            uex = -_ux[idxex_rho];
        }
        if (cin == 3)
        {
            fin0 = fin[4];
            fin1 = fin[3];
            fex0 = fex[3];
            fex1 = fex[4];
            uin = -_uy[idxin_rho];
            uex = +_uy[idxex_rho];
        }
        if (cin == 4)
        {
            fin0 = fin[3];
            fin1 = fin[4];
            fex0 = fex[4];
            fex1 = fex[3];
            uin = +_uy[idxin_rho];
            uex = -_uy[idxex_rho];
        }

        REAL rho_in_temp = rho_in_lattice + offsetin * (fin1 - fin0 + rho_in_lattice * uin) * (REAL)3.0 * tao_inv_in;
        if (rho_in_temp < 0.0)
        {
            //if (rho_in_lattice < 0.0)
                rho_in_temp = 0.0;
            //else
            //    rho_in_temp = rho_in_lattice;
        }
        _rho_in[ik] = rho_in_temp;

        REAL rho_ex_temp = rho_ex_lattice + offsetex * (fex1 - fex0 + rho_ex_lattice * uex) * (REAL)3.0 * tao_inv_ex;
        if (rho_ex_temp < 0.0)
        {
            //if (rho_ex_lattice < 0.0)
                rho_ex_temp = 0.0;
            //else
            //    rho_ex_temp = rho_ex_lattice;
        }
        _rho_ex[ik] = rho_ex_temp;
    }
}

void ADBoundary::computeConcentrationOnBoundary(const ADmethod& method, const REAL* _f)
{
    int len = kernel_launch_size; // size();
    if (len < 1)return;
    kernel_computeConcentrationOnBoundary CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        rho_in.data_ptr(),
        rho_ex.data_ptr(),
        idx.data_ptr(),
        _f,
        offset.data_ptr(),
        m,
        n,
        boundary_size.data_ptr(),
        method.get_tao_inv()
        );
}

void ADBoundary::computeConcentrationOnBoundary(const ADmethod& method, const REAL* _f, const REAL* _ux, const REAL* _uy)
{
    int len = kernel_launch_size; // size();
    if (len < 1)return;
    kernel_computeConcentrationOnBoundary CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        rho_in.data_ptr(),
        rho_ex.data_ptr(),
        idx.data_ptr(),
        _f,
        offset.data_ptr(),
        _ux,
        _uy,
        m,
        n,
        boundary_size.data_ptr(),
        method.get_tao_inv()
        );
}

void ADBoundary::computeConcentrationOnBoundary(const ADmethod& method, const REAL* _f, const int* _Didx)
{
    int len = kernel_launch_size; // size();
    if (len < 1)return;
    kernel_computeConcentrationOnBoundary CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        rho_in.data_ptr(),
        rho_ex.data_ptr(),
        idx.data_ptr(),
        _f,
        offset.data_ptr(),
        m,
        n,
        boundary_size.data_ptr(),
        method.get_tao_inv_list().data_ptr(),
        _Didx);
}

void ADBoundary::computeConcentrationOnBoundary(const ADmethod& method, const REAL* _f, const REAL* _ux, const REAL* _uy, const int* _Didx)
{
    int len = kernel_launch_size; // size();
    if (len < 1)return;
    kernel_computeConcentrationOnBoundary CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        rho_in.data_ptr(),
        rho_ex.data_ptr(),
        idx.data_ptr(),
        _f,
        offset.data_ptr(),
        _ux,
        _uy,
        m,
        n,
        boundary_size.data_ptr(),
        method.get_tao_inv_list().data_ptr(),
        _Didx
        );
}

// ************************************************************************************************
template<bool is_static, bool is_const_D>
// calcualte the concentration gradient along with / perpendicular to
// ci direction
// in LBM, derivative calculation is based on local distrbution function [1]
// [1] juntao huang, Second-order curved boundary treatments of the lattice
//     Boltzmann method for convection�Cdiffusion equations,
//     Journal of Computational Physics 310 (2016) 26�C44
__device__ void device_calculate_drho_dci
(
REAL& drhodci, // derivative along ci direction
REAL& drhodti, // derivative along the direction rotate(ci,-90)
const int& ci, // direction of outward micro velocity
const int& idx,
const int& mn,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _f0,
REAL D,
const REAL* __restrict__ _D_list,
const int* __restrict__ _D_label
)
{
    if (!is_const_D) D = _D_list[_D_label[idx]];
    REAL tao_inv = 1.0 / (D*3.0 + 0.5);

    REAL rho = 0.0;
    REAL f[5];

#pragma unroll
    for (int it = 0; it != Q; ++it){
        f[it] = _f0[idx + it*mn];
        rho += f[it];
    }

    REAL drhodx = f[2] - f[1];
    REAL drhody = f[4] - f[3];
    if (!is_static)
    {
        drhodx += (_ux[idx] * rho);
        drhody += (_uy[idx] * rho);
    }
    drhodx *= (3.0*tao_inv);
    drhody *= (3.0*tao_inv);
    if (ci == 1)
    {
        drhodci = drhodx;
        drhodti = -drhody;
    }
    if (ci == 2)
    {
        drhodci = -drhodx;
        drhodti = drhody;
    }
    if (ci == 3)
    {
        drhodci = drhody;
        drhodti = drhodx;
    }
    if (ci == 4)
    {
        drhodci = -drhody;
        drhodti = -drhodx;
    }
}

template<bool is_static, bool is_const_D>
__device__ void device_calculate_BC_midpoint_dirichlet
(
REAL& C_mid,
const REAL& C, // boundary condition value
const REAL& offset,
const int& ci, // direction of outward micro velocity
const int& idx_rho,
const int& m,
const int& n,
const bool& is_x_periodic,
const bool& is_y_periodic,
const char* __restrict__ _tag,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _f0,
const REAL& D,
const REAL* __restrict__ _D_list,
const int* __restrict__ _D_label
)
{
    REAL drhodci, drhodti;

    // this method is from [1]
    // [1] juntao huang, Second-order curved boundary treatments of the lattice
    //     Boltzmann method for convection�Cdiffusion equations,
    //     Journal of Computational Physics 310 (2016) 26�C44
    // it seems this method is unstable when tau (or D) is very small and offset > 0.5
    //
    // I try to fix this problem by using (C-rho(t=n-1, i,j))/offset
    // to approximate the derivative, instead of using formula in [1]
    // when offset > 0.5
    // Hengdi ZHANG, May.28 2018.
    // hengdi.zhang@univ-grenoble-alpes.fr
    //
    if (offset < 0.5)
    {
        device_calculate_drho_dci<is_static, is_const_D>(drhodci, drhodti, ci, idx_rho, m*n, _ux, _uy, _f0, D, _D_list, _D_label);
        C_mid = C + drhodci * (0.5 - offset);
    }
    else
    {
        REAL rho_prev = 0.0;
#pragma unroll
        for (int it = 0; it != Q; ++it)
        {
            rho_prev += _f0[idx_rho + it*m*n];
        }
        C_mid = 0.5 / offset * (C - rho_prev) + rho_prev;
    }
}

template<bool is_static, bool is_const_D>
__device__ void device_calculate_BC_midpoint_neumann
(
REAL& C_mid,
const REAL& C, // boundary condition value
const REAL& nx, // outward normal x component
const REAL& ny, // outward normal y component
const REAL& offset,
const int& ci, // direction of outward micro velocity (the definition differs from "incoming intersecting velocity", be careful)
const int& idx_rho,
const int& m,
const int& n,
const bool& is_x_periodic,
const bool& is_y_periodic,
const char* __restrict__ _tag,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const REAL* __restrict__ _f0,
const REAL& D,
const REAL* __restrict__ _D_list,
const int* __restrict__ _D_label
)
{
    int i = idx_rho % m;
    int j = idx_rho / m;
    REAL drhodn_mid, drhodt_mid; // C_mid = ny*drhodn_mid + nx*drhodt_mid, n for normal t for tangential
    drhodn_mid = C;

    // calculate drho/dtangent
    // calculate drhodn (normal) and drhodt (tangent) at (i,j)
    REAL drhodx_ij, drhody_ij, drhodn_ij, drhodt_ij;
    device_calculate_drho_dci<is_static, is_const_D>(drhodx_ij, drhody_ij, 1, idx_rho, m*n, _ux, _uy, _f0, D, _D_list, _D_label);
    drhody_ij = -drhody_ij; // y coordinate is inverted in function device_calculate_drho_dci
    drhodn_ij = drhodx_ij * nx + drhody_ij * ny;
    drhodt_ij = -drhodx_ij * ny + drhody_ij * nx;

    drhodt_mid = drhodt_ij;

    // check interpolation validity of its neighbor (opposite to ci)
    int cx = ((int)(ci == 1) - (int)(ci == 2));
    int cy = ((int)(ci == 3) - (int)(ci == 4));
    int in = i - cx;
    int jn = j - cy;

    // a neighbor (in,jn) is valid when tags at (i,j) and (in,jn) do not have opposite value
    bool neighbor_validity = false;
    char neighbor_tag = 0;

#define VALID_POSITION(i,j) (!( (((i)>(m-1) |(i)<0) & (!is_x_periodic)) | (((j)>(n-1) |(j)<0) & (!is_y_periodic)) ))
    if (VALID_POSITION(in, jn))
    {
		{
			in = (in + m) % m;
			jn = (jn + n) % n;
			neighbor_tag = _tag[in + jn*m];

			// this condition allows any neighbour points with the same tag (or with a non-boundary tag) to be valid.
			neighbor_validity = (_tag[idx_rho] * neighbor_tag >= 0);
		}
    }

#define _LONELY_POINTS_IS_FIRST_ORDER
#ifdef _LONELY_POINTS_IS_FIRST_ORDER
    // a point is considered as lonely if its tangential neighbors (L and R) are both inversely tagged
    // if _LONELY_POINTS_IS_FIRST_ORDER is defined, then a lonely point will not use neighbor extrapolation
    int inL = i - cy;
    int jnL = j + cx;
    int inR = i + cy;
    int jnR = j - cx;
    if (VALID_POSITION(inL, jnL) & VALID_POSITION(inR, jnR))
    {
        inL = (inL + m) % m;
        jnL = (jnL + n) % n;
        inR = (inR + m) % m;
        jnR = (jnR + n) % n;
        bool is_left_same = (_tag[idx_rho] * _tag[inL + jnL*m] >= 0);
        bool is_right_same = (_tag[idx_rho] * _tag[inR + jnR*m] >= 0);
        bool is_lonely_point = (!is_left_same) & (!is_right_same);
        if (is_lonely_point) neighbor_validity = false;
    }
#endif // _LONELY_POINTS_IS_FIRST_ORDER

    // debug with 1st order precision
    // neighbor_validity = false;

    if (neighbor_validity)
    {
        // calculate drhodn (normal) and drhodt (tangent) at (in,jn)
        REAL drhodx_injn, drhody_injn, drhodn_injn, drhodt_injn;
        device_calculate_drho_dci<is_static, is_const_D>
            (drhodx_injn, drhody_injn, 1, (in+jn*m), m*n, _ux, _uy, _f0, D, _D_list, _D_label);
        drhody_injn = -drhody_injn; // y coordinate is inverted in function device_calculate_drho_dci
        drhodn_injn = drhodx_injn * nx + drhody_injn * ny;
        drhodt_injn = -drhodx_injn * ny + drhody_injn * nx;

		///*
		if (offset < 0.5)
		{
			drhodn_mid += ((drhodn_ij - drhodn_injn) * ((REAL)0.5 - offset));
		}
		else
		{
			drhodn_mid = (C - drhodn_ij) / offset*0.5 + drhodn_ij;
		}
		//*/
		// drhodn_mid += ((drhodn_ij - drhodn_injn) * ((REAL)0.5 - offset));
		drhodt_mid += ((drhodt_ij - drhodt_injn) * (REAL)0.5);
    }
    else
    {
        // debug
        // printf("at (%i,%i) outward micro velocity c = %i is 1st order condition\n", i, j, ci);
    }

    C_mid = (cx*nx + cy*ny) * drhodn_mid + (cy*nx - cx*ny) * drhodt_mid;


}
// ************************************************************************************************
__global__ void kernel_setAllBOundaryConditions
(
REAL* __restrict__ _f,
REAL* __restrict__ _f0,
char* __restrict__ _tag,
const int* __restrict__ _idxin,
const REAL* __restrict__ offset,
const REAL* __restrict__ _nx,
const REAL* __restrict__ _ny,
const REAL* __restrict__ _C_in,
const REAL* __restrict__ _C_ex,
const short* _Ctype_in,
const short* _Ctype_ex,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic,
const int* __restrict__ _len,
const REAL D
){
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {

        int mn, cin, idxin_rho, idxex_rho;
        int idxin = _idxin[ik];
		device_compute_index(mn, cin, idxin_rho, idxex_rho, idxin, m, n);
		if (cin == 0) return;
        int cex = 2 * (int)(cin == 1) + 1 * (int)(cin == 2) + 4 * (int)(cin == 3) + 3 * (int)(cin == 4);
        int idxex = idxex_rho + cex * mn;

        short Ctype_in = _Ctype_in[ik];
        short Ctype_ex = _Ctype_ex[ik];

        REAL C_in = _C_in[ik];
        REAL C_ex = _C_ex[ik];

        // if needed, read normal
        if (Ctype_in == 0 || Ctype_ex == 0)
        {
            REAL nx = _nx[ik];
            REAL ny = _ny[ik];
            REAL cx = ((REAL)(cin == 1) - (REAL)(cin == 2));
            REAL cy = ((REAL)(cin == 3) - (REAL)(cin == 4));
            if (Ctype_in == 0)
            {
                C_in *= -nx * cx - ny * cy;
            }
            if (Ctype_ex == 0)
            {
                C_ex *= nx * cx + ny * cy;
            }
        }

        REAL fin = _f[idxin];
        REAL fex = _f[idxex];
        REAL fin_set, fex_set;

        // better to sync here, _f will be read, _f will be written
        // __syncthreads();

        // calculate internal boundary condition
        if (Ctype_in == 0)
        {
            // Neumann condition
            fin_set = NEUMANN_CONDITION(fex, C_in, D);
        }
        else
        {
            // Ctype_ex == 1
            // Dirichlet condition
            fin_set = DIRICHLET_CONDITION(fex, C_in);
        }

        // calculate external boundary condition
        if (Ctype_ex == 0)
        {
            // Neumann condition
            fex_set = NEUMANN_CONDITION(fin, C_ex, D);
        }
        else
        {
            // Ctype_ex == 1
            // Dirichlet condition
            fex_set = DIRICHLET_CONDITION(fin, C_ex);
        }

        _f[idxin] = fin_set;
        _f[idxex] = fex_set;

    }
}

__global__ void kernel_setAllBOundaryConditions
(
REAL* __restrict__ _f,
REAL* __restrict__ _f0,
char* __restrict__ _tag,
const int* __restrict__ _idxin,
const REAL* __restrict__ offset,
const REAL* __restrict__ _nx,
const REAL* __restrict__ _ny,
const REAL* __restrict__ _C_in,
const REAL* __restrict__ _C_ex,
const short* _Ctype_in,
const short* _Ctype_ex,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic,
const int* __restrict__ _len,
const REAL* __restrict__ D_list,
const int* __restrict__ _Didx
){
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {

        int mn, cin, idxin_rho, idxex_rho;
        int idxin = _idxin[ik];
		device_compute_index(mn, cin, idxin_rho, idxex_rho, idxin, m, n);
		if (cin == 0) return;
        int cex = 2 * (int)(cin == 1) + 1 * (int)(cin == 2) + 4 * (int)(cin == 3) + 3 * (int)(cin == 4);
        int idxex = idxex_rho + cex * mn;

        int Didx_in = _Didx[idxin_rho];
        int Didx_ex = _Didx[idxex_rho];
        bool is_masked_in = Didx_in < 0;
        bool is_masked_ex = Didx_ex < 0;
        if (is_masked_in && is_masked_ex) return;
        Didx_in = is_masked_in ? Didx_ex : Didx_in;
        Didx_ex = is_masked_ex ? Didx_in : Didx_ex;
        REAL D_in, D_ex;

        short Ctype_in = _Ctype_in[ik];
        short Ctype_ex = _Ctype_ex[ik];

        REAL C_in = _C_in[ik];
        REAL C_ex = _C_ex[ik];

        // if needed, read normal
        if (Ctype_in == 0 || Ctype_ex == 0)
        {
            REAL nx = _nx[ik];
            REAL ny = _ny[ik];
            REAL cx = ((REAL)(cin == 1) - (REAL)(cin == 2));
            REAL cy = ((REAL)(cin == 3) - (REAL)(cin == 4));
            if (Ctype_in == 0)
            {
                D_in = D_list[Didx_in];
                C_in *= -nx * cx - ny * cy;
            }
            if (Ctype_ex == 0)
            {
                D_ex = D_list[Didx_ex];
                C_ex *= nx * cx + ny * cy;
            }
        }

        REAL fin = _f[idxin];
        REAL fex = _f[idxex];
        REAL fin_set, fex_set;

        // better to sync here, _f will be read, _f will be written
        // __syncthreads();

        // calculate internal boundary condition
        if (Ctype_in == 0)
        {
            // Neumann condition
            fin_set = NEUMANN_CONDITION(fex, C_in, D_in);
        }
        else
        {
            // Ctype_in == 1
            // Dirichlet condition
            fin_set = DIRICHLET_CONDITION(fex, C_in);
        }

        // calculate external boundary condition
        if (Ctype_ex == 0)
        {
            // Neumann condition
            fex_set = NEUMANN_CONDITION(fin, C_ex, D_ex);
        }
        else
        {
            // Ctype_ex == 1
            // Dirichlet condition
            fex_set = DIRICHLET_CONDITION(fin, C_ex);
        }

        if (~is_masked_in) _f[idxin] = fin_set;
        if (~is_masked_ex) _f[idxex] = fex_set;

    }
}

__global__ void kernel_setAllBOundaryConditions
(
REAL* __restrict__ _f,
REAL* __restrict__ _f0,
char* __restrict__ _tag,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const int* __restrict__ _idxin,
const REAL* __restrict__ _offset,
const REAL* __restrict__ _nx,
const REAL* __restrict__ _ny,
const REAL* __restrict__ _C_in,
const REAL* __restrict__ _C_ex,
const short* _Ctype_in,
const short* _Ctype_ex,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic,
const int* __restrict__ _len,
const REAL D
)
{
    int len = *_len;
    int ik = blockDim.x*blockIdx.x + threadIdx.x;
    if (ik < len)
    {
        int mn, cin, idxin_rho, idxex_rho;
        int idxin = _idxin[ik];
		device_compute_index(mn, cin, idxin_rho, idxex_rho, idxin, m, n);
		if (cin == 0) return;
        int cex = 2 * (int)(cin == 1) + 1 * (int)(cin == 2) + 4 * (int)(cin == 3) + 3 * (int)(cin == 4);
        int idxex = idxex_rho + cex * mn;

        REAL offset_in = _offset[ik];

        // use position of offset_in in next time step
        // estimate boundary velocity (a non slip boundary condition is assumed, be careful)
        /*
        REAL uxb = _ux[idxin_rho] * (1.0 - offset_in) + _ux[idxex_rho] * offset_in;
        REAL uyb = _uy[idxin_rho] * (1.0 - offset_in) + _uy[idxex_rho] * offset_in;
        REAL ub =
            - ((REAL)(cin == 1) - (REAL)(cin == 2)) * uxb
            - ((REAL)(cin == 3) - (REAL)(cin == 4)) * uyb;
        offset_in += ub;
        */

        REAL offset_ex = 1.0 - offset_in;

        short Ctype_in = _Ctype_in[ik];
        short Ctype_ex = _Ctype_ex[ik];

        REAL C_in = _C_in[ik];
        REAL C_ex = _C_ex[ik];
        //REAL offset = _offset[ik];

        // u_factor = 1.0 / (1.0 - 3.0*uc), when uc==0, u_factor=1, uc is the projection of macro velocity to boundary micro velocity c
        REAL u_factor_in, u_factor_ex;

        // if needed, read normal
        REAL nx, ny, cx, cy;
        if (Ctype_in == 0 || Ctype_ex == 0)
        {
            nx = _nx[ik];
            ny = _ny[ik];
            cx = ((REAL)(cin == 1) - (REAL)(cin == 2));
            cy = ((REAL)(cin == 3) - (REAL)(cin == 4));
            // REAL ux = offset * (_ux[idxex_rho] - _ux[idxin_rho]) + _ux[idxin_rho];
            // REAL uy = offset * (_uy[idxex_rho] - _uy[idxin_rho]) + _uy[idxin_rho];
            if (Ctype_in == 0)
            {
                // u_factor_in = ux * cx + uy * cy;
                u_factor_in = _ux[idxin_rho] * cx + _uy[idxin_rho] * cy;
                u_factor_in = (REAL)1.0 / ((REAL)1.0 - 3.0 * u_factor_in);
                // C_in *= -nx * cx - ny * cy; // don't do this, this will be estimate by a 2nd order precision function later
            }
            if (Ctype_ex == 0)
            {
                // u_factor_ex = -ux * cx - uy * cy;
                u_factor_ex = -_ux[idxex_rho] * cx - _uy[idxex_rho] * cy;
                u_factor_ex = (REAL)1.0 / ((REAL)1.0 - 3.0 * u_factor_ex);
                // C_ex *= nx * cx + ny * cy; // don't do this, this will be estimate by a 2nd order precision function later
            }
        }

        REAL fin = _f[idxin];
        REAL fex = _f[idxex];
        REAL fin_set, fex_set;

        // better to sync here, _f will be read, _f will be written // no, dont't do that 20180609
        // __syncthreads();

        // calculate internal boundary condition

        // debug
        //printf("idx=%i, u_factor_in, u_factor_ex = %.17f, %.17f\n", ik, u_factor_in, u_factor_ex);
        if (Ctype_in == 0)
        {
            // Neumann condition
            REAL C_in_mid;
            device_calculate_BC_midpoint_neumann<false, true>(
                C_in_mid, C_in, nx, ny, offset_in,
                cex, idxin_rho,
                m, n, is_x_periodic, is_y_periodic,
                _tag, _ux, _uy, _f0, D,
                nullptr, nullptr);
            fin_set = NEUMANN_CONDITION_WITH_ADVECTION(fex, C_in_mid, D, u_factor_in);
            // fin_set = NEUMANN_CONDITION_WITH_ADVECTION(fex, -C_in*(nx * cx + ny * cy), D, u_factor_in);
        }
        else
        {
            // Ctype_in == 1
            // Dirichlet condition
            REAL C_in_mid;
            // cin and cex are INCOMING micro velocity directions, so here you need to exchange them in the argument list
            device_calculate_BC_midpoint_dirichlet<false, true>(
                C_in_mid, C_in, offset_in,
                cex, idxin_rho,
                m, n, is_x_periodic, is_y_periodic,
                _tag, _ux, _uy, _f0, D,
                nullptr, nullptr);
            // fin_set = DIRICHLET_CONDITION(fex, C_in);
            fin_set = DIRICHLET_CONDITION(fex, C_in_mid);
        }

        // calculate external boundary condition
        if (Ctype_ex == 0)
        {
            // Neumann condition
            REAL C_ex_mid;
            device_calculate_BC_midpoint_neumann<false, true>(
                C_ex_mid, -C_ex, -nx, -ny, offset_ex,
                cin, idxex_rho,
                m, n, is_x_periodic, is_y_periodic,
                _tag, _ux, _uy, _f0, D,
                nullptr, nullptr);
            fex_set = NEUMANN_CONDITION_WITH_ADVECTION(fin, C_ex_mid, D, u_factor_ex);
            // fex_set = NEUMANN_CONDITION_WITH_ADVECTION(fin, C_ex*(nx * cx + ny * cy), D, u_factor_ex);
        }
        else
        {
            // Ctype_ex == 1
            // Dirichlet condition
            REAL C_ex_mid;
            // cin and cex are INCOMING micro velocity directions, so here you need to exchange them in the argument list
            device_calculate_BC_midpoint_dirichlet<false, true>
                (C_ex_mid, C_ex, offset_ex,
                cin, idxex_rho,
                m, n, is_x_periodic, is_y_periodic,
                _tag, _ux, _uy, _f0, D,
                nullptr, nullptr);
            //fex_set = DIRICHLET_CONDITION(fin, C_ex);
            fex_set = DIRICHLET_CONDITION(fin, C_ex_mid);
        }

        _f[idxin] = fin_set;
        _f[idxex] = fex_set;
    }
}

__global__ void kernel_setAllBOundaryConditions
(
REAL* __restrict__ _f,
REAL* __restrict__ _f0,
char* __restrict__ _tag,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const int* __restrict__ _idxin,
const REAL* __restrict__ _offset,
const REAL* __restrict__ _nx,
const REAL* __restrict__ _ny,
const REAL* __restrict__ _C_in,
const REAL* __restrict__ _C_ex,
const short* _Ctype_in,
const short* _Ctype_ex,
const int m,
const int n,
const bool is_x_periodic,
const bool is_y_periodic,
const int* __restrict__ _len,
const REAL* __restrict__ D_list,
const int* __restrict__ _Didx
)
{
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {

        int mn, cin, idxin_rho, idxex_rho;
        int idxin = _idxin[ik];
		device_compute_index(mn, cin, idxin_rho, idxex_rho, idxin, m, n);
		if (cin == 0) return;
        int cex = 2 * (int)(cin == 1) + 1 * (int)(cin == 2) + 4 * (int)(cin == 3) + 3 * (int)(cin == 4);
        int idxex = idxex_rho + cex * mn;

        REAL offset_in = _offset[ik];

        // use position of offset_in in next time step
        // estimate boundary velocity (a non slip boundary condition is assumed, be careful)
        /*
        REAL uxb = _ux[idxin_rho] * (1.0 - offset_in) + _ux[idxex_rho] * offset_in;
        REAL uyb = _uy[idxin_rho] * (1.0 - offset_in) + _uy[idxex_rho] * offset_in;
        REAL ub =
            -((REAL)(cin == 1) - (REAL)(cin == 2)) * uxb
            - ((REAL)(cin == 3) - (REAL)(cin == 4)) * uyb;
        offset_in += ub;
        */

        REAL offset_ex = 1.0 - offset_in;

        int Didx_in = _Didx[idxin_rho];
        int Didx_ex = _Didx[idxex_rho];
        bool is_masked_in = Didx_in < 0;
        bool is_masked_ex = Didx_ex < 0;
        if (is_masked_in && is_masked_ex) return;
        Didx_in = is_masked_in ? Didx_ex : Didx_in;
        Didx_ex = is_masked_ex ? Didx_in : Didx_ex;
        REAL D_in, D_ex;

        short Ctype_in = _Ctype_in[ik];
        short Ctype_ex = _Ctype_ex[ik];

        REAL C_in = _C_in[ik];
        REAL C_ex = _C_ex[ik];

        // u_factor = 1.0 / (1.0 - 3.0*uc), when uc==0, u_factor=1, uc is the projection of macro velocity to boundary micro velocity c
        REAL u_factor_in, u_factor_ex;

        // if needed, read normal
        REAL nx, ny, cx, cy;

        // if boundary is neumann type (Ctype==0), read normal vector
        if (Ctype_in == 0 || Ctype_ex == 0)
        {
            nx = _nx[ik];
            ny = _ny[ik];
            cx = ((REAL)(cin == 1) - (REAL)(cin == 2));
            cy = ((REAL)(cin == 3) - (REAL)(cin == 4));
            // REAL ux = offset * (_ux[idxex_rho] - _ux[idxin_rho]) + _ux[idxin_rho];
            // REAL uy = offset * (_uy[idxex_rho] - _uy[idxin_rho]) + _uy[idxin_rho];
            if (Ctype_in == 0)
            {
                D_in = D_list[Didx_in];
                // u_factor_in = ux * cx + uy * cy;
                u_factor_in = _ux[idxin_rho] * cx + _uy[idxin_rho] * cy;
                u_factor_in = (REAL)1.0 / ((REAL)1.0 - 3.0 * u_factor_in);
                C_in *= -nx * cx - ny * cy;
            }
            if (Ctype_ex == 0)
            {
                D_ex = D_list[Didx_ex];
                // u_factor_ex = -ux * cx - uy * cy;
                u_factor_ex = -_ux[idxex_rho] * cx - _uy[idxex_rho] * cy;
                u_factor_ex = (REAL)1.0 / ((REAL)1.0 - 3.0 * u_factor_ex);
                C_ex *= nx * cx + ny * cy;
            }
        }

        REAL fin = _f[idxin];
        REAL fex = _f[idxex];
        REAL fin_set, fex_set;

        // better to sync here, _f will be read, _f will be written // no, dont't do that 20180609
        // __syncthreads();

        // calculate internal boundary condition
        if (Ctype_in == 0)
        {
            // Neumann condition
            REAL C_in_mid;
            device_calculate_BC_midpoint_neumann<false, true>(
                C_in_mid, C_in, nx, ny, offset_in,
                cex, idxin_rho,
                m, n, is_x_periodic, is_y_periodic,
                _tag, _ux, _uy, _f0, D_in,
                nullptr, nullptr);
            fin_set = NEUMANN_CONDITION_WITH_ADVECTION(fex, C_in_mid, D_in, u_factor_in);
            // fin_set = NEUMANN_CONDITION_WITH_ADVECTION(fex, C_in, D_in, u_factor_in);
        }
        else
        {
            // Ctype_in == 1
            // Dirichlet condition
            REAL C_in_mid;
            // cin and cex are INCOMING micro velocity directions, so here you need to exchange them in the argument list
            device_calculate_BC_midpoint_dirichlet<false, true>(
                C_in_mid, C_in, offset_in,
                cex, idxin_rho,
                m, n, is_x_periodic, is_y_periodic,
                _tag, _ux, _uy, _f0, D_in,
                nullptr, nullptr);
            // fin_set = DIRICHLET_CONDITION(fex, C_in);
            fin_set = DIRICHLET_CONDITION(fex, C_in_mid);
        }

        // calculate external boundary condition
        if (Ctype_ex == 0)
        {
            // Neumann condition
            REAL C_ex_mid;
            device_calculate_BC_midpoint_neumann<false, true>(
                C_ex_mid, -C_ex, -nx, -ny, offset_ex,
                cin, idxex_rho,
                m, n, is_x_periodic, is_y_periodic,
                _tag, _ux, _uy, _f0, D_ex,
                nullptr, nullptr);
            fex_set = NEUMANN_CONDITION_WITH_ADVECTION(fin, C_ex_mid, D_ex, u_factor_ex);
            // fex_set = NEUMANN_CONDITION_WITH_ADVECTION(fin, C_ex, D_ex, u_factor_ex);
        }
        else
        {
            // Ctype_ex == 1
            // Dirichlet condition
            REAL C_ex_mid;
            // cin and cex are INCOMING micro velocity directions, so here you need to exchange them in the argument list
            device_calculate_BC_midpoint_dirichlet<false, true>
                (C_ex_mid, C_ex, offset_ex,
                cin, idxex_rho,
                m, n, is_x_periodic, is_y_periodic,
                _tag, _ux, _uy, _f0, D_ex,
                nullptr, nullptr);
            // fex_set = DIRICHLET_CONDITION(fin, C_ex);
            fex_set = DIRICHLET_CONDITION(fin, C_ex_mid);
        }

        if (~is_masked_in) _f[idxin] = fin_set;
        if (~is_masked_ex) _f[idxex] = fex_set;

    }
}

void ADBoundary::setAllBoundaryConditions(const ADmethod& method, REAL* _f, REAL* _f0){
    int len = kernel_launch_size; // size();
    if (len < 1)return;
    kernel_setAllBOundaryConditions CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        _f,
        _f0,
		voxelizer.tag_previous.data_ptr(), // this actually is tag_conn8, tag_conn8 and tag are swapped in method registerCurveFinished
        idx.data_ptr(),
        offset.data_ptr(),
        nx.data_ptr(),
        ny.data_ptr(),
        C_in.data_ptr(),
        C_ex.data_ptr(),
        Ctype_in.data_ptr(),
        Ctype_ex.data_ptr(),
        m,
        n,
        is_x_periodic,
        is_y_periodic,
        boundary_size.data_ptr(),
        method.get_D()
        );
}

void ADBoundary::setAllBoundaryConditions(const ADmethod& method, REAL* _f, REAL* _f0, const int* _Didx){
    int len = kernel_launch_size; // size();
    if (len < 1)return;
    kernel_setAllBOundaryConditions CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        _f,
		_f0,
		voxelizer.tag_previous.data_ptr(), // this actually is tag_conn8, tag_conn8 and tag are swapped in method registerCurveFinished
        idx.data_ptr(),
        offset.data_ptr(),
        nx.data_ptr(),
        ny.data_ptr(),
        C_in.data_ptr(),
        C_ex.data_ptr(),
        Ctype_in.data_ptr(),
        Ctype_ex.data_ptr(),
        m,
        n,
        is_x_periodic,
        is_y_periodic,
        boundary_size.data_ptr(),
        method.get_D_list().data_ptr(),
        _Didx
        );
}

void ADBoundary::setAllBoundaryConditions(const ADmethod& method, REAL* _f, REAL* _f0, const REAL* _ux, const REAL* _uy){
    int len = kernel_launch_size; // size();
    if (len < 1)return;
    kernel_setAllBOundaryConditions CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        _f,
		_f0,
		voxelizer.tag_previous.data_ptr(), // this actually is tag_conn8, tag_conn8 and tag are swapped in method registerCurveFinished
        _ux,
        _uy,
        idx.data_ptr(),
        offset.data_ptr(),
        nx.data_ptr(),
        ny.data_ptr(),
        C_in.data_ptr(),
        C_ex.data_ptr(),
        Ctype_in.data_ptr(),
        Ctype_ex.data_ptr(),
        m,
        n,
        is_x_periodic,
        is_y_periodic,
        boundary_size.data_ptr(),
        method.get_D()
        );
}

void ADBoundary::setAllBoundaryConditions(const ADmethod& method, REAL* _f, REAL* _f0, const REAL* _ux, const REAL* _uy, const int* _Didx)
{
    int len = kernel_launch_size; // size();
    if (len < 1)return;
    kernel_setAllBOundaryConditions CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        _f,
		_f0,
		voxelizer.tag_previous.data_ptr(), // this actually is tag_conn8, tag_conn8 and tag are swapped in method registerCurveFinished
        _ux,
        _uy,
        idx.data_ptr(),
        offset.data_ptr(),
        nx.data_ptr(),
        ny.data_ptr(),
        C_in.data_ptr(),
        C_ex.data_ptr(),
        Ctype_in.data_ptr(),
        Ctype_ex.data_ptr(),
        m,
        n,
        is_x_periodic,
        is_y_periodic,
        boundary_size.data_ptr(),
        method.get_D_list().data_ptr(),
        _Didx
        );
}

// export boundary information to file in the format of ARRAYFIRE (GOOGLE IT)
void ADBoundary::exportToFile_ArrayFire(const char fname[]) const{
    int host_boundary_size = size();
    DeviceArray<int>(m).af_save("m", fname, false, host_boundary_size);
    DeviceArray<int>(n).af_save("n", fname, true, host_boundary_size);
    DeviceArray<int> ilist_in = idx % m;
    DeviceArray<int> jlist_in = (idx % (m*n)) / m;
    ilist_in.af_save("ilist_in", fname, true, host_boundary_size);
    jlist_in.af_save("jlist_in", fname, true, host_boundary_size);
    DeviceArray<int> clist_in = idx / (m*n);
    clist_in.af_save("clist_in", fname, true, host_boundary_size);
    offset.af_save("offsetlist_in", fname, true, host_boundary_size);
    s.af_save("slist_in", fname, true, host_boundary_size);
    nx.af_save("nxlist_in", fname, true, host_boundary_size);
    ny.af_save("nylist_in", fname, true, host_boundary_size);
    DeviceArray<int> ilist_ex =
        ilist_in -
        DeviceArray<int>(clist_in == 1) +
        DeviceArray<int>(clist_in == 2);
    DeviceArray<int> jlist_ex =
        jlist_in -
        DeviceArray<int>(clist_in == 3) +
        DeviceArray<int>(clist_in == 4);
    DeviceArray<int> clist_ex =
        DeviceArray<int>(clist_in == 1) * 2 +
        DeviceArray<int>(clist_in == 2) * 1 +
        DeviceArray<int>(clist_in == 3) * 4 +
        DeviceArray<int>(clist_in == 4) * 3;
    DeviceArray<REAL> offsetlist_ex = (REAL)1.0 - offset;
    ilist_ex.af_save("ilist_ex", fname, true, host_boundary_size);
    jlist_ex.af_save("jlist_ex", fname, true, host_boundary_size);
    clist_ex.af_save("clist_ex", fname, true, host_boundary_size);
    offsetlist_ex.af_save("offsetlist_ex", fname, true, host_boundary_size);
    s.af_save("slist_ex", fname, true, host_boundary_size);
    nx.af_save("nxlist_ex", fname, true, host_boundary_size);
    ny.af_save("nylist_ex", fname, true, host_boundary_size);
}

__global__ void kernel_getBoundaryCoordinates
(
REAL* _x,
REAL* _y,
int* __restrict__ _idxin,
REAL* __restrict__ _offset,
const int m,
const int n,
const int* __restrict__ _len
)
{
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {
        // calculate coordinates of probing positions out from boundary lattice index information
        int mn, cin, idxin_rho;
        int idxin = _idxin[ik];
        mn = m*n;
        cin = idxin / mn;
        idxin_rho = idxin % mn;
        int jin = idxin_rho / m;
        int iin = idxin_rho % m;
        REAL x = (REAL)iin + ((REAL)(cin == 2) - (REAL)(cin == 1))*_offset[ik];
        REAL y = (REAL)jin + ((REAL)(cin == 4) - (REAL)(cin == 3))*_offset[ik];
        _x[ik] = x;
        _y[ik] = y;
    }
}

void ADBoundary::getBoundaryCoordinates(REAL* _x, REAL* _y){
    int len = kernel_launch_size; // size();
    kernel_getBoundaryCoordinates CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (_x, _y, idx.data_ptr(), offset.data_ptr(), m, n, boundary_size.data_ptr());
}

// Hard copy from "immersedBoundaryMethod.cu"
//******************************************************************************************************
// ********************************************************************
#include "PeskinDeltaFunction.h"
// ********************************************************************

__device__ void interpolate_velocity_by_PeskinDeltaFunction
(
REAL& lag_ux,
REAL& lag_uy,
const REAL& x,
const REAL& y,
const REAL* __restrict__ eul_ux,
const REAL* __restrict__ eul_uy,
const int& m,
const int& n,
const bool& is_x_periodic,
const bool& is_y_periodic
)
{
    int _i = (int)floor(x);
    int i0 = _i - 1;
    int i1 = _i + 2;
    int _j = (int)floor(y);
    int j0 = _j - 1;
    int j1 = _j + 2;

    REAL delta_dx[4];
#pragma unroll
    for (int i = i0; i <= i1; ++i){
        REAL dx = abs((REAL)i - x);
        delta_dx[i - i0] = DELTA1D(dx);
    }

    REAL delta_dy[4];
#pragma unroll
    for (int j = j0; j <= j1; ++j){
        REAL dy = abs((REAL)j - y);
        delta_dy[j - j0] = DELTA1D(dy);
    }

    lag_ux = 0.0;
    lag_uy = 0.0;
#pragma unroll
    for (int j = j0; j <= j1; ++j)
    {
        REAL delta_dy_j = delta_dy[j - j0];
        int j_box;
        if ((j < 0 || j >= n) && (!is_y_periodic)) continue;
        else j_box = POSITIVE_MOD(j, n);
#pragma unroll
        for (int i = i0; i <= i1; ++i)
        {
            REAL delta_dx_i = delta_dx[i - i0];
            int i_box;
            if ((i < 0 || i >= m) && (!is_x_periodic)) continue;
            else i_box = POSITIVE_MOD(i, m);
            REAL delta_r = delta_dy_j * delta_dx_i;
            int idx_box = j_box*m + i_box;
            lag_ux += delta_r * eul_ux[idx_box];
            lag_uy += delta_r * eul_uy[idx_box];
        }
    }

}
//******************************************************************************************************


__global__
void kernel_calculate_boundary_shear_stress
(
REAL*__restrict__ _shear_stress,
const int* __restrict__ _idxin,
const REAL* __restrict__ _offset,
const REAL* __restrict__ _nx,
const REAL* __restrict__ _ny,
const REAL* __restrict__ _ux,
const REAL* __restrict__ _uy,
const int* __restrict__ _len,
const int m,
const int n,
const REAL dx_probe,
const REAL mu, // dynamic viscosity
const bool x_periodic,
const bool y_periodic
)
{
    int len = *_len;
    int ik = blockIdx.x * blockDim.x + threadIdx.x;
    if (ik < len)
    {
        // calculate coordinates of probing positions out from boundary lattice index information
        int mn, cin, idxin_rho;
        int idxin = _idxin[ik];
        REAL nx = _nx[ik];
        REAL ny = _ny[ik];
        mn = m*n;
        cin = idxin / mn;
        idxin_rho = idxin % mn;
        int jin = idxin_rho / m;
        int iin = idxin_rho % m;

        // velocity probe 0 position is on the membrane
        REAL x0 = (REAL)iin + ((REAL)(cin == 2) - (REAL)(cin == 1))*_offset[ik];
        REAL y0 = (REAL)jin + ((REAL)(cin == 4) - (REAL)(cin == 3))*_offset[ik];

        // velocity probe 1 position is dx_probe distant away from probe 1 along the normal vector
        REAL x1 = x0 + nx * dx_probe;
        REAL y1 = y0 + ny * dx_probe;

        // use peskin delta function from Immersed Boundary Method (IBM) to interpolate the velocity
        REAL ux0, uy0, ux1, uy1;
        interpolate_velocity_by_PeskinDeltaFunction(ux0, uy0, x0, y0, _ux, _uy, m, n, x_periodic, y_periodic);
        interpolate_velocity_by_PeskinDeltaFunction(ux1, uy1, x1, y1, _ux, _uy, m, n, x_periodic, y_periodic);

        // calculate the tangential velocity at each probe
        // tx = ny; ty = -nx;
        REAL ut0 = ux0*ny - uy0*nx;
        REAL ut1 = ux1*ny - uy1*nx;

        _shear_stress[ik] = abs((ut1 - ut0) / dx_probe) * mu;

    }

}

void ADBoundary::calculate_boundary_shear_stress
(
REAL* shear_stress,
const REAL* _ux,
const REAL* _uy,
const REAL dx_probe,
const REAL mu, // dynamics viscosity
const bool is_x_periodic,
const bool is_y_periodic,
const REAL dt
)
{
    int len = kernel_launch_size; // size();
    kernel_calculate_boundary_shear_stress
        CUDALAUNCH((len + BSIZE - 1) / BSIZE, BSIZE, 0, method.stream)
        (
        shear_stress,
        idx.data_ptr(),
        offset.data_ptr(),
        nx.data_ptr(),
        ny.data_ptr(),
        _ux,
        _uy,
        boundary_size.data_ptr(),
        m, n,
        dx_probe,
        mu / dt,
        is_x_periodic,
        is_y_periodic
        );
}