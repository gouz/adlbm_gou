#include "RBC_ATP_Release_multi_vesicle.cuh"

// cuda launch parameters
#define BSIZE 256
#define CUDALAUNCH(n) <<<((n) + BSIZE - 1) / BSIZE, BSIZE>>>

__global__ void kernel_set_boundary
(
REAL* __restrict__ _cin,
REAL* __restrict__ _cex,
short* __restrict__ _ctype_in,
short* __restrict__ _ctype_ex,
const REAL* __restrict__ _rho_in, // ATP concentration
const REAL* __restrict__ _shear_stress,
const REAL* __restrict__ _curvature_change,
const int* __restrict__ _boundary_size,
const int* __restrict__ _bid,
const REAL cc_critical, // critical curvature change value, 200 /(um*s)
const REAL cc_slope, // CFTR-ATP release amplification / cc value
const REAL ss_critical, // shear stress criteria, Px1 protein
const REAL k_sigma, // phenomenological coefficient, k_sigma
const REAL Ka, // ATP uptake rate, m*s^-1
const REAL EC_ATP_release_model, // 1 for linear model; 0 for sigmoidal model
const REAL s0, // maximum ATP production rate on EC surface, [nM/L]*m*s^-1
const REAL tau_m, // characteristic wall shear stress, Pa
const REAL Datp, // ATP diffusion rate in plasma
const REAL a_infty, // ATP concentration at inlet, nM/L
const REAL dx,
const REAL dt
)
{
    int len = *_boundary_size;
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        int bid = _bid[idx]; // boundary pair id, >= 0 for vesicle, -1 for vessel wall; _ex stores boundary condition values on lumen side
        if (bid < 0)
            bid = -((-bid) % 10000);
        REAL ss = _shear_stress[idx];

        if (bid == -1) // EC wall boundary condition
        {
            _ctype_ex[idx] = 1; 
            _cex[idx] = 0.0;

            _ctype_in[idx] = 0;
            REAL exp_factor = 0.0;
            if (EC_ATP_release_model)
            {
                exp_factor = ss / tau_m;
            }
            else
            {
                exp_factor = (1.0 - exp(-ss / tau_m))
                            *(1.0 - exp(-ss / tau_m))
                            *(1.0 - exp(-ss / tau_m));
            }
            REAL flux = -Ka * _rho_in[idx] + s0 * exp_factor;
            _cin[idx] = flux * dx / Datp;
        }

        if (bid == -2) // constant value boundary condition
        {
            _ctype_ex[idx] = 1;
            _cex[idx] = 0.0;

            _ctype_in[idx] = 1;
            _cin[idx] = a_infty;
        }

        if (bid == -3) // zero flux boundary condition
        {
            _ctype_ex[idx] = 1;
            _cex[idx] = 0.0;

            _ctype_in[idx] = 0;
            _cin[idx] = 0.0;
        }

        if (bid >= 0) // vesicle boundary condition
        {
            _ctype_ex[idx] = 1;
            _cex[idx] = 0.0;

            _ctype_in[idx] = 0;
            REAL curvature_change = _curvature_change[idx];

            REAL factor_cftr_upregulation =
                (abs(curvature_change) > cc_critical ? (abs(curvature_change) - cc_critical) * cc_slope : 0.0) + 1.0;
            factor_cftr_upregulation = factor_cftr_upregulation < 2.5 ? factor_cftr_upregulation : 2.5;

            REAL factor_shear_stress = ss > ss_critical ? 1.0 : 0.0;

            REAL flux = k_sigma * factor_cftr_upregulation * factor_shear_stress;

            _cin[idx] = flux * dx / Datp;
        }
    }
}

void RBC_ATP_Release_multi_vesicle::set_boundary
(
REAL* __restrict__ _cin,
REAL* __restrict__ _cex,
short* __restrict__ _ctype_in,
short* __restrict__ _ctype_ex,
const REAL* __restrict__ _rho_in, // ATP concentration
const REAL* __restrict__ _shear_stress,
const REAL* __restrict__ _curvature_change,
const int maximum_kernel_launch_size,
const int* __restrict__ _boundary_size,
const int* __restrict__ _bid,
const REAL cc_critical, // critical curvature change value, 200 /(um*s)
const REAL cc_slope, // CFTR-ATP release amplification / cc value
const REAL ss_critical, // shear stress criteria, Px1 protein
const REAL k_sigma, // phenomenological coefficient, k_sigma
const REAL Ka, // ATP uptake rate, m*s^-1
const REAL EC_ATP_release_model, // 1 for linear model; 0 for sigmoidal model
const REAL s0, // maximum ATP production rate on EC surface, [nM/L]*m*s^-1
const REAL tau_m, // characteristic wall shear stress, Pa
const REAL Datp, // ATP diffusion rate in plasma
const REAL a_infty, // ATP concentration at inlet, nM/L
const REAL dx,
const REAL dt
)
{
    const int block_dim = 256;
    int grid_size = (maximum_kernel_launch_size + block_dim - 1) / block_dim;
    kernel_set_boundary <<<grid_size, block_dim>>>
        (
        _cin,
        _cex,
        _ctype_in,
        _ctype_ex,
        _rho_in,
        _shear_stress,
        _curvature_change,
        _boundary_size,
        _bid,
        cc_critical, // critical curvature change value, 200 /(um*s)
        cc_slope, // CFTR-ATP release amplification / cc value
        ss_critical, // shear stress criteria, Px1 protein
        k_sigma, // phenomenological coefficient, k_sigma
        Ka, // ATP uptake rate, m*s^-1
        EC_ATP_release_model, // 1 for linear model; 0 for sigmoidal model
        s0, // maximum ATP production rate on EC surface, [nM/L]*m*s^-1
        tau_m, // characteristic wall shear stress, Pa
        Datp, // ATP diffusion rate in plasma
        a_infty, // ATP concentration at inlet, nM/L
        dx,
        dt
        );
}

__global__ void kernel_interpolate_curvature_change
(
REAL* __restrict__ _curvature_change,
const int* __restrict__ _boundary_size,
const int curve_size,
const REAL* __restrict__ _s,
const REAL* __restrict__ _ves_curvature_change,
const int* _bid,
const int nves
)
{
    int boundary_size = *_boundary_size;
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < boundary_size)
    {
        int bid = _bid[idx];
        if (bid >= 0 && bid < nves) // the bid th vesicle
        {
            REAL s = _s[idx];
            int idxL = (int)floor(s);
            REAL ds = s - (REAL)idxL;
            int idxR = (idxL + 1) % curve_size;
            idxL += bid*curve_size;
            idxR += bid*curve_size;

            /*
            // smooth the _ves_curvature_change data by a 5 point stencil
            REAL ccL = 0.0;
            REAL ccR = 0.0;
            const int stencil_size = 7;
            const REAL w = 1.0 / stencil_size;
            #pragma unroll
            for (int i = 0; i != stencil_size; ++i){
            ccL += (_ves_curvature_change[(idxL - (stencil_size / 2) + i + curve_size) % curve_size]);
            ccR += (_ves_curvature_change[(idxR - (stencil_size / 2) + i + curve_size) % curve_size]);
            }
            ccL *= w;
            ccR *= w;
            */

            // read raw data
            REAL ccL = _ves_curvature_change[idxL];
            REAL ccR = _ves_curvature_change[idxR];

            _curvature_change[idx] = ds*(ccR - ccL) + ccL;
        }
    }
}

void RBC_ATP_Release_multi_vesicle::interpolate_curvature_change
(
DeviceArray<REAL>& curvature_change,
DeviceArray<REAL>& s,
DeviceArray<int>& bid,
const DeviceArray<REAL>& ves_curvature_change,
const int maximum_kernel_launch_size,
const DeviceArray<int>& boundary_size,
const int nves
)
{
    const int block_dim = 256;
    int grid_size = (maximum_kernel_launch_size + block_dim - 1) / block_dim;
    int curve_size = ves_curvature_change.size() / nves; // equal to vesicles[0]->size()
    kernel_interpolate_curvature_change <<<grid_size, block_dim >>>
        (
        curvature_change.data_ptr(),
        boundary_size.data_ptr(),
        curve_size,
        s.data_ptr(),
        ves_curvature_change.data_ptr(),
        bid.data_ptr(),
        nves
        );
}

// n_history can be understood as a stencil of [-n_history+1 ... 0] is used in sav-gal filter
// it must be decided in compile time. Only n_history == 5 is implemented now
template<int n_history>
__global__ void kernel_calculate_curvature_change
(
REAL* __restrict__ _ves_curvature_change,
const REAL* __restrict__ _curvature,
const int vesicle_size,
const int i_history,
const REAL dt
)
{
    // sav-gal filger with stencil = [-4, ..., 0], 5 points linear filter weight factor
    const REAL w05[5] = { -0.2, -0.1, 0.0, 0.1, 0.2 };
    // sav-gal filger with stencil = [-9, ..., 0], 10 points linear filter weight factor
    const REAL w10[10] = {
        -9.0 / 165.0, -7.0 / 165.0, -5.0 / 165.0, -3.0 / 165.0, -1.0 / 165.0,
        +1.0 / 165.0, +3.0 / 165.0, +5.0 / 165.0, +7.0 / 165.0, +9.0 / 165.0 };

    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < vesicle_size)
    {
        // read curvature history data from last previous to current step
        REAL c[n_history];
#pragma unroll
        for (int i = 0; i != n_history; ++i)
        {
            int ii = (i_history + 1 + i) % n_history;
            c[i] = _curvature[idx + vesicle_size*ii];
        }

        // calculate curvature change
        REAL cc = 0.0;
#pragma unroll
        for (int i = 0; i != n_history; ++i)
        {
            if (n_history == 05) cc += (w05[i] * c[i]);
            if (n_history == 10) cc += (w10[i] * c[i]);
        }
        cc /= dt;
        _ves_curvature_change[idx] = cc;
    }
}

__global__ void kernel_calculate_curvature_change_finite_difference
(
REAL* __restrict__ _ves_curvature_change,
const REAL* __restrict__ _curvature,
const int vesicle_size,
const int i_history,
const int n_history,
const REAL dt
)
{

    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < vesicle_size)
    {
        // read curvature history data from last previous to current step
        REAL c0, c1;
        int i0 = (i_history + 1) % n_history;
        c0 = _curvature[idx + vesicle_size*i0];
        c1 = _curvature[idx + vesicle_size*i_history];
        _ves_curvature_change[idx] = abs(c1 - c0) / ((n_history - 1)*dt);
    }
}

void RBC_ATP_Release_multi_vesicle::calculate_curvature_change
(
DeviceArray<REAL>& ves_curvature_change,
const DeviceArray<REAL>& curvature,
const int vesicle_size,
const int n_history,
const int i_history,
const REAL dt
)
{
    if (n_history == 5)
    {
        kernel_calculate_curvature_change<5> CUDALAUNCH(vesicle_size)
            (
            ves_curvature_change.data_ptr(),
            curvature.data_ptr(),
            vesicle_size,
            i_history,
            dt
            );
    }
    else
    {
        if (n_history == 10)
        {
            kernel_calculate_curvature_change<10> CUDALAUNCH(vesicle_size)
                (
                ves_curvature_change.data_ptr(),
                curvature.data_ptr(),
                vesicle_size,
                i_history,
                dt
                );
        }
        else
        {
            kernel_calculate_curvature_change_finite_difference CUDALAUNCH(vesicle_size)
                (
                ves_curvature_change.data_ptr(),
                curvature.data_ptr(),
                vesicle_size,
                i_history,
                n_history,
                dt
                );
        }
    }
}

__global__ void kernel_calculate_shear_stress
(
REAL* __restrict__ _shear_stress_field,
const REAL* __restrict__ _ux,
const int* __restrict__ _label,
const REAL viscosity_in,
const REAL viscosity_out,
const REAL dt,
const int m,
const int n
)
{
    int i = blockDim.x*blockIdx.x + threadIdx.x;
    int j = blockDim.y*blockIdx.y + threadIdx.y;
    if (i < m & j < n)
    {
        int idx = i + j*m;
        REAL viscosity = _label[idx] == 1 ? viscosity_in : viscosity_out;
        REAL ux1, ux0, dx_inv;
        if (j>0 & j < (n - 1)){
            ux1 = _ux[i + m*(j + 1)];
            ux0 = _ux[i + m*(j - 1)];
            dx_inv = 0.5;
        }
        if (j == 0){
            ux1 = _ux[i + m*(j + 1)];
            ux0 = _ux[idx];
            dx_inv = 1.0;
        }
        if (j == n - 1){
            ux1 = _ux[idx];
            ux0 = _ux[i + m*(j - 1)];
            dx_inv = 1.0;
        }
        _shear_stress_field[idx] = (ux1 - ux0)  *  dx_inv / dt * viscosity;
    }
}

REAL RBC_ATP_Release_multi_vesicle::calculate_shear_stress
(
const REAL* __restrict__ _ux,
const int* __restrict__ _label,
const REAL viscosity_in,
const REAL viscosity_out,
const REAL dt,
const int m,
const int n
)
{
    const int BSIZEM = 32;
    const int BSIZEN = 16;
    const dim3 block(BSIZEM, BSIZEN);
    dim3 grid((m + BSIZEM - 1) / BSIZEM, (n + BSIZEN - 1) / BSIZEN);
    DeviceArray<REAL> shear_stress_field(0.0, m, n);
    kernel_calculate_shear_stress <<< grid, block>>>
        (
        shear_stress_field.data_ptr(),
        _ux,
        _label,
        viscosity_in,
        viscosity_out,
        dt,
        m,
        n
        );
    return DA::sum(shear_stress_field) / (m*n);
}
