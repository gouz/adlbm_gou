#ifndef PeskinDeltaFunction_h
#define PeskinDeltaFunction_h
// a peskin deltafunction macro defined for immersed boundary method
// used in immersedBoundaryMethod.cu
// and in NSsolver.cu

#define DELTA_FUNCTION_3POINTS
#ifdef DELTA_FUNCTION_3POINTS

#ifndef PI
#define PI 3.14159265358979324
#endif

#define DELTA1D_p1(r) \
( \
17.0 / 48.0 + PI*sqrt(3.0) / 108.0 + (r - r*r)*0.25 + \
(1.0 - 2.0*r)*0.0625*sqrt(12.0*(r - r*r) + 1.0) - \
sqrt(3.0) / 12.0*asin(sqrt(3.0) *(r - 0.5)) \
)

#define DELTA1D_p2(r) \
( \
55.0 / 48.0 - PI*sqrt(3.0) / 108.0 + (3.0*r*r - 13.0*r) / 12.0 + \
(2.0*r - 3.0) / 48.0*sqrt(12.0*(3.0*r - r*r) - 23.0) + \
sqrt(3.0) / 36.0*asin(sqrt(3.0)*(r - 1.5)) \
)

// Peskin delta function
#define DELTA1D(r) ((r) < 2.0 ? ((r) < 1.0 ? DELTA1D_p1(r) : DELTA1D_p2(r)) : 0.0)

#endif //DELTA_FUNCTION_3POINTS

#endif