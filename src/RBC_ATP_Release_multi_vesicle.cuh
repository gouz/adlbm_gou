// Calculate ATP released by multiple RBCs
// Based on RBC_ATP_Release.cuh
// Modified by Zhe Gou at 20191103

// Try to fix 'updatelabel' function for multi-vesicle problem
// Modified by Zhe Gou at 20191120

// Add endothelial cell model
// Modified by Zhe Gou at 20191212

// Add adhesion force due to Lennard Jones potential
// Modified by Zhe Gou at 20201017

#ifndef RBC_ATP_Release_multi_vesicle_cuh
#define RBC_ATP_Release_multi_vesicle_cuh
#include <iostream>
#include <fstream>
#include <cmath>
#include "LBMheader.cuh"

class RBC_ATP_Release_multi_vesicle{
private:

    void set_boundary // _cex stores boundary condition values on lumen side
        (
        REAL* __restrict__ _cin,
        REAL* __restrict__ _cex,
        short* __restrict__ _ctype_in,
        short* __restrict__ _ctype_ex,
        const REAL* __restrict__ _rho_in, // ATP concentration
        const REAL* __restrict__ _shear_stress,
        const REAL* __restrict__ _curvature_change,
        const int maximum_kernel_launch_size,
        const int* __restrict__ _boundary_size,
        const int* __restrict__ _bid,
        const REAL cc_critical, // critical curvature change value, 200 /(um*s)
        const REAL cc_slope, // CFTR-ATP release amplification / cc value
        const REAL ss_critical, // shear stress criteria, Px1 protein
        const REAL k_sigma, // phenomenological coefficient, k_sigma
        const REAL Ka, // ATP uptake rate, m*s^-1
        const REAL EC_ATP_release_model, // 1 for linear model; 0 for sigmoidal model
        const REAL s0, // maximum ATP production rate on EC surface, [nM/L]*m*s^-1
        const REAL tau_m, // characteristic wall shear stress, Pa
        const REAL Datp, // ATP diffusion rate in plasma
        const REAL a_infty, // ATP concentration at inlet, nM/L
        const REAL dx,
        const REAL dt
        );

    void interpolate_curvature_change
        (
        DeviceArray<REAL>& curvature_change,
        DeviceArray<REAL>& s,
        DeviceArray<int>& bid,
        const DeviceArray<REAL>& ves_curvature_change,
        const int maximum_kernel_launch_size,
        const DeviceArray<int>& boundary_size,
        const int nves // total vesicle number
        );

    // calculate_curvature_change
    // a sav-gal filter along time is used with a stencil of n_history size.
    // i_history indicates that i_history th row in "curvature" stores the current
    // curvature data. previous steps are at its relative position (in a loop manner)
    void calculate_curvature_change
        (
        DeviceArray<REAL>& ves_curvature_change,
        const DeviceArray<REAL>& curvature,
        const int vesicle_size,
        const int n_history,
        const int i_history,
        const REAL dt
        );
    REAL calculate_shear_stress
        (
        const REAL* __restrict__ _ux,
        const int* __restrict__ _label,
        const REAL viscosity_in,
        const REAL viscosity_out,
        const REAL dt,
        const int m,
        const int n
        );

public:
    inline RBC_ATP_Release_multi_vesicle(){};
    inline ~RBC_ATP_Release_multi_vesicle(){};
public:
    // this is a combination of shear flow and poiseuille flow condition
    inline void red_blood_cell_release_atp_in_straight_channel(){

        ConfigFile cfg("red_blood_cell_release_atp_in_straight_channel.txt");

        DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
        if (flag_run == 0)
        {
            printf("flag_run==0, simulation red_blood_cell_release_atp_in_straight_channel not issued\n");
            return;
        }

        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_vesicle_on, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_collision_MRT, 0);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_using_repulsive_force, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_using_adhesion_force, 0); // added by Zhe Gou at 20201017
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_vesicle_adhesion, 0); // added by Zhe Gou at 20220603
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_boundary_adhesion, 0); // added by Zhe Gou at 20220603
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_vesicle_relax, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_ATP_on, 0);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_ATP_reset, 0);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_viscosity_labeling, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_x_periodic, 1);
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_y_periodic, 0);

        // is_shear_flow==1: this is a shear flow with simple geometry(a long straight periodic channel)
        // is_shear_flow==0: this is a pressure driven flow (typically a poiseuille flow)
        DEFINE_FROM_CONFIGFILE(int, cfg, "module_switch", is_shear_flow, 0);

        // "_phys" refers to physical unit, "_simu" refers to LB unit
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", viscosity_cyto_phys, 0.012); // red blood cell cytoplasmic viscosity, 12 mPa*s
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", viscosity_contrast, 1.0);
        REAL viscosity_solvent_phys = viscosity_cyto_phys / viscosity_contrast;
        printf("viscosity_solvent_phys = %f Pa*s\n", viscosity_solvent_phys);

        // physical density (should be small to guarantee Stokes flow condition)
        // added by Zhe Gou on 20200719
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", density_phys, 0.001);

        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", umax_phys, 800.0e-6); // maximum velocity in physical units, m/s
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", pressure_drop_inclination_phys, 0.0); // the angle between pressure drop direction and x axis, unit: degree
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Datp_phys, (REAL)2.36e-10);

        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", cc_critical_phys, (REAL)2.0e8); // critical curvature change value, 200 /(um*s)
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", cc_slope_phys, (REAL)1.5e-9); // CFTR-ATP release amplification / cc value
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", ss_critical_phys, (REAL)0.03); // Px1 shear stress criteria, 0.01 Pa
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", k_sigma_phys, (REAL)7e-3); // phenomenological coefficient for Px1, k_sigma, [nM/L] * m*s^-1
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", Ka_phys, 1.68e-5); // ATP uptake rate (m*s^-1)
        DEFINE_FROM_CONFIGFILE(int, cfg, "physical", EC_ATP_release_model, 0); // 1 for linear model; 0 for sigmoidal model
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", s0_phys, 1e-3); // maximum ATP production rate on EC surface, [nM/L]*m*s^-1
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", tau_m_phys, 1); // characteristic wall shear stress (Pa)
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", a_infty_phys, 100); // ATP concentration at inlet, nM/L
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "physical", dx_phys, 0.2e-6);

        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", m, 1001); // length
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n, 201); // width
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", c_l, 20.0); // characteristic length
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", adhesion_relaxation_nmax, 50000);
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", adhesion_relaxation_frames, 1000); // total export frames for field information
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tend, 2000000);
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tbeg_export, 0); // export starts from this time step
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames, 322); // total export frames for field information
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_boundary, frames / 20); // total export frames for boundary information
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_iteration_step, 10); // iteration step for the correction of fluid velocity and pressure gradient
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_prestep, 100000);
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", flow_prestep_tolerance, 1e-9);
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", use_initial_f_file, 0);
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "simulation", initial_f_filename, std::string("initial_flow_field_afdat"));
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", use_initial_ad_file, 0);
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "simulation", initial_ad_filename, std::string("initial_ad_file_afdat"));

        // in LB units, this is a fake value in simulation, which makes Re = 0.1 approximately
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", viscosity_simu, 0.166666666666666667);
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", umax_simu, 0.02);
        DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", n_history, 1000);
        
        // calculate shear rate
        REAL shear_rate_phys = umax_phys / c_l / dx_phys; // physical unit
        REAL shear_rate_simu = umax_simu / c_l; // LB unit
        if (!is_shear_flow)
        {
            shear_rate_phys = 4 * shear_rate_phys;
            shear_rate_simu = 4 * shear_rate_simu;
        }
        
        // calculate dt in physical unit
        // added 0 condition
        // modified by Zhe Gou on 20200712
        REAL dt_phys = 0;
        if (umax_phys == 0)
        {
            dt_phys = viscosity_simu * density_phys * dx_phys * dx_phys 
                    / viscosity_solvent_phys;
        }
        else
            dt_phys = umax_simu / umax_phys * dx_phys;
        
        // calculate dissipation rate in LB unit
        REAL D_simu = Datp_phys / dx_phys / dx_phys * dt_phys;
        printf("dt_phys = %e\n", dt_phys);
        printf("Datp_LB = %.17f\n", D_simu);

        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", inclination_phys, 0.0); // vesicle initial inclination angle
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", R0ves_simu, 13.0); // characteristic radius
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", tau, 0.693); // reduced area
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kb_simu, 1.0); // bending modulus
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kl_simu, 0.3); // local stretching penalty factor
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", ks_simu, 200.0); // area penalty factor
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", l0, 0.667); // grid length ratio between vesicle and flow
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", De, 1e-5); // adhesion coefficient, added by Zhe Gou at 20201017
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", d0, 4.0); // critical distance of adhesion force, added by Zhe Gou at 20201017
        DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", n_relax, 1000000); // vesicle relaxation steps
        DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", frames_relax, 200); // total export frames for random initialization
        DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", damp_relax, 0.5); // vesicle relaxation damping factor
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_shape_filename, std::string("initial_vesicle_shape.txt"));
        DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", use_initial_shape_file, 0);

        // this list specifies the initial position, inclination, tau and kb etc. of vesicles.
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", vesicle_initialization_filename,
            std::string("vesicle_initialization.txt"));

        // added by Zhe Gou on 20210208
        // vesicle number
        DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", nves, 0);
        // random initialization of vesicles
        DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", use_random_initialization, 0);

        // added by Zhe Gou on 20211201
        // various vesicle properties
        DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", use_various_vesicle_property, 0);
        
        // vessel geometry file
        //DEFINE_FROM_CONFIGFILE(int, cfg, "geometry", use_vessel_geometry_file, 0);
        //DEFINE_FROM_CONFIGFILE(std::string, cfg, "geometry", vessel_geometry_filename, std::string("vessel_geometry.bmp"));

        // vessel boundary file
        DEFINE_FROM_CONFIGFILE(int, cfg, "geometry", use_vessel_boundary_file, 0);
        DEFINE_FROM_CONFIGFILE(std::string, cfg, "geometry", vessel_boundary_filename, std::string("vessel_boundary.txt"));

        // Peclet number:
        REAL Pe = shear_rate_simu * c_l*c_l / D_simu; 
        if (is_vesicle_on)
            Pe = shear_rate_simu * R0ves_simu*R0ves_simu / D_simu; 
        REAL Pe_simu = umax_simu / D_simu;
        printf("Pe = shear_rate_simu * R0_simu^2 / D_simu = %f\n", Pe);
        printf("Pe_simu = umax_simu / D_simu = %f\n", Pe_simu);
        printf("Pe_simu should be at the order of ~10, at least less than 100!\n");

        std::ofstream shear_stress_file;
        shear_stress_file.open("shear_stress.dat", std::ios::trunc);
        shear_stress_file.close();

        // Reynolds number:
        REAL Re_simu = shear_rate_simu * c_l*c_l / viscosity_simu;
        if (is_vesicle_on)
            Re_simu = shear_rate_simu * R0ves_simu*R0ves_simu / viscosity_simu;
        printf("An artificial Reynolds number is set to approach the stokes flow:\n");
        printf("Re_simu = shear_rate_simu * R0_simu^2 / viscosity_simu = %f\n", Re_simu);
        
        // capillary number: 
        REAL kappa_simu = (REAL)0.25 * kb_simu * l0;
        REAL Ca = viscosity_simu * shear_rate_simu * c_l*c_l*c_l / kappa_simu;
        if (is_vesicle_on)
            Ca = viscosity_simu * shear_rate_simu * R0ves_simu*R0ves_simu*R0ves_simu / kappa_simu;
        printf("Ca = viscosity_simu * R0_simu^3 * shear_rate_simu / kappa_simu = %.17f\n", Ca);

        // dimensionless adhesion energy:
        REAL epsilon = 1.6862 * d0 * De * R0ves_simu * R0ves_simu / kappa_simu;
        printf("adhesion energy = %.17f\n", epsilon);

        // channel confinement
        if (is_vesicle_on)
        {
            REAL confinement = 2.0 * R0ves_simu / c_l;
            printf("channel confinement = %f\n", confinement);        
        }      

        // a flag to label internal zone of vesicle as 1
        const bool do_viscosity_labeling = is_viscosity_labeling;

        // build vessel boundary (including walls, inlet, and outlet)
        DataFile vessel_boundary(vessel_boundary_filename.c_str());
        int n_bound = vessel_boundary.get_n() / 4;
        if (!use_vessel_boundary_file) n_bound = 0;
        printf("boundary curve number = %i\n", n_bound);
        std::vector< DeviceArray<REAL> > x_bound(n_bound);
        std::vector< DeviceArray<REAL> > y_bound(n_bound);
        std::vector< bool > is_circle_bound(n_bound);
        std::vector< int > bound_type(n_bound);
        int vessel_boundary_segment_number = 0;
        if (use_vessel_boundary_file)
        {
            for (int i = 0; i != n_bound; ++i)
            {
                DeviceArray<REAL> x_bound_original, y_bound_original;
                x_bound_original = DeviceArray<REAL>
                    (
                    &vessel_boundary.getData()[i * 4 + 2][0],
                    vessel_boundary.getData()[i * 4 + 2].size()
                    );
                y_bound_original = DeviceArray<REAL>
                    (
                    &vessel_boundary.getData()[i * 4 + 3][0],
                    vessel_boundary.getData()[i * 4 + 3].size()
                    );

                // 0 refers to non-circular boundary, 1 refers to circular boundary
                bool is_circle_bound_this;
                is_circle_bound_this = std::abs(vessel_boundary.getData()[i * 4][0]) > 1e-5;
                is_circle_bound[i] = is_circle_bound_this;
                printf("is_circle_bound[%d] = %d\n", i, (int)is_circle_bound[i]);

                bound_type[i] = (int)vessel_boundary.getData()[i*4 + 1][0];
                printf("bound_type[%d] = %i\n", i, bound_type[i]);

                LBMGPUvoxelizer::refine_curve
                    (
                    x_bound[i],
                    y_bound[i],
                    x_bound_original.data_ptr(),
                    y_bound_original.data_ptr(),
                    x_bound_original.size(),
                    1.0,
                    is_circle_bound_this
                    );

                vessel_boundary_segment_number += x_bound[i].size();
            }   
        }

        // label_flow has its value equal to -1 in solid lattices, 0 in flow lattice, 1 in vesicle with viscosity contrast
        DeviceArray< int > label_flow;
        DeviceArray<REAL> X, Y;
        if (use_vessel_boundary_file)
        {
            // build a voxelizer to construct label and boundary curves
            LBMGPUvoxelizer* voxelizer = new LBMGPUvoxelizer(
                m, n, is_x_periodic, is_y_periodic, 
                vessel_boundary_segment_number*2, n_bound);
            
            for (int i = 0; i != n_bound; ++i)
            {
                voxelizer->setBoundaryCurve
                    (
                    x_bound[i].data_ptr(),
                    y_bound[i].data_ptr(),
                    x_bound[i].size(),
                    is_circle_bound[i],
                    0
                    );
            }
            voxelizer->setBoundaryCurveFinished(
                nullptr, nullptr, nullptr, nullptr, nullptr, 
                nullptr, nullptr, nullptr, nullptr, false);
            // auto tag_temp = DeviceArray<int>(voxelizer->tag_previous);
            // tag_temp.af_save("tag", "tag.afdat");
            label_flow = voxelizer->build_filled_tag_map();
            delete voxelizer;
            label_flow = DeviceArray<int>(label_flow == -1) - 1;
            // label_flow.af_save("label", "label_tag.afdat");
        }
        else
        {
            DA::meshgrid(X, Y, -0.5, -0.5, 1.0, 1.0, m, n);
            label_flow = DeviceArray<int>(0, m, n);
            if (is_x_periodic)
            {
                REAL W = (REAL)n - 2.0;
                auto ux = (1.0 - Y / W)*(Y / W);
                label_flow -= (ux < 0.0);
            }
            if (is_y_periodic)
            {
                REAL W = (REAL)m - 2.0;
                auto uy = (1.0 - X / W)*(X / W);
                label_flow -= (uy < 0.0);
            }
        }
        da_saveAF(label_flow);

        // calculate the body force in simulation
        REAL f_body_simu = 8.0 * viscosity_simu * umax_simu / c_l / c_l; // LB unit
        REAL f_body_phys = 8.0 * viscosity_solvent_phys * umax_phys / (c_l*dx_phys) / (c_l*dx_phys); // physical unit
        REAL fx_body_simu = std::cos(pressure_drop_inclination_phys / 180.0 * PI) * f_body_simu;
        REAL fy_body_simu = std::sin(pressure_drop_inclination_phys / 180.0 * PI) * f_body_simu;

        // body force field, initialized as an empty array on GPU, with data_ptr = nullptr
        DeviceArray<REAL> Fx_simu;
        DeviceArray<REAL> Fy_simu;
        if (!is_shear_flow)
        {
            // if the flow is not a shear flow, then it is considered as driven by a pressure drop (body force)
            Fx_simu = DeviceArray<REAL>(fx_body_simu, m, n);
            Fy_simu = DeviceArray<REAL>(fy_body_simu, m, n); 
            printf("pressure drop per unit = %f, inclination = %f degree\n", f_body_phys, 
                   pressure_drop_inclination_phys);

            // make sure that the body force only acts on fluid domain
            Fx_simu = Fx_simu*DeviceArray<REAL>(label_flow + 1);
            Fy_simu = Fy_simu*DeviceArray<REAL>(label_flow + 1);
        }

        // create flow solver
        std::vector< REAL > viscosity_list({ viscosity_simu, viscosity_simu*viscosity_contrast });
        // added 0 condition
        // modified by Zhe Gou on 20200712
        REAL UMAX_SIMU = umax_simu;
        if (umax_simu == 0)
            UMAX_SIMU = 1;
        NSsolver flow(
            &viscosity_list[0], 2,
            m, n,
            is_x_periodic ? NS::PERIODIC : NS::BOUNCEBACK,
            is_y_periodic ? NS::PERIODIC : NS::BOUNCEBACK,
            Fx_simu.data_ptr(),
            Fy_simu.data_ptr(),
            label_flow.data_ptr(),
            UMAX_SIMU*2.0);
        flow.initializeByDensity();

        // make boundary from file
        // added by Zhe Gou on 20211223
        if (use_vessel_boundary_file)
        {
            // merge all boundary points
            DeviceArray<REAL> x_bound_full;
            DeviceArray<REAL> y_bound_full;
            for (int i = 0; i != n_bound; ++i)
            {
                x_bound_full = DA::join(x_bound_full, x_bound[i]);
                y_bound_full = DA::join(y_bound_full, y_bound[i]);
            }

            flow.ibm.make_boundary_from_array(x_bound_full, y_bound_full);
        }

        // save physical boundary
        // added by Zhe Gou on 20201019
        flow.ibm.xBoundary.af_save("xbound", "xbound.afdat");
        flow.ibm.yBoundary.af_save("ybound", "ybound.afdat");

        // save cellList_boundary.count for debugging
        // added by Zhe Gou on 20201019
        // flow.ibm.cellList_boundary.count.af_save("boundary_count", "boundary_count.afdat");

        // create vesicles by using vesicle template or initial shape file
        DataFile vesicle_initial_position(vesicle_initialization_filename.c_str());
        DataFile ves_initial_shape(initial_shape_filename.c_str());
        // int nves = 0; // total vesicle number
        int npoint = 0; // total point number of one vesicle
                        // maximum value if using different vesicle properties
        if (use_random_initialization == 1)
        {
            nves = nves;
        }
        else if (use_initial_shape_file == 0)
        {
            nves = vesicle_initial_position.get_n(); 
        }
        else
        {
            ves_initial_shape.transpose();
            nves = ves_initial_shape.get_n() / 2;
            npoint = ves_initial_shape.get_m();
        }
        if (!is_vesicle_on) nves = 0;
        printf("total vesicle number = %i\n", nves);

        std::vector< Vesicle* > vesicles(nves);
        if (use_random_initialization == 1)
        {
            // random initialization of vesicles
            // added by Zhe Gou on 20210208
            int ives = 0;
            int dist = (int)ceil(R0ves_simu*0.5);
            std::vector< REAL > xpos(nves);
            std::vector< REAL > ypos(nves);
            std::vector< int > hlabel(m*n);
            cudaMemcpy(&hlabel[0], label_flow.data_ptr(), sizeof(int)*m*n, cudaMemcpyDeviceToHost);
            while (ives != nves)
            {
                int xc = rand() % m;
                if ( (xc < dist) && (!is_x_periodic) )
                    xc = dist;
                else if ( (m - xc < dist) && (!is_x_periodic) )
                    xc = m - dist;
                int yc = rand() % n;
                if ( (yc < dist) && (!is_y_periodic) )
                    yc = dist;
                else if ( (n - yc < dist) && (!is_y_periodic) )
                    yc = n - dist;
                int ic_0 = xc + yc*m;
                int ic_1 = (xc + dist) % m + yc*m;
                int ic_2 = (xc - dist) % m + yc*m;
                int ic_3 = xc + ((yc + dist) % n)*m;
                int ic_4 = xc + ((yc - dist) % n)*m;
                int ic_5 = (xc + dist) % m + (yc + dist)*m;
                int ic_6 = (xc - dist) % m + (yc + dist)*m;
                int ic_7 = (xc - dist) % m + (yc - dist)*m;
                int ic_8 = (xc + dist) % m + (yc - dist)*m;
                int label_0 = hlabel[ic_0];
                int label_1 = hlabel[ic_1];
                int label_2 = hlabel[ic_2];
                int label_3 = hlabel[ic_3];
                int label_4 = hlabel[ic_4];
                int label_5 = hlabel[ic_5];
                int label_6 = hlabel[ic_6];
                int label_7 = hlabel[ic_7];
                int label_8 = hlabel[ic_8];
                if (label_0 + label_1 + label_2 + label_3 + label_4 
                + label_5 + label_6 + label_7 + label_8 == 0)
                {
                    xpos[ives] = (REAL)xc;
                    ypos[ives] = (REAL)yc;
                    
                    int flag_ves = 0;
                    for (int jves = 0; jves != ives; ++ jves)
                    {
                        REAL dx = abs(xpos[ives] - xpos[jves]);
                        if (is_x_periodic && (m - dx < dx) )
                            dx = m - dx;
                        REAL dy = abs(ypos[ives] - ypos[jves]);
                        if (is_y_periodic && (n - dy < dy) )
                            dy = n - dy;
                        if (sqrt(dx*dx + dy*dy) < (REAL)dist*2.0)
                        {
                            flag_ves++;
                        }
                    }

                    if (flag_ves == 0)
                    {
                        printf("Create No.%i: Position = (%.2f,%.2f), \n", 
                            ives, xpos[ives], ypos[ives]);
                        ives++;
                    }
                }
            }

            for (int ives = 0; ives != nves; ++ives)
            {
                // create vesicle
                vesicles[ives] = new Vesicle
                    (
                    xpos[ives], 
                    ypos[ives], 
                    0.0, 
                    R0ves_simu, tau, kb_simu, kl_simu, ks_simu, l0, 
                    nullptr, false, 0.15);

                // immerse the vesicle into flow
                flow.immerseAnObject(vesicles[ives]);
                if (ives == 0) 
                    printf("Add vesicle(s) into flow:\n");
                printf("No.%i: Position = (%.2f,%.2f), \n", 
                    ives, xpos[ives], ypos[ives]);
                printf("       N_point = %d\n", vesicles[ives]->size());
            }
            npoint = vesicles[0]->size();
        }
        else if (use_initial_shape_file == 0)
        {
            // amplification factor for vesicle shape
            REAL amp_ves = 0.0;
            if (is_vesicle_relax)
                amp_ves = 1.0;
            else
                amp_ves = 0.25;

            if (!use_various_vesicle_property)
            {
                // create vesicle template and relaxation
                Vesicle ves_template(0.0, 0.0, 0.0, R0ves_simu, tau, 
                    kb_simu, kl_simu, ks_simu, l0, nullptr, false, amp_ves);
                if (is_vesicle_relax)
                    ves_template.relax(n_relax, damp_relax);
                ves_template.exportShape(initial_shape_filename.c_str());
                npoint = ves_template.size();
            }

            for (int ives = 0; ives != nves; ++ives)
            {
                // load values from vesicle initialization file
                // different vesicle properties
                // modified by Zhe Gou on 20211201
                REAL xpos = (m - 1) / 2;
                REAL ypos = (n - 1) / 2;
                REAL inclination = inclination_phys;
                REAL R0_this = R0ves_simu;
                REAL tau_this = tau;
                REAL kb_this = kb_simu;
                REAL kl_this = kl_simu;
                REAL ks_this = ks_simu;
                auto& ves_ini_data_this = vesicle_initial_position.getData()[ives];
                int len_ves_ini_data_this = ves_ini_data_this.size(); // total initial parameter number
                if (len_ves_ini_data_this > 0) xpos = ves_ini_data_this[0];
                if (len_ves_ini_data_this > 1) ypos = ves_ini_data_this[1];
                if (len_ves_ini_data_this > 2) inclination = ves_ini_data_this[2];
                if (len_ves_ini_data_this > 3) R0_this = ves_ini_data_this[3];
                if (len_ves_ini_data_this > 4) tau_this = ves_ini_data_this[4];
                if (len_ves_ini_data_this > 5) kb_this = ves_ini_data_this[5];
                if (len_ves_ini_data_this > 6) kl_this = ves_ini_data_this[6];
                if (len_ves_ini_data_this > 7) ks_this = ves_ini_data_this[7];
                
                if (!use_various_vesicle_property)
                {
                    vesicles[ives] = new Vesicle
                    (
                        xpos, // x position
                        ypos, // y position
                        inclination, // inclination
                        R0ves_simu, tau, kb_simu, kl_simu, ks_simu, l0, 
                        initial_shape_filename.c_str(), true);
                }
                else
                {
                    if (is_vesicle_relax)
                    {
                        amp_ves = 1.0;
                    }
                    else
                    {
                        amp_ves = R0ves_simu * 0.25 / R0_this;
                    }

                    vesicles[ives] = new Vesicle
                    (
                        xpos, // x position
                        ypos, // y position
                        inclination, // inclination
                        R0_this, // radius
                        tau_this, // reduced area
                        kb_this, // bending modulus
                        kl_this, // stretch modulus
                        ks_this, // area modulus
                        l0, nullptr, false, amp_ves);

                    if (is_vesicle_relax)
                    {
                        vesicles[ives]->relax(n_relax, damp_relax);
                    }
                    
                    if (npoint < vesicles[ives]->size())
                        npoint = vesicles[ives]->size();
                }

                // immerse the vesicle into flow
                flow.immerseAnObject(vesicles[ives]);
                if (ives == 0) 
                    printf("Add vesicle(s) into flow:\n");
                printf("No.%i: Position = (%.2f,%.2f), Inclination = %.2f, \n", 
                    ives, xpos, ypos, inclination);
                printf("       N_point = %d\n", vesicles[ives]->size());
            }
        }
        else
        {
            // load values from vesicle initial shape file
            for (int ives = 0; ives != nves; ++ives)
            {
                // load values from vesicle initialization file
                // different vesicle properties
                // modified by Zhe Gou on 20211201
                REAL R0_this = R0ves_simu;
                REAL tau_this = tau;
                REAL kb_this = kb_simu;
                REAL kl_this = kl_simu;
                REAL ks_this = ks_simu;
                if (use_various_vesicle_property)
                {
                    auto& ves_ini_data_this = vesicle_initial_position.getData()[ives];
                    int len_ves_ini_data_this = ves_ini_data_this.size(); // total initial parameter number
                    if (len_ves_ini_data_this > 3) R0_this = ves_ini_data_this[3];
                    if (len_ves_ini_data_this > 4) tau_this = ves_ini_data_this[4];
                    if (len_ves_ini_data_this > 5) kb_this = ves_ini_data_this[5];
                    if (len_ves_ini_data_this > 6) kl_this = ves_ini_data_this[6];
                    if (len_ves_ini_data_this > 7) ks_this = ves_ini_data_this[7];
                }

                REAL p0 = 2.0*PI*R0_this / std::sqrt(tau_this);
                int npoint_this = (int)std::ceil(p0 / l0);

                vesicles[ives] = new Vesicle
                    (
                    &ves_initial_shape.getData()[ives*2][0],
                    &ves_initial_shape.getData()[ives*2 + 1][0],
                    npoint_this, 
                    R0_this, // radius
                    tau_this, // reduced area
                    kb_this, // bending modulus
                    kl_this, // stretch modulus
                    ks_this, // area modulus
                    l0, false);

                // immerse the vesicle into flow
                flow.immerseAnObject(vesicles[ives]);
                if (ives == 0) 
                    printf("Add vesicle(s) into flow:\n");
                printf("No.%i, \n", ives);
                printf("       N_point = %d\n", vesicles[ives]->size());
            }
        }

        // call immerseObjectFinished() when you finished immersing all vesicles into the flow
        if (nves) 
            flow.immerseObjectFinished();

        // calculate kb of relaxation
        // added by Mehdi Abbasi on 20210210
        std::vector< REAL > kb_simu_relax(nves);
        REAL capillary_number_relax = 0.1; // fixed at 0.1
        REAL kappa_simu_relax = umax_simu / c_l * viscosity_simu * R0ves_simu*R0ves_simu*R0ves_simu / capillary_number_relax; 
        for (int ives = 0; ives != nves; ++ives)
        {
            // load values from vesicle initialization file
            // different vesicle properties
            // modified by Zhe Gou on 20211201
            REAL kb_this = kb_simu;
            if (use_various_vesicle_property)
            {
                auto& ves_ini_data_this = vesicle_initial_position.getData()[ives];
                int len_ves_ini_data_this = ves_ini_data_this.size(); // total initial parameter number
                if (len_ves_ini_data_this > 5) kb_this = ves_ini_data_this[5];
            }
            kb_simu_relax[ives] = kb_this * kappa_simu_relax / kappa_simu;
        }

        // vesicle relaxation
        // added by Zhe Gou on 20210208
        if ((use_initial_shape_file == 0) && (is_vesicle_relax == 0))
        {
            flow.body_force_x = nullptr;
            flow.body_force_y = nullptr;
                
            printf("vesicle relaxation:\n");
            for (int i = 0; i != n_relax; ++i)
            {	
                // calculate vesicle force based on membrane force
                for (int ives = 0; ives != nves; ++ives)
                {
                    // the elastic modulus are modified
                    vesicles[ives]->calculateForce(
                        kb_simu_relax[ives],
                        kl_simu*damp_relax,
                        ks_simu*damp_relax);
                }
                for (int ives = 0; ives != nves; ++ives)
                    vesicles[ives]->sync_operation();

            	// add repulsive force
                flow.ibm.add_short_range_repulsive_force();
                
                if (LBM::getStepStampFlag(i, frames_relax, n_relax))
                {
                    printf("step = %i, Nstep = %i\n", i, n_relax);
                
                    // save vesicle shape
                    printf("vesicle shapes snapshot\n");
                    flow.ibm.export_all_objects_shapes_af("sx_relax.afdat", "sy_relax.afdat", i != 0);
                }

                flow.advanceFlowField();
                flow.advanceImmersedObjects();
                // added for relax adhesion in quiescent flow
                if (i % 10 == 0)
                {
                    flow.initializeByDensity();
                }
            } 
            cudaDeviceSynchronize();
            printf("vesicles relaxation complete.\n");
        }
        printf("%i vesicles' initialization complete.\n", nves);

        // calculate De of adhesion relaxation
        // added by Zhe Gou on 20211106
        REAL De_relax = epsilon / (1.6862 * d0 * R0ves_simu * R0ves_simu / kappa_simu_relax);

        // adhesion relaxation
        // added by Zhe Gou on 20201204
        DeviceArray<REAL> curvature_export((REAL)0.0, npoint*nves);
        if (adhesion_relaxation_nmax)
        {
            flow.body_force_x = nullptr;
            flow.body_force_y = nullptr;
                
            for (int i = 0; i != adhesion_relaxation_nmax; ++i)
            {	
                // calculate vesicle force based on membrane force
                for (int ives = 0; ives != nves; ++ives)
                {
                    // calculate vesicle force
                    vesicles[ives]->calculateForce(
                        kb_simu_relax[ives],
                        kl_simu,
                        ks_simu);

                    // calculate curvature, store the data at i%n_history th row
                    vesicles[ives]->calculate_curvature(
                        curvature_export.data_ptr() + ives*vesicles[ives]->size(),
                        vesicles[ives]->get_l0(), dx_phys);
                }
                for (int ives = 0; ives != nves; ++ives)
                    vesicles[ives]->sync_operation();

            	if (is_using_adhesion_force)
                    flow.ibm.add_adhesion_force(
                        curvature_export, De_relax, d0, 
                        vesicles[0]->get_l0(), dx_phys,
                        is_vesicle_adhesion, is_boundary_adhesion);
                
                if (LBM::getStepStampFlag(i, adhesion_relaxation_frames, adhesion_relaxation_nmax))
                {
                    printf("step = %i, Nstep = %i\n", i, adhesion_relaxation_nmax);
                
                    // save vesicle shape
                    printf("vesicle shapes snapshot\n");
                    flow.ibm.export_all_objects_shapes_af("sx_adh_relax.afdat", "sy_adh_relax.afdat", i != 0);
                }

                flow.advanceFlowField();
                flow.advanceImmersedObjects();
                // added for relax adhesion in quiescent flow
                if (i % 10 == 0)
                {
                    flow.initializeByDensity();
                }
            } 
            cudaDeviceSynchronize();
            printf("vesicles adhesion relaxation complete.\n");
        }

        // set the force back to F_simu after relaxation
        flow.body_force_x = Fx_simu.data_ptr();
        flow.body_force_y = Fy_simu.data_ptr();
        
        // initialize flow field or read initial flow field from file
        flow.initializeByDensity();
        flow.config.IMPOSING_VELOCITY = (is_shear_flow > 0);
            
        // initialize flow field
        if (is_shear_flow)
        {
            // impose velocity boundary as a shear flow
            DA::meshgrid(X, Y, -(REAL)0.5, -(REAL)0.5, (REAL)1.0 / (m - 1), (REAL)1.0 / (n - 1), m, n);
            flow.ux = Y*umax_simu;
            flow.uy = 0.0;
        }
        else
        {
            //pre-step for initializing flow field
            printf("\n");
            flow.develop_steady_flow(flow_prestep, flow_prestep_tolerance);

            // correct fluid velocity and pressure gradient
            // added by Zhe Gou on 20210327
            REAL umax_simu_ini = 0.0;
            for (int i = 0; i != flow_iteration_step; ++i)
            {
                DA::meshgrid(X, Y, -(REAL)0.5, -(REAL)0.5, (REAL)1.0, (REAL)1.0, m, n);
                auto ux_simu = flow.ux*DeviceArray<REAL>(X < 0.0);
                auto uy_simu = flow.uy*DeviceArray<REAL>(X < 0.0);
                umax_simu_ini = std::sqrt(DA::max(ux_simu*ux_simu + uy_simu*uy_simu));
                printf("Initialized umax_simu = %f \n", umax_simu_ini);
                REAL factor_umax_simu = umax_simu / umax_simu_ini;
                if (abs(umax_simu_ini - umax_simu) < 1.0e-9)
                {
                    printf("Expected velocity profile reached \n");
                    break;
                }
                Fx_simu = Fx_simu*DeviceArray<REAL>(factor_umax_simu, m, n);
                Fy_simu = Fy_simu*DeviceArray<REAL>(factor_umax_simu, m, n);
                flow.body_force_x = Fx_simu.data_ptr();
                flow.body_force_y = Fy_simu.data_ptr();
                flow.develop_steady_flow(flow_prestep, flow_prestep_tolerance);
                //flow.ux = flow.ux*DeviceArray<REAL>(factor_umax_simu, m, n);
                //flow.uy = flow.uy*DeviceArray<REAL>(factor_umax_simu, m, n);
            }  
            //flow.af_save_flow_field(initial_f_filename.c_str());
        }

        // read initial flow field from file
        if (use_initial_f_file)
        {
            flow.initializeByFile(initial_f_filename.c_str());
            printf("Initialize flow using initial_f_file \n");
            flow.config.IMPOSING_VELOCITY = (is_shear_flow > 0); // added for shear flow, Zhe Gou at 20200518
        }
        
        flow.ux.af_save("ux_ini", "ux_ini.afdat"); // for validation

        // viscosity labeling
        if (do_viscosity_labeling)
        {
            for (int ives = 0; ives != nves; ++ives)
            {
                // update label
                vesicles[ives]->updateLabel(label_flow.data_ptr(), m, n, true, false);
            }
        }

        // estimate maximum boundary pair size
        REAL max_boundary_pair_size = std::ceil(npoint*1.5)*nves + vessel_boundary_segment_number*1.5;
        
        // advection diffusion solver and its boundary
        ADsolver diffusion
            (
            D_simu, // diffusion coefficient
            m, n, // mesh size
            max_boundary_pair_size, // maximum boundary pair size
            flow.ux.data_ptr(), // advecting velocity x
            flow.uy.data_ptr(), // advecting velocity y
            nullptr, nullptr, // source term and diffusion coefficient index map
            AD::PERIODIC, // x boundary type
            AD::PERIODIC // y boundary type
            );

        // trim the label in order to make it the same in NS and AD solver
        // only trim when there is viscosity contrast
        // modified by Zhe Gou at 20201018
        // commented by Zhe Gou on 20201211
        /*if (do_viscosity_labeling)
        {
            for (int ives = 0; ives != nves; ++ives)
            {
                diffusion.boundary.voxelizer.trim_label_by_curve
                    (
                    label_flow.data_ptr(),
                    0, 1,
                    vesicles[ives]->get_x().data_ptr(),
                    vesicles[ives]->get_y().data_ptr(),
                    vesicles[ives]->size()
                    );
            }
        }*/

        diffusion.setInitialConcentration(DeviceArray<REAL>(label_flow == 0)*a_infty_phys);
        // initialize AD field from file
        if (use_initial_ad_file)
        {
            diffusion.initializeByFile(initial_ad_filename.c_str());
            printf("Initialize AD field using initial_ad_file \n");
        }
        diffusion.rho.af_save("rhoini", "rhoini.afdat");

        // reset concentration inside vesicle to 0
        // added by Zhe Gou on 20211208
        if (is_ATP_reset)
        {
            DeviceArray< int > label_reset;
            label_reset = label_flow;
            if (!do_viscosity_labeling)
            {
                for (int ives = 0; ives != nves; ++ives)
                {
                    vesicles[ives]->updateLabel(label_reset.data_ptr(), m, n, true, false);
                }
            }
            diffusion.reset(label_reset);
        }

        // register vesicle boundary
        for (int ives = 0; ives != nves; ++ives)
        {
            diffusion.registerCurve(vesicles[ives]->get_x(), vesicles[ives]->get_y(), 
                true, ives);
        }

        // register vessel boundary
        for (int ibound = 0; ibound != n_bound; ++ibound)
        {
            diffusion.registerCurve
                (
                x_bound[ibound], 
                y_bound[ibound], 
                is_circle_bound[ibound], 
                bound_type[ibound]
                );
        }
        
        if (nves || n_bound)
            diffusion.registerCurveFinished();
        diffusion.computeConcentrationOnBoundary();        

        // shear_stress along the membrane
        DeviceArray<REAL> shear_stress((REAL)0.0, diffusion.boundary.BOUNDARY_LIST_CAPACITY);

        // curvature of current or previous steps on the membrane
        DeviceArray<REAL> curvature((REAL)0.0, npoint*nves, n_history);
        DeviceArray<REAL> ves_curvature_change((REAL)0.0, npoint*nves);
        for (int ives = 0; ives != nves; ++ives)
        {
            vesicles[ives]->calculate_curvature(
                curvature_export.data_ptr() + ives*vesicles[ives]->size(), 
                vesicles[ives]->get_l0(), dx_phys);
        }
        for (int i = 0; i != n_history; ++i)
        {
            for (int ives = 0; ives != nves; ++ives)
                vesicles[ives]->calculate_curvature(
                    curvature.data_ptr() + (ives + i*nves)*vesicles[ives]->size(), 
                    vesicles[ives]->get_l0(), dx_phys);
        }        
        calculate_curvature_change(ves_curvature_change, curvature, 
            npoint*nves, n_history, 0, dt_phys);
        
        // curvature change (time varient) on the membrane along boundary list
        DeviceArray<REAL> curvature_change((REAL)0.0, diffusion.boundary.BOUNDARY_LIST_CAPACITY);

        // start timer
        SimpleTimer timer;
        timer.tic();

        // start time loop
        for (int i = 0; i != Tend; ++i)
        {
            // *********************************************************************
            // vesicle dynamics
            if (is_vesicle_on)
            {
                // set label to zero
                if (do_viscosity_labeling) // modified by Zhe Gou at 20191122
                {
                    label_flow.reset(0); // set the label of fluid domain to 0
                }

                for (int ives = 0; ives != nves; ++ives)
                {
                    // deflation of vesicle
                    // added by Zhe Gou on 20211205
                    // vesicles[ives]->deflation(); 
                    // advantage: maintain area conservation at 1e-13
                    // disadvantage: slow down simulation

                    // calculate vesicle force
                    vesicles[ives]->calculateForce();

                    // calculate curvature, store the data at i%n_history th row
                    vesicles[ives]->calculate_curvature(
                        curvature.data_ptr() + (ives + (i%n_history)*nves)*vesicles[ives]->size(),
                        vesicles[ives]->get_l0(), dx_phys);

                    // calculate curvature, store for out put
                    vesicles[ives]->calculate_curvature(
                        curvature_export.data_ptr() + ives*vesicles[ives]->size(), 
                        vesicles[ives]->get_l0(), dx_phys);

                    // update vesicle label
                    if (do_viscosity_labeling)
                        vesicles[ives]->updateLabel(
                            label_flow.data_ptr(), m, n, 
                            is_x_periodic, is_y_periodic);
                }
                
                // sync calculation of vesicles
                for (int ives = 0; ives != nves; ++ives)
                {
                    vesicles[ives]->sync_operation();
                }
            }

            // *********************************************************************
            // exporting and screen display
            if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
            {
                std::cout << "i = " << i << ", Tend = " << Tend << std::endl;
            
                std::cout << "i = " << i << ", write frame" << std::endl;

                // save vesicle shape
                if (is_vesicle_on)
                {
                    printf("vesicle shapes snapshot\n");
                    flow.ibm.export_all_objects_shapes_af("sx.afdat", "sy.afdat", i != 0);
                }

                // save flow field
                printf("flow field snapshot\n");
                flow.ux.af_save("ux", "ux.afdat", i != 0);
                flow.uy.af_save("uy", "uy.afdat", i != 0);
                // flow.rho.af_save("density", "density.afdat", i != 0);
                flow.af_save_flow_field("f_last_frame.afdat");
                label_flow.af_save("label", "label.afdat", i != 0);

                // calculate and save total shear stress
                REAL sigma_mean = calculate_shear_stress(
                    flow.ux.data_ptr(), flow.label,
                    viscosity_cyto_phys, viscosity_solvent_phys,
                    dt_phys, m, n);
                shear_stress_file.open("shear_stress.dat", std::ios::app);
                shear_stress_file << i*dt_phys << " " << sigma_mean << std::endl;
                shear_stress_file.close();

                if (is_vesicle_on)
                {
                    // save membrane curvature
                    curvature_export.af_save("curvature", "curvature.afdat", i != 0);
                }
            }

            if (is_vesicle_on)
            {
                // add repulsive force                
                if (is_using_repulsive_force)
                    flow.ibm.add_short_range_repulsive_force();

                // add adhesion force
                if (is_using_adhesion_force)
                    flow.ibm.add_adhesion_force(
                        curvature_export, De, d0, 
                        vesicles[0]->get_l0(), dx_phys,
                        is_vesicle_adhesion, is_boundary_adhesion);
            }

            // *********************************************************************
            // diffusion of ATP
            if (is_ATP_on)
            {
                if (is_vesicle_on)
                {
                    // calculate time-averaged curvature change rate in Lagrangian form
                    calculate_curvature_change(ves_curvature_change, curvature, 
                        npoint*nves, n_history, i%n_history, dt_phys);
                
                    // register vesicles
                    for (int ives = 0; ives != nves; ++ives)
                    {
                        diffusion.registerCurve(
                            vesicles[ives]->get_x(), 
                            vesicles[ives]->get_y(), 
                            true, ives);
                    }
                }

                // register vessel boundaries
                for (int ibound = 0; ibound != n_bound; ++ibound)
                {
                    diffusion.registerCurve
                        (
                        x_bound[ibound], 
                        y_bound[ibound], 
                        is_circle_bound[ibound], 
                        bound_type[ibound]
                        );
                }

                if (nves || n_bound)
                    diffusion.registerCurveFinished(); // contains "cudaStreamSynchronize"
                
                diffusion.computeConcentrationOnBoundary();
                
                // calculate membrane shear stress in Eulerian form
                diffusion.calculate_shear_stress_on_boundary(
                    shear_stress.data_ptr(), viscosity_solvent_phys, 
                    dt_phys, (REAL)(-1.2)); // change 1.5 to 2.0

                if (is_vesicle_on)
                {
                    // interpolate vesicle curvature change to Eulerian form
                    interpolate_curvature_change
                        (
                        curvature_change,
                        diffusion.boundary.s,
                        diffusion.boundary.id,
                        ves_curvature_change,
                        diffusion.boundary.kernel_launch_size,
                        diffusion.boundary.boundary_size,
                        nves
                        );
                }

                set_boundary(
                    diffusion.boundary.C_in.data_ptr(),
                    diffusion.boundary.C_ex.data_ptr(),
                    diffusion.boundary.Ctype_in.data_ptr(),
                    diffusion.boundary.Ctype_ex.data_ptr(),
                    diffusion.boundary.rho_in.data_ptr(),
                    shear_stress.data_ptr(),
                    curvature_change.data_ptr(),
                    diffusion.boundary.kernel_launch_size,
                    diffusion.getBoundarySizeOnDevice().data_ptr(),
                    diffusion.boundary.id.data_ptr(),
                    cc_critical_phys,
                    cc_slope_phys,
                    ss_critical_phys,
                    k_sigma_phys,
                    Ka_phys, 
                    EC_ATP_release_model,
                    s0_phys, 
                    tau_m_phys,
                    Datp_phys,
                    a_infty_phys,
                    dx_phys,
                    dt_phys
                    );

                diffusion.advanceTimeStep();

                // save AD data
                if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
                {

                    // get boundary size
                    int diffusion_boundary_size = diffusion.boundary.size();

                    // save shear stress on membrane
                    shear_stress.af_save(
                        "boundary_shear_stress_euler",
                        "boundary_shear_stress_euler.afdat",
                        i != 0,
                        diffusion_boundary_size);
                    auto normalized_membrane_shear_rate = shear_stress / viscosity_solvent_phys / shear_rate_phys;
                    /*normalized_membrane_shear_rate.af_save(
                        "normalized_membrane_shear_rate_euler",
                        "normalized_membrane_shear_rate_euler.afdat",
                        i != 0,
                        diffusion_boundary_size);*/
                    
                    if (is_vesicle_on)
                    {
                        // save membrane curvature change
                        ves_curvature_change.af_save("curvature_change", 
                            "curvature_change.afdat", i != 0);

                        // save curvature change value on boundary intersecting points of membrane and lattice lines
                        curvature_change.af_save(
                            "curvature_change_euler",
                            "curvature_change_euler.afdat",
                            i != 0, diffusion_boundary_size);
                    }

                    // save ATP concentration on boundary
                    // added by Zhe Gou on 20200806
                    DeviceArray<REAL> x_bound_euler(
                        0.0, diffusion_boundary_size);
                    DeviceArray<REAL> y_bound_euler(
                        0.0, diffusion_boundary_size);
                    diffusion.boundary.getBoundaryCoordinates(
                        x_bound_euler.data_ptr(), 
                        y_bound_euler.data_ptr());
                    x_bound_euler.af_save(
                        "x_bound_euler", "x_bound_euler.afdat", 
                        i != 0, diffusion_boundary_size);
                    y_bound_euler.af_save(
                        "y_bound_euler", "y_bound_euler.afdat", 
                        i != 0, diffusion_boundary_size);
                    diffusion.boundary.rho_in.af_save(
                        "boundary_rho_in", "boundary_rho_in.afdat",
                        i != 0, diffusion_boundary_size);

                    diffusion.boundary.s.af_save(
                        "boundary_s_lag", "boundary_s_lag.afdat", 
                        i != 0, diffusion_boundary_size);
                    diffusion.boundary.id.af_save(
                        "boundary_id", "boundary_id.afdat", 
                        i != 0, diffusion_boundary_size);

                    // export solute field
                    diffusion.rho.af_save("rho", "rho.afdat", i != 0);
                    diffusion.af_save_ad_field("ad_last_frame.afdat");

                    // export data for boundary structure, for matlab post process script
                    if (LBM::getStepStampFlag(i, frames_boundary, Tend, Tbeg_export))
                    {
                        char boundary_file_name[100];
                        sprintf(boundary_file_name, "bound%08d.afdat", i);
                        diffusion.boundary.exportToFile_ArrayFire(boundary_file_name);
                        //char vesicle_shape_file_name[100];
                        //sprintf(vesicle_shape_file_name, "shape%08d.afdat", i);
                        //ves.exportShapeAF(vesicle_shape_file_name);
                    }
                }
            }

            // advance flow
            if (is_collision_MRT) flow.advanceFlowField_MRT();
            else flow.advanceFlowField();

            // convect immersed objects
            if (is_vesicle_on)
            {

                /*if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
                {
                    // save vesicle shape
                    for (int ives = 0; ives != nves; ++ives)
                    {
                        char vesicle_fx_number[100];
                        sprintf(vesicle_fx_number, "lag_fx_%06d.afdat", ives);
                        vesicles[ives]->get_fx().af_save("lag_fx", vesicle_fx_number, i != 0);

                        char vesicle_fy_number[100];
                        sprintf(vesicle_fy_number, "lag_fy_%06d.afdat", ives);
                        vesicles[ives]->get_fy().af_save("lag_fy", vesicle_fy_number, i != 0);

                        char vesicle_shape_number[100];
                        sprintf(vesicle_shape_number, "ves_shape_%06d.afdat", ives);
                        vesicles[ives]->exportShapeAF(vesicle_shape_number, i != 0);
                    }
                }*/

                flow.advanceImmersedObjects();

                if (LBM::getStepStampFlag(i, frames, Tend, Tbeg_export))
                {
                    // flow.ibm.eul_fx.af_save("eul_fx", "eul_fx.afdat", i != 0);
                    // flow.ibm.eul_fy.af_save("eul_fy", "eul_fy.afdat", i != 0);
                }
            }
        }
        cudaDeviceSynchronize();
        timer.toc();
    }

};

#endif
