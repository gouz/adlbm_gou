#ifndef _deviceReduceOperation_cuh
#define _deviceReduceOperation_cuh

#include "LBMheader.cuh"

template <typename T, int blockSize, bool nIsPow2>
__device__
// this function is modified from nvidia example of reduced summation
void _device_reduce_add
(
const T* __restrict__ g_idata,
T* __restrict__ sdata,
const int n,
const T factor
)
{
    // input sdata is: __shared__ T sdata[blockSize * 2]; should be allocated externally.
    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    int tid = threadIdx.x;
    int i = threadIdx.x;
    T mySum = (T)0;
    // we reduce multiple elements per thread.  The number is determined by the
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n) {
        mySum += g_idata[i];
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (i + blockSize < n) mySum += g_idata[i + blockSize];
        i += blockSize * 2;
    }

    // each thread puts its local sum into shared memory
    sdata[tid] = mySum; __syncthreads();
    // do reduction in shared mem
    if ((blockSize >= 512) && (tid < 256)) sdata[tid] = mySum = mySum + sdata[tid + 256]; __syncthreads();
    if ((blockSize >= 256) && (tid < 128)) sdata[tid] = mySum = mySum + sdata[tid + 128]; __syncthreads();
    if ((blockSize >= 128) && (tid <  64)) sdata[tid] = mySum = mySum + sdata[tid + 64]; __syncthreads();
#if (__CUDA_ARCH__ >= 300 )
    if (tid < 32) {
        // Fetch final intermediate sum from 2nd warp
        if (blockSize >= 64) mySum += sdata[tid + 32];
        // Reduce final warp using shuffle
        for (int offset = warpSize / 2; offset > 0; offset /= 2) mySum += __shfl_down(mySum, offset);
    }
#else
    // fully unroll reduction within a single warp
    if ((blockSize >= 64) && (tid < 32)) sdata[tid] = mySum = mySum + sdata[tid + 32]; __syncthreads();
    if ((blockSize >= 32) && (tid < 16)) sdata[tid] = mySum = mySum + sdata[tid + 16]; __syncthreads();
    if ((blockSize >= 16) && (tid <  8)) sdata[tid] = mySum = mySum + sdata[tid + 8]; __syncthreads();
    if ((blockSize >= 8) && (tid <  4)) sdata[tid] = mySum = mySum + sdata[tid + 4]; __syncthreads();
    if ((blockSize >= 4) && (tid <  2)) sdata[tid] = mySum = mySum + sdata[tid + 2]; __syncthreads();
    if ((blockSize >= 2) && (tid <  1)) sdata[tid] = mySum = mySum + sdata[tid + 1]; __syncthreads();
#endif
    // The reduced result is stored at blockIdx.x position of sdata
    if (tid == 0) sdata[0] = mySum * factor;
    __syncthreads();
}

template <typename T, int blockSize, bool nIsPow2>
// this function is modified from nvidia example on reduced summation
// make sure that length of g_idata is less than (blockSize * 2)
__device__
void _device_reduce_min
(
const T* __restrict__ g_idata,
T* __restrict__ sdata,
const int n
)
{
    // input sdata is: __shared__ T sdata[blockSize * 2]; should be allocated externally.
    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    int tid = threadIdx.x;
    int i = blockIdx.x*blockSize * 2 + threadIdx.x;
    int gridSize = blockSize * 2 * gridDim.x;
    T myMin = (T)1e100;
    // we reduce multiple elements per thread.  The number is determined by the
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n) {
        myMin = min(g_idata[i], myMin);
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < n) myMin = min(g_idata[i + blockSize], myMin);
        i += gridSize;
    }
    // each thread puts its local sum into shared memory
    sdata[tid] = myMin; __syncthreads();
    // do reduction in shared mem
    if ((blockSize >= 512) && (tid < 256)) sdata[tid] = myMin = min(sdata[tid + 256], myMin); __syncthreads();
    if ((blockSize >= 256) && (tid < 128)) sdata[tid] = myMin = min(sdata[tid + 128], myMin); __syncthreads();
    if ((blockSize >= 128) && (tid <  64)) sdata[tid] = myMin = min(sdata[tid + 64], myMin); __syncthreads();
    if ((blockSize >= 64) && (tid < 32)) sdata[tid] = myMin = min(sdata[tid + 32], myMin); __syncthreads();
    if ((blockSize >= 32) && (tid < 16)) sdata[tid] = myMin = min(sdata[tid + 16], myMin); __syncthreads();
    if ((blockSize >= 16) && (tid <  8)) sdata[tid] = myMin = min(sdata[tid + 8], myMin); __syncthreads();
    if ((blockSize >= 8) && (tid <  4)) sdata[tid] = myMin = min(sdata[tid + 4], myMin); __syncthreads();
    if ((blockSize >= 4) && (tid <  2)) sdata[tid] = myMin = min(sdata[tid + 2], myMin); __syncthreads();
    if ((blockSize >= 2) && (tid <  1)) sdata[tid] = myMin = min(sdata[tid + 1], myMin); __syncthreads();
    // The reduced result is stored at blockIdx.x position of sdata
    if (tid == 0) sdata[blockIdx.x] = myMin;
    __syncthreads();
}

template <typename T, int blockSize, bool nIsPow2>
// this function is modified from nvidia example on reduced summation
// make sure that length of g_idata is less than (blockSize * 2)
__device__
void _device_reduce_max
(
const T* __restrict__ g_idata,
T* __restrict__ sdata,
const int n
)
{
    // input sdata is: __shared__ T sdata[blockSize * 2]; should be allocated externally.
    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    int tid = threadIdx.x;
    int i = blockIdx.x*blockSize * 2 + threadIdx.x;
    int gridSize = blockSize * 2 * gridDim.x;
    T myMax = (T)(-1e100);
    // we reduce multiple elements per thread.  The number is determined by the
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n) {
        myMax = max(g_idata[i], myMax);
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < n) myMax = max(g_idata[i + blockSize], myMax);
        i += gridSize;
    }
    // each thread puts its local sum into shared memory
    sdata[tid] = myMax; __syncthreads();
    // do reduction in shared mem
    if ((blockSize >= 512) && (tid < 256)) sdata[tid] = myMax = max(sdata[tid + 256], myMax); __syncthreads();
    if ((blockSize >= 256) && (tid < 128)) sdata[tid] = myMax = max(sdata[tid + 128], myMax); __syncthreads();
    if ((blockSize >= 128) && (tid <  64)) sdata[tid] = myMax = max(sdata[tid + 64], myMax); __syncthreads();
    // fully unroll reduction within a single warp
    if ((blockSize >= 64) && (tid < 32)) sdata[tid] = myMax = max(sdata[tid + 32], myMax); __syncthreads();
    if ((blockSize >= 32) && (tid < 16)) sdata[tid] = myMax = max(sdata[tid + 16], myMax); __syncthreads();
    if ((blockSize >= 16) && (tid <  8)) sdata[tid] = myMax = max(sdata[tid + 8], myMax); __syncthreads();
    if ((blockSize >= 8) && (tid <  4)) sdata[tid] = myMax = max(sdata[tid + 4], myMax); __syncthreads();
    if ((blockSize >= 4) && (tid <  2)) sdata[tid] = myMax = max(sdata[tid + 2], myMax); __syncthreads();
    if ((blockSize >= 2) && (tid <  1)) sdata[tid] = myMax = max(sdata[tid + 1], myMax); __syncthreads();
    // The reduced result is stored at blockIdx.x position of sdata
    if (tid == 0) sdata[blockIdx.x] = myMax;
    __syncthreads();
}

#endif