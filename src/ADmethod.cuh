#ifndef ADmethod_cuh
#define ADmethod_cuh
// ************************************************************************************************
// A D2Q5 lattice-boltzmann model for advection-diffusion-reaction
// function advance can be SRT(defaut) or MRT(for anisotropic diffusion)
// it uses a 2D int array Didx to indexing the diffusion coefficient on each lattice
// Positions where Didx[pos] < 0 and its adjacent lattices are masked and will not be calculated
// to construct a ADmethod object you must provide a positive REAL number as diffusion coefficient
// (in LBM unit) or a pointer and number to a list of diffusion coefficients.
//
// if AD_DEFAULT_STREAM tag is off, a unique cuda stream will be assigned for ADmethod object
// if AD_ALWASY_WRITE_RHO is on, ADmethod::advance will always write rho to global memory
//************************************************************************************************
// 2D MRT explanation:
// this method is from Hiroaki yoshita, Makoto Nagaoka
// Multiple-relaxation-time lattice Boltzmann model for the convection and anisotropic diffusion
// equation, JCP 2010 7774-7795
// in BGK model, collision relaxation is
//     f' = f + 1/tao * (feq-f), tao = D/eps + 1/2
// in MRT model, 1/tao is replaced by relaxation matrix Mc, thus
//     f' = f + Mc * (feq-f)
// this Mc is the product of 
// Mc = M^-1 * S^-1 * M
//   M: projecting matrix
//   M = [1,  1,  1,  1,  1,
//        0,  1, -1,  0,  0,
//        0,  0,  0,  1, -1,
//        4, -1, -1, -1, -1,
//        0,  1,  1, -1, -1]
//   S: relaxation matrix in moment space (correspounds to tao in BGK model)
//   S = [1,  0,  0,  0,  0,
//        0,Txx,Txy,  0,  0,
//        0,Tyx,Tyy,  0,  0,
//        0,  0,  0,  1,  0,
//        0,  0,  0,  0,  1]
//   In witch
//   [Txx,Txy,
//    Tyx,Tyy] = [D] / eps + (1/2) * I
//
//   here D can be anisotropic [Dxx, Dxy; Dyx Dyy], in which Dxy = Dyx
//   in isotropic case, Dxx = Dyy = D, Dxy = Dyx = 0
//   Mc = [1,  0,  0,  0,  0,
//         0,  A,  B,  0,  0,        A = 0.5 * (1 + 1/tao)
//         0,  B,  A,  0,  0,        B = 0.5 * (1 - 1/tao)
//         0,  0,  0,  A,  B,        tao = D / eps + 1/2
//         0,  0,  0,  B,  A]
//   form of Mc is simple enough so the multiplication
//   Mc * (feq-f) is calculatedinside the kernel while only passing tao_inv into it
//   
//   in cases of anisotropic diffusion
//   Mc = [1,  0,  0,  0,  0,        A = 0.5 * (1 + 1/taoxx); taoxx = Dxx / eps + 1/2
//         0,  A,  B,  E,  E,        B = 0.5 * (1 - 1/taoxx)
//         0,  B,  A,  E,  E,        C = 0.5 * (1 + 1/taoyy); taoyy = Dyy / eps + 1/2
//         0,  0,  0,  C,  D,        D = 0.5 * (1 - 1/taoyy)
//         0,  0,  0,  D,  C]        E = 0.5 * taoxy;         taoxy = Dxy / eps
//   THIS IS SIMPLE BUT NOT IMPLEMENTED YET, Feb 26, 2017
// ************************************************************************************************

// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

// DeviceArray header
#include "DeviceArray.cuh"

// LBM constant
#include "LBMconstant.hpp"
#include <vector>

#ifndef REAL
#define REAL double
#endif

// #define AD_DEFAULT_STREAM

namespace AD{
    typedef enum{
        FREE,
        BOUNCEBACK,
        PERIODIC,
    } BOUNDARY_TYPE;

    struct CONFIG{
        BOUNDARY_TYPE X_BOUNDARY_TYPE;
        BOUNDARY_TYPE Y_BOUNDARY_TYPE;
        inline CONFIG() :X_BOUNDARY_TYPE(FREE), Y_BOUNDARY_TYPE(FREE){ return; }
    };
}

class ADmethod{
public:
    const static int Q = 5; // AD-LBM-D2Q5 model
public:

    AD::CONFIG config;

    // cuda stream
    cudaStream_t stream;
private:
    // diffusion coefficients that may be used in the simulation
    REAL D;
    // number of diffusion coefficient in ADmethod object
    int num_D;
    DeviceArray<REAL> D_list;
    // ralaxation time tao, tao_inv = 1/tao
    REAL tao_inv;
    DeviceArray<REAL> tao_inv_list;

    // number of exact diffCoeff you have
    int size_diffCoeff;
public:
    inline ~ADmethod(){
#ifndef DEFAULT_STREAM
        cudaStreamDestroy(stream);
#endif
    }
    inline const REAL& get_D() const { return D; }
    inline const DeviceArray<REAL>& get_D_list() const { return D_list; }
    inline const REAL& get_tao_inv() const { return tao_inv; }
    inline const DeviceArray<REAL>& get_tao_inv_list() const { return tao_inv_list; }
    inline ADmethod(const REAL* DiffCoeff, const int n) : D(DiffCoeff[0]), num_D(n),tao_inv(1.0 / (3.0*DiffCoeff[0] + 0.5)){
        D_list = DeviceArray<REAL>(DiffCoeff, n);
        tao_inv_list = (REAL)1.0 / ((REAL)3.0 * D_list + (REAL)0.5);
        size_diffCoeff = n;

        cudaStreamCreate(&stream);
        config.X_BOUNDARY_TYPE = AD::FREE;
        config.Y_BOUNDARY_TYPE = AD::FREE;
    }

    inline ADmethod(const REAL DiffCoeff = 1.0 / 6.0) : D(DiffCoeff), num_D(1), tao_inv(1.0 / (3.0*DiffCoeff + 0.5)){
        D_list = DeviceArray<REAL>(DiffCoeff, 1);
        tao_inv_list = (REAL)1.0 / ((REAL)3.0 * D_list + (REAL)0.5);
        size_diffCoeff = 1;
        cudaStreamCreate(&stream);
        config.X_BOUNDARY_TYPE = AD::FREE;
        config.Y_BOUNDARY_TYPE = AD::FREE;
    };

    inline void reset_diffusion_coefficient(const REAL* DiffCoeff, const int n){
        D_list = DeviceArray<REAL>(DiffCoeff, n);
        tao_inv_list = (REAL)1.0 / ((REAL)3.0 * D_list + (REAL)0.5);
        size_diffCoeff = n;
    }
    inline void reset_diffusion_coefficient(const REAL DiffCoeff = 1.0 / 6.0){
        D_list = DeviceArray<REAL>(DiffCoeff, 1);
        tao_inv_list = (REAL)1.0 / ((REAL)3.0 * D_list + (REAL)0.5);
        size_diffCoeff = 1;
    }

    // calculate equilibrium distribution function from macro concentration rho
    void feq(REAL* f, const REAL* rho, const int m, const int n);
    void feq(REAL* f, const REAL* rho, const REAL* ux, const REAL* uy, const int m, const int n);
    
    // cauculate macro concentration rho alone from distribution function f
    void exportRho(REAL* rho, const REAL* f, const int m, const int n);

    // ********************************************************************************************
    // advance time step on pure diffusion model
    // single diffusion coefficient, single-relaxation-time
    void advance_diffusion(
        REAL* rho1, REAL* f1, const REAL* f0, const int m, const int n);

    // advance time step on pure diffusion model
    // multiple diffusion coefficient, single-relaxation-time
    void advance_diffusion(
        REAL* rho1, REAL* f1, const REAL* f0, const int* Didx, const int m, const int n);

    // advance time step on advection-diffusion model
    // single diffusion coefficient, single-relaxation-time
    void advance_advection_diffusion(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const int m, const int n);

    // advance time step on advection-diffusionmodel
    // multiple diffusion coefficient, single-relaxation-time
    void advance_advection_diffusion(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const int* Didx, const int m, const int n);
    // ********************************************************************************************

    // ********************************************************************************************
    // advance time step on diffusion-reaction model
    // single diffusion coefficient, single-relaxation-time
    void advance_diffusion_reaction(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* s, const int m, const int n);

    // advance time step on diffusion-reaction model
    // multiple diffusion coefficient, single-relaxation-time
    void advance_diffusion_reaction(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* s, const int* Didx, const int m, const int n);

    // advance time step on advection-diffusion-reaction model
    // single diffusion coefficient, single-relaxation-time
    void advance_advection_diffusion_reaction(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const REAL* s, const int m, const int n);

    // advance time step on advection-diffusion-reaction model
    // multiple diffusion coefficient, single-relaxation-time
    void advance_advection_diffusion_reaction(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const REAL* s, const int* Didx, const int m, const int n);
    // ********************************************************************************************

    // ********************************************************************************************
    // advance time step on pure diffusion model
    // single diffusion coefficient, multiple-relaxation-time
    void advance_diffusion_MRT(
        REAL* rho1, REAL* f1, const REAL* f0, const int m, const int n);

    // advance time step on pure diffusion model
    // multiple diffusion coefficient, multiple-relaxation-time
    void advance_diffusion_MRT(
        REAL* rho1, REAL* f1, const REAL* f0, const int* Didx, const int m, const int n);

    // advance time step on advection-diffusion model
    // single diffusion coefficient, multiple-relaxation-time
    void advance_advection_diffusion_MRT(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const int m, const int n);

    // advance time step on advection-diffusionmodel
    // multiple diffusion coefficient, multiple-relaxation-time
    void advance_advection_diffusion_MRT(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const int* Didx, const int m, const int n);
    // ********************************************************************************************

    // ********************************************************************************************
    // advance time step on diffusion-reaction model
    // single diffusion coefficient, multiple-relaxation-time
    void advance_diffusion_reaction_MRT(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* s, const int m, const int n);

    // advance time step on diffusion-reaction model
    // multiple diffusion coefficient, multiple-relaxation-time
    void advance_diffusion_reaction_MRT(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* s, const int* Didx, const int m, const int n);

    // advance time step on advection-diffusion-reaction model
    // single diffusion coefficient, multiple-relaxation-time
    void advance_advection_diffusion_reaction_MRT(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const REAL* s, const int m, const int n);

    // advance time step on advection-diffusion-reaction model
    // multiple diffusion coefficient, multiple-relaxation-time
    void advance_advection_diffusion_reaction_MRT(
        REAL* rho1, REAL* f1, const REAL* f0, const REAL* Ux, const REAL* Uy, const REAL* s, const int* Didx, const int m, const int n);
    // ********************************************************************************************

    // reset concentration inside vesicle to 0
    // added by Zhe Gou, 20211208
    void reset(REAL* rho1, REAL* f1, REAL* f0, const int* label, const int m, const int n);
};

#endif