// Try to fix 'updatelabel' function for multi-vesicle problem
// Modified by Zhe Gou at 20191120

#include "ImmersedObject.cuh"

// #define MIN(a,b) ((a) < (b) ? (a) : (b))
// #define MAX(a,b) ((a) > (b) ? (a) : (b))
#define BSIZE1D 256

#define POSITIVE_MOD(i,n) (((i) % (n)) + (n)) % (n)

// _device_reduce_min() and _device_reduce_max() are for fundBox() method
#include "_deviceReduceOperation.cuh"

template<int blockSize, bool nIsPow2>
__global__ void kernel_findBox
(
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
REAL* __restrict__ _box,
const int n
)
{
    __shared__ REAL sdata[blockSize * 2];
    __shared__ REAL sbox[4];
    _device_reduce_min<REAL, blockSize, nIsPow2>(_x, sdata, n);
    if (threadIdx.x == 0){
        sbox[0] = sdata[0];
    }
    _device_reduce_max<REAL, blockSize, nIsPow2>(_x, sdata, n);
    if (threadIdx.x == 0){
        sbox[1] = sdata[0];
    }
    _device_reduce_min<REAL, blockSize, nIsPow2>(_y, sdata, n);
    if (threadIdx.x == 0){
        sbox[2] = sdata[0];
    }
    _device_reduce_max<REAL, blockSize, nIsPow2>(_y, sdata, n);
    if (threadIdx.x == 0){
        sbox[3] = sdata[0];
    }
    if (threadIdx.x < 4) _box[threadIdx.x] = sbox[threadIdx.x];
}


void ImmersedObject::findBox(){
    if ((x.size()&(x.size() - 1)) == 0)
    {
        kernel_findBox<BSIZE1D, true> <<<1, BSIZE1D>>>
            (x.data_ptr(), y.data_ptr(), box.data_ptr(), x.size());
    }
    else
    {
        kernel_findBox<BSIZE1D, false> <<<1, BSIZE1D>>>
            (x.data_ptr(), y.data_ptr(), box.data_ptr(), x.size());
    }
}

//__forceinline__ __device__ void _deviceIsInside

template<bool x_periodic, bool y_periodic>
// a kernel for tagging inside / outside of an immersed object, using scanline algorithm
// also works on periodic boundary
__global__ void kernel_updateLabel
(
int* __restrict__ _label,
const REAL* __restrict__ _box,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int len,
const int m,
const int n,
const int valin,
const int valex
){
    extern __shared__ REAL shared_mem[];
    REAL* sx = shared_mem;
    REAL* sy = &shared_mem[len];

    __shared__ REAL sbox[4];
    __shared__ bool flagOutOfBox;
    __shared__ REAL xShift;
    __shared__ REAL yShift;

    // load box range
    int idxLocalThread = threadIdx.y * blockDim.x + threadIdx.x;
    if (idxLocalThread < 4) sbox[idxLocalThread] = _box[idxLocalThread];

    // if this whole block lies out of object box, then write true to flagOutOfBox
    // and prepare to terminate this block
    if (idxLocalThread==0)
    {
        flagOutOfBox = false;
        xShift = (REAL)0;
        yShift = (REAL)0;

        const REAL wApron = (REAL)2;
        REAL x0 = sbox[0] - wApron;
        REAL x1 = sbox[1] + wApron;
        REAL y0 = sbox[2] - wApron;
        REAL y1 = sbox[3] + wApron;
        REAL posx0 = (REAL)(blockDim.x * blockIdx.x);
        REAL posx1 = (REAL)(blockDim.x * (blockIdx.x + 1) - 1); posx1 = posx1 < (REAL)m ? posx1 : (REAL)m;
        REAL posy0 = (REAL)(blockDim.y * blockIdx.y);
        REAL posy1 = (REAL)(blockDim.y * (blockIdx.y + 1) - 1); posy1 = posy1 < (REAL)n ? posy1 : (REAL)n;
        if (x_periodic){
            REAL _xShift = -floor(x0 / (REAL)m)*(REAL)m;
            x0 += _xShift;
            x1 += _xShift;
            xShift = _xShift;
        }
        if (y_periodic){
            REAL _yShift = -floor(y0 / (REAL)n)*(REAL)n;
            y0 += _yShift;
            y1 += _yShift;
            yShift = _yShift;
        }
        flagOutOfBox =
            (posx1<x0 | posx0>x1 | posy1<y0 | posy0>y1) &
            ((x_periodic & (posx0 + (REAL)m > x1)) | !x_periodic) &
            ((y_periodic & (posy0 + (REAL)n > y1)) | !y_periodic);

        // debug, show box position and flagOutOfBox
        /*
        if (!flagOutOfBox)
            printf("objBox = [%f,%f,%f,%f], blockbox = [%f,%f,%f,%f], flagOutOfBox = %i\n",
            x0, x1, y0, y1, posx0, posx1, posy0, posy1, (int)flagOutOfBox);
        */
    }
    __syncthreads();

    // if this block overlaps with object box, then tag the points with value valin or valex
    if (!flagOutOfBox){

        // load object coordinates into shared mem
        // an external prestep of divding object coordinates into local buckets will accelerate this method
        // but it's not implemented yet
        REAL _xShift = xShift;
        REAL _yShift = yShift;
        while (idxLocalThread < len)
        {
            sx[idxLocalThread] = _x[idxLocalThread] + _xShift;
            sy[idxLocalThread] = _y[idxLocalThread] + _yShift;
            idxLocalThread += (blockDim.x * blockDim.y);
        }
        __syncthreads();

        // find out if (posx, posy) lies in the object
        int posi = blockDim.x * blockIdx.x + threadIdx.x;
        int posj = blockDim.y * blockIdx.y + threadIdx.y;

        if (posi < m & posj < n)
        {

            REAL posx0 = (REAL)posi;
            REAL posy0 = (REAL)posj;
            REAL posx1, posy1;
            if (x_periodic) posx1 = posx0 + (REAL)m;
            if (y_periodic) posy1 = posy0 + (REAL)n;

            // intersecting counter in scanline algorithm, consider periodic images
            int c00 = 0; 
            int c01 = 0;
            int c10 = 0;
            int c11 = 0;
            REAL xprev = sx[len - 1];
            REAL yprev = sy[len - 1];
            REAL x, y, dx, dy, dy_abs, dy_sgn;
            for (int i = 0; i < len; i++)
            {
                x = sx[i];
                y = sy[i];
                dx = xprev - x;
                dy = yprev - y;
                dy_abs = abs(dy);
                dy_sgn = ((REAL)0 < dy) - (dy<(REAL)0);

                // if position (posx,posy) is out of segment box or not intersecting with 
                c00 += (int)(((y > posy0) != (yprev > posy0)) & ((posx0 - x)*dy_abs < dy_sgn*(posy0 - y)*dx));
                if (x_periodic)
                    c01 += (int)(((y > posy0) != (yprev > posy0)) & ((posx1 - x)*dy_abs < dy_sgn*(posy0 - y)*dx));
                if (y_periodic)
                    c10 += (int)(((y > posy1) != (yprev > posy1)) & ((posx0 - x)*dy_abs < dy_sgn*(posy1 - y)*dx));
                if (x_periodic & y_periodic)
                    c11 += (int)(((y > posy1) != (yprev > posy1)) & ((posx1 - x)*dy_abs < dy_sgn*(posy1 - y)*dx));

                xprev = x;
                yprev = y;
            }
            c00 %= 2;
            if (x_periodic) c01 %= 2;
            if (y_periodic) c10 %= 2;
            if (x_periodic & y_periodic) c11 %= 2;
            bool c = (bool)c00 | (bool)c01 | (bool)c10 | (bool)c11;

            //  a negative value of label indicates this grid is fixed solid boundary
            int label = _label[posj*m + posi];
            if (label >= 0)_label[posj*m + posi] += c ? valin : valex; // modified by Zhe Gou at 20191120
        }
    }

}
void ImmersedObject::updateLabel // modified by Zhe Gou at 20191121
(                                // asynchronous calls
int* _label,
const int m,
const int n,
const bool x_periodic,
const bool y_periodic,
const int valin,
const int valex)
{
    const int BSIZEX = 32;
    const int BSIZEY = 16;
    const dim3 block(BSIZEX, BSIZEY);
    dim3 grid((m + BSIZEX - 1) / BSIZEX, (n + BSIZEY - 1) / BSIZEY);

    findBox();

    if (!x_periodic && !y_periodic)
    {
        kernel_updateLabel<false, false> <<<grid, block, x.size() * 2 * sizeof(REAL), cudaStream>>>
            (_label, box.data_ptr(), x.data_ptr(), y.data_ptr(), x.size(), m, n, valin, valex);
    }
    if (x_periodic && !y_periodic)
    {
        kernel_updateLabel<true, false> <<<grid, block, x.size() * 2 * sizeof(REAL), cudaStream>>>
            (_label, box.data_ptr(), x.data_ptr(), y.data_ptr(), x.size(), m, n, valin, valex);
    }
    if (!x_periodic && y_periodic)
    {
        kernel_updateLabel<false, true> <<<grid, block, x.size() * 2 * sizeof(REAL), cudaStream>>>
            (_label, box.data_ptr(), x.data_ptr(), y.data_ptr(), x.size(), m, n, valin, valex);
    }
    if (x_periodic && y_periodic)
    {
        kernel_updateLabel<true, true> <<<grid, block, x.size() * 2 * sizeof(REAL), cudaStream>>>
            (_label, box.data_ptr(), x.data_ptr(), y.data_ptr(), x.size(), m, n, valin, valex);
    }

}


// This sav-gal filter code is from Mohd Suhail Rizv
// originally written in matlab, translated by Hengdi ZHANG
// 2017.11.27
// ************************************************************************
/*
function [yy,dydx]=smooth_derivative(x,y,ns,np)
% Written by Anupam Pal
% smootheing code following the idea of Savitzky-Golay method.
% x = abscissa (can be nonuniformly spaced)
% y = ordinate
% ns = order of the polynomial fit
% np = no. of data points to be used for polynomial fit
%
% yy = smoothed ordinate values at x
% dydx = derivative dy/dx
%
% The code is not robust. So works better if there are at least 10 points
% or more.
%
n=length(x);
np=min(n-4,np);

if mod(np,2)==0,
np=np-1;
end
ns=min(ns,np-1);

np1=floor((np-1)/2);
np2=np-np1-1;

x=x(:)';
y=y(:)';

[p,s,mu]=polyfit(x(1:np),y(1:np),ns);

yy(1:np1+1)=polyval(p,(x(1:np1+1)-mu(1))/mu(2));
coeff(1:np1+1,1:ns+1)=repmat(p,np1+1,1);
muval(1:np1+1,1:2)=repmat(mu',np1+1,1);

for i=np1+2:n-np1
[p,s,mu]=polyfit(x(i-np1:i+np2),y(i-np1:i+np2),ns);
yy(i)=polyval(p,(x(i)-mu(1))/mu(2));
coeff(i,:)=p;
muval(i,1:2)=mu';
end

yy(n-np1+1:n)=polyval(p,(x(n-np1+1:n)-mu(1))/mu(2));
coeff(n-np1+1:n,:)=repmat(p,np1,1);
muval(n-np1+1:n,1:2)=repmat(mu',np1,1);

dydx(1:n)=0;
for i=1:ns
dydx(1:n)=dydx(1:n)+(ns-i+1)*coeff(1:n,i)'.* ...
((x(1:n)-muval(1:n,1)')./muval(1:n,2)').^(ns-i);
end
dydx(1:n)=dydx(1:n)./muval(1:n,2)';

return
*/
// ************************************************************************

__device__ void device_calculate_curvature
(
const REAL xL,
const REAL x,
const REAL xR,
const REAL yL,
const REAL y,
const REAL yR,
REAL& curvature
)
{
    REAL dxL = x - xL;
    REAL dxR = xR - x;
    REAL dyL = y - yL;
    REAL dyR = yR - y;
    REAL dsL = sqrt(dxL*dxL + dyL*dyL);
    REAL dsR = sqrt(dxR*dxR + dyR*dyR);
    REAL factorL = (REAL)1.0 / (dsL*dsL + dsL*dsR);
    REAL factorR = (REAL)1.0 / (dsR*dsR + dsL*dsR);
    REAL dxds = dsR*factorL*dxL + dsL*factorR*dxR;
    REAL dyds = dsR*factorL*dyL + dsL*factorR*dyR;
    REAL dxds2 = -(REAL)2.0*factorL*dxL + (REAL)2.0*factorR*dxR;
    REAL dyds2 = -(REAL)2.0*factorL*dyL + (REAL)2.0*factorR*dyR;
    REAL temp = dxds*dxds + dyds*dyds;
    curvature = abs((dxds*dyds2 - dxds2*dyds) / (temp*sqrt(temp)));
}

__global__ void device_calculate_curvature_for_vesicle
(
REAL* __restrict__ _curvature,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const REAL l0,
const REAL dx,
const int len
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        // 5 point 2nd order sav-gal filter is implemented on 2nd order derivative calculation
        // f(i) = a0 + a1 * i + a2/2 * i^2
        const REAL a1[5] = { (REAL)-0.2, (REAL)-0.1, (REAL)0.0, (REAL)0.1, (REAL)0.2 };
        const REAL a2[5] = { (REAL)(2.0 / 7.0), (REAL)(-1.0 / 7.0), (REAL)(-2.0 / 7.0), (REAL)(-1.0 / 7.0), (REAL)(2.0 / 7.0) };

        REAL x[5];
        REAL y[5];
#pragma unroll
        for (int i = 0; i != 5; ++i)
        {
            int pos = (idx - 2 + i + len) % len;
            x[i] = _x[pos];
            y[i] = _y[pos];
        }

        REAL dx1 = 0.0;
        REAL dy1 = 0.0;
        REAL dx2 = 0.0;
        REAL dy2 = 0.0;
#pragma unroll
        for (int i = 0; i != 5; ++i)
        {
            dx1 += x[i] * a1[i];
            dy1 += y[i] * a1[i];
            dx2 += x[i] * a2[i];
            dy2 += y[i] * a2[i];
        }
        dx1 /= l0;
        dy1 /= l0;
        dx2 /= (l0*l0);
        dy2 /= (l0*l0);

        REAL temp = dx1*dx1 + dy1*dy1;
        _curvature[idx] = (dx1*dy2 - dx2*dy1) / (temp*sqrt(temp)) / dx;
    }
}

__global__
void kernel_calculate_curvature
(
REAL* __restrict__ _curvature,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int len,
const REAL dx
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        // L and R are coordinate depiction rather than logical position in the array

        int idxR = (idx + len - 1) % len;
        int idxL = (idx + 1) % len;
        int idxRR = (idx + len - 2) % len;
        int idxLL = (idx + 2) % len;
        REAL x = _x[idx];
        REAL xL = _x[idxL];
        REAL xLL = _x[idxLL];
        REAL xR = _x[idxR];
        REAL xRR = _x[idxRR];
        REAL y = _y[idx];
        REAL yL = _y[idxL];
        REAL yLL = _y[idxLL];
        REAL yR = _y[idxR];
        REAL yRR = _y[idxRR];
        REAL curvature, curvatureL, curvatureR;
        device_calculate_curvature(xL, x, xR, yL, y, yR, curvature);
        device_calculate_curvature(xLL, xL, x, yLL, yL, y, curvatureL);
        device_calculate_curvature(x, xR, xRR, y, yR, yRR, curvatureR);
        _curvature[idx] = (curvatureL + curvature * (REAL)2.0 + curvatureR) * (REAL)0.25 / dx;
    }
}

void ImmersedObject::calculate_curvature(REAL* __restrict__ _curvature, const REAL dx) // modified by Zhe Gou at 20191121, asynchronous calls
{
    int len = size();
    kernel_calculate_curvature <<< (len + BSIZE1D - 1) / BSIZE1D, BSIZE1D, 0, cudaStream >>>
        (_curvature, x.data_ptr(), y.data_ptr(), x.size(), dx);
}

void ImmersedObject::calculate_curvature(REAL* __restrict__ _curvature, const REAL l0, const REAL dx)
{
    int len = x.size();
    device_calculate_curvature_for_vesicle <<< (len + BSIZE1D - 1) / BSIZE1D, BSIZE1D, 0, cudaStream >>>
        (_curvature, x.data_ptr(), y.data_ptr(), l0, dx, len);
}

__device__ __forceinline__ REAL device_positiveMod(const REAL& x, const int& m)
{
    return x - floor(x / m) * m;
}

__global__ void kernel_registerCurve
(
int* __restrict__ _id,
int* __restrict__ _count,
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int len,
const int m,
const int n,
const int r,
const bool is_x_periodic,
const bool is_y_periodic,
const int id0
)
{
    int idx = blockDim.x*blockIdx.x + threadIdx.x;
    if (idx < len)
    {
        REAL x = _x[idx];
        REAL y = _y[idx];
        if (is_x_periodic) x = device_positiveMod(x, m);
        if (is_y_periodic) y = device_positiveMod(y, n);

        // put this point into _id bucket
        if (x >= 0 & x < m & y >= 0 & y < n)
        {
            int mc = (m + r - 1) / r;
            int nc = (n + r - 1) / r;
            int ic = (int)floor(x / r);
            int jc = (int)floor(y / r);
            int pos_id = atomicAdd(_count + ic + jc*mc, 1);
            if (pos_id < CellList::MAX_POINT_NUMBER_IN_CELL)
                _id[ic + jc*mc + pos_id*mc*nc] = id0 + idx;
        }
    }
}


void CellList::registerCurve
(
const REAL* __restrict__ _x,
const REAL* __restrict__ _y,
const int len,
const int id0,
const cudaStream_t& cudaStream
)
{
    kernel_registerCurve <<< (len + BSIZE1D - 1) / BSIZE1D, BSIZE1D, 0, cudaStream >>>
        (
        id.data_ptr(),
        count.data_ptr(),
        _x,
        _y,
        len,
        m,
        n,
        idr,
        is_x_periodic,
        is_y_periodic,
        id0
        );
}

__global__ void kernel_build_mask
(
int* __restrict__ _mask,
const int* __restrict__ _count,
const int mc,
const int nc,
const bool is_x_periodic,
const bool is_y_periodic
)
{

    int ic = blockDim.x*blockIdx.x + threadIdx.x;
    int jc = blockDim.y*blockIdx.y + threadIdx.y;
    if (ic < mc && jc < nc)
    {

        // iter_jc starts from (-1 - (is_y_periodic&j == 0))
        // since the actual size of last cell can be less than cellListR if (m%cellListR!=0).
        // the same goes for i
        int count_total = 0;
        for (
            int iter_jc = -CellList::MASK_GHOST_THICKNESS - 1 - (is_y_periodic & jc == 0);
            iter_jc <= 1 + CellList::MASK_GHOST_THICKNESS;
        ++iter_jc
            )
        {
            int pos_jc = iter_jc + jc; if (is_y_periodic) pos_jc = POSITIVE_MOD(pos_jc, nc);
            if (pos_jc >= 0 & pos_jc < nc)
            {
                for (
                    int iter_ic = -CellList::MASK_GHOST_THICKNESS - 1 - (is_x_periodic & ic == 0);
                    iter_ic <= 1 + CellList::MASK_GHOST_THICKNESS;
                ++iter_ic
                    )
                {
                    int pos_ic = iter_ic + ic; if (is_x_periodic) pos_ic = POSITIVE_MOD(pos_ic, mc);
                    if (pos_ic >= 0 & pos_ic < mc)
                    {
                        int count = _count[pos_ic + pos_jc*mc];
                        count_total += count;
                    }
                }
            }
        }

        _mask[ic + jc*mc] = count_total > 0 ? 1 : 0;
    }

}

void CellList::build_mask()
{
    const int BSIZEX = 32;
    const int BSIZEY = 16;
    const dim3 block(BSIZEX, BSIZEY);
    dim3 grid((mc + BSIZEX - 1) / BSIZEX, (nc + BSIZEY - 1) / BSIZEY);
    kernel_build_mask <<<grid, block>>>
        (mask.data_ptr(), count.data_ptr(), mc, nc, is_x_periodic, is_y_periodic);
}


__global__
// step 1 rebuild block_count from cellList.count
// launch block must be dim3(NeighborList::M_BLOCK, NeighborList::N_BLOCK)
void kernel_update_block_list_step1
(
int * __restrict__ _block_count,
const int * __restrict__ _cell_count,
const int mc,
const int nc,
const int mb,
const int nb
)
{
    __shared__ int block_count;
    int ic = blockDim.x * blockIdx.x + threadIdx.x;
    int jc = blockDim.y * blockIdx.y + threadIdx.y;
    if (ic < mc & jc < nc)
    {

        if (threadIdx.x == 0 & threadIdx.y == 0) block_count = 0; __syncthreads();

        int cell_count = _cell_count[ic + jc*mc];

        if (cell_count > 0) atomicAdd(&block_count, cell_count); __syncthreads();

        if (threadIdx.x == 0 & threadIdx.y == 0)
        {
            int ib = blockIdx.x;
            int jb = blockIdx.y;
            _block_count[ib + jb * mb] = block_count;
            // debug, dump block_count to screen
            /*
            if (block_count>0)
                printf("block_count(%i,%i) = %i\n", ib, jb, _block_count[ib + jb * mb]);
            */
        }
        __syncthreads();
    }
}

__global__
// step 2 build block_flag (dilation operation by 3*3 kenel on non-zero points in block_count)
void kernel_update_block_list_step2
(
int* __restrict__ _block_flag,
const int* __restrict__ _block_count,
const int mb,
const int nb,
const bool is_x_periodic,
const bool is_y_periodic
)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    if (i < mb & j < nb)
    {
        int block_flag = 0;
        // scan block_count(i,j) and its neighbor
        // (defined by distance of 1 block, this may be non-trivial at the last block, when m%M_BLOCK !=0)
        // iter_jb starts from (-1 - (is_y_periodic&j == 0))
        // since the actual size of last cell can be less than cellListR if (m%cellListR!=0).
        // the same goes for i
        for (int iter_j = -1 - (is_y_periodic & j == 0); iter_j <= 1; ++iter_j)
        {
            int pos_j = iter_j + j; if (is_y_periodic) pos_j = POSITIVE_MOD(pos_j, nb);
            if (pos_j >= 0 & pos_j < nb)
            {
                for (int iter_i = -1 - (is_x_periodic & i == 0); iter_i <= 1; ++iter_i)
                {
                    int pos_i = iter_i + i; if (is_x_periodic) pos_i = POSITIVE_MOD(pos_i, mb);
                    if (pos_i >= 0 & pos_i < mb)
                    {
                        block_flag = block_flag | (_block_count[pos_i + pos_j*mb] > 0);
                    }
                }
            }
        }
        _block_flag[i + j*mb] = block_flag;
        // debug, dump block_flag to the screen
        /*
        if (_block_flag[i + j*mb] > 0){
            printf("block_flag(%i,%i) = %i\n", i, j, _block_flag[i + j*mb]);
        }
        */
    }
}

__global__
// step 4 fill block_list and copy it to host
// block_list is a sparse storage of block_flag, yes... in the most intuitive way
void kernel_update_block_list_step4
(
int* __restrict__ _block_list,
int* __restrict__ _device_block_list_len,
const int* __restrict__ _block_idx,
const int* __restrict__ _block_flag,
const int mb,
const int nb
)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    if (i < mb & j < nb)
    {
        int val = i + j*mb;
        int flag = _block_flag[val];
        if (flag)
        {
            int pos = _block_idx[val];
            _block_list[pos] = val;
        }

        // the last thread will write the block_list length to _device_block_list_len
        if (i == mb - 1 & j == nb - 1)
        {
            int prefix_sum = _block_idx[val];
            *_device_block_list_len = prefix_sum + flag;
        }
    }
}

void NeighborList::update_block_list(const CellList& cellList)
{
    const int& m_block_size = NeighborList::M_BLOCK;
    const int& n_block_size = NeighborList::N_BLOCK;
    const dim3 block_size(m_block_size, n_block_size);

    // step 0 clear block_count and block_flag
    block_count = 0;
    block_flag = 0;

    // step 1 rebuild block_count from cellList.count
    dim3 grid_step1((mc + m_block_size - 1) / m_block_size, (n + n_block_size - 1) / n_block_size);
    kernel_update_block_list_step1 <<<grid_step1, block_size >>>
        (block_count.data_ptr(), cellList.count.data_ptr(), mc, nc, mb, nb);

    // step 2 build block_flag (dilation operation by 3*3 kenel on non-zero points in block_count)
    dim3 grid_step2((mb + m_block_size - 1) / m_block_size, (nb + n_block_size - 1) / n_block_size);
    kernel_update_block_list_step2 <<<grid_step2, block_size >>>
        (block_flag.data_ptr(), block_count.data_ptr(), mb, nb, is_x_periodic, is_y_periodic);

    // step 3 compute block_idx from block_flag, using prefix sum
    prefixSum(block_flag.data_ptr(), block_idx.data_ptr(), mb*nb);

    // step 4 fill block_list and copy it to host
    kernel_update_block_list_step4 <<<grid_step2, block_size >>>
        (
        block_list.data_ptr(),
        device_block_list_len.data_ptr(),
        block_idx.data_ptr(),
        block_flag.data_ptr(),
        mb,
        nb
        );
    device_block_list_len.host(ptr_block_list_len);
    block_list.host(h_block_list);
}

__global__
void kernel_NeighborList_update
(
int* __restrict__ _id, // _id[pos + order * (block_list_capacity*M_BLOCK*N_BLOCK)]
int* __restrict__ _count,
const int* __restrict__ _block_list, // this is the sparse block_idx in NeighborList, not cuda block index
const int* __restrict__ _cellList_id,
const int* __restrict__ _cellList_count,
const int m,
const int n,
const int mc,
const int nc,
const int mb,
const int nb,
const bool is_x_periodic,
const bool is_y_periodic,
const int block_list_capacity, // CAPACITY!!! NOT len!!!! size(id) = (block_list_capacity*M_BLOCK*N_BLOCK, MAX_POINT_NUMBER_IN_CELL)
const int PROTECTION_LAYER_THICKNESS,
const int CUTOFF_DISTANCE
)
{
    // position in _id
    int pos = blockDim.x * blockIdx.x + threadIdx.x;

    // calculate (ic,jc)
    int block_idx = _block_list[blockIdx.x];
    int ib = block_idx % mb;
    int jb = block_idx / mb;
    int ic = ib * NeighborList::M_BLOCK + threadIdx.x % NeighborList::M_BLOCK;
    int jc = jb * NeighborList::N_BLOCK + threadIdx.x / NeighborList::M_BLOCK;

    if (ic < mc & jc < nc)
    {
        // boundary protection layer for neighbor scanning
        int dx_bp = is_x_periodic & (ic == 0) & (m % CellList::idr != 0);
        int dy_bp = is_y_periodic & (jc == 0) & (n % CellList::idr != 0);

        // neighbor thickness
        const int& dn = PROTECTION_LAYER_THICKNESS;
        const int cutoff = CUTOFF_DISTANCE / CellList::idr + (CUTOFF_DISTANCE%CellList::idr != 0); // change unit into cell size

        int block_list_capacity_of_cell = block_list_capacity* (NeighborList::M_BLOCK*NeighborList::N_BLOCK);

        int count_total = 0;
        int order_now = 0;
        for (int iter_jc = -dn - cutoff - dy_bp; iter_jc <= cutoff + dn; ++iter_jc)
        {
            int pos_jc = iter_jc + jc; if (is_y_periodic) pos_jc = POSITIVE_MOD(pos_jc, nc);
            if (pos_jc >= 0 & pos_jc < nc)
            {
                for (int iter_ic = -dn - cutoff - dx_bp; iter_ic <= cutoff + dn; ++iter_ic)
                {
                    int pos_ic = iter_ic + ic; if (is_x_periodic) pos_ic = POSITIVE_MOD(pos_ic, mc);
                    if (pos_ic >= 0 & pos_ic < mc)
                    {
                        int count = _cellList_count[pos_ic + pos_jc*mc];
                        if (count == 0) continue;
                        count_total += count;
                        for (int iter_id = 0; iter_id < count; ++iter_id)
                        {
                            int id = _cellList_id[pos_ic + pos_jc*mc + iter_id*mc*nc];
                            _id[pos + order_now * block_list_capacity_of_cell] = id;
                            order_now++;
                        }
                    }
                }
            }
        } // end construction of neighbor list
        _count[pos] = count_total;
    }
}

void NeighborList::update(const CellList& cellList)
{
    const int block_size = M_BLOCK * N_BLOCK;
    count = 0;
    kernel_NeighborList_update <<<*ptr_block_list_len, block_size >>>
        (
        id.data_ptr(),
        count.data_ptr(),
        block_list.data_ptr(),
        cellList.id.data_ptr(),
        cellList.count.data_ptr(),
        m,
        n,
        mc,
        nc,
        mb,
        nb,
        is_x_periodic,
        is_y_periodic,
        block_list_capacity,
        PROTECTION_LAYER_THICKNESS,
        CUTOFF_DISTANCE
        );
}
