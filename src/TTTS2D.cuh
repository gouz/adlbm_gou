#ifndef TTTS2D_cuh
#define TTTS2D_cuh

#include <iostream>
#include <fstream>
#include <cmath>
#include "LBMheader.cuh"

inline void TTTS2D()
{
    ConfigFile cfg("TTTS2D.txt");

    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", flag_run, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "switch", is_using_initial_flow_field, 0);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "switch", initial_flow_field, std::string("initial_flow_field_afdat"));

    if (flag_run == 0)
    {
        printf("flag_run==0, simulation TTTS2D not issued\n");
        return;
    }

    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", Tend, 1000000);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", viscosity, 0.166666666666666667);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_ves, 1000); // total export frames for vesicle shape
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", frames_flow, 10); // total export frames for field information

    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", flow_prestep, 1000000);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", is_x_periodic, 0);
    DEFINE_FROM_CONFIGFILE(int, cfg, "simulation", is_y_periodic, 0);

    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", fx_heart_left, 0e0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", fx_heart_right, 0e0);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", fy_heart_left, 1e-5);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "simulation", fy_heart_right, 1e-5);

    DEFINE_FROM_CONFIGFILE(std::string, cfg, "geometry", filename_bmp_two_bridge_circulation, std::string("ttts_bridge_circulation.bmp"));
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "geometry", filename_bmp_heart_left, std::string("ttts_heart_left.bmp"));
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "geometry", filename_bmp_heart_right, std::string("ttts_heart_right.bmp"));

    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", filename_vesicle_initial_pos, std::string("ttts_vesicle_initial_pos.txt"));
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", R0, 10.0); // characteristic radius for red blood cell in simulation dx unit
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", R0_phy, 3.0e-6); // characteristic radius for red blood cell (meter)
    REAL dx = R0_phy / R0;
    printf("dx = %f\n", dx);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", tau, 0.7); // reduced area
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", Ca, 10.0); // capillary number defined with wall shear rate at constriction
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", W_ref, 20.0); // a reference width to calculate capillary number, Ca = eta*4umax/W_ref*R0^3/kappa_bending
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", kl, 0.3); // vesicle strech modulus (set it adequately large tu ensure membrane inextensibility)
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", l0, 0.6667);
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", ks, 200.0); // area energy
    DEFINE_FROM_CONFIGFILE(REAL, cfg, "vesicle", damp, 0.01); // box size for initialization

    // high priority
    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", is_using_input_rbc_shapes, 0);
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", input_rbc_shapes_x_filename, std::string("In_x.txt"));
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", input_rbc_shapes_y_filename, std::string("In_y.txt"));

    // lower priority
    DEFINE_FROM_CONFIGFILE(int, cfg, "vesicle", is_using_initial_shape, 0); // if >1 then use the initial shape initial_shape_filename
    DEFINE_FROM_CONFIGFILE(std::string, cfg, "vesicle", initial_shape_filename, std::string("rbc_initial_shape_hint.txt"));

    BMPdataFile two_bridge_circulation_bmp(filename_bmp_two_bridge_circulation);
    BMPdataFile heart_left_bmp(filename_bmp_heart_left);
    BMPdataFile heart_right_bmp(filename_bmp_heart_right);

    // initialze size geometry from file;
    int m = two_bridge_circulation_bmp.get_width();
    int n = two_bridge_circulation_bmp.get_height();
    printf("read from %s simulation box (m,n) = (%i,%i)\n", filename_bmp_two_bridge_circulation.c_str(), m, n);

    // initialze label, Fx, and Fy
    DeviceArray<int> label_two_bridge_circulation(&two_bridge_circulation_bmp.get_binary()[0], m, n);
    label_two_bridge_circulation = -label_two_bridge_circulation;
    DeviceArray<REAL> label_heart_left = DeviceArray<REAL>(1 - DeviceArray<int>(&heart_left_bmp.get_binary()[0], m, n));
    DeviceArray<REAL> label_heart_right = DeviceArray<REAL>(1 - DeviceArray<int>(&heart_right_bmp.get_binary()[0], m, n));

    DeviceArray<REAL> Fx = fx_heart_left * label_heart_left + fx_heart_right * label_heart_right;
    DeviceArray<REAL> Fy = fy_heart_left * label_heart_left + fy_heart_right * label_heart_right;

    DeviceArray<int> label_flow = label_two_bridge_circulation;
    da_saveAF(label_flow);

    NS::BOUNDARY_TYPE x_boundary_type = is_x_periodic > 0 ? NS::PERIODIC : NS::BOUNCEBACK;
    NS::BOUNDARY_TYPE y_boundary_type = is_y_periodic > 0 ? NS::PERIODIC : NS::BOUNCEBACK;
    NSsolver flow(viscosity, m, n, x_boundary_type, y_boundary_type, Fx.data_ptr(), Fy.data_ptr(), label_flow.data_ptr());

    flow.ibm.xBoundary.af_save("xbound", "xbound.afdat");
    flow.ibm.yBoundary.af_save("ybound", "ybound.afdat");

    REAL umax = 0.0;
    if (is_using_initial_flow_field)
    {
        flow.initializeByFile(initial_flow_field.c_str());
    }
    else
    {
        flow.initializeByDensity();
        //pre-step for initializing flow field
        std::vector<REAL> hlabel(label_flow.size());
        for (int i = 0; i != flow_prestep; ++i)
        {
            if ((i + 1) % std::max(1, (flow_prestep / 100)) == 0 || i == 0 || i == flow_prestep - 1){
                std::cout << "prestep, iteration = " << i + 1;
                std::vector<REAL> hux(flow.ux.size());
                std::vector<REAL> huy(flow.ux.size());
                flow.ux.host(&hux[0]);
                flow.uy.host(&huy[0]);
                REAL umaxnow = 0.0;
                for (int j = (int)((double)hux.size()*0.45); j != (int)((double)hux.size()*0.55); ++j){
                    REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
                    umaxnow = std::max(umaxnow, uj);
                }
                std::cout << ", u_at_probe = " << umaxnow << std::endl;
                if (std::abs(umaxnow - umax) < 1e-9 && i > flow_prestep*0.01){
                    printf("u_at_probe reached its maximum, end prestep\n");
                    break;
                }
                umax = umaxnow;
            }
            flow.advanceFlowFieldWithoutImmersedObjects();
        }
        flow.af_save_flow_field(initial_flow_field.c_str());
    }

    // calculate umax
    std::vector<REAL> hux(flow.ux.size());
    std::vector<REAL> huy(flow.ux.size());
    flow.ux.host(&hux[0]);
    flow.uy.host(&huy[0]);
    REAL umaxnow = 0.0;
    for (int j = (int)((double)hux.size()*0.00); j != (int)((double)hux.size()*1.00); ++j)
    {
        REAL uj = std::sqrt(hux[j] * hux[j] + huy[j] * huy[j]);
        umaxnow = std::max(umaxnow, uj);
    }
    umax = umaxnow;

    // vesicle bending modulous
    REAL kb = viscosity * R0*R0*R0 / l0 * 16.0 * umax / W_ref / Ca;
    printf("vesicle bending modulous kb (simulation) = %f\n", kb);

    Vesicle ves_template(0.0, 0.0, 0.0, R0, tau, kb, kl, ks, l0);
    if (is_using_input_rbc_shapes == 0)
    {
        if (is_using_initial_shape == 0)
        {
            ves_template.relax(10000000, damp);
            ves_template.exportShape(initial_shape_filename.c_str());
        }
    }

    // make vesicles
    int nves;
    std::vector<Vesicle*> vec_vesicles;
    if (is_using_input_rbc_shapes)
    {
        DataFile input_shape_x(input_rbc_shapes_x_filename.c_str()); input_shape_x.transpose();
        DataFile input_shape_y(input_rbc_shapes_y_filename.c_str()); input_shape_y.transpose();
        nves = input_shape_x.get_n();
        vec_vesicles.resize(nves);
        int ves_size = input_shape_x.get_m();
        printf("Nvesicle = %i\n", nves);
        printf("vesicle size = %i\n", ves_size);

        for (int i = 0; i != nves; ++i)
        {
            vec_vesicles[i] = new Vesicle
                (
                &input_shape_x.getData()[i][0],
                &input_shape_y.getData()[i][0],
                ves_size, R0, tau, kb, kl, 200.0, l0,
                true
                );
            flow.immerseAnObject(vec_vesicles[i]);
        }
    }
    else
    {
        DataFile vesicle_initial_position_list(filename_vesicle_initial_pos);
        nves = vesicle_initial_position_list.get_n();
        printf("Nvesicle = %i\n", nves);
        vec_vesicles.resize(nves);
        for (int i = 0; i != nves; ++i)
        {
            REAL xpos = vesicle_initial_position_list.getData()[i][0];
            REAL ypos = vesicle_initial_position_list.getData()[i][1];
            REAL inclination = vesicle_initial_position_list.get_m() > 2 ? vesicle_initial_position_list.getData()[i][2] : 0.0;

            vec_vesicles[i] = new Vesicle(xpos, ypos, inclination, R0, tau, kb, kl, 200.0, l0, initial_shape_filename.c_str(), true);
            flow.immerseAnObject(vec_vesicles[i]);
        }
    }
    printf("%i vesicles' initialization complete.\n", nves);
    flow.immerseObjectFinished();

    SimpleTimer timer;

    timer.tic();

    for (int i = 0; i != Tend; ++i)
    {

        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->calculateForce();
        for (int ives = 0; ives != nves; ++ives)
            vec_vesicles[ives]->sync_operation();
        flow.ibm.add_short_range_repulsive_force();

        if (LBM::getStepStampFlag(i, frames_ves, Tend))
        {
            printf("i = %i, Tend = %i\n", i, Tend);
        }

        if (LBM::getStepStampFlag(i, frames_ves, Tend))
        {
            printf("vesicle shapes snapshot\n");
			flow.ibm.export_all_objects_shapes_af("sx.afdat", "sy.afdat", i != 0);
        }

        if (LBM::getStepStampFlag(i, frames_flow, Tend))
        {
            printf("flow field snapshot\n");
            flow.ux.af_save("ux", "ux.afdat", i != 0);
            flow.uy.af_save("uy", "uy.afdat", i != 0);

            // debug export
            /*
            flow.ibm.eul_fx.af_save("eul_fx", "eul_fx.afdat", i != 0);
            flow.ibm.eul_fy.af_save("eul_fy", "eul_fy.afdat", i != 0);
            auto fabs = sqrt(flow.ibm.eul_fx * flow.ibm.eul_fx + flow.ibm.eul_fy * flow.ibm.eul_fy);
            fabs.af_save("eul_f", "eul_f.afdat", i != 0);
            flow.ibm.cellList.count.af_save("cellListCount", "cellList_count.afdat", i != 0);
            flow.ibm.neighborList.id.af_save("neighbor_id", "neighbor_id.afdat", i != 0);
            flow.ibm.neighborList.count.af_save("neighbor_count", "neighbor_count.afdat", i != 0);
            flow.ibm.neighborList.block_idx.af_save("block_idx", "block_idx.afdat", i != 0, *flow.ibm.neighborList.ptr_block_list_len);
            */
        }

        flow.advanceFlowField();
        flow.advanceImmersedObjects();
    }
    cudaDeviceSynchronize();
    timer.toc();
}

#endif