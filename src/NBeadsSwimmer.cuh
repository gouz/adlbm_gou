#ifndef NBeadsSwimmer_cuh
#define NBeadsSwimmer_cuh

#ifndef PI
#define PI 3.141592656589793238464338327950
#endif

#include <cmath>
#include "ImmersedObject.cuh"
#include "ConfigFile.hpp"

// a N beads swimmer model from Mohd Suhail Rizvi
// mohd-suhail.Rizvi@univ-grenoble-alpes.fr
class NBeadsSwimmer : public ImmersedObject{
public: // private: // debug
    DeviceArray<REAL> alps; // phase difference
    REAL l0; // force-free length of spring
    REAL k; // active force amp 
    REAL f0; // spring factor
    REAL T; // swimming period
    int n; // number of beads
    REAL t; // time
    REAL rs; // radius of a bead (should be adequately comparing to spring length)
    REAL gamma; // fluid kinematic viscosity
    REAL rho; // fluid density

public:
    void calculateForce();
    inline NBeadsSwimmer
        (
        const REAL x0,
        const REAL y0,
        const REAL inclination,
        const REAL _l0,
        const REAL _k,
        const REAL _f0,
        const REAL _T,
        const REAL _rs,
        const REAL _gamma,
        const REAL _rho,
        const REAL _dx,
        const std::vector<REAL> _alps = std::vector<REAL>({3, 0.0}),
        const int _n = 3
        )
    {
        // constants
        l0 = _l0;
        k = _k;
        f0 = _f0;
        T = _T;
        n = _n;
        rs = _rs;
        gamma = _gamma;
        rho = _rho;
        t = 0.0;
        printf("N beads swimmer created:\n");
        printf("  x0 = %f\n", x0);
        printf("  y0 = %f\n", y0);
        printf("  l0 = %f\n", l0);
        printf("  k = %e\n", k);
        printf("  f0 = %e\n", f0);
        printf("  T = %f\n", T);
        printf("  n = %i\n", n);
        printf("  rs = %f\n", rs);
        printf("  gamma = %f\n", gamma);
        for (int i = 0; i != n; ++i){
            printf("  alp[%i] = %e\n", i, _alps[i]);
        }

        // initialize the shape
        REAL r = l0 * 0.5 / std::sin(PI / (REAL)n);
        
        printf("  l = %e\n", r);
        DeviceArray<REAL> theta = DA::range(0.0, 2.0*PI / (REAL)n, n) + inclination;
        x = r * cos(theta) + x0;
        y = r * sin(theta) + y0;
        
        // initialize the phase differnece
        alps = DeviceArray<REAL>(&_alps[0], n);

        // mem allocation for fx, fy, ux, uy
        fx = DeviceArray<REAL>(0.0, n);
        fy = DeviceArray<REAL>(0.0, n);
        ux = DeviceArray<REAL>(0.0, n);
        uy = DeviceArray<REAL>(0.0, n);

    }
    inline void setTime(const REAL _t){
        t = _t;
    }
    void move_freespace(const REAL dt);
    void move_in_fluid(const REAL dt = 1.0);
};

#endif