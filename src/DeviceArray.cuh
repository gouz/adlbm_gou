#ifndef DeviceArray_cuh
#define DeviceArray_cuh

//#define DA_DEBUG // Tag for debug

//*************************************************************************************************
// DeviceArray<T> is a simple warpper for CUDA device array.
// Its style follows ARRAYFIRE (http://arrayfire.org/). But it's simpler and more light-weight
//
// To use DeviceArray<T> you just simply include "DeviceArray.cuh" in your code header
//
// Each DeviceArray<T> object holds a pointer to ONLY ONE piece of continuous device (GPU) memory
// through cudaMalloc() function. it will be free automatically when a DeviceArray object is destructed
//
// DA namespace is occupied for relevant functions, kernels are also prefixed with _DA_kernel
//
// Dimension is supported up to 4
//
// UNLIKE Arrayfire, chaining operators in DeviceArray are not optimized, which means N operator
// calls will have N kernel launches
//
// Supported functions/operators:
//      basic unary / binary arithmetic operators
//      matrix operations: reshape(), transpose(), tile()
//      series generator range(), join()
//      helper (or helper member) functions: dim(), data_ptr(), swap(), da_print(), af_save(),
//                                           save2D(), isempty() etc...
//
// cuda operations are all executed in the default cuda stream
//
// NOT-YET implemented: (write your own kernels)
//      all redunction operations: max / min / sum
//      etc...
// Now they are implemented 201806
//
// Hengdi ZHANG
// Laboratoire Interdisciplinaire de Physique (LiPhy)
// 140 Rue de la Physique, 38402 Saint-Martin-d'Heres
// France
// Mar 15, 2017
//
// Add reset function by Zhe Gou at 20191122
//
//  Example code:
/*
    using namespace DA;
    const int m = 9, n = m;
    const double x0 = -1.0, x1 = 1.0, dx = (x1 - x0) / m;
    const double y0 = -1.0, y1 = 1.0, dy = (y1 - y0) / n;
    DeviceArray<double> x = tile(range(x0, dx, m + 1), 1, n + 1);
    DeviceArray<double> y = tile(transpose(range(y0, dy, n + 1)), m + 1, 1);
    da_print(x);
    da_print(y);
    auto r2 = x*x + y*y;
    da_print( -sqrt(r2) * (r2 > 0.7*0.7 && r2 < 0.9*0.9) );
*/
// The above code will print:
/*
    DeviceArray<double> x:
    dimension = [10, 10, 1, 1]
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1
    -1   -0.778   -0.556   -0.333   -0.111    0.111    0.333    0.556    0.778        1

    DeviceArray<double> y:
    dimension = [10, 10, 1, 1]
    -1       -1       -1       -1       -1       -1       -1       -1       -1       -1
    -0.778   -0.778   -0.778   -0.778   -0.778   -0.778   -0.778   -0.778   -0.778   -0.778
    -0.556   -0.556   -0.556   -0.556   -0.556   -0.556   -0.556   -0.556   -0.556   -0.556
    -0.333   -0.333   -0.333   -0.333   -0.333   -0.333   -0.333   -0.333   -0.333   -0.333
    -0.111   -0.111   -0.111   -0.111   -0.111   -0.111   -0.111   -0.111   -0.111   -0.111
     0.111    0.111    0.111    0.111    0.111    0.111    0.111    0.111    0.111    0.111
     0.333    0.333    0.333    0.333    0.333    0.333    0.333    0.333    0.333    0.333
     0.556    0.556    0.556    0.556    0.556    0.556    0.556    0.556    0.556    0.556
     0.778    0.778    0.778    0.778    0.778    0.778    0.778    0.778    0.778    0.778
    1        1        1        1        1        1        1        1        1        1

    DeviceArray<double> -sqrt(r2) * (r2 > 0.7*0.7 && r2 < 0.9*0.9):
    dimension = [10, 10, 1, 1]
     0        0        0        0        0        0        0        0        0        0
     0        0        0   -0.846   -0.786   -0.786   -0.846        0        0        0
     0        0   -0.786        0        0        0        0   -0.786        0        0
     0   -0.846        0        0        0        0        0        0   -0.846        0
     0   -0.786        0        0        0        0        0        0   -0.786        0
     0   -0.786        0        0        0        0        0        0   -0.786        0
     0   -0.846        0        0        0        0        0        0   -0.846        0
     0        0   -0.786        0        0        0        0   -0.786        0        0
     0        0        0   -0.846   -0.786   -0.786   -0.846        0        0        0
     0        0        0        0        0        0        0        0        0        0
*/
//*************************************************************************************************

//*************************************************************************************************

// CUDA header
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

// io header
#include <iostream>
#include <iomanip>
#include <fstream>

// i don't want to implement "summation" on DeviceArray myself
// DeviceArray<REAL>::data_ptr is allocated by cudaMalloc
// its' ownership to data will temporarily pass to a thrust::device_vector (not copying data)
// thrust::device_vector provides reduce operation and add operation, etc.

// cuda thrust header
#include "thrust/host_vector.h"
#include "thrust/device_vector.h"
#include "thrust/device_ptr.h"

// others
#include <algorithm>

// cuda thrust header for some algorithm implementation

//*************************************************************************************************
// af_dtype defination from arrayfire
// http://arrayfire.org/docs/defines_8h_source.htm
typedef enum {
    f32,    //< 32-bit floating point values
    c32,    //< 32-bit complex floating point values
    f64,    //< 64-bit complex floating point values
    c64,    //< 64-bit complex floating point values
    b8,     //< 8-bit boolean values
    s32,    //< 32-bit signed integral values
    u32,    //< 32-bit unsigned integral values
    u8,     //< 8-bit unsigned integral values
    s64,    //< 64-bit signed integral values
    u64,    //< 64-bit unsigned integral values
    s16,    //< 16-bit signed integral values
    u16,    //< 16-bit unsigned integral values
} af_dtype;

// af_dtype pointer type identifier
template<typename T>
inline af_dtype af_ptr_typeof(const T* data_ptr){ return s32; }
template<>
inline af_dtype af_ptr_typeof<float>(const float* data_ptr){ return f32; }
template<>
inline af_dtype af_ptr_typeof<double>(const double* data_ptr){ return f64; }
template<>
inline af_dtype af_ptr_typeof<bool>(const bool* data_ptr){ return b8; }
template<>
inline af_dtype af_ptr_typeof<int>(const int* data_ptr){ return s32; }
template<>
inline af_dtype af_ptr_typeof<unsigned int>(const unsigned int* data_ptr){ return u32; }
template<>
inline af_dtype af_ptr_typeof<long long int>(const long long int* data_ptr){ return s64; }
template<>
inline af_dtype af_ptr_typeof<unsigned long long int>(const unsigned long long int* data_ptr){ return u64; }
template<>
inline af_dtype af_ptr_typeof<short int>(const short int* data_ptr){ return s16; }
template<>
inline af_dtype af_ptr_typeof<unsigned short int>(const unsigned short int* data_ptr){ return u16; }
// ************************************************************************************************
// DeviceArray operator overloading kernels
#define _DA_OP_KERNEL(op_name) _DA_kernel_DeviceArray_Operator_##op_name

// <0>
// unary operator kernel
// expr has a form of a[i] = -a[i]
#define _DA_DEFINE_UNR_OP_KERNEL(op_name, op_notation) \
template<typename T> \
__global__ void _DA_OP_KERNEL(op_name)(T* a, const int n){ \
    int i = blockDim.x * blockIdx.x + threadIdx.x; if (i < n) a[i] = op_notation(a[i]); }
_DA_DEFINE_UNR_OP_KERNEL(uminus, -)
_DA_DEFINE_UNR_OP_KERNEL(not, !)
_DA_DEFINE_UNR_OP_KERNEL(abs, abs)
_DA_DEFINE_UNR_OP_KERNEL(sqrt, sqrt)
_DA_DEFINE_UNR_OP_KERNEL(log, log)
_DA_DEFINE_UNR_OP_KERNEL(log2, log2)
_DA_DEFINE_UNR_OP_KERNEL(log10, log10)
_DA_DEFINE_UNR_OP_KERNEL(exp, exp)
_DA_DEFINE_UNR_OP_KERNEL(exp2, exp2)
_DA_DEFINE_UNR_OP_KERNEL(exp10, exp10)
_DA_DEFINE_UNR_OP_KERNEL(sin, sin)
_DA_DEFINE_UNR_OP_KERNEL(asin, asin)
_DA_DEFINE_UNR_OP_KERNEL(sinh, sinh)
_DA_DEFINE_UNR_OP_KERNEL(cos, cos)
_DA_DEFINE_UNR_OP_KERNEL(acos, acos)
_DA_DEFINE_UNR_OP_KERNEL(cosh, cosh)
_DA_DEFINE_UNR_OP_KERNEL(tan, tan)
_DA_DEFINE_UNR_OP_KERNEL(atan, atan)
_DA_DEFINE_UNR_OP_KERNEL(tanh, tanh)
_DA_DEFINE_UNR_OP_KERNEL(ceil, ceil)
_DA_DEFINE_UNR_OP_KERNEL(floor, floor)
_DA_DEFINE_UNR_OP_KERNEL(round, round)

template<typename T>
__global__ void _DA_OP_KERNEL(not)(bool* a, const int n){
        int i = blockDim.x * blockIdx.x + threadIdx.x;
        if (i < n) a[i] = !a[i];
}

// <1>
// binary operation kernel
// type_lhs, type_rhs can be T or T*
// expr has a form of a[i] += b[i]
#define _DA_DEFINE_BIN_OP_KERNEL(op_name, type_lhs, type_rhs, expr) \
template<typename T> \
__global__ void _DA_OP_KERNEL(op_name)(type_lhs a, const type_rhs b, const int n){ \
    int i = blockDim.x * blockIdx.x + threadIdx.x; if (i < n) expr; }
_DA_DEFINE_BIN_OP_KERNEL(add, T*, T*, a[i] += b[i])         // addition kernel
_DA_DEFINE_BIN_OP_KERNEL(add, T*, T, a[i] += b)             // addition kernel
_DA_DEFINE_BIN_OP_KERNEL(mul, T*, T*, a[i] *= b[i])         // multiplication kernel
_DA_DEFINE_BIN_OP_KERNEL(mul, T*, T, a[i] *= b)             // multiplication kernel
_DA_DEFINE_BIN_OP_KERNEL(minus, T*, T*, a[i] -= b[i])       // minus kernel
_DA_DEFINE_BIN_OP_KERNEL(minus, T*, T, a[i] -= b)           // minus kernel
_DA_DEFINE_BIN_OP_KERNEL(minus_rhs, T*, T, a[i] = b - a[i]) // minus kernel, with a on the right-hand-side
_DA_DEFINE_BIN_OP_KERNEL(div, T*, T*, a[i] /= b[i])         // division kernel
_DA_DEFINE_BIN_OP_KERNEL(div, T*, T, a[i] /= b)             // division kernel
_DA_DEFINE_BIN_OP_KERNEL(div_rhs, T*, T, a[i] = b / a[i])   // division kernel, with a on the right-hand-side
_DA_DEFINE_BIN_OP_KERNEL(mod, T*, T*, a[i] %= b[i])         // division kernel
_DA_DEFINE_BIN_OP_KERNEL(mod, T*, T, a[i] %= b)             // division kernel
_DA_DEFINE_BIN_OP_KERNEL(mod_rhs, T*, T, a[i] = b % a[i])   // division kernel, with a on the right-hand-side

// <2>
// comparison operation kernel
// type_lhs, type_rhs can be T or T*
// expr has a form of out[i] = a[i] > b[i]
#define _DA_DEFINE_CMP_OP_KERNEL(op_name, type_lhs, type_rhs, expr) \
template<typename T> \
__global__ void _DA_OP_KERNEL(op_name)(bool* out, type_lhs a, const type_rhs b, const int n){ \
    int i = blockDim.x * blockIdx.x + threadIdx.x; if (i < n) expr; }
_DA_DEFINE_CMP_OP_KERNEL(GT, T*, T*, out[i] = a[i] > b[i])      // Greater-Than kernel
_DA_DEFINE_CMP_OP_KERNEL(GT, T*, T, out[i] = a[i] > b)          // Greater-Than kernel
_DA_DEFINE_CMP_OP_KERNEL(GT_rhs, T*, T, out[i] = b > a[i])      // Greater-Than kernel, with a on the right-hand-side
_DA_DEFINE_CMP_OP_KERNEL(GE, T*, T*, out[i] = a[i] >= b[i])     // Greater-or-Equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(GE, T*, T, out[i] = a[i] >= b)         // Greater-or-Equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(GE_rhs, T*, T, out[i] = b >= a[i])     // Greater-or-Equal-to kernel, with a on the right-hand-side

_DA_DEFINE_CMP_OP_KERNEL(LT, T*, T*, out[i] = a[i] < b[i])      // Less-Than kernel
_DA_DEFINE_CMP_OP_KERNEL(LT, T*, T, out[i] = a[i] < b)          // Less-Than kernel
_DA_DEFINE_CMP_OP_KERNEL(LT_rhs, T*, T, out[i] = b < a[i])      // Less-Than kernel, with a on the right-hand-side
_DA_DEFINE_CMP_OP_KERNEL(LE, T*, T*, out[i] = a[i] <= b[i])     // Less-or-Equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(LE, T*, T, out[i] = a[i] <= b)         // Less-or-Equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(LE_rhs, T*, T, out[i] = b <= a[i])     // Less-or-Equal-to kernel, with a on the right-hand-side

_DA_DEFINE_CMP_OP_KERNEL(EQ, T*, T*, out[i] = a[i] == b[i])     // equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(EQ, T*, T, out[i] = a[i] == b)         // equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(EQ_rhs, T*, T, out[i] = b == a[i])     // equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(NE, T*, T*, out[i] = a[i] != b[i])     // not-equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(NE, T*, T, out[i] = a[i] != b)         // not-equal-to kernel
_DA_DEFINE_CMP_OP_KERNEL(NE_rhs, T*, T, out[i] = b != a[i])     // not-equal-to kernel

_DA_DEFINE_CMP_OP_KERNEL(AND, T*, T*, out[i] = a[i] & b[i])    // logical AND kernel, bitwise op avoids branching?
_DA_DEFINE_CMP_OP_KERNEL(OR, T*, T*, out[i] = a[i] | b[i])     // logical AND kernel, bitwise op avoids branching?

// ************************************************************************************************
// kernels for DeviceArray
template<typename T> __global__
void _DA_kernel_DeviceArray_OperatorEqual_const(T* darr, const int n, const T value){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < n) darr[i] = value;
}
template<typename T, typename T2> __global__
void _DA_kernel_DeviceArray_OperatorEqual_array(T* darr, const T2* darr_in, const int n){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < n) darr[i] = (T)darr_in[i];
}
template<typename T> __global__
void _DA_kernel_DeviceArray_OperatorEqual_array(T* darr, const T* darr_in, const int n){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < n) darr[i] = darr_in[i];
}
template<typename T> __global__
void _DA_kernel_DeviceArray_transpose(T* __restrict AT, const T* const  __restrict A, const int m, const int n){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    int idy = threadIdx.y + blockDim.y*blockIdx.y;
    // pad to avoid bank-conflicts
    // make sure blockDim = [32, 32] when calling this kernel
    __shared__ T s_A[32][32 + 1];
    // check this is a legal matrix entry
    if (idx < m && idy < n) s_A[threadIdx.y][threadIdx.x] = A[idx + idy*m];
    // make sure all threads in this thread-block
    // have read into shared
    __syncthreads();
    // find coordinates of thread in transposed block
    int idxT = threadIdx.x + blockDim.y*blockIdx.y;
    int idyT = threadIdx.y + blockDim.x*blockIdx.x;
    // output
    if (idxT < n && idyT < m) AT[idxT + idyT*n] = s_A[threadIdx.x][threadIdx.y];
}

template<typename T> __global__
void _DA_kernel_DeviceArray_tile(T* __restrict AT, const T* const  __restrict A,
const int m0, const int n0, const int k0, const int l0,
const int mT, const int nT, const int kT, const int lT){
    int idx0123 = blockDim.x * blockIdx.x + threadIdx.x;
    int D0 = m0 * mT;
    int D1 = n0 * nT;
    int D2 = k0 * kT;
    int D3 = l0 * lT;
    int D01 = D0 * D1;
    int D012 = D01 * D2;
    int D0123 = D012 * D3;

    if (idx0123 < D0123) {
        int idx012 = idx0123 % D012;
        int idx01 = idx012 % D01;
        int idx3 = (idx0123 / D012) % l0;
        int idx2 = (idx012 / D01) % k0;
        int idx1 = (idx01 / D0) % n0;
        int idx0 = (idx01 % D0) % m0;
        int idx = idx0 + m0 * (idx1 + n0 * (idx2 + idx3*k0));
        AT[idx0123] = A[idx];
    }
}

// kernel function for reset
// added by Zhe Gou at 20191122
template<typename T> __global__
void _DA_kernel_reset(T* darr, const int n, const T value){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < n && darr[i] > -1)
            darr[i] = value;
}
// ************************************************************************************************
// ************************************************************************************************
// ************************************************************************************************
// DeviceArray definition starts here
// ************************************************************************************************
// ************************************************************************************************
// ************************************************************************************************

template<typename T>
// A simple warpper of array on device memory
// it provides similar basic functions as ArrayFire Array object (see http://arrayfire.org)
// namespace DA contains related simple tools
// Be careful:
// arithmetic operators between array with incompatible (or not) dimension may fail your previously
// set data_ptr
class DeviceArray{
    template<typename T2> friend class DeviceArray;

private:
    int dims[4]; // array dimensions
    int data_size;
    T* data;

private:
    inline void safe_cudaFree(){
        if (nullptr != data){
#ifdef DA_DEBUG
            std::cout << "cudaFree called by save_cudaFree" << std::endl;
            std::cout << "before cudaFree data pointer = " << data << std::endl;
#endif
            cudaFree(data);
            data = nullptr;
#ifdef DA_DEBUG
            std::cout << "after cudaFree data pointer = " << data << std::endl;
#endif
        }
    }

public:
    //*************************************************************************************************
    inline int size() const { return data_size; }
    inline void clear() {
        safe_cudaFree();
        dims[0] = 0;
        dims[1] = 0;
        dims[2] = 0;
        dims[3] = 0;
        data_size = 0;
    }
    //*************************************************************************************************
    inline int dim(const int i) const { return dims[i]; }
    inline int& dim_ref(const int i) { return dims[i]; }
    inline bool isempty() const { return nullptr == data; }
    //*************************************************************************************************
    void realloc(const int m, const int n = 1, const int k = 1, const int l = 1){
#ifdef DA_DEBUG
        if (m < 1){
            std::cout << "DeviceArray<T>::realloc error, wrong size" << std::endl;
            return;
        }
        std::cout << "DeviceArray<T>::realloc called" << std::endl;
        if (!isempty()) std::cout << "    cudaFree, cudaMalloc called" << std::endl;
        else std::cout << "    cudaMalloc called" << std::endl;
#endif
        safe_cudaFree();
        dims[0] = m;
        dims[1] = n;
        dims[2] = k;
        dims[3] = l;
        data_size = dims[0] * dims[1] * dims[2] * dims[3];
        if (data_size > 0){
            cudaError_t eror = cudaMalloc((void**)&data, data_size*sizeof(T));
#ifdef DA_DEBUG
            std::cout << "cudaMalloc: cudaError_t error = " << eror << std::endl;
            std::cout << "data pointer = " << data << std::endl;
            if (eror != 0){
                data = nullptr;
                std::cout << "data pointer reset to nullptr" << eror << std::endl;
            }
#endif
        }
    }
    //*************************************************************************************************
    // deconstructor
    ~DeviceArray(){
#ifdef DA_DEBUG
        std::cout << "DeviceArray::~DeviceArray() called" << std::endl;
        if (!isempty()) std::cout << "    cudaFree called" << std::endl;
#endif
        safe_cudaFree();
    }
    //*************************************************************************************************
    // copy constructor
    DeviceArray(const DeviceArray& darr) : data(nullptr){

        realloc(darr.dims[0], darr.dims[1], darr.dims[2], darr.dims[3]);
#ifdef DA_DEBUG
        std::cout << "DeviceArray copy constructor called" << std::endl;
#endif
        if (!isempty()){
#ifdef DA_DEBUG
            std::cout << "    cudaMalloc, cudaMemcpy called" << std::endl;
#endif
            // deep copy on device
            cudaMemcpy(data, darr.data, data_size * sizeof(T), cudaMemcpyDeviceToDevice);
        }
    }
    //*************************************************************************************************
    // move constructor
    inline DeviceArray(DeviceArray&& darr) : data(nullptr){
#ifdef DA_DEBUG
        std::cout << "DeviceArray move constructor called" << std::endl;
#endif
        dims[0] = darr.dims[0];
        dims[1] = darr.dims[1];
        dims[2] = darr.dims[2];
        dims[3] = darr.dims[3];
        data_size = darr.data_size;
        data = darr.data;
        darr.data = nullptr;
    }
    //*************************************************************************************************
    // move assignment operator
    inline DeviceArray& operator=(DeviceArray&& r_darr){
        if (this == &r_darr) {
#ifdef DA_DEBUG
            std::cout << "DeviceArray move assignment operator called, self assignment" << std::endl;
#endif
            return *this;
        }
#ifdef DA_DEBUG
        std::cout << "DeviceArray move assignment operator called" << std::endl;
#endif
        safe_cudaFree();
        dims[0] = r_darr.dims[0];
        dims[1] = r_darr.dims[1];
        dims[2] = r_darr.dims[2];
        dims[3] = r_darr.dims[3];
        data_size = r_darr.data_size;
        data = r_darr.data;
        r_darr.data = nullptr;
        return *this;
    }
    //*************************************************************************************************
    // assignment operator
    inline DeviceArray& operator=(const DeviceArray& darr){
        if (this == &darr) {
#ifdef DA_DEBUG
            std::cout << "DeviceArray assignment operator called, self assignment" << std::endl;
#endif
            return *this;
        }
        safe_cudaFree();
        realloc(darr.dims[0], darr.dims[1], darr.dims[2], darr.dims[3]);
        const int BN = 256;
        if (!isempty()){
#ifdef DA_DEBUG
            std::cout << "DeviceArray assignment operator called" << std::endl;
            std::cout << "    cudaFree(if not empty), cudaMalloc, assignment kernel are called" << std::endl;
#endif
            _DA_kernel_DeviceArray_OperatorEqual_array <<<(data_size + BN - 1) / BN, BN >>>(data, darr.data, data_size);
        }
        return *this;
    }
    //*************************************************************************************************
    // assignment by const value of other type
    template<typename T2>
    inline DeviceArray<T>& operator=(const T2 value){
        if (isempty()) return *this;
#ifdef DA_DEBUG
        std::cout << "DeviceArray assignment by other type const value called" << std::endl;
        std::cout << "    assignment kernel is called" << std::endl;
#endif
        const int BN = 256;
        if (data_size == 0) return *this;
        _DA_kernel_DeviceArray_OperatorEqual_const<<<(data_size + BN - 1) / BN, BN >>>(data, data_size, (T)value);
        return *this;
    }
    //*************************************************************************************************
    // assignment by array of other type
    template<typename T2>
    inline DeviceArray& operator=(const DeviceArray<T2>& darr){
#ifdef DA_DEBUG
        std::cout << "assigned by a different type of array" << std::endl;
#endif
        safe_cudaFree();
        realloc(darr.dims[0], darr.dims[1], darr.dims[2], darr.dims[3]);
        const int BN = 256;
        if (!isempty()){
#ifdef DA_DEBUG
            std::cout << "DeviceArray assignment by other type of array called" << std::endl;
            std::cout << "    cudaFree, cudaMalloc, assignment kernel are called" << std::endl;
#endif
            _DA_kernel_DeviceArray_OperatorEqual_array <<< (data_size + BN - 1) / BN, BN >>> (data, darr.data, data_size);
        }
        return *this;
    }
    //*************************************************************************************************
    // move(no effect) assignment by array of other type
    template<typename T2>
    inline DeviceArray& operator=(DeviceArray<T2>&& darr){
#ifdef DA_DEBUG
        std::cout << "assigned by a different type of array" << std::endl;
#endif
        safe_cudaFree();
        realloc(darr.dims[0], darr.dims[1], darr.dims[2], darr.dims[3]);
        const int BN = 256;
        if (!isempty()){
#ifdef DA_DEBUG
            std::cout << "DeviceArray assignment by other type of array called" << std::endl;
            std::cout << "    cudaFree, cudaMalloc, assignment kernel are called" << std::endl;
#endif
            _DA_kernel_DeviceArray_OperatorEqual_array <<< (data_size + BN - 1) / BN, BN >>> (data, darr.data, data_size);
        }
        return *this;
    }
    //*************************************************************************************************
    // default constructor
    DeviceArray(){
        dims[0] = 0;
        dims[1] = 0;
        dims[2] = 0;
        dims[3] = 0;
        data_size = dims[0] * dims[1] * dims[2] * dims[3];
        data = nullptr;
    }
    //*************************************************************************************************
    // constructor by array of other type
    template<typename T2>
    DeviceArray(const DeviceArray<T2>& darr) : data(nullptr){
        realloc(darr.dims[0], darr.dims[1], darr.dims[2], darr.dims[3]);
        if (!isempty()){
#ifdef DA_DEBUG
            std::cout << "DeviceArray constructor by other type of array called" << std::endl;
            std::cout << "    cudaFree, cudaMalloc, assignment kernel are called" << std::endl;
#endif
            const int BN = 256;
            _DA_kernel_DeviceArray_OperatorEqual_array <<<(data_size + BN - 1) / BN, BN >>>(data, darr.data, data_size);
        }
    }
    //*************************************************************************************************
    // constructor by const value
    DeviceArray(const T value, const int m = 1, const int n = 1, const int k = 1, const int l = 1) : data(nullptr){
        realloc(m, n, k, l);
#ifdef DA_DEBUG
        if(size() < 1) return;
        std::cout << "DeviceArray constructor by const value called" << std::endl;
        std::cout << "    cudaMalloc, assignment kernel are called" << std::endl;
#endif
        const int BN = 256;
        if (data_size == 0) return;
        _DA_kernel_DeviceArray_OperatorEqual_const <<<(data_size + BN - 1) / BN, BN >>>(data, data_size, value);
    }
    //*************************************************************************************************
    // constructor by host array
    DeviceArray(const T* h_array, const int m = 1, const int n = 1, const int k = 1, const int l = 1) : data(nullptr){
        realloc(m, n, k, l);
#ifdef DA_DEBUG
        if (size() < 1) return;
        if (nullptr == h_array) return;
        std::cout << "DeviceArray constructor by host array called" << std::endl;
        std::cout << "    cudaMalloc, assignment kernel are called" << std::endl;
#endif
        // cudaPointerGetAttributes() NOT WORKING CORRECTLY!
        /*
        cudaPointerAttributes h_array_attributes;
        cudaError_t eror = cudaPointerGetAttributes(&h_array_attributes, h_array);
        if (h_array_attributes.memoryType == cudaMemoryTypeHost){
#ifdef DA_DEBUG
            std::cout << "      cudaMemcpyHostToDevice" << std::endl;
#endif
            eror = cudaMemcpy(data, h_array, data_size * sizeof(T), cudaMemcpyHostToDevice);
        }
        else{
#ifdef DA_DEBUG
            std::cout << "      cudaMemcpyDeviceToDevice" << std::endl;
#endif
            eror = cudaMemcpy(data, h_array, data_size * sizeof(T), cudaMemcpyDeviceToDevice);
        }
        */
        cudaError_t eror = cudaMemcpy(data, h_array, data_size * sizeof(T), cudaMemcpyHostToDevice);
    }
    //*************************************************************************************************
    // device data pointer
    inline T* data_ptr() const { return data; }
    //*************************************************************************************************
    // memcpy from DeviceArray to host pointer
    // malloc on host should be done firstly by the user.
    inline void host(const T* h_ptr) const {
        if (nullptr == data){
#ifdef DA_DEBUG
            std::cout << "DeviceArray::host(const T* h_ptr) warning: empty DeviceArray" << std::endl;
#endif
            return;
        }
        cudaError_t eror = cudaMemcpy((void *)h_ptr, data, data_size * sizeof(T), cudaMemcpyDeviceToHost);
    }
    //*************************************************************************************************
    // transpose 1D/2D array
    void transpose(){
        if (isempty()) return;
        if (dims[2] > 1 || dims[3] > 1) return;
        if (dims[0] == 1 || dims[1] == 1){
            std::swap(dims[0], dims[1]);
            return;
        }
        T* data_transposed = nullptr;
        cudaError_t eror = cudaMalloc((void**)&data_transposed, data_size*sizeof(T));
#ifdef DA_DEBUG
        std::cout << "DeviceArray::transpose() called" << std::endl;
        std::cout << "    cudaMalloc, transpose kernel are called" << std::endl;
        std::cout << "transpose cudaMalloc, new data pointer = " << data << std::endl;
        std::cout << "cudaMalloc: cudaError_t error = " << eror << std::endl;
#endif
        const int BN = 32;
        const dim3 bDim(BN, BN, 1);
        dim3 gDim((dims[0] + BN - 1) / BN, (dims[1] + BN - 1) / BN, 1);
        _DA_kernel_DeviceArray_transpose <<<gDim, bDim >>> (data_transposed, data, dims[0], dims[1]);
        std::swap(data_transposed, data);
        cudaFree(data_transposed);
        std::swap(dims[0], dims[1]);
        return;
    }
    //*************************************************************************************************
    void tile(const int mT, const int nT = 1, const int kT = 1, const int lT = 1){
        if (isempty()) return;
        data_size *= mT * nT * kT * lT;
        T* data_tiled = nullptr;
        const int BN = 256;
        cudaError_t eror = cudaMalloc((void**)&data_tiled, data_size*sizeof(T));
#ifdef DA_DEBUG
        std::cout << "tile cudaMalloc, new data pointer = " << data << std::endl;
        std::cout << "cudaMalloc: cudaError_t error = " << eror << std::endl;
#endif
        _DA_kernel_DeviceArray_tile <<< (data_size + BN - 1) / BN, BN >>> (data_tiled, data, dims[0], dims[1], dims[2], dims[3], mT, nT, kT, lT);
        std::swap(data_tiled, data);
        cudaFree(data_tiled);
        dims[0] *= mT;
        dims[1] *= nT;
        dims[2] *= kT;
        dims[3] *= lT;
        return;
    }
    //*************************************************************************************************
    // shallow swap
    inline void swap(DeviceArray& darr){
        std::swap(dims, darr.dims);
        std::swap(data_size, darr.data_size);
        std::swap(data, darr.data);
    }
    // swap data pointer ONLY
    // be sure they have same dimensions
    inline void swap_data_ptr(DeviceArray& darr){
#ifdef DA_DEBUG
        if (data_size!= darr.data_size){
            std::cout << "DeviceArray::swap_data_ptr(darr) error: data_size incompatible, function will do nothing" << std::endl;
        }
#endif
        //std::swap(data, darr.data);
        T* temp = data;
        data = darr.data;
        darr.data = temp;
    }
    //*************************************************************************************************
    // make DeviceArray to 1d, this is a noop
    void flat(){
        if (!isempty()){
            dims[0] = data_size;
            dims[1] = 1;
            dims[2] = 1;
            dims[3] = 1;
        }
        return;
    }
    //*************************************************************************************************
    void reshape(const int m, const int n){
        if (!isempty()){
            if (m*n == data_size){
                dims[0] = m;
                dims[1] = n;
                dims[2] = 1;
                dims[3] = 1;
            }
            else{
                std::cout << "DeviceArray::reshape error: wrong size" << std::endl;
            }
        }
        return;
    }
    void reshape(const int m, const int n, const int k){
        if (!isempty()){
            if (m*n*k == data_size){
                dims[0] = m;
                dims[1] = n;
                dims[2] = k;
                dims[3] = 1;
            }
            else{
                std::cout << "DeviceArray::reshape error: wrong size" << std::endl;
            }
        }
        return;
    }
    void reshape(const int m, const int n, const int k, const int l){
        if (!isempty()){
            if (m*n*k == data_size){
                dims[0] = m;
                dims[1] = n;
                dims[2] = k;
                dims[3] = l;
            }
            else{
                std::cout << "DeviceArray::reshape error: wrong size" << std::endl;
            }
        }
        return;
    }
    //*************************************************************************************************
    // display array in 2D form
    void display(const char* array_name = "", const int precision = 2) const {
        /*
        const size_t M_MAX = 10;
        const size_t N_MAX = 25;
        size_t M_stride = dims[0] / M_MAX;
        size_t N_stride = dims[1] / N_MAX;
        */
        size_t M_stride = 1;
        size_t N_stride = 1;

        using namespace std;

        af_dtype array_type = af_ptr_typeof(this->data);
        cout << "DeviceArray";
        switch (array_type)
        {
        case f32: cout << "<float>"; break;
        case c32: cout << "<complex float>"; break;
        case f64: cout << "<double>"; break;
        case c64: cout << "<complex double>"; break;
        case b8: cout << "<bool>"; break;
        case s32: cout << "<int>"; break;
        case u32: cout << "<unsigned int>"; break;
        case u8: cout << "<unsigned short short int>"; break;
        case s64: cout << "<long int>"; break;
        case u64: cout << "<unsigned long int>"; break;
        case s16: cout << "<short int>"; break;
        case u16: cout << "<short unsigned int>"; break;
        default: break;
        }
        cout << " " << array_name << ":" << endl;

        if (data_size < 1){
            cout << "-empty array-" << endl;
            return;
        }

        // copy data to host memory
        T* h_data_ptr;
        if (data_size > 0){
            h_data_ptr = new T[data_size];
#ifdef DA_DEBUG
            std::cout << "DeviceArray display called" << std::endl;
            std::cout << "    cudaMemcpyDeviceToHost called" << std::endl;
#endif
            cudaMemcpy(h_data_ptr, this->data, data_size*sizeof(T), cudaMemcpyDeviceToHost);
        }
        cout.precision(precision);
        cout << "dimension = [" << dims[0] << ", " << dims[1] << ", " << dims[2] << ", " << dims[3] << "]" << endl;
        size_t pos = 0;
        for (size_t l = 0; l != dims[3]; ++l){
            for (size_t k = 0; k != dims[2]; ++k){
                if (dims[2] > 1 || dims[3] > 1 || M_stride > 1 || N_stride > 1){
                    cout << endl << "[";
                    if (M_stride < 2) cout << "span, ";
                    else cout << "0:" << M_stride << ":" << dims[0] << ", ";
                    if (N_stride < 2) cout << "span, ";
                    else cout << "0:" << N_stride << ":" << dims[1] << ", ";
                    cout << k << ", " << l << "]" << endl;
                }
                for (size_t j = 0; j != dims[1]; ++j){
                    if (!(N_stride > 1 && j%N_stride != 0) || j == dims[1] - 1){
                        for (size_t i = 0; i != dims[0]; ++i){
                            if (!(M_stride > 1 && i%M_stride != 0) || i == dims[0] - 1){
                                cout << setfill(' ') << setw(precision + 2) << h_data_ptr[pos] << " ";
                            }
                            pos++;
                        }
                        cout << endl;
                    }
                    else{
                        pos += dims[0];
                    }
                }
            }
        }
        cout << endl;

        // delete host copy
        delete[] h_data_ptr;

        return;
    }
    //*************************************************************************************************
    // save an 2D array in ascII form
    int save2D(const char* filename = "array.txt", const int precision = 9) const {
        using namespace std;
        if (dims[0] < 1 | dims[1] < 1 | dims[2] != 1 | dims[3]!=1){
            cout << "DeviceArray::save2D error: this array is not a 2D array." << endl;
            return 1;
        }

        // copy data to host memory
        T* h_data_ptr;
        if (data_size > 0){
            h_data_ptr = new T[data_size]; // on heap, but okay
            cudaMemcpy(h_data_ptr, this->data, data_size*sizeof(T), cudaMemcpyDeviceToHost);
        }

        fstream file(filename, fstream::trunc | fstream::out);
        file << scientific;
        file.precision(precision);
        size_t pos = 0;
        for (size_t j = 0; j != dims[1] - 1; ++j){
            for (size_t i = 0; i != dims[0] - 1; ++i){
                file << h_data_ptr[pos] << " ";
                pos++;
            }
            file << h_data_ptr[pos] << endl;
            pos++;
        }
        for (size_t i = 0; i != dims[0] - 1; ++i){
            file << h_data_ptr[pos] << " ";
            pos++;
        }
        file << h_data_ptr[pos];
        file.close();

        // delete host copy
        delete[] h_data_ptr;

        return 0;
    }
    //*************************************************************************************************
    // read DeviceArray in the ARRAYFIRE form
    // see details in http://arrayfire.org/docs/group__stream__func__save.htm or
    // https://github.com/arrayfire/arrayfire/blob/6799ae4f7002b8c864406d9508d76837240e97fe/src/api/c/stream.cpp
    // line 147, 165
    // this function follows the same way as they do
    void read_arrayfire(const char* filename, const int index = 0)
    {
        char version = 0;
        int n_arrays = 0;

        std::fstream fs(filename, std::fstream::in | std::fstream::binary);

        if (!fs.is_open())
        {
            std::cout << "error DeviceArray::af_read(): File failed to open" << std::endl;
            return;
        }

        if (fs.peek() == std::fstream::traits_type::eof())
        {
            std::cout << "error DeviceArray::af_read(): File is empty" << std::endl;
            return;
        }

        fs.read(&version, sizeof(char));
        fs.read((char*)&n_arrays, sizeof(int));

        if (index >= n_arrays)
        {
            std::cout << "error DeviceArray::af_read(): Index out of bounds" << std::endl;
            return;
        }

        for (int i = 0; i < (int)index; i++) {
            // (int    )   Length of the key
            // (cstring)   Key
            // (intl   )   Offset bytes to next array (type + dims + data)
            // (char   )   Type
            // (intl   )   dim4 (x 4)
            // (T      )   data (x elements)
            int klen = -1;
            fs.read((char*)&klen, sizeof(int));

            //char* key = new char[klen];
            //fs.read((char*)&key, klen * sizeof(char));

            // Skip the array name tag
            fs.seekg(klen, std::ios_base::cur);

            // Read data offset
            long long int offset = -1;
            fs.read((char*)&offset, sizeof(long long int));

            // Skip data
            fs.seekg(offset, std::ios_base::cur);
        }

        int klen = -1;
        fs.read((char*)&klen, sizeof(int));

        //char* key = new char[klen];
        //fs.read((char*)&key, klen * sizeof(char));

        // Skip the array name tag
        fs.seekg(klen, std::ios_base::cur);

        // Read data offset
        long long int offset = -1;
        fs.read((char*)&offset, sizeof(long long int));

        // Read type and dims
        char type_ = -1;
        fs.read(&type_, sizeof(char));

        af_dtype type = (af_dtype)type_; // cfloat, cdouble are not implemented!!! Hengdi ZHANG, March 2017

        long long int len[4];
        fs.read((char*)&len, 4 * sizeof(long long int));

        realloc(len[0], len[1], len[2], len[3]);

        T* ha = new T[data_size];
        if (data_size == 0) return;

#define _DA_AF_READ_DATA_AS(TYPE) \
TYPE * ha_temp = new TYPE [data_size]; \
fs.read((char*)ha_temp, data_size * sizeof(TYPE)); \
for (int it = 0; it != data_size; ++it) ha[it] = (T)ha_temp[it]; \
delete[] ha_temp

        switch (type) {
        case f32: { _DA_AF_READ_DATA_AS(float); break; }
        case c32: break; // complex float, do nothing
            
        case f64: { _DA_AF_READ_DATA_AS(double); break; }
        case c64: break; // complex float, do nothing
        case b8: { _DA_AF_READ_DATA_AS(bool); break; }
        case s32: { _DA_AF_READ_DATA_AS(int); break; }
        case u32: { _DA_AF_READ_DATA_AS(unsigned int); break; }
        case u8: { _DA_AF_READ_DATA_AS(char); break; }
        case s64: { _DA_AF_READ_DATA_AS(long int); break; }
        case u64: { _DA_AF_READ_DATA_AS(unsigned long int); break; }
        case s16: { _DA_AF_READ_DATA_AS(short int); break; }
        case u16: { _DA_AF_READ_DATA_AS(unsigned short int); break; }
        default: break; // do nothing
        }

        fs.close();

#ifdef DA_DEBUG
        std::cout << "  cudaMemcpy called in DeviceArray::af_read()" << std::endl;
#endif
        cudaError_t eror = cudaMemcpy(data, ha, data_size * sizeof(T), cudaMemcpyHostToDevice);
        delete[] ha;

    }
    //*************************************************************************************************
    // save DeviceArray in the ARRAYFIRE form
    // see details in http://arrayfire.org/docs/group__stream__func__save.htm
    // additional option: custom_size, if custom_size > 0 and custom_size < size()
    // this function will save only the first custom_size elements as a 1-dim array
    // by Hengdi ZHANG Jan 2017
    int af_save
        (const char* key,
        const char* filename = "device_array.afdat",
        bool append = false,
        const int custom_size = -1
        ) const
    {
        bool is_custom_size = custom_size >= 0 && custom_size < data_size;
        int copy_data_size = is_custom_size ? custom_size : data_size;

        // copy data to host memory
        T* h_data_ptr;
        if (copy_data_size > 0){
            h_data_ptr = new T[copy_data_size];
            cudaMemcpy(h_data_ptr, this->data, copy_data_size*sizeof(T), cudaMemcpyDeviceToHost);
        }

        using namespace std;
        fstream file;

        //**************************************
        // create the file if it doesn't exist
        file.open(filename, fstream::binary | fstream::in | fstream::out);
        if (!file) {
            file.open(filename, fstream::binary | fstream::trunc | fstream::out);
            append = false;
        }
        file.close();
        //**************************************

        //**************************************
        if (!append){
            // if append==false, clear file, write file header

            // clear file
            file.open(filename, fstream::binary | fstream::trunc | fstream::out);
            file.close();

            // write file header
            file.open(filename, fstream::binary | fstream::in | fstream::out);
            // Version: ArrayFire File Format Version for future use. Currently set to 1
            const char arrayfire_version = 1;
            file.write((const char*)&arrayfire_version, 1);
            // Array Count: No. of Arrays stored in file
            int array_count = 1;
            file.write((const char*)&array_count, 4);
            file.close();
        }
        else
        {
            // if append==true, modify Array Count number, array_count++ ;
            file.open(filename, fstream::binary | fstream::in | fstream::out);
            int array_count;
            file.seekg(1, fstream::beg);
            file.read((char*)&array_count, 4);
            array_count++;
            file.seekp(1, fstream::beg);
            file.write((const char*)&array_count, 4);
            file.close();
        }
        //**************************************

        //**************************************
        // write data and header
        file.open(filename, fstream::binary | fstream::in | fstream::out | fstream::app);

        // write data header

        // Length of Key String: No. of characters (excluding null ending) in the key string
        size_t key_len = strlen(key);
        file.write((const char*)&key_len, 4);

        // Key: Key of the Array. Used when reading from file
        file.write(key, key_len);

        // Offset: No of bytes between offset and start of next array
        // The offset is equal to 1 byte (type) + 32 bytes (dims) + size of data.
        size_t Offset = 1 + 32 + copy_data_size * sizeof(T);
        file.write((const char*)&Offset, 8);

        // Array Type: Type corresponding to af_dtype enum
        af_dtype array_type = af_ptr_typeof(this->data);
        file.write((const char*)&array_type, 1);

        // Dims (4 values): Dimensions of the Array (int64)
        long long int af_dims[4];
        if (is_custom_size)
        {
            af_dims[0] = copy_data_size;
            af_dims[1] = 1;
            af_dims[2] = 1;
            af_dims[3] = 1;
        }
        else
        {
            af_dims[0] = dims[0];
            af_dims[1] = dims[1];
            af_dims[2] = dims[2];
            af_dims[3] = dims[3];
        }
        file.write((const char*)af_dims, 32);

        // write data
        if (copy_data_size>0) file.write((const char*)h_data_ptr, copy_data_size * sizeof(T));
        file.close();

        // delete host copy
        if (copy_data_size>0) delete[] h_data_ptr;
        //**************************************
        return 0;
    }
    //*************************************************************************************************
    // reset DeviceArray value except for boundary points
    inline void reset(const T value)
    {
        const int BN = 256;
        _DA_kernel_reset <<<(data_size + BN - 1) / BN, BN >>>(data, data_size, value);
    }

    //*************************************************************************************************
    // arithmetic operators
    // unary minus operator
    DeviceArray operator-(){
        DeviceArray out(*this);
        if (!isempty()){
            const int BN = 256;
            _DA_OP_KERNEL(uminus) <<< (data_size + BN - 1) / BN, BN >>> (out.data, data_size);
        }
        return out;
    }

    // binary arithmetic operators
#define _DA_DEFINE_BIN_MEMBER_OP(op_name, op_notion) \
    DeviceArray& op_notion(const DeviceArray& rhs){ \
        const int BN = 256; \
        if (!isempty()) _DA_OP_KERNEL(op_name)<<< (data_size + BN - 1) / BN, BN >>>(data, rhs.data, data_size); \
        return *this; } \
    DeviceArray& op_notion(const T rhs){ \
        const int BN = 256; \
        if (!isempty()) _DA_OP_KERNEL(op_name)<<< (data_size + BN - 1) / BN, BN >>>(data, rhs, data_size); \
        return *this; }

    _DA_DEFINE_BIN_MEMBER_OP(add, operator+= )
    _DA_DEFINE_BIN_MEMBER_OP(mul, operator*=)
    _DA_DEFINE_BIN_MEMBER_OP(minus, operator-=)
    _DA_DEFINE_BIN_MEMBER_OP(div, operator/=)
    _DA_DEFINE_BIN_MEMBER_OP(mod, operator%=)

    DeviceArray& minus_after_const(const T& lhs){
        const int BN = 256;
        if (!isempty()) _DA_OP_KERNEL(minus_rhs) <<< (data_size + BN - 1) / BN, BN >>>(data, lhs, data_size);
        return *this;
    }
    DeviceArray& div_after_const(const T& lhs){
        const int BN = 256;
        if (!isempty()) _DA_OP_KERNEL(div_rhs) <<< (data_size + BN - 1) / BN, BN >>>(data, lhs, data_size);
        return *this;
    }
    DeviceArray& mod_after_const(const T& lhs){
        const int BN = 256;
        if (!isempty()) _DA_OP_KERNEL(mod_rhs) <<< (data_size + BN - 1) / BN, BN >>>(data, lhs, data_size);
        return *this;
    }
    // ********************************************************************************************
    // specialized member operator! declaration
    DeviceArray<bool> operator!();
};

// specialized member defines
template<>
inline DeviceArray<bool> DeviceArray<bool>::operator!(){
    DeviceArray out(*this);
    if (!isempty()){
        const int BN = 256;
        _DA_OP_KERNEL(not) <<< (data_size + BN - 1) / BN, BN >>> (out.data, data_size);
    }
    return out;
}
// ************************************************************************************************
// DeviceArray operators and arithmetic functions

// operator+
template<typename T> inline DeviceArray<T> operator+(const DeviceArray<T>& lhs, const DeviceArray<T>& rhs) {
    DeviceArray<T> out(lhs); out += rhs; return out;
}
template<typename T> inline DeviceArray<T> operator+(const DeviceArray<T>& lhs, const T rhs) {
    DeviceArray<T> out(lhs); out += rhs; return out;
}
template<typename T> inline DeviceArray<T> operator+(const T& lhs, const DeviceArray<T>& rhs) {
    DeviceArray<T> out(rhs); out += lhs; return out;
}

// operator*
template<typename T> inline DeviceArray<T> operator*(const DeviceArray<T>& lhs, const DeviceArray<T>& rhs) {
    DeviceArray<T> out(lhs); out *= rhs; return out;
}
template<typename T> inline DeviceArray<T> operator*(const DeviceArray<T>& lhs, const T rhs) {
    DeviceArray<T> out(lhs); out *= rhs; return out;
}
template<typename T> inline DeviceArray<T> operator*(const T& lhs, const DeviceArray<T>& rhs) {
    DeviceArray<T> out(rhs); out *= lhs; return out;
}
// operator* boolean operand SPECIALIZATION
template<typename T> inline DeviceArray<T> operator*(const DeviceArray<bool>& lhs, const DeviceArray<T>& rhs) {
    DeviceArray<T> out(lhs); out *= rhs;
    return out;
}
template<typename T> inline DeviceArray<T> operator*(const DeviceArray<T>& lhs, const DeviceArray<bool>& rhs) {
    DeviceArray<T> out(rhs); out *= lhs; return out;
}

// operator-
template<typename T> inline DeviceArray<T> operator-(const DeviceArray<T>& lhs, const DeviceArray<T>& rhs){
    DeviceArray<T> out(lhs); out -= rhs; return out;
}
template<typename T> inline DeviceArray<T> operator-(const DeviceArray<T>& lhs, const T& rhs){
    DeviceArray<T> out(lhs); out -= rhs; return out;
}
template<typename T> inline DeviceArray<T> operator-(const T& lhs, const DeviceArray<T>& rhs){
    DeviceArray<T> out(rhs); out.minus_after_const(lhs); return out;
}

// operator/
template<typename T> inline DeviceArray<T> operator/(const DeviceArray<T>& lhs, const DeviceArray<T>& rhs){
    DeviceArray<T> out(lhs); out /= rhs; return out;
}
template<typename T> inline DeviceArray<T> operator/(const DeviceArray<T>& lhs, const T& rhs){
    DeviceArray<T> out(lhs); out /= rhs; return out;
}
template<typename T> inline DeviceArray<T> operator/(const T& lhs, const DeviceArray<T>& rhs){
    DeviceArray<T> out(rhs); out.div_after_const(lhs); return out;
}

// operator%
template<typename T> inline DeviceArray<T> operator%(const DeviceArray<T>& lhs, const DeviceArray<T>& rhs){
    DeviceArray<T> out(lhs); out %= rhs; return out;
}
template<typename T> inline DeviceArray<T> operator%(const DeviceArray<T>& lhs, const T& rhs){
    DeviceArray<T> out(lhs); out %= rhs; return out;
}
template<typename T> inline DeviceArray<T> operator%(const T& lhs, const DeviceArray<T>& rhs){
    DeviceArray<T> out(rhs); out.mod_after_const(lhs); return out;
}
//*************************************************************************************************
// unary arithmetic functions
#define _DA_DEFINE_UNARY_FUNCTION(func_name) \
template<typename T> inline DeviceArray<T> func_name(const DeviceArray<T>& in){ \
    DeviceArray<T> out(in); \
    if(!out.isempty()) { \
        const int BN = 256; \
        _DA_OP_KERNEL(func_name) <<< (out.size() + BN - 1) / BN, BN >>> (out.data_ptr(), out.size()); \
    } \
    return out; \
}
_DA_DEFINE_UNARY_FUNCTION(abs)
_DA_DEFINE_UNARY_FUNCTION(sqrt)
_DA_DEFINE_UNARY_FUNCTION(log)
_DA_DEFINE_UNARY_FUNCTION(log2)
_DA_DEFINE_UNARY_FUNCTION(log10)
_DA_DEFINE_UNARY_FUNCTION(exp)
_DA_DEFINE_UNARY_FUNCTION(exp2)
_DA_DEFINE_UNARY_FUNCTION(exp10)
_DA_DEFINE_UNARY_FUNCTION(sin)
_DA_DEFINE_UNARY_FUNCTION(asin)
_DA_DEFINE_UNARY_FUNCTION(sinh)
_DA_DEFINE_UNARY_FUNCTION(cos)
_DA_DEFINE_UNARY_FUNCTION(acos)
_DA_DEFINE_UNARY_FUNCTION(cosh)
_DA_DEFINE_UNARY_FUNCTION(tan)
_DA_DEFINE_UNARY_FUNCTION(atan)
_DA_DEFINE_UNARY_FUNCTION(tanh)
_DA_DEFINE_UNARY_FUNCTION(floor)
_DA_DEFINE_UNARY_FUNCTION(round)
_DA_DEFINE_UNARY_FUNCTION(ceil)

// comparison operators
#define _DA_DEFINE_COMPARISON_OPERATOR(op_name, op_notation) \
template<typename T> inline DeviceArray<bool> op_notation(const DeviceArray<T>& lhs, const DeviceArray<T>& rhs){ \
    DeviceArray<bool> out; \
    if (!lhs.isempty()){ \
        const int BN = 256; out.realloc(lhs.dim(0), lhs.dim(1), lhs.dim(2), lhs.dim(3)); \
        _DA_OP_KERNEL(op_name) <<< (out.size() + BN - 1) / BN, BN >>> (out.data_ptr(), lhs.data_ptr(), rhs.data_ptr(), lhs.size()); } \
    return out; } \
template<typename T> inline DeviceArray<bool> op_notation(const DeviceArray<T>& lhs, const T& rhs){ \
    DeviceArray<bool> out; \
    if (!lhs.isempty()){ \
        const int BN = 256; out.realloc(lhs.dim(0), lhs.dim(1), lhs.dim(2), lhs.dim(3)); \
        _DA_OP_KERNEL(op_name) <<< (out.size() + BN - 1) / BN, BN >>> (out.data_ptr(), lhs.data_ptr(), rhs, lhs.size()); } \
    return out; } \
template<typename T> inline DeviceArray<bool> op_notation(const T& lhs, const DeviceArray<T>& rhs){ \
    DeviceArray<bool> out; \
    if (!rhs.isempty()){ \
        const int BN = 256; out.realloc(rhs.dim(0), rhs.dim(1), rhs.dim(2), rhs.dim(3)); \
        _DA_OP_KERNEL(op_name##_rhs) <<< (out.size() + BN - 1) / BN, BN >>> (out.data_ptr(), rhs.data_ptr(), lhs, lhs.size()); } \
    return out; }
_DA_DEFINE_COMPARISON_OPERATOR(GT, operator>)
_DA_DEFINE_COMPARISON_OPERATOR(GE, operator>=)
_DA_DEFINE_COMPARISON_OPERATOR(LT, operator<)
_DA_DEFINE_COMPARISON_OPERATOR(LE, operator<=)
_DA_DEFINE_COMPARISON_OPERATOR(EQ, operator==)
_DA_DEFINE_COMPARISON_OPERATOR(NE, operator!=)
//*************************************************************************************************
// logical operators
inline DeviceArray<bool> operator&&(const DeviceArray<bool>& lhs, const DeviceArray<bool>& rhs){
    DeviceArray<bool> out;
    if (!lhs.isempty()){
        const int BN = 256;
        out.realloc(lhs.dim(0), lhs.dim(1), lhs.dim(2), lhs.dim(3));
        _DA_OP_KERNEL(AND) <<< (out.size() + BN - 1) / BN, BN >>> (out.data_ptr(), lhs.data_ptr(), rhs.data_ptr(), lhs.size());
    }
    return out;
}
inline DeviceArray<bool> operator||(const DeviceArray<bool>& lhs, const DeviceArray<bool>& rhs){
    DeviceArray<bool> out;
    if (!lhs.isempty()){
        const int BN = 256;
        out.realloc(lhs.dim(0), lhs.dim(1), lhs.dim(2), lhs.dim(3));
        _DA_OP_KERNEL(OR) <<< (out.size() + BN - 1) / BN, BN >>> (out.data_ptr(), lhs.data_ptr(), rhs.data_ptr(), lhs.size());
    }
    return out;
}

// ************************************************************************************************
// DA namespace contains simple tools for DeviceArray class
template<typename T> __global__
void _DA_kernel_DA_Range(int* darr, const T n_in){
    int n = (int)n_in;
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < n) darr[i] = i;
}
template<typename T> __global__
void _DA_kernel_DA_Range(T* darr, const T x0, const T dx, const int n){
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < n) darr[i] = x0 + i * dx;
}
template<typename T> __global__ void _DA_kernel_DA_join(T* out, const T* a0, const T* a1, const int n0, const int n1){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    T value;
    if (idx < n0 + n1){
        if (idx < n0) value = a0[idx];
        else value = a1[idx - n0];
        out[idx] = value;
    }
}
// from nvidia example
// Utility class used to avoid linker errors with extern
// unsized shared memory arrays with templated type
template<class T>
struct SharedMemory{
    __device__ inline operator T *(){
        extern __shared__ int __smem[];
        return (T *)__smem;
    }
    __device__ inline operator const T *() const{
        extern __shared__ int __smem[];
        return (T *)__smem;
    }
};
// specialize for double to avoid unaligned memory
// access compile errors
template<>
struct SharedMemory<double>{
    __device__ inline operator double *(){
        extern __shared__ double __smem_d[];
        return (double *)__smem_d;
    }
    __device__ inline operator const double *() const{
        extern __shared__ double __smem_d[];
        return (double *)__smem_d;
    }
};

template <typename T, unsigned int blockSize, bool nIsPow2>
// reduce summation from NVIDIA cuda example, renamed from reduce6 to _DA_kernel_reduce_add
/*
This version adds multiple elements per thread sequentially.  This reduces the overall
cost of the algorithm while keeping the work complexity O(n) and the step complexity O(log n).
(Brent's Theorem optimization)
Note, this kernel needs a minimum of 64*sizeof(T) bytes of shared memory.
In other words if blockSize <= 32, allocate 64*sizeof(T) bytes.
If blockSize > 32, allocate blockSize*sizeof(T) bytes.
*/
__global__ void _DA_kernel_reduce_add(const T* __restrict__ g_idata, T* __restrict__ g_odata, const unsigned int n, const T factor){
    __shared__ T sdata[blockSize*2];
    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockSize * 2 + threadIdx.x;
    unsigned int gridSize = blockSize * 2 * gridDim.x;
    T mySum = 0;
    // we reduce multiple elements per thread.  The number is determined by the
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n) {
        mySum += g_idata[i];
        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < n) mySum += g_idata[i + blockSize];
        i += gridSize;
    }
    // each thread puts its local sum into shared memory
    sdata[tid] = mySum; __syncthreads();
    // do reduction in shared mem
    if ((blockSize >= 512) && (tid < 256)) sdata[tid] = mySum = mySum + sdata[tid + 256]; __syncthreads();
    if ((blockSize >= 256) && (tid < 128)) sdata[tid] = mySum = mySum + sdata[tid + 128]; __syncthreads();
    if ((blockSize >= 128) && (tid <  64)) sdata[tid] = mySum = mySum + sdata[tid + 64]; __syncthreads();
#if (__CUDA_ARCH__ >= 300 )
    if (tid < 32) {
        // Fetch final intermediate sum from 2nd warp
        if (blockSize >= 64) mySum += sdata[tid + 32];
        // Reduce final warp using shuffle
        for (int offset = warpSize / 2; offset > 0; offset /= 2) mySum += __shfl_down(mySum, offset);
    }
#else
    // fully unroll reduction within a single warp
    if ((blockSize >= 64) && (tid < 32)) sdata[tid] = mySum = mySum + sdata[tid + 32]; __syncthreads();
    if ((blockSize >= 32) && (tid < 16)) sdata[tid] = mySum = mySum + sdata[tid + 16]; __syncthreads();
    if ((blockSize >= 16) && (tid <  8)) sdata[tid] = mySum = mySum + sdata[tid + 8]; __syncthreads();
    if ((blockSize >= 8) && (tid <  4)) sdata[tid] = mySum = mySum + sdata[tid + 4]; __syncthreads();
    if ((blockSize >= 4) && (tid <  2)) sdata[tid] = mySum = mySum + sdata[tid + 2]; __syncthreads();
    if ((blockSize >= 2) && (tid <  1)) sdata[tid] = mySum = mySum + sdata[tid + 1]; __syncthreads();
#endif
    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = mySum * factor;
}
// reduce add warpper from nvidia cuda example
// renamed from reduce to _DA_warpper_reduce_add
////////////////////////////////////////////////////////////////////////////////
// Wrapper function for kernel launch
////////////////////////////////////////////////////////////////////////////////
template <class T>
void _DA_warpper_reduce_add(int size, int threads, int blocks, const T* d_idata, T* d_odata, const T& factor){
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);

    // when there is only one warp per block, we need to allocate two warps
    // worth of shared memory so that we don't index shared memory out of bounds
    int smemSize = (threads <= 32) ? 2 * threads * sizeof(T) : threads * sizeof(T);

    // choose which of the optimized versions of reduction to launch
    // (size&(size - 1)) == 0 returns true if size is a power of 2
    if ((size&(size - 1)) == 0){
        switch (threads){
        case 512: _DA_kernel_reduce_add<T, 512, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case 256: _DA_kernel_reduce_add<T, 256, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case 128: _DA_kernel_reduce_add<T, 128, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case  64: _DA_kernel_reduce_add<T, 64, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case  32: _DA_kernel_reduce_add<T, 32, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case  16: _DA_kernel_reduce_add<T, 16, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case   8: _DA_kernel_reduce_add<T, 8, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case   4: _DA_kernel_reduce_add<T, 4, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case   2: _DA_kernel_reduce_add<T, 2, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case   1: _DA_kernel_reduce_add<T, 1, true> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        }
    }
    else{
        switch (threads){
        case 512: _DA_kernel_reduce_add<T, 512, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case 256: _DA_kernel_reduce_add<T, 256, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case 128: _DA_kernel_reduce_add<T, 128, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case  64: _DA_kernel_reduce_add<T, 64, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case  32: _DA_kernel_reduce_add<T, 32, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case  16: _DA_kernel_reduce_add<T, 16, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case   8: _DA_kernel_reduce_add<T, 8, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case   4: _DA_kernel_reduce_add<T, 4, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case   2: _DA_kernel_reduce_add<T, 2, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        case   1: _DA_kernel_reduce_add<T, 1, false> <<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size, factor); break;
        }
    }
}

// Instantiate the reduction function for 3 types
template void _DA_warpper_reduce_add<int>(int size, int threads, int blocks, const int *d_idata, int *d_odata, const int& factor);
template void _DA_warpper_reduce_add<float>(int size, int threads, int blocks, const float *d_idata, float *d_odata, const float& factor);
template void _DA_warpper_reduce_add<double>(int size, int threads, int blocks, const double *d_idata, double *d_odata, const double& factor);
//*************************************************************************************************
namespace DA{

    // DeviceArray display macro
#define da_print(da) do{(da).display(#da);} while(0)

    // DeviceArray save macro. Besure that all your expressions inside the bracket 
    // can be a legal file name
#define da_saveAF(da) do{(da).af_save(#da,#da".afdat");} while(0)
#define da_save2D(da) do{(da).save2D(#da".txt");} while(0)
    //*************************************************************************************************
    inline DeviceArray<int> range(const int n){
        DeviceArray<int> out;
        if (n >0)
        {
            out.realloc(n);
            const int BN = 256;
            _DA_kernel_DA_Range <<< (n + BN - 1) / BN, BN >>> (out.data_ptr(), n);
        }
        else
        {
            std::cout << "DeviceArrayUtility::range : error, input n < 1" << std::endl;
        }
        return out;
    }

    template<typename T>
    DeviceArray<T> range(const T x0, const T dx, const int n){
        DeviceArray<T> out;
        if (n > 0)
        {
            out.realloc(n);
            const int BN = 256;
            _DA_kernel_DA_Range << < (n + BN - 1) / BN, BN >> > (out.data_ptr(), x0, dx, n);
        }
        else
        {
            std::cout << "DeviceArrayUtility::range : error, input n < 1" << std::endl;
        }
        return out;
    }
    //*************************************************************************************************
    // join two DeviceArrays together, the output is a flat array
    template<typename T>
    DeviceArray<T> join(const DeviceArray<T>& a0, const DeviceArray<T>& a1){
        DeviceArray<T> out;
        if (a0.isempty()) {
            out = a1;
            return out;
        }
        if (a1.isempty()) {
            out = a0;
            return out;
        }
        out.realloc(a0.size() + a1.size());
        const int BN = 256;
        _DA_kernel_DA_join <<< (out.size() + BN - 1) / BN, BN >>> (out.data_ptr(), a0.data_ptr(), a1.data_ptr(), a0.size(), a1.size());
        return out;
    }
    //*************************************************************************************************
    // shallow swap
    template<typename T>
    inline void swap(DeviceArray<T>& a, DeviceArray<T>& b){
        a.swap(b);
    }
    // swap data pointer ONLY
    // be sure they have the same dimension
    template<typename T>
    inline void swap_data_ptr(DeviceArray<T>& a, DeviceArray<T>& b){
        a.swap_data_ptr(b);
    }
    //*************************************************************************************************
    // DA::reshape left-value version
    template<typename T>
    DeviceArray<T> reshape(const DeviceArray<T>& a, const int m, const int n = 1, const int k = 1, const int l = 1){
#ifdef DA_DEBUG
        std::cout << "DA::reshape left-value version called" << std::endl;
#endif
        DeviceArray<T> out(a);
        if (!out.isempty()){
            if (m*n*k*l == out.size()){
                out.dim_ref(0) = a.dim(0);
                out.dim_ref(1) = a.dim(1);
                out.dim_ref(2) = a.dim(2);
                out.dim_ref(3) = a.dim(3);
            }
            else{
                std::cout << "DA::reshape error: wrong input size" << std::endl;
            }
        }
        return out;
    }

    // DA::reshape right-value version
    template<typename T>
    DeviceArray<T> reshape(DeviceArray<T>&& a, const int m, const int n = 1, const int k = 1, const int l = 1){
#ifdef DA_DEBUG
        std::cout << "DA::reshape right-value version called" << std::endl;
#endif
        DeviceArray<T> out(std::move(a));
        if (!out.isempty()){
            if (m*n*k*l == out.size()){
                out.dim_ref(0) = m;
                out.dim_ref(1) = n;
                out.dim_ref(2) = k;
                out.dim_ref(3) = l;
            }
            else{
                std::cout << "DA::reshape error: wrong input size" << std::endl;
            }
        }
        return out;
    }
    //*************************************************************************************************
    template<typename T>
    DeviceArray<T> tile(const DeviceArray<T>& A, const int m, const int n = 1, const int k = 1, const int l = 1){
        DeviceArray<T> out;
        out.realloc(m * A.dim(0), n * A.dim(1), k * A.dim(2), l * A.dim(3));
        const int BN = 256;
        _DA_kernel_DeviceArray_tile <<< (out.size() + BN - 1) / BN, BN >>> (out.data_ptr(), A.data_ptr(), A.dim(0), A.dim(1), A.dim(2), A.dim(3), m, n, k, l);
        return out;
    }
    //*************************************************************************************************
    template<typename T>
    DeviceArray<T> transpose(const DeviceArray<T>& A){
        DeviceArray<T> out;
        out.realloc(A.dim(1), A.dim(0), A.dim(2), A.dim(3));
        if (!A.isempty()){
            if (!(A.dim(2) > 1 || A.dim(3) > 1)){
                const int BN = 32;
                const dim3 bDim(BN, BN, 1);
                dim3 gDim((out.dim(1) + BN - 1) / BN, (out.dim(0) + BN - 1) / BN, 1);
                _DA_kernel_DeviceArray_transpose <<<gDim, bDim >>> (out.data_ptr(), A.data_ptr(), out.dim(1), out.dim(0));
            }
        }
        return out;
    }
    //*************************************************************************************************
    template<typename T>
    // similar to Matlab meshgrid function
    // X: output X mesh
    // Y: output Y mesh
    inline void meshgrid
        (
        DeviceArray<T>& X, DeviceArray<T>& Y,
        const T x0, const T y0,
        const T dx, const T dy,
        const int m, const int n
        )
    {
        X = tile(range(x0, dx, m), 1, n);
        Y = tile(transpose(range(y0, dy, n)), m, 1);
    }

    template<typename T>
    inline DeviceArray<T> readArray(const char* filename, const int index = 0){
        DeviceArray<T> out;
        out.read_arrayfire(filename, index);
        return out;
    }
    //*************************************************************************************************
    template<typename T>
    // reduce operation sum
    // input DeviceArray will be treated as 1D array with size = a::data_size
    // implemented by cuda thrust
    // VERY SLOW if called in a loop!!!
    // use DA::reduce_add with high efficiency
    inline T sum(const DeviceArray<T>& a){
        int n = a.size();

        if (n == 0){
            return (T)0;
        }
        thrust::device_ptr<T> dp = thrust::device_pointer_cast(a.data_ptr());
        return thrust::reduce(dp, dp + n, (T)0, thrust::plus<T>());

    }
    template<typename T>
    // reduce operation max
    inline T max(const DeviceArray<T>& a){
        int n = a.size();

        if (n == 0){
            return (T)0;
        }
        thrust::device_ptr<T> dp = thrust::device_pointer_cast(a.data_ptr());
        return thrust::reduce(dp, dp + n, (T)0, thrust::maximum<T>());

    }

    template<typename T>
    inline void reduce_add(const DeviceArray<T>& a, DeviceArray<T>& out, const T& factor = (T)1){
        const int BSIZE = 256; 
        _DA_warpper_reduce_add(a.size(), BSIZE, (a.size() + BSIZE - 1) / BSIZE, a.data_ptr(), out.data_ptr(), factor);
    }

}

namespace DA
{
// ************************************************************************************************
// helper functions
// Beginning of GPU Architecture definitions
inline int _ConvertSMVer2Cores(int major, int minor) {
    // Defines for GPU Architecture types (using the SM version to determine
    // the # of cores per SM
    typedef struct {
        int SM;  // 0xMm (hexidecimal notation), M = SM Major version,
        // and m = SM minor version
        int Cores;
    } sSMtoCores;

    sSMtoCores nGpuArchCoresPerSM[] = {
        {0x30, 192},
        {0x32, 192},
        {0x35, 192},
        {0x37, 192},
        {0x50, 128},
        {0x52, 128},
        {0x53, 128},
        {0x60,  64},
        {0x61, 128},
        {0x62, 128},
        {0x70,  64},
        {0x72,  64},
        {0x75,  64},
        {-1, -1} };

    int index = 0;

    while (nGpuArchCoresPerSM[index].SM != -1) {
        if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor)) {
            return nGpuArchCoresPerSM[index].Cores;
        }

        index++;
    }

    // If we don't find the values, we default use the previous one
    // to run properly
    printf(
        "MapSMtoCores for SM %d.%d is undefined."
        "  Default to use %d Cores/SM\n",
        major, minor, nGpuArchCoresPerSM[index - 1].Cores);
    return nGpuArchCoresPerSM[index - 1].Cores;
}
// end of GPU Architecture definitions

inline int showCudaDeviceProperties() {
    printf("THIS FUNCTION IS COPIED FROM ""NVIDIA/CUDA SAMPLES/1_UTILITIES/DEVICEQUERY""\n\n");
    printf(
        " CUDA Device Query (Runtime API) version (CUDART static linking)\n\n");

    int deviceCount = 0; // number of CUDA capable devices
    cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

    if (error_id != cudaSuccess) {
        printf("cudaGetDeviceCount returned %d\n-> %s\n",
            static_cast<int>(error_id), cudaGetErrorString(error_id));
        printf("Result = FAIL\n");
        exit(EXIT_FAILURE);
    }

    // This function call returns 0 if there are no CUDA capable devices.
    if (deviceCount == 0) {
        printf("There are no available device(s) that support CUDA\n");
    }
    else {
        printf("Detected %d CUDA Capable device(s)\n", deviceCount);
    }

    int dev, driverVersion = 0, runtimeVersion = 0;

    for (dev = 0; dev < deviceCount; ++dev) {
        cudaSetDevice(dev);
        cudaDeviceProp deviceProp;
        cudaGetDeviceProperties(&deviceProp, dev);

        printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);

        // Console log
        cudaDriverGetVersion(&driverVersion);
        cudaRuntimeGetVersion(&runtimeVersion);
        printf("  CUDA Driver Version / Runtime Version          %d.%d / %d.%d\n",
            driverVersion / 1000, (driverVersion % 100) / 10,
            runtimeVersion / 1000, (runtimeVersion % 100) / 10);
        printf("  CUDA Capability Major/Minor version number:    %d.%d\n",
            deviceProp.major, deviceProp.minor);

        char msg[256];
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
        sprintf_s(msg, sizeof(msg),
            "  Total amount of global memory:                 %.0f MBytes "
            "(%llu bytes)\n",
            static_cast<float>(deviceProp.totalGlobalMem / 1048576.0f),
            (unsigned long long)deviceProp.totalGlobalMem);
#else
        snprintf(msg, sizeof(msg),
            "  Total amount of global memory:                 %.0f MBytes "
            "(%llu bytes)\n",
            static_cast<float>(deviceProp.totalGlobalMem / 1048576.0f),
            (unsigned long long)deviceProp.totalGlobalMem);
#endif
        printf("%s", msg);

        printf("  (%2d) Multiprocessors, (%3d) CUDA Cores/MP:     %d CUDA Cores\n",
            deviceProp.multiProcessorCount,
            _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor),
            _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) *
            deviceProp.multiProcessorCount);
        printf(
            "  GPU Max Clock rate:                            %.0f MHz (%0.2f "
            "GHz)\n",
            deviceProp.clockRate * 1e-3f, deviceProp.clockRate * 1e-6f);

#if CUDART_VERSION >= 5000
        // This is supported in CUDA 5.0 (runtime API device properties)
        printf("  Memory Clock rate:                             %.0f Mhz\n",
            deviceProp.memoryClockRate * 1e-3f);
        printf("  Memory Bus Width:                              %d-bit\n",
            deviceProp.memoryBusWidth);
        printf("  Peak Memory Bandwidth:                         %6.2f (GB/s)\n",
            2.0*deviceProp.memoryClockRate*(deviceProp.memoryBusWidth / 8) / 1.0e6);

        if (deviceProp.l2CacheSize) {
            printf("  L2 Cache Size:                                 %d bytes\n",
                deviceProp.l2CacheSize);
        }

#else
        // This only available in CUDA 4.0-4.2 (but these were only exposed in the
        // CUDA Driver API)
        int memoryClock;
        getCudaAttribute<int>(&memoryClock, CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE,
            dev);
        printf("  Memory Clock rate:                             %.0f Mhz\n",
            memoryClock * 1e-3f);
        int memBusWidth;
        getCudaAttribute<int>(&memBusWidth,
            CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH, dev);
        printf("  Memory Bus Width:                              %d-bit\n",
            memBusWidth);
        int L2CacheSize;
        getCudaAttribute<int>(&L2CacheSize, CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE, dev);

        if (L2CacheSize) {
            printf("  L2 Cache Size:                                 %d bytes\n",
                L2CacheSize);
        }

#endif

        printf(
            "  Maximum Texture Dimension Size (x,y,z)         1D=(%d), 2D=(%d, "
            "%d), 3D=(%d, %d, %d)\n",
            deviceProp.maxTexture1D, deviceProp.maxTexture2D[0],
            deviceProp.maxTexture2D[1], deviceProp.maxTexture3D[0],
            deviceProp.maxTexture3D[1], deviceProp.maxTexture3D[2]);
        printf(
            "  Maximum Layered 1D Texture Size, (num) layers  1D=(%d), %d layers\n",
            deviceProp.maxTexture1DLayered[0], deviceProp.maxTexture1DLayered[1]);
        printf(
            "  Maximum Layered 2D Texture Size, (num) layers  2D=(%d, %d), %d "
            "layers\n",
            deviceProp.maxTexture2DLayered[0], deviceProp.maxTexture2DLayered[1],
            deviceProp.maxTexture2DLayered[2]);

        printf("  Total amount of constant memory:               %lu bytes\n",
            deviceProp.totalConstMem);
        printf("  Total amount of shared memory per block:       %lu bytes\n",
            deviceProp.sharedMemPerBlock);
        printf("  Total number of registers available per block: %d\n",
            deviceProp.regsPerBlock);
        printf("  Warp size:                                     %d\n",
            deviceProp.warpSize);
        printf("  Maximum number of threads per multiprocessor:  %d\n",
            deviceProp.maxThreadsPerMultiProcessor);
        printf("  Maximum number of threads per block:           %d\n",
            deviceProp.maxThreadsPerBlock);
        printf("  Max dimension size of a thread block (x,y,z): (%d, %d, %d)\n",
            deviceProp.maxThreadsDim[0], deviceProp.maxThreadsDim[1],
            deviceProp.maxThreadsDim[2]);
        printf("  Max dimension size of a grid size    (x,y,z): (%d, %d, %d)\n",
            deviceProp.maxGridSize[0], deviceProp.maxGridSize[1],
            deviceProp.maxGridSize[2]);
        printf("  Maximum memory pitch:                          %lu bytes\n",
            deviceProp.memPitch);
        printf("  Texture alignment:                             %lu bytes\n",
            deviceProp.textureAlignment);
        printf(
            "  Concurrent copy and kernel execution:          %s with %d copy "
            "engine(s)\n",
            (deviceProp.deviceOverlap ? "Yes" : "No"), deviceProp.asyncEngineCount);
        printf("  Run time limit on kernels:                     %s\n",
            deviceProp.kernelExecTimeoutEnabled ? "Yes" : "No");
        printf("  Integrated GPU sharing Host Memory:            %s\n",
            deviceProp.integrated ? "Yes" : "No");
        printf("  Support host page-locked memory mapping:       %s\n",
            deviceProp.canMapHostMemory ? "Yes" : "No");
        printf("  Alignment requirement for Surfaces:            %s\n",
            deviceProp.surfaceAlignment ? "Yes" : "No");
        printf("  Device has ECC support:                        %s\n",
            deviceProp.ECCEnabled ? "Enabled" : "Disabled");
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
        printf("  CUDA Device Driver Mode (TCC or WDDM):         %s\n",
            deviceProp.tccDriver ? "TCC (Tesla Compute Cluster Driver)"
            : "WDDM (Windows Display Driver Model)");
#endif
        printf("  Device supports Unified Addressing (UVA):      %s\n",
            deviceProp.unifiedAddressing ? "Yes" : "No");
        // not availible for cuda 8.0
        // edited by Zhe Gou in 20191007
        /*printf("  Device supports Compute Preemption:            %s\n",
            deviceProp.computePreemptionSupported ? "Yes" : "No");
        printf("  Supports Cooperative Kernel Launch:            %s\n",
            deviceProp.cooperativeLaunch ? "Yes" : "No");
        printf("  Supports MultiDevice Co-op Kernel Launch:      %s\n",
            deviceProp.cooperativeMultiDeviceLaunch ? "Yes" : "No");*/
        printf("  Device PCI Domain ID / Bus ID / location ID:   %d / %d / %d\n",
            deviceProp.pciDomainID, deviceProp.pciBusID, deviceProp.pciDeviceID);

        const char *sComputeMode[] = {
            "Default (multiple host threads can use ::cudaSetDevice() with device "
            "simultaneously)",
            "Exclusive (only one host thread in one process is able to use "
            "::cudaSetDevice() with this device)",
            "Prohibited (no host thread can use ::cudaSetDevice() with this "
            "device)",
            "Exclusive Process (many threads in one process is able to use "
            "::cudaSetDevice() with this device)",
            "Unknown",
            NULL };
        printf("  Compute Mode:\n");
        printf("     < %s >\n", sComputeMode[deviceProp.computeMode]);
    }

    // If there are 2 or more GPUs, query to determine whether RDMA is supported
    if (deviceCount >= 2) {
        cudaDeviceProp prop[64];
        int gpuid[64];  // we want to find the first two GPUs that can support P2P
        int gpu_p2p_count = 0;

        for (int i = 0; i < deviceCount; i++) {
            cudaGetDeviceProperties(&prop[i], i);

            // Only boards based on Fermi or later can support P2P
            if ((prop[i].major >= 2)
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
                // on Windows (64-bit), the Tesla Compute Cluster driver for windows
                // must be enabled to support this
                && prop[i].tccDriver
#endif
                ) {
                // This is an array of P2P capable GPUs
                gpuid[gpu_p2p_count++] = i;
            }
        }

        // Show all the combinations of support P2P GPUs
        int can_access_peer;

        if (gpu_p2p_count >= 2) {
            for (int i = 0; i < gpu_p2p_count; i++) {
                for (int j = 0; j < gpu_p2p_count; j++) {
                    if (gpuid[i] == gpuid[j]) {
                        continue;
                    }
                    cudaDeviceCanAccessPeer(&can_access_peer, gpuid[i], gpuid[j]);
                    printf("> Peer access from %s (GPU%d) -> %s (GPU%d) : %s\n",
                        prop[gpuid[i]].name, gpuid[i], prop[gpuid[j]].name, gpuid[j],
                        can_access_peer ? "Yes" : "No");
                }
            }
        }
    }

    // csv masterlog info
    // *****************************
    // exe and CUDA driver name
    printf("\n");
    std::string sProfileString = "deviceQuery, CUDA Driver = CUDART";
    char cTemp[16];

    // driver version
    sProfileString += ", CUDA Driver Version = ";
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
    sprintf_s(cTemp, 10, "%d.%d", driverVersion / 1000, (driverVersion % 100) / 10);
#else
    snprintf(cTemp, sizeof(cTemp), "%d.%d", driverVersion / 1000,
        (driverVersion % 100) / 10);
#endif
    sProfileString += cTemp;

    // Runtime version
    sProfileString += ", CUDA Runtime Version = ";
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
    sprintf_s(cTemp, 10, "%d.%d", runtimeVersion / 1000, (runtimeVersion % 100) / 10);
#else
    snprintf(cTemp, sizeof(cTemp), "%d.%d", runtimeVersion / 1000,
        (runtimeVersion % 100) / 10);
#endif
    sProfileString += cTemp;

    // Device count
    sProfileString += ", NumDevs = ";
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
    sprintf_s(cTemp, 10, "%d", deviceCount);
#else
    snprintf(cTemp, sizeof(cTemp), "%d", deviceCount);
#endif
    sProfileString += cTemp;
    sProfileString += "\n";
    printf("%s", sProfileString.c_str());

    printf("Result = PASS\n");

    // add return statement
    // edited by Zhe Gou in 20191007
    return deviceCount;
}

inline void selectCudaDevice(const int id = 0) {
    cudaError_t eror = cudaSetDevice(id);
    std::cout << "device " << id << " selected." << std::endl;
}
}


#endif
