#ifndef Vesicle_cuh
#define Vesicle_cuh
#include <cmath>
#include "ImmersedObject.cuh"
#include "ConfigFile.hpp"

#ifndef PI
#define PI 3.141592656589793238464338327950
#endif

class Vesicle : public ImmersedObject{
private:
    //DeviceArray<REAL> s;  // area
    REAL s0; // fixed initial area
    REAL kb; // kappa_bending
    REAL kl; // kappa length (set as large as possible)
    REAL ks; // kappa_swell (set as large as possible)
    REAL l0; // steady length of spring (in LBM unit set as 0.65 by default)
    REAL R0; // charecteristic radius
    REAL tau; // reduced area

    //DeviceArray<REAL> theta0; // steady angle for each node, typically set as zero
    //DeviceArray<REAL> xmean; // sum(x) / x.size();
    //DeviceArray<REAL> ymean; // sum(y) / x.size();
    //DeviceArray<REAL> area_piecewise; // temporary array for area calculation
public:
    // debug
    /*
    DeviceArray<REAL> flx;
    DeviceArray<REAL> fly;
    DeviceArray<REAL> fbx;
    DeviceArray<REAL> fby;
    DeviceArray<REAL> fsx;
    DeviceArray<REAL> fsy;
    DeviceArray<REAL> Eb; // bending energy
    */

    DeviceArray<REAL> El; // stretching energy
    DeviceArray<REAL> ATP_release_rate;
    DeviceArray<REAL> shearStress; // shear stress along the membrane
    DeviceArray<REAL> xSSprobe;
    DeviceArray<REAL> ySSprobe;
    DeviceArray<REAL> uxSSprobe;
    DeviceArray<REAL> uySSprobe;
    inline const REAL& get_l0(){ return l0; }


public:
    inline Vesicle
        (
        const REAL x0 = 0.0,
        const REAL y0 = 0.0,
        const REAL inclination = 0.0,
        const REAL R0 = 13.0,
        const REAL _tau = 0.7,
        const REAL _kb = 0.056,
        const REAL _kl = 0.2,
        const REAL _ks = 200.0,
        const REAL _l0 = 2.0 / 3.0,
        const char* initial_shape_hint_filename = nullptr,
        const bool is_silent = false,
        const REAL _amp = 1.0
        )
    {
        tau = _tau;
        s0 = PI * R0*R0;
        REAL p0 = 2.0*PI*R0 / std::sqrt(_tau);
        int n = (int)std::ceil(p0 / _l0);
        l0 = p0 / (REAL)n;
        kb = _kb;
        kl = _kl;
        ks = _ks;

        //s = DeviceArray<REAL>(0.0);
        //xmean = DeviceArray<REAL>(0.0);
        //ymean = DeviceArray<REAL>(0.0);

        DeviceArray<REAL> xr, yr;
        if (nullptr == initial_shape_hint_filename)
        {
            DeviceArray<REAL> theta = DA::range(0.0, 2.0*PI / n, n);
            REAL R = R0 / std::sqrt(tau);
            auto cosTheta = cos(theta);
            auto cosTheta2 = cosTheta*cosTheta;
            xr = (REAL)1.3 * cosTheta * R;
            yr = (REAL)1.3*R*sin(theta)*((REAL)0.1035805 + (REAL)1.001279*cosTheta2 - (REAL)0.561381*cosTheta2*cosTheta2);

            if (std::abs(_amp - 1.0) > 1e-9)
            {
                xr *= _amp;
                yr = (REAL)1.3 * sin(theta) * R; // start from circle
                yr *= _amp;
            }

        }
        else
        {
            if (!is_silent) printf("vesicle initial shape set from file %s\n", initial_shape_hint_filename);
            DataFile shape(initial_shape_hint_filename);
            shape.transpose();
            xr = DeviceArray<REAL>(&shape.getData()[0][0], shape.get_m());
            xr -= DA::sum(xr) / shape.get_m();
            yr = DeviceArray<REAL>(&shape.getData()[1][0], shape.get_m());
            yr -= DA::sum(yr) / shape.get_m();
            
        }
        x = xr * std::cos(inclination) - yr * std::sin(inclination) + x0;
        y = xr * std::sin(inclination) + yr * std::cos(inclination) + y0;
        x_prev = x;
        y_prev = y;

        // debug with initial shape from Zaiyi's code
        //DataFile shape("celltype001");
        //shape.transpose();
        //xr = DeviceArray<REAL>(DeviceArray<double>(&shape.getData()[0][0], shape.get_m()));
        //yr = DeviceArray<REAL>(DeviceArray<double>(&shape.getData()[1][0], shape.get_m()));
        //x = xr * std::cos(inclination) - yr * std::sin(inclination) + x0;
        //y = xr * std::sin(inclination) + yr * std::cos(inclination) + y0;

        //x = cosTheta*R0 + x0;
        //y = sin(theta)*R0 + y0;
        fx = DeviceArray<REAL>(0.0, x.size());
        fy = DeviceArray<REAL>(0.0, y.size());
        //theta0 = DeviceArray<REAL>(0.0, x.size());
        //area_piecewise = DeviceArray<REAL>(0.0, x.size());
        //Eb = DeviceArray<REAL>(0.0, x.size());

        El = DeviceArray<REAL>(0.0, x.size());
        ATP_release_rate = DeviceArray<REAL>(0.0, x.size());

        shearStress = DeviceArray<REAL>(0.0, x.size());
        xSSprobe = DeviceArray<REAL>(0.0, x.size());
        ySSprobe = DeviceArray<REAL>(0.0, x.size());
        uxSSprobe = DeviceArray<REAL>(0.0, x.size());
        uySSprobe = DeviceArray<REAL>(0.0, x.size());

        ux = DeviceArray<REAL>(0.0, x.size());
        uy = DeviceArray<REAL>(0.0, x.size());

        // debug
        //flx = DeviceArray<REAL>(0.0, x.size());
        //fly = DeviceArray<REAL>(0.0, x.size());
        //fbx = DeviceArray<REAL>(0.0, x.size());
        //fby = DeviceArray<REAL>(0.0, x.size());
        //fsx = DeviceArray<REAL>(0.0, x.size());
        //fsy = DeviceArray<REAL>(0.0, x.size());

        if (!is_silent)
        {
            printf("vesicle created:\n");
            printf("in numerical units:\n");
            printf("  x = %f, y0 = %f\n", x0, y0);
            printf("  Npoints = %d\n", x.size());
            printf("  R0 = %f\n", R0);
            printf("  kb = %f\n", kb);
            printf("  kl = %f\n", kl);
            printf("  ks = %f\n", ks);
            printf("  l0 = %f\n", l0);
            printf("  s0 = %f\n", s0);
        }
    }

    inline Vesicle // initialize with an input shape
        (
        const REAL* _x,
        const REAL* _y,
        const int size,
        const REAL R0 = 13.0,
        const REAL _tau = 0.7,
        const REAL _kb = 0.056,
        const REAL _kl = 0.2,
        const REAL _ks = 200.0,
        const REAL _l0 = 2.0 / 3.0,
        const bool is_silent = false
        )
    {
        assert(nullptr != _x && nullptr != _y);
        x = DeviceArray<REAL>(_x, size);
        y = DeviceArray<REAL>(_y, size);

        tau = _tau;
        s0 = PI * R0*R0;
        REAL p0 = 2.0*PI*R0 / std::sqrt(_tau);
        int n = (int)std::ceil(p0 / _l0);
        l0 = p0 / (REAL)n;
        kb = _kb;
        kl = _kl;
        ks = _ks;

        //s = DeviceArray<REAL>(0.0);
        //xmean = DeviceArray<REAL>(0.0);
        //ymean = DeviceArray<REAL>(0.0);

        x_prev = x;
        y_prev = y;

        // debug with initial shape from Zaiyi's code
        //DataFile shape("celltype001");
        //shape.transpose();
        //xr = DeviceArray<REAL>(DeviceArray<double>(&shape.getData()[0][0], shape.get_m()));
        //yr = DeviceArray<REAL>(DeviceArray<double>(&shape.getData()[1][0], shape.get_m()));
        //x = xr * std::cos(inclination) - yr * std::sin(inclination) + x0;
        //y = xr * std::sin(inclination) + yr * std::cos(inclination) + y0;

        //x = cosTheta*R0 + x0;
        //y = sin(theta)*R0 + y0;
        fx = DeviceArray<REAL>(0.0, x.size());
        fy = DeviceArray<REAL>(0.0, y.size());
        //theta0 = DeviceArray<REAL>(0.0, x.size());
        //area_piecewise = DeviceArray<REAL>(0.0, x.size());
        //Eb = DeviceArray<REAL>(0.0, x.size());

        El = DeviceArray<REAL>(0.0, x.size());
        ATP_release_rate = DeviceArray<REAL>(0.0, x.size());

        shearStress = DeviceArray<REAL>(0.0, x.size());
        xSSprobe = DeviceArray<REAL>(0.0, x.size());
        ySSprobe = DeviceArray<REAL>(0.0, x.size());
        uxSSprobe = DeviceArray<REAL>(0.0, x.size());
        uySSprobe = DeviceArray<REAL>(0.0, x.size());

        ux = DeviceArray<REAL>(0.0, x.size());
        uy = DeviceArray<REAL>(0.0, x.size());

        // debug
        //flx = DeviceArray<REAL>(0.0, x.size());
        //fly = DeviceArray<REAL>(0.0, x.size());
        //fbx = DeviceArray<REAL>(0.0, x.size());
        //fby = DeviceArray<REAL>(0.0, x.size());
        //fsx = DeviceArray<REAL>(0.0, x.size());
        //fsy = DeviceArray<REAL>(0.0, x.size());

        if (!is_silent)
        {
            printf("vesicle created from input shape:\n");
            printf("  R0 = %f\n", R0);
            printf("  kb = %f\n", kb);
            printf("  kl = %f\n", kl);
            printf("  ks = %f\n", ks);
            printf("  l0 = %f\n", l0);
            printf("  s0 = %f\n", s0);
        }
    }


    inline ~Vesicle(){}

    // calculate the force on vesicle membrane
    //this method will also update area s as a side effect
    void calculateForce();
    void calculateForce(const REAL _kb);
    void calculateForce(const REAL _kb, const REAL _kl, const REAL _ks);
    void calculateForce(DeviceArray<REAL>& fx, DeviceArray<REAL>& fy, const DeviceArray<REAL>& x, const DeviceArray<REAL>& y);

    void calculateArea();
    void calculateArea(const DeviceArray<REAL>& x, const DeviceArray<REAL>& y);

    /*
    inline REAL getArea() const {
        REAL hs;
        s.host(&hs);
        return hs;
    }
    */

    // deflation of vesicle to ensure volume conservation
    // added by Zhe Gou on 20211205
    void deflation();

    inline REAL getR0() const { return R0; }
    inline REAL getReducedArea() const { return tau; }
    void relax(const int nmax = 1000000, const REAL damp = 1.0);
    void calculate_ATP_release_rate();
};
#endif