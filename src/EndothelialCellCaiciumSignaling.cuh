#ifndef EndothelialCellCaiciumSignaling_cuh
#define EndothelialCellCaiciumSignaling_cuh

#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include "LBMheader.cuh"

// parameter package, plese use lattice Boltzmann units (except dx and dt)
struct ECparameterPack
{
    int m;
    int n;
    REAL dx;
    REAL dt;
    REAL D_ip3; // lbm units, cytosolic IP3 diffusion coefficient
    REAL D_calcium; // lbm units, cytosolic calcium diffusion coefficient
    REAL D_calcium_ER; // lbm units, Endoplasmic Reticulum calcium diffusion coefficient
    REAL calcium_0; // steady cytoplasmic calcium concentration
    REAL calcium_ER_0; // steady ER calcium concentration
};

struct ECCurve_ptr
{
    std::vector< const DeviceArray<REAL>* > _vec_da_x;
    std::vector< const DeviceArray<REAL>* > _vec_da_y;
    std::vector< const DeviceArray<REAL>* > _vec_da_membrane_variable;
    std::vector< bool > is_circular;
    inline ECCurve_ptr() :
        _vec_da_x(std::vector< const DeviceArray<REAL>* >(0)),
        _vec_da_y(std::vector< const DeviceArray<REAL>* >(0)),
        _vec_da_membrane_variable(std::vector< const DeviceArray<REAL>* >(0)),
        is_circular(false)
    {

    }
    inline size_t size(){ return _vec_da_x.size(); }
};

// curves that used to depict the shape of endothelial cells
struct ECgeometry
{
    ECCurve_ptr vesselLumenECInterface; // vessel-EC interface
    ECCurve_ptr gapJunction; // EC-EC interface
    ECCurve_ptr endoplasmicReticulum; // EC-ER interface
    ECCurve_ptr other; // other (EC-SmoothMuscle, EC-intima) interface
    ECCurve_ptr nucleus; // other (EC-SmoothMuscle, EC-intima) interface
    inline ECgeometry() :
        vesselLumenECInterface(ECCurve_ptr()),
        gapJunction(ECCurve_ptr()),
        endoplasmicReticulum(ECCurve_ptr()),
        other(ECCurve_ptr())
    {

    }
};

class EndothelialCell
{

public:
    //private:
    ECparameterPack parameters;
    ECgeometry geometry;
    DeviceArray<int> index_map_ip3;
    DeviceArray<int> index_map_calcium;
    DeviceArray<REAL> ip3_source;
    DeviceArray<REAL> calcium_source;
    ADsolver ip3; // cytosolic messenger [IP_3] https://en.wikipedia.org/wiki/Inositol_trisphosphate
    ADsolver calcium; // cytosolic messenger [Ca2+] https://en.wikipedia.org/wiki/Calcium_signaling
    DeviceArray<REAL> buffer;
    DeviceArray<REAL> calcium_buffer_complex;
public:

    inline EndothelialCell(const ECparameterPack _parameters, const ECgeometry _geometry) :
        parameters(_parameters),
        geometry(_geometry),

        index_map_ip3
        (
        0,
        _parameters.m,
        _parameters.n
        ),

        index_map_calcium(
        0,
        _parameters.m,
        _parameters.n
        ),

        ip3_source
        (
        0.0,
        _parameters.m,
        _parameters.n
        ),

        calcium_source
        (
        0.0,
        _parameters.m,
        _parameters.n
        ),

        ip3 // diffusion solver for [ip3] concentration
        (
        _parameters.D_ip3,
        _parameters.m,
        _parameters.n,
        std::max(_parameters.m * _parameters.n / 10, 1000),
        nullptr,
        nullptr,
        ip3_source.data_ptr(),
        index_map_ip3.data_ptr(), AD::PERIODIC, AD::PERIODIC
        ),

        calcium // diffusion solver for [Ca^2+] concentration
        (
        _parameters.D_calcium,
        _parameters.m,
        _parameters.n,
        std::max(_parameters.m * _parameters.n / 10, 1000),
        nullptr,
        nullptr,
        ip3_source.data_ptr(),
        index_map_ip3.data_ptr(), AD::PERIODIC, AD::PERIODIC
        ),
        buffer(0.0, parameters.m, parameters.n),
        calcium_buffer_complex(0.0, parameters.m, parameters.n)
    {

        // ********************* build index table for ip3 and calcium **********************************************

        // build label_EC
        LBMGPUvoxelizer* voxelizer = new LBMGPUvoxelizer(parameters.m, parameters.n, false, false, 100000,1000);
        auto& curves = geometry.vesselLumenECInterface;
        // register geometry.vesselLumenECInterface
        curves = geometry.vesselLumenECInterface;
        for (int i = 0; i != curves.size(); ++i){
            voxelizer->setBoundaryCurve(
                curves._vec_da_x[i]->data_ptr(),
                curves._vec_da_y[i]->data_ptr(),
                curves.size(), curves.is_circular[i], 0);
        }
        // register geometry.other
        curves = geometry.other;
        for (int i = 0; i != curves.size(); ++i){
            voxelizer->setBoundaryCurve(
                curves._vec_da_x[i]->data_ptr(),
                curves._vec_da_y[i]->data_ptr(),
                curves.size(), curves.is_circular[i], 0);
        }
        voxelizer->setBoundaryCurveFinished(
            nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, false);
        auto label_EC = voxelizer->build_filled_tag_map();
        label_EC = DeviceArray<int>(label_EC == -1);
        delete voxelizer;

        // build label_ER
        voxelizer = new LBMGPUvoxelizer(parameters.m, parameters.n, false, false, 100000, 1000);
        // register geometry.endoplasmicReticulum
        curves = geometry.endoplasmicReticulum;
        for (int i = 0; i != curves.size(); ++i){
            voxelizer->setBoundaryCurve(
                curves._vec_da_x[i]->data_ptr(),
                curves._vec_da_y[i]->data_ptr(),
                curves.size(), curves.is_circular[i], 0);
        }
        voxelizer->setBoundaryCurveFinished(
            nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, false);
        auto label_ER = voxelizer->build_filled_tag_map();
        label_ER = DeviceArray<int>(label_ER == -1);
        delete voxelizer;

        auto label_cytoplasm = label_EC * (1 - label_ER);

        index_map_ip3 = (1 - label_EC) * (-1) + label_ER * (-1);
        index_map_calcium = (1 - label_EC) * (-1) + label_cytoplasm * 0 + label_ER * 0;

        // fix ADsolver's index_map data pointer
        // (since DeviceArray<T> operations doesn't gurantee fixed memory in GPU, sorry. By Hengdi)
        ip3.Didx = index_map_ip3.data_ptr();
        calcium.Didx = index_map_calcium.data_ptr();

        // only advection-diffusion solver has the second order precision treatment (it is not finished for pure diffusion solver)
        DeviceArray<REAL> ux_zero(0.0, parameters.m, parameters.n);
        DeviceArray<REAL> uy_zero(0.0, parameters.m, parameters.n);
        ip3.ux = ux_zero.data_ptr();
        ip3.uy = uy_zero.data_ptr();
        calcium.ux = ux_zero.data_ptr();
        calcium.uy = uy_zero.data_ptr();

        // ********************* build index table for ip3 and calcium done *****************************************

        int curves_id;

        // set initial value for ip3 and calcium
        ip3.setInitialConcentration(0.0);
        auto calcium_ini =
            DeviceArray<REAL>(label_cytoplasm) * parameters.calcium_0 +
            DeviceArray<REAL>(label_ER) * parameters.calcium_ER_0;
        calcium.setInitialConcentration(calcium_ini);

        // register boundaries for ip3 and calcium
        // ip3 boundary id:
        //    0 lumen-EC interface
        //    1 gap junction, EC-EC interface
        //    2 ER membrane
        //    3 other membrane parts
        //    4 nucleus, a barrier
        // calcium boundary id:
        //    0 lumen-EC interface
        //    1 gap junction, EC-EC interface
        //    2 ER membrane
        //    3 other membrane parts
        //    4 nucleus, a barrier
        curves = geometry.vesselLumenECInterface;
        curves_id = 0;
        for (int i = 0; i != curves.size(); ++i){
            ip3.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
            calcium.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
        }

        curves = geometry.gapJunction;
        curves_id = 1;
        for (int i = 0; i != curves.size(); ++i){
            ip3.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
            calcium.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
        }

        curves = geometry.endoplasmicReticulum;
        curves_id = 2;
        for (int i = 0; i != curves.size(); ++i){
            ip3.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
            calcium.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
        }

        curves = geometry.other;
        curves_id = 3;
        for (int i = 0; i != curves.size(); ++i){
            ip3.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
            calcium.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
        }

        curves = geometry.nucleus;
        curves_id = 4;
        for (int i = 0; i != curves.size(); ++i){
            ip3.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
            calcium.registerCurve(*curves._vec_da_x[i], *curves._vec_da_y[i], curves.is_circular[i], curves_id);
        }

        ip3.registerCurveFinished();
        calcium.registerCurveFinished();



    }

    inline ~EndothelialCell()
    {

    }

};

#endif