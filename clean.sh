#!/bin/sh
#rm *.afdat
#rm bin/*
#rm build/*
#rm CPU/*

# clear directory
echo "clear dir"
for dir in Debug Release build x64
do
    if [ -d "$dir" ]
    then
        if [ "$(ls -A $dir)" ]
        then
            rm -r $dir/*
	        echo "  dir cleared: $dir"
        else
            echo "  dir clean: $dir"
        fi
    else
        echo "  dir not exist:  $dir"
    fi
done

echo "clear file"
for f in *.sdf *.mat *.pdb *.afdat *.dat *.log *.avi
do
    if [ -f "$f" ]
    then
        rm $f
        echo "  file cleared: $f"
    else
        echo "  file not exist: $f"
    fi
done

echo "cleaning done."
