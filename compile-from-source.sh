#!/bin/sh
./clean.sh
echo "start nvcc"
nvcc -O3 -arch=sm_37 -rdc=false -std=c++11 --use_fast_math -o ./bin/ARDLBM-cuda-stepLJ.e src/*.cu src/*.cpp thirdparty/EasyBMP/*.cpp
echo "compiling finished"
